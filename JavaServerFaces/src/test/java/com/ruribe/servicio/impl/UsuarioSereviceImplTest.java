package com.ruribe.servicio.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ruribe.dao.UsuarioDao;
import com.ruribe.servicio.interfaces.UsuarioService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.RoleBean;
import com.ruribe.util.bean.UsuarioBean;
import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUsuarioBean;
import com.ruribe.util.entidades.jpa.interfaces.IUsuarioDao;
@RunWith(SpringJUnit4ClassRunner.class)
//ApplicationContext will be loaded from "classpath:/com/example/OrderServiceTest-context.xml"
//@ContextConfiguration("applicationContext.xml")
//@RequestMapping(value = "/com/ruribe")
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
//@Transactional
//@TransactionConfiguration(defaultRollback = false)
public class UsuarioSereviceImplTest {
	
	@Inject
	private UsuarioDao<IUsuarioDao> asUserDao;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Test
	public void login0() throws SAExcepcion{
		 String username="ronald"; 
		 if (asUserDao==null){
		 }
		 String password="d8578edf8458ce06fbc5bb76a58c5ca4";
		 boolean connected = asUserDao.login(username,  password);
		 assertTrue (connected);

	}
	
	@Test
	public void login2(){
		 
		//test data
	      String str1 = new String ("abc");
	      String str2 = new String ("abc");
	      String str3 = null;
	      String str4 = "abc";
	      String str5 = "abc";
			
	      int val1 = 5;
	      int val2 = 6;

	      String[] expectedArray = {"one", "two", "three"};
	      String[] resultArray =  {"one", "two", "three"};

	      //Check that two objects are equal
	      assertEquals(str1, str2);

	      //Check that a condition is true
	      assertTrue (val1 < val2);

	      //Check that a condition is false
	      assertFalse(val1 > val2);

	      //Check that an object isn't null
	      assertNotNull(str1);

	      //Check that an object is null
	      assertNull(str3);

	      //Check if two object references point to the same object
	      assertSame(str4,str5);

	      //Check if two object references not point to the same object
	      assertNotSame(str1,str3);

	      //Check whether two arrays are equal to each other.
	      assertArrayEquals(expectedArray, resultArray);
	}
	
	
	@Test
	public void crearUsuario() throws SAExcepcion{
		try {
			
			 IUsuarioBean bean = new UsuarioBean();
			 String password="d8578edf8458ce06fbc5bb76a58c5ca4";
			 bean.setApellido("Rosales");
			 bean.setEnabled(true);
			 bean.setNombre("Vanessa");
			 bean.setTelefono("300400500");
			 bean.setClave(password);
			 bean.setUsername("carita4");
		
			 List<IRoleBean> roles= new ArrayList<IRoleBean>();
			 IRoleBean  role = new RoleBean();
			 role.setId((long) 1);
			 role.setAuthority("ROLE_ADMIN");
			 roles.add(role);
			 role = new RoleBean();
			 role.setId((long) 2);
			 role.setAuthority("ROLE_OPERATOR");
			 roles.add(role);
			 role = new RoleBean();
			 role.setId((long) 3);
			 role.setAuthority("ROLE_READ");
			 roles.add(role);
			 bean.setRoles(roles);
			 
			// usuarioService.crearUsuario(bean);
			
			 
		}catch(Exception e) {
			System.out.println("####ERROR:" + e.getMessage());
			e.printStackTrace();
		}
			 assertTrue (true);

	}
	

}
