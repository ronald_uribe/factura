package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IClienteDao;


public interface ClienteDao<T  extends IClienteDao> extends GenericoDao<T> {
	
	/**busca todos los Clientes.
	 * @throws SAExcepcion */
	public List<T> buscarClientes() throws SAExcepcion;
	
	/**busca un Cliente por su identificador.*/
	public T buscarCliente(String id_cliente);

	/**crear cliente */
	public void crearCliente(T iCliente);
	
	/**Editar cliente */
	public void editarCliente(T iCliente);
	
	/**crear cliente */
	public void eliminarCliente(T iCliente);
	
	/**busca un cliente pasado por parametro 
	 * @throws SAExcepcion */
	public List<T> buscarClientes(T iCliente) throws SAExcepcion;

	public List<T> obtenerClientesByRazonSocial(String razonSocial) throws SAExcepcion;

}
