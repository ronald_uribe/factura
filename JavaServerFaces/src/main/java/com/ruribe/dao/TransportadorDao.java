package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.ITransportadorDao;


public interface TransportadorDao<T  extends ITransportadorDao> extends GenericoDao<T> {
	
	/**busca todos los Transportadors.
	 * @throws SAExcepcion */
	public List<T> buscarTransportadores() throws SAExcepcion;
	
	/**busca un Transportador por su identificador.*/
	public T buscarTransportador(String documento);

	public void crearTransportador(T iTransportador);
	
	public void editarTransportador(T iTransportador);
	
	public void eliminarTransportador(T iTransportador);

	public List<T> buscarTransportadores(T iTransportador) throws SAExcepcion;

	

}
