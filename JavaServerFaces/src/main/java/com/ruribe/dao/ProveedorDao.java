package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IProveedorDao;


public interface ProveedorDao<T  extends IProveedorDao> extends GenericoDao<T> {
	
	/**busca todos los Proveedors.
	 * @throws SAExcepcion */
	public List<T> buscarProveedores() throws SAExcepcion;
	
	/**busca un Proveedor por su identificador.*/
	public T buscarProveedor(String documento);

	public void crearProveedor(T iproveedor);
	
	public void editarProveedor(T iproveedor);
	
	public void eliminarProveedor(T iproveedor);

	public List<T> buscarProveedores(T iproveedor) throws SAExcepcion;

	

}
