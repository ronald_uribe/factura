package com.ruribe.dao;

import java.util.Date;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IEntradaDao;


public interface EntradaDao<T  extends IEntradaDao> extends GenericoDao<T> {

	/**
	 * Crear entrada
	 * @param iEntrada
	 * @return
	 */
	public T crearEntrada(T iEntrada);
	
	/**
	 * Editar entrada
	 * @param iEntrada
	 * @return
	 */
	public void editarEntrada(T iEntrada);
	
	/**
	 * Eliminar entrada 
	 * @param iEntrada
	 */
	public void eliminarEntrada(T iEntrada);

	/**
	 * obtiene las ultimas 10 devoluciones.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarEntradas() throws SAExcepcion;
	/**
	 * obtiene una remision por su id.
	 * @param idEntrada
	 * @return
	 */
	public T buscarEntrada(int idEntrada);
	
	/**
	 * obtiene las entradas de un cliente
	 * @param idCliente
	 * @return
	 */
	public List<T> buscarEntradasPorProveedor(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las entradas de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarEntradasPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	/**
	 * obtiene las entradas de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<T> buscarEntradasPorProveedorYRangogoFecha(String idCliente, Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	

}
