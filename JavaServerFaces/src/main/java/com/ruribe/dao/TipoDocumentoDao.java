package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.ITipoDocumentoDao;


public interface TipoDocumentoDao<T  extends ITipoDocumentoDao> extends GenericoDao<T> {
	
	/**busca todos los tipos de documento.
	 * @throws SAExcepcion */
	public List<T> buscarTipoDocumento() throws SAExcepcion;
	/**busca todos los tipos de documento.*/
	public List<T> buscarTipoDocumento(T TipoDocumento);

}
