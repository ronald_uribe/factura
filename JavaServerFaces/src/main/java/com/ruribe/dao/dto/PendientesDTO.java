package com.ruribe.dao.dto;

public class PendientesDTO {
	
	 private int id_remision;
	 private String id_cliente;
	 private String idObra;
	 private int codigo_producto;
	 private String descripcion;
	 private int cantidad;
	 private Long cantidadDevuelta;
	 private Long cantidadRepuesta;

	/**
	 * @param id_remision
	 * @param id_cliente
	 * @param idObra
	 * @param codigo_producto
	 * @param descripcion
	 * @param cantidad
	 * @param cantidadDevuelta
	 * @param cantidadRepuesta
	 */
	public PendientesDTO(int id_remision, String id_cliente, String idObra,
			int codigo_producto, String descripcion, int cantidad,
			Long cantidadDevuelta, Long cantidadRepuesta) {
		super();
		this.id_remision = id_remision;
		this.id_cliente = id_cliente;
		this.idObra = idObra;
		this.codigo_producto = codigo_producto;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.cantidadDevuelta = cantidadDevuelta;
		this.cantidadRepuesta = cantidadRepuesta;
	}
	
	/**
	 * @param id_remision
	 * @param id_cliente
	 * @param idObra
	 * @param codigo_producto
	 * @param descripcion
	 * @param cantidad
	 * @param cantidadDevuelta
	 * @param cantidadRepuesta
	 */
	public PendientesDTO(int id_remision, String id_cliente, String idObra,
			int codigo_producto, String descripcion,Long cantidadDevuelta) {
		super();
		this.id_remision = id_remision;
		this.id_cliente = id_cliente;
		this.idObra = idObra;
		this.codigo_producto = codigo_producto;
		this.descripcion = descripcion;
		this.cantidadDevuelta = cantidadDevuelta;
	}

	/**
	 * @return the id_cliente
	 */
	public String getId_cliente() {
		return id_cliente;
	}
	/**
	 * @param id_cliente the id_cliente to set
	 */
	public void setId_cliente(String id_cliente) {
		this.id_cliente = id_cliente;
	}
	/**
	 * @return the idObra
	 */
	public String getIdObra() {
		return idObra;
	}
	/**
	 * @param idObra the idObra to set
	 */
	public void setIdObra(String idObra) {
		this.idObra = idObra;
	}
	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto() {
		return codigo_producto;
	}
	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}
	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the id_remision
	 */
	public int getId_remision() {
		return id_remision;
	}
	/**
	 * @param id_remision the id_remision to set
	 */
	public void setId_remision(int id_remision) {
		this.id_remision = id_remision;
	}
	/**
	 * @return the cantidadDevuelta
	 */
	public Long getCantidadDevuelta() {
		return cantidadDevuelta;
	}
	/**
	 * @param cantidadDevuelta the cantidadDevuelta to set
	 */
	public void setCantidadDevuelta(Long cantidadDevuelta) {
		this.cantidadDevuelta = cantidadDevuelta;
	}
	/**
	 * @return the cantidadRepuesta
	 */
	public Long getCantidadRepuesta() {
		return cantidadRepuesta;
	}
	/**
	 * @param cantidadRepuesta the cantidadRepuesta to set
	 */
	public void setCantidadRepuesta(Long cantidadRepuesta) {
		this.cantidadRepuesta = cantidadRepuesta;
	}

}
