package com.ruribe.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IFacturaDao;


public interface FacturaDao<T  extends IFacturaDao> extends GenericoDao<T> {

	public T crearFactura(T iFactura);
	
	public void editarFactura(T iFactura);
	
	public void eliminarFactura(T iFactura);
	
	public List<Object> consultarCorte(Map<String, Object> params) throws SAExcepcion;

	/** calcula el transporte de un periodo de facturacion
	 * @consultarTransportePeriodoFacturacion
	 * @param parametros de consulta.
	 * @throws SAExcepcion 
	 * */
	public List<Object> consultarTransportePeriodoFacturacion(Map<String, Object> params) throws SAExcepcion;

	/**
	 * Genera un corte de facturacion parcial con todos los productos que tiene el cliente.
	 * @param Map<String, Object> 
	 * @return List<Object> 
	 * @throws SAExcepcion 
	 */
	public List<Object> periodoFacturacionParcial(Map<String, Object> params) throws SAExcepcion;
	
	/**
	 * obtiene las ultimas 10 Facturas.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarFacturas() throws SAExcepcion;
	/**
	 * obtiene una Factura por su id.
	 * @param idDevolucion
	 * @return
	 */
	public T buscarFactura(int idFactura);
	/**
	 * obtiene las Facturas de un cliente
	 * @param idCliente
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarFacturaPorCliente(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las Facturas de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarFacturaPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	
	/**
	 * obtiene las Facturas de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarFacturaPorClienteYRangogoFecha(String idCliente, Date fechaInicial, Date fechaFinal) throws SAExcepcion;

	/**
	 * obtiene la ultima factura generada a una obra
	 * @param id_cliente
	 * @param idObra
	 * @return
	 * @throws SAExcepcion 
	 */
	public Collection<? extends IFacturaDao> buscarLastFacturaClienteObra(String id_cliente, int idObra) throws SAExcepcion;

	public List<Object> consultarFacturaIdPDF(HashMap<String, Object> params) throws SAExcepcion;
	

}
