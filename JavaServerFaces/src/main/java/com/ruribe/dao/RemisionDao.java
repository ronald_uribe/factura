package com.ruribe.dao;

import java.util.Date;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;


public interface RemisionDao<T  extends IRemisionDao> extends GenericoDao<T> {
	
	/**
	 * crear remision
	 * @param iRemision
	 * @return
	 */
	public T crearRemision(T iRemision);
	/**
	 * permite editar una remision
	 * @param iRemision
	 */
	public void editarRemision(T iRemision);
	/**
	 * elimina una remision
	 * @param iRemision
	 */
	public void eliminarRemision(T iRemision);
	/**
	 * obtiene las ultimas 10 devoluciones.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarRemisiones() throws SAExcepcion;
	/**
	 * obtiene una remision por su id.
	 * @param idRemision
	 * @return
	 */
	public T buscarRemision(String idRemision);
	/**
	 * obtiene las remisiones de un cliente
	 * @param idCliente
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarRemisionesPorCliente(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las remisiones de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarRemisionesPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	/**
	 * obtiene las remisiones de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarRemisionesPorClienteYRangogoFecha(String idCliente, Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	
	/**
	 * obtiene una remision por su id agrupado por codigo interno.
	 * @param idRemision
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<Object> buscarRemisionPDFCliente(String idRemision) throws SAExcepcion;
	public boolean remisionIsPrinted(int idRemision);
	
	public void marcarRemisionImpresa(int idRemision);

}
