package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IUsuarioDao;


public interface UsuarioDao<T  extends IUsuarioDao> extends GenericoDao<T> {
	
	/**busca todos los usuarios.
	 * @throws SAExcepcion */
	public List<T> buscarUser() throws SAExcepcion;
	
	/**busca un usuario por su identificador.*/
	public T buscarUsuario(String documento);

	public void crearUsuario(T iusuario);
	
	public void editarUsuario(T iusuario);
	
	public void eliminarUsuario(Long idUsuario);

	public List<T> buscarUser(String string) throws SAExcepcion;

	public boolean login(String username, String password) throws SAExcepcion;

	public boolean existeUsuario(String username) throws SAExcepcion;

	public boolean actualizarContrasena(String username, String generateMD5Signature) throws SAExcepcion;

	public T findUserByUsername(String username) throws SAExcepcion;

	

}
