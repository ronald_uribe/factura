package com.ruribe.dao;

import java.util.Date;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.ISalidaDao;


public interface SalidaDao<T  extends ISalidaDao> extends GenericoDao<T> {

	/**
	 * Crear salida
	 * @param iSalida
	 * @return
	 */
	public T crearSalida(T iSalida);
	
	/**
	 * Editar salida
	 * @param iSalida
	 * @return
	 */
	public void editarSalida(T iSalida);
	
	/**
	 * Eliminar salida 
	 * @param iSalida
	 */
	public void eliminarSalida(T iSalida);

	/**
	 * obtiene las ultimas 10 devoluciones.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarSalidas() throws SAExcepcion;
	/**
	 * obtiene una remision por su id.
	 * @param idSalida
	 * @return
	 */
	public T buscarSalida(String idSalida);
	
	/**
	 * Obtiene los productos que se le debe a un proveedor
	 * @param idproveedorSelect
	 * @return
	 */
	public List<Object> obtenerProductosPendientesPorProveedor(String idproveedorSelect) throws SAExcepcion;
	
	/**
	 * obtiene las entradas de un cliente
	 * @param idCliente
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarSalidasPorProveedor(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las entradas de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<T> buscarSalidasPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	/**
	 * obtiene las entradas de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<T> buscarSalidasPorProveedorYRangogoFecha(String idProveedor, Date fechaInicial, Date fechaFinal) throws SAExcepcion;

}
