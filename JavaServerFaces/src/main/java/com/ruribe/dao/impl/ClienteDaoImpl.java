package com.ruribe.dao.impl;

import java.util.HashMap;
import java.util.List;

import com.ruribe.dao.ClienteDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.interfaces.IClienteDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Clientes.
 * */


public class ClienteDaoImpl<T extends IClienteDao> extends GenericoDaoImpl<T> implements ClienteDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarClientes() throws SAExcepcion {
		 List<T> clientes=null;
		 clientes= (List<T>) this.findNamedAsObjects(Cliente.QUERY_FIND_ALL_CLIENTE,null);
		 return clientes;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarClientes(T iCliente) throws SAExcepcion {
		List<T> clientes=null;
		 clientes=(List<T>) this.findNamedAsObjects(Cliente.QUERY_FIND_ALL_CLIENTE,null);
		 return clientes;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> obtenerClientesByRazonSocial(String razonSocial) throws SAExcepcion {
		 List<T> clientes=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("razonSocial","%"+razonSocial+"%");
		 clientes=(List<T>) this.findNamedAsObjects(Cliente.QUERY_FIND_RAZON_SOCIAL,hmap);
		 return clientes;
	}
	
	/**
	 * busqueda de Cliente por llave primaria.
	 * String identificador de Cliente
	 */
	public T buscarCliente(String documento){
		return this.findPk(documento);
	}

	@Override
	public void crearCliente(T iCliente) {
			this.save(iCliente);	
	}
	
	/**
	 * editar Cliente
	 */
	public void editarCliente(T iCliente) {
		this.update(iCliente);	
	}
	
	@Override
	public void eliminarCliente(T iCliente) {
			this.delete(iCliente);
	}

}
