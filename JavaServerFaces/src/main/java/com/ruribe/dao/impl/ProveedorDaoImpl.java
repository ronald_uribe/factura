package com.ruribe.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.ProveedorDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.interfaces.IProveedorDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Proveedors.
 * */


public class ProveedorDaoImpl<T extends IProveedorDao> extends GenericoDaoImpl<T> implements ProveedorDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarProveedores() throws SAExcepcion {
		 List<T> proveedores=null;
		 proveedores=(List<T>) this.findNamedAsObjects(Proveedor.QUERY_FIND_ALL,null);
		 return proveedores;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarProveedores(T iProveedor) throws SAExcepcion {
		 List<T> Proveedors=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("documento","%"+iProveedor.getNo_documento()+"%");
		 Proveedors=(List<T>) this.findNamedAsObjects(Proveedor.QUERY_SELECT_ID_LIKE,hmap);
		 return Proveedors;
	}
	
	/**
	 * busqueda de Proveedor por llave primaria.
	 * String identificador de Proveedor
	 */
	public T buscarProveedor(String documento){
		T Proveedor=null;
		Proveedor = this.findPk(documento);
		return Proveedor;
		
	}

	@Override
	public void crearProveedor(T iProveedor) {
			this.save(iProveedor);	
	}
	
	/**
	 * editar Proveedor
	 */
	public void editarProveedor(T iProveedor) {
		this.update(iProveedor);	
	}
	
	@Override
	public void eliminarProveedor(T iProveedor) {
		this.delete(iProveedor);
		
	}

}
