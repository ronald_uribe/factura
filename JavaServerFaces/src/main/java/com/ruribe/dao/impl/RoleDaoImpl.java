package com.ruribe.dao.impl;
import java.util.List;

import com.ruribe.dao.RoleDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Role;
import com.ruribe.util.entidades.jpa.interfaces.IRoleDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Role.
 * */


public class RoleDaoImpl<T extends IRoleDao> extends GenericoDaoImpl<T> implements RoleDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAllRoles() throws SAExcepcion {
		 List<T> roles=null;
		 roles=(List<T>) this.findNamedAsObjects(Role.QUERY_FIND_ALL_ROLES,null);
		 return roles;
	}
	
	/**
	 * busqueda de Role por llave primaria.
	 * String identificador de Role
	 */
	public T findRole(int id_obra){
		T Role=null;
		Role = this.findPk(id_obra);
		return Role;
		
	}


}
