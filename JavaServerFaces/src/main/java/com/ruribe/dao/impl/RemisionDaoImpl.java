package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;

import com.ruribe.dao.RemisionDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Remision;
import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Remisions.
 * */


public class RemisionDaoImpl<T extends IRemisionDao> extends GenericoDaoImpl<T> implements RemisionDao<T> {
	
	/**
	 * busqueda de Remision por llave primaria.
	 * @param String identificador de Remision
	 * @return remision
	 */
	public T buscarRemision(String documento){
		 return this.findPk(Integer.parseInt(documento));
	}

	/**
	 * Crear una Remision
	 * @return 
	 */
	public T crearRemision(T iRemision) {
		
		T remision=null;
		remision =  this.save(iRemision);	
		return remision;
			
	}
	
	/**
	 * editar Remision
	 */
	public void editarRemision(T iRemision) {
		this.update(iRemision);	
	}
	
	@Override
	public void eliminarRemision(T iRemision) {
		this.delete(iRemision);
		
	}
	
	/**
	 * obtiene las ultimas 10 remisiones
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarRemisiones() throws SAExcepcion {
		List<T> remisiones=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		// hmap.put("id_cliente","%"+iObra.getCliente().getId_cliente()+"%");
	     // los ultimos 10 
	     remisiones=(List<T>) this.findNamed(Remision.QUERY_FIND_ALL_REMISIONES,hmap,0,10);
		 //remisiones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_ALL_REMISIONES,hmap);
		 return remisiones;
	}
	
	/**
	 * obtiene las remisiones de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarRemisionesPorCliente(String id_cliente) throws SAExcepcion {
		List<T> remisiones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idCliente",id_cliente);
		 remisiones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_REMISION_CLIENTE,hmap);
		 return remisiones;
	}
	
	
	/**
	 * obtiene las remisiones entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarRemisionesPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> remisiones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 remisiones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_RANGO_FECHAS,hmap);
		 return remisiones;
	}

	
	/**
	 * obtiene las remisiones de un cliente entre un rango de fechas 
	 * @param String idCliente
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarRemisionesPorClienteYRangogoFecha(String idCliente,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
		 List<T> remisiones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     hmap.put("idCliente",idCliente);
	     hmap.put("fechaInicial",fechaInicial);
		 hmap.put("fechaCorte",fechaFinal);
		 remisiones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_REMISION_CLIENTE_RANGO_FECHAS,hmap);
		 return remisiones;
	}

	/**
	 * Obtiene la remision de un cliente agrupado por codigo interno.
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> buscarRemisionPDFCliente(String idRemision) throws SAExcepcion {
		 List<Object> listRemision=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("id_remision",Integer.parseInt(idRemision));
		 listRemision=(List<Object>) this.findNamedAsObjects(Remision.QUERY_PDF_REMISION_PDF_CLIENTE,hmap);
		 return listRemision;
	}

	@Override
	public boolean remisionIsPrinted(int idRemision) {
		return this.findPk(idRemision).isPrinted();
	}
	
	@Override
	public void marcarRemisionImpresa(int idRemision) {
		T remision= this.findPk(idRemision);
		remision.setPrinted(true);
		this.editarRemision(remision);
	}
	

}
