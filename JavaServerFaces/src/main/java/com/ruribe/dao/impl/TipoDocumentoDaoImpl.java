package com.ruribe.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.TipoDocumentoDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.TipoDocumento;
import com.ruribe.util.entidades.jpa.interfaces.ITipoDocumentoDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Tipos de documento.
 * */


public class TipoDocumentoDaoImpl<T extends ITipoDocumentoDao> extends GenericoDaoImpl<T> implements TipoDocumentoDao<T> {
	
	/**
	 * retorna los tipos de documentos
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarTipoDocumento() throws SAExcepcion {
		 List<T> tipoDocumentos=null;
		 tipoDocumentos=(List<T>) this.findNamedAsObjects(TipoDocumento.QUERY_FIND_ALL_TIPO_DOCUMENTO,null);
		 return tipoDocumentos;
	}

	@Override
	public List<T> buscarTipoDocumento(T TipoDocumento) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
