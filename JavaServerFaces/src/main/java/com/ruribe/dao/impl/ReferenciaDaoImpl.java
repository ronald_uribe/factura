package com.ruribe.dao.impl;


import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.ReferenciaDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Referencia;
import com.ruribe.util.entidades.jpa.interfaces.IReferenciaDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de referencia.
 * */

public class ReferenciaDaoImpl<T extends IReferenciaDao> extends GenericoDaoImpl<T> implements ReferenciaDao<T> {
	
	/**
	 * obtiene todos las referencias
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarReferencias() throws SAExcepcion {
		 
		 List<T> referencia=null;
		 referencia=(List<T>) this.findNamedAsObjects(Referencia.QUERY_FIND_ALL_REFERENCIA,null);
		return referencia;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> obtenerReferenciasByDescripcion(String filtro) throws SAExcepcion {
		 List<T> referencia=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("filtro","%"+filtro+"%");
		 referencia= (List<T>)this.findNamedAsObjects(Referencia.QUERY_FIND_REFERENCIA_BY_DESCRIPCION,hmap);
		 return referencia;
	}
	
	/**
	 * busqueda de Referencia por llave primaria.
	 * String identificador de Referencia
	 */
	public T buscarReferencia(String cod_referencia){
		T Referencia=null;
		Referencia = this.findPk(cod_referencia);
		return Referencia;
		
	}

	@Override
	public void crearReferencia(T iReferencia) {
		this.save(iReferencia);	
	}
	
	/**
	 * editar Referencia
	 */
	public void editarReferencia(T iReferencia) {
		this.update(iReferencia);	
	}
	
	@Override
	public void eliminarReferencia(T iReferencia) {
		this.delete(iReferencia);
		
	}

}
