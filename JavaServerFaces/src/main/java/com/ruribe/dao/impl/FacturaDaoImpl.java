package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.FacturaDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Factura;
import com.ruribe.util.entidades.jpa.interfaces.IFacturaDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Facturas.
 * */


public class FacturaDaoImpl<T extends IFacturaDao> extends GenericoDaoImpl<T> implements FacturaDao<T> {
	
	/**
	 * busqueda de Factura por llave primaria.
	 * String identificador de Factura
	 */
	public T buscarFactura(int documento){
		return this.findPk(documento);
	}

	/**
	 * Crear una Factura
	 * @return 
	 */
	public T crearFactura(T iFactura) {
		T factura=null;
		factura =  this.save(iFactura);	
		return factura;
	}
	
	/**
	 * editar Factura
	 */
	public void editarFactura(T iFactura) {
		this.update(iFactura);	
	}
	
	/**
	 * eliminar factura
	 */
	@Override
	public void eliminarFactura(T iFactura) {
		this.delete(iFactura);
		
	}

	/**
	 * obtiene los productos que se ha llevado un cliente en un periodo de tiempo
	 * @param fechainicio,fechacorte,idcliente
	 * @return List<Object>
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> consultarCorte(Map<String, Object> params) throws SAExcepcion {
		List<Object> detalle=this.findNamedAsObjects(Factura.QUERY_PROCESAR_CORTE, params);
		return detalle;
	}
	
	/**
	 * obtiene el valor del tranporte de un periodo de un cliente
	 * @param Map<String, Object> fechainicio,fechacorte,idcliente
	 * @return List<Object> valor del transporte BigDecimal
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> consultarTransportePeriodoFacturacion(Map<String, Object> params) throws SAExcepcion {
		List<Object> transporte=this.findNamedAsObjects(Factura.QUERY_TRANSPORTE_PERIODO_FACT, params);
		return transporte;
	}
	
	/**
	 * Genera un corte de facturacion parcial con todos los productos que tiene el cliente.
	 * @param Map<String, Object> 
	 * @return List<Object> 
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> periodoFacturacionParcial(Map<String, Object> params) throws SAExcepcion {
		List<Object> facturacion=this.findNamedAsObjects(Factura.QUERY_FACT_PARCIAL, params);
		return facturacion;
	}
	
	/**
	 * obtiene las ultimas 10 facturas
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarFacturas() throws SAExcepcion {
		List<T> facturas=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     // los ultimos 10 
	     facturas=(List<T>) this.findNamed(Factura.QUERY_FIND_ALL_FACTURAS,hmap,0,10);
		 //devoluciones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_ALL_REMISIONES,hmap);
		 return facturas;
	}
	
	/**
	 * obtiene las facturas de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarFacturaPorCliente(String id_cliente) throws SAExcepcion {
		List<T> facturas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idCliente",id_cliente);
		 facturas=(List<T>) this.findNamedAsObjects(Factura.QUERY_FIND_FACTURA_CLIENTE,hmap);
		 return facturas;
	}
	
	
	/**
	 * obtiene las facturas entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarFacturaPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> facturas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 facturas=(List<T>) this.findNamedAsObjects(Factura.QUERY_FIND_RANGO_FECHAS,hmap);
		 return facturas;
	}

	
	/**
	 * obtiene las facturas de un cliente entre un rango de fechas 
	 * @param String idCliente
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarFacturaPorClienteYRangogoFecha(String idCliente,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
		 List<T> facturas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     hmap.put("idCliente",idCliente);
	     hmap.put("fechaInicial",fechaInicial);
		 hmap.put("fechaCorte",fechaFinal);
		 facturas=(List<T>) this.findNamedAsObjects(Factura.QUERY_FIND_FACTURA_CLIENTE_RANGO_FECHAS,hmap);
		 return facturas;
	}

	/**
	 * obtiene la ultima factura de una obra.
	 * @param String id_cliente
	 * @param String idObra
	 * @throws SAExcepcion 
	 */
	@Override
	public List<T> buscarLastFacturaClienteObra(String id_cliente, int id_obra) throws SAExcepcion {
		List<T> facturas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     hmap.put("idCliente",id_cliente);
	     hmap.put("idObra",id_obra);
		 facturas=(List<T>) this.findNamed(Factura.QUERY_FIND_FACTURA_CLIENTE_OBRA,hmap,0,1);
		 return facturas;
	}

	@Override
	public List<Object> consultarFacturaIdPDF(HashMap<String, Object> params) throws SAExcepcion {
		List<Object> detalle=this.findNamedAsObjects(Factura.QUERY_PROCESAR_PDF, params);
		return detalle;
	}
}
