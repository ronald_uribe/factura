package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.DevolucionDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Devolucion;
import com.ruribe.util.entidades.jpa.interfaces.IDevolucionDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Devolucions.
 * */


public class DevolucionDaoImpl<T extends IDevolucionDao> extends GenericoDaoImpl<T> implements DevolucionDao<T> {
	
	/**
	 * busqueda de Devolucion por llave primaria.
	 * @param String identificador de Remision
	 * @return Devolucion
	 */
	public T buscarDevolucion(String documento){
		 return this.findPk(Integer.parseInt(documento));
	}

	/**
	 * Crear una Devolucion
	 * @return 
	 */
	public T crearDevolucion(T iDevolucion) {
		
		T devolucion=null;
		devolucion =  this.save(iDevolucion);	
		return devolucion;
			
	}
	
	/**
	 * editar Devolucion
	 */
	public void editarDevolucion(T iDevolucion) {
		this.update(iDevolucion);	
	}
	
	@Override
	public void eliminarDevolucion(T iDevolucion) {
		this.delete(iDevolucion);
		
	}

	/**
	 * obtiene los articulos que tiene un cliente hasta la fecha
	 * @return lista de objetos
	 */
	/*
	@SuppressWarnings("unchecked")
	@Override
	public List<PendientesDTO> obtenerproductosPendientes(String idCliente) {
		List<PendientesDTO> pendientes=null;
	    HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("cliente",idCliente);
		hmap.put("fecha", new Date());
		pendientes= (List<PendientesDTO>) this.findNativeAsObjectsPropio(Devolucion.QUERY_PRODUCTOS_PENDIENTES_CLIENTE_DTO,PendientesDTO.class,hmap);
		return  pendientes;
	}
	*/
	
	/**
	 * obtiene los articulos que tiene un cliente hasta la fecha
	 * @return lista de objetos
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> obtenerProductosPendientes(String idCliente,int idobra) throws SAExcepcion {
		List<Object> pendientes=null;
	    HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("cliente",idCliente);
		hmap.put("idobra",idobra);
		hmap.put("fecha", new Date());
		pendientes= (List<Object>) this.findNamedAsObjects(Devolucion.QUERY_PRODUCTOS_PENDIENTES_CLIENTE,hmap);
		return  pendientes;
	}
	
	/**
	 * obtiene los articulos que tiene un cliente hasta la fecha
	 * @return lista de objetos
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> consultarAllSaldosPendientes() throws SAExcepcion {
		List<Object> pendientes=null;
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		pendientes= (List<Object>) this.findNamedAsObjects(Devolucion.QUERY_ALL_PENDIENTES_CLIENTE,hmap);
		return  pendientes;
	}
	
//	/**
//	 * Obtiene las devoluciones por fecha 
//	 */
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<T> obtenerproductosPendientesbyFecha(HashMap<String, Object> params) {
//		List<T> devoluciones=null;
//		devoluciones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_DEVOLUCIONES_BY_DATES,params);
//		 return devoluciones;
//	}
	
	/**
	 * obtiene las ultimas 10 remisiones
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarDevoluciones() throws SAExcepcion {
		List<T> devoluciones=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     // los ultimos 10 
	     devoluciones=(List<T>) this.findNamed(Devolucion.QUERY_FIND_ALL_DEVOLUCIONES,hmap,0,10);
		 //devoluciones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_ALL_REMISIONES,hmap);
		 return devoluciones;
	}
	
//	/**
//	 * Obtiene las deoluciones por fecha 
//	 */
//	@SuppressWarnings("unchecked")
//	public List<T> buscarDevolucion() {
//		List<T> remisiones=null;
//	     HashMap<String, Object> hmap = new HashMap<String, Object>();
//		// hmap.put("id_cliente","%"+iObra.getCliente().getId_cliente()+"%");
//		 remisiones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_ALL_DEVOLUCIONES,hmap);
//		 return remisiones;
//	}
	
	/**
	 * obtiene las remisiones de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarDevolucionesPorCliente(String id_cliente) throws SAExcepcion {
		List<T> devoluciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idCliente",id_cliente);
		 devoluciones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_DEVOLUCION_CLIENTE,hmap);
		 return devoluciones;
	}
	
	
	/**
	 * obtiene las remisiones entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarDevolucionesPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> devoluciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 devoluciones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_RANGO_FECHAS,hmap);
		 return devoluciones;
	}

	
	/**
	 * obtiene las remisiones de un cliente entre un rango de fechas 
	 * @param String idCliente
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarDevolucionesPorClienteYRangogoFecha(String idCliente,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
		 List<T> devoluciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     hmap.put("idCliente",idCliente);
	     hmap.put("fechaInicial",fechaInicial);
		 hmap.put("fechaCorte",fechaFinal);
		 devoluciones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_DEVOLUCION_CLIENTE_RANGO_FECHAS,hmap);
		 return devoluciones;
	}

	/**
	 * Obtiene una revolucion agrupada por id_remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public IDevolucionDao buscarDevolucionPDF(int idDevolucion) throws SAExcepcion {
		List<T> remisiones=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("id_devolucion",idDevolucion);
		 remisiones=(List<T>) this.findNamedAsObjects(Devolucion.QUERY_FIND_PK_GROUP_BY_REMISION_DEVOLUCION,hmap);
		 return remisiones.get(0);
	}
	
	@Override
	public boolean devolucionIsPrinted(int idDevolucion) {
		return this.findPk(idDevolucion).isPrinted();
	}
	
	@Override
	public void marcarDevolucionImpresa(int idDevolucion) {
		T devolucion= this.findPk(idDevolucion);
		devolucion.setPrinted(true);
		this.editarDevolucion(devolucion);
	}

}
