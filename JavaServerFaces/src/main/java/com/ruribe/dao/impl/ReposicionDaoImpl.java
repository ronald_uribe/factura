package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.ReposicionDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Reposicion;
import com.ruribe.util.entidades.jpa.interfaces.IReposicionDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Reposicions.
 * */


public class ReposicionDaoImpl<T extends IReposicionDao> extends GenericoDaoImpl<T> implements ReposicionDao<T> {
	
	/**
	 * busqueda de Reposicion por llave primaria.
	 * @param String identificador de Remision
	 * @return remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public T buscarReposicion(String documento) throws SAExcepcion{
		 return this.findPk(Integer.parseInt(documento));
	}

	/**
	 * Crear una Reposicion
	 * @return 
	 */
	public T crearReposicion(T iReposicion) {
		T reposicion=null;
		reposicion =  this.save(iReposicion);	
		return reposicion;
	}
	
	/**
	 * editar Reposicion
	 */
	public void editarReposicion(T iReposicion) {
		this.update(iReposicion);	
	}
	
	@Override
	public void eliminarReposicion(T iReposicion) {
		this.delete(iReposicion);
		
	}
	
	/**
	 * obtiene los articulos que tiene un cliente hasta la fecha
	 * @return lista de objetos
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> obtenerProductosPendientes(String idCliente,String idobra) throws SAExcepcion {
		List<Object> pendientes=null;
	    HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("cliente",idCliente);
		hmap.put("idobra",idobra);
		hmap.put("fecha", new Date());
		pendientes= (List<Object>) this.findNamedAsObjects(Reposicion.QUERY_PRODUCTOS_PENDIENTES_CLIENTE,hmap);
		return  pendientes;
	}
		
	/**
	 * obtiene las ultimas 10 remisiones
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarReposiciones() throws SAExcepcion {
		List<T> reposiciones;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     // los ultimos 10 
	     reposiciones=(List<T>) this.findNamed(Reposicion.QUERY_FIND_ALL_REPOSICIONES,hmap,0,10);
		 //reposiciones=(List<T>) this.findNamedAsObjects(Remision.QUERY_FIND_ALL_REMISIONES,hmap);
		 return reposiciones;
	}
	
	
	/**
	 * obtiene las remisiones de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarReposicionesPorCliente(String id_cliente) throws SAExcepcion {
		List<T> reposiciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idCliente",id_cliente);
		 reposiciones=(List<T>) this.findNamedAsObjects(Reposicion.QUERY_FIND_REPOSICION_CLIENTE,hmap);
		 return reposiciones;
	}
	
	
	/**
	 * obtiene las remisiones entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarReposicionesPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> reposiciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 reposiciones=(List<T>) this.findNamedAsObjects(Reposicion.QUERY_FIND_RANGO_FECHAS,hmap);
		 return reposiciones;
	}

	
	/**
	 * obtiene las remisiones de un cliente entre un rango de fechas 
	 * @param String idCliente
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarReposicionesPorClienteYRangogoFecha(String idCliente,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
		 List<T> reposiciones=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     hmap.put("idCliente",idCliente);
	     hmap.put("fechaInicial",fechaInicial);
		 hmap.put("fechaCorte",fechaFinal);
		 reposiciones=(List<T>) this.findNamedAsObjects(Reposicion.QUERY_FIND_REPOSICION_CLIENTE_RANGO_FECHAS,hmap);
		 return reposiciones;
	}

	/**
	 * Obtiene una revolucion agrupada por id_remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public IReposicionDao buscarReposicionPDF(int idReposicion) throws SAExcepcion {
		List<T> remisiones=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("id_reposicion",idReposicion); 
		 remisiones=(List<T>) this.findNamedAsObjects(Reposicion.QUERY_FIND_PK_GROUP_BY_REMISION_REPOSICION,hmap);
		 return remisiones.get(0);
	}

}
