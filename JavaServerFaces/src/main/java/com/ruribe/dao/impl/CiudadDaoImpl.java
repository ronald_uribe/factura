package com.ruribe.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.CiudadDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Ciudad;
import com.ruribe.util.entidades.jpa.interfaces.ICiudadDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de usuarios.
 * */


public class CiudadDaoImpl<T extends ICiudadDao> extends GenericoDaoImpl<T> implements CiudadDao<T> {
	
	/**
	 * retorna los tipos de documentos
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarCiudades() throws SAExcepcion {
		 List<T> ciudades=null;
		 ciudades=(List<T>) this.findNamedAsObjects(Ciudad.QUERY_FIND_ALL_CIUDADES,null);
		 return ciudades;
	}

	@Override
	public T buscarCiudad(Integer codigo_ciudad) {
		return this.findPk(codigo_ciudad);
	}

	
}
