package com.ruribe.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.TransportadorDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Transportador;
import com.ruribe.util.entidades.jpa.interfaces.ITransportadorDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Transportadors.
 * */

public class TransportadorDaoImpl<T extends ITransportadorDao> extends GenericoDaoImpl<T> implements TransportadorDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarTransportadores() throws SAExcepcion {
		 List<T> proveedores=null;
		 proveedores=(List<T>) this.findNamedAsObjects(Transportador.QUERY_FIND_ALL,null);
		 return proveedores;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarTransportadores(T iTransportador) throws SAExcepcion {
		 List<T> Transportadors=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("documento","%"+iTransportador.getNo_documento()+"%");
		 Transportadors=(List<T>) this.findNamedAsObjects("QUERY_SELECT_ID_LIKE",hmap);
		 return Transportadors;
	}
	
	/**
	 * busqueda de Transportador por llave primaria.
	 * String identificador de Transportador
	 */
	public T buscarTransportador(String documento){
		T Transportador=null;
		Transportador = this.findPk(documento);
		return Transportador;
		
	}

	@Override
	public void crearTransportador(T iTransportador) {
			this.save(iTransportador);	
	}
	
	/**
	 * editar Transportador
	 */
	public void editarTransportador(T iTransportador) {
		this.update(iTransportador);	
	}
	
	@Override
	public void eliminarTransportador(T iTransportador) {
		this.delete(iTransportador);
		
	}

}
