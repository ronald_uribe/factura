package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.EntradaDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Entrada;
import com.ruribe.util.entidades.jpa.interfaces.IEntradaDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Entradas.
 * */


public class EntradaDaoImpl<T extends IEntradaDao> extends GenericoDaoImpl<T> implements EntradaDao<T> {
	
	/**
	 * busqueda de Entrada por llave primaria.
	 * @param String identificador de Remision
	 * @return remision
	 */
	public T buscarEntrada(int id){
		return this.findPk(id);
	}
 
	/**
	 * Crear una Entrada
	 * @return 
	 */
	public T crearEntrada(T iEntrada) {
		
		T entrada=null;
		entrada =  this.save(iEntrada);	
		return entrada;
			
	}
	
	/**
	 * editar Entrada
	 */
	public void editarEntrada(T iEntrada) {
		this.update(iEntrada);	
	}
	
	@Override
	public void eliminarEntrada(T iEntrada) {
		this.delete(iEntrada);
		
	}
	
	/**
	 * obtiene las ultimas 10 remisiones
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarEntradas() throws SAExcepcion {
		List<T> entradaes=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     // los ultimos 10 
	     entradaes=(List<T>) this.findNamed(Entrada.QUERY_FIND_ALL_ENTRADAS,hmap,0,10);
		 return entradaes;
	}
	
	/**
	 * obtiene las entradas de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarEntradasPorProveedor(String id_cliente) throws SAExcepcion {
		List<T> entradaes=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idProveedor",id_cliente);
		 entradaes=(List<T>) this.findNamedAsObjects(Entrada.QUERY_FIND_ENTRADA_PROVEEDOR,hmap);
		 return entradaes;
	}
	
	
	/**
	 * obtiene las entradas entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarEntradasPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> entradas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 entradas=(List<T>) this.findNamedAsObjects(Entrada.QUERY_FIND_RANGO_FECHAS,hmap);
		 return entradas;
	}

	@SuppressWarnings("unchecked")
	public List<T> buscarEntradasPorProveedorYRangogoFecha(String idProveedor,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
			List<T> entradas=new ArrayList<T>();
		    HashMap<String, Object> hmap = new HashMap<String, Object>();
		    hmap.put("idProveedor",idProveedor);
		    hmap.put("fechaInicial",fechaInicial);
			hmap.put("fechaCorte",fechaFinal);
			entradas=(List<T>) this.findNamedAsObjects(Entrada.QUERY_FIND_ENTRADA_PROVEEDOR_RANGO_FECHAS,hmap);
		return entradas;
	}

}
