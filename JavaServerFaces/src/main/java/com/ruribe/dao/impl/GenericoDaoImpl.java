package com.ruribe.dao.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.GenericoDao;
import com.ruribe.util.SAExcepcion;


/**
 * Clase abstracta que implementa los métodos de la interfaz DAO mediante la tecnología JPA.
 * Permite a sus subclases definir un funcionamiento particular para los métodos de búsqueda
 * y almacenamiento.
 *
 * @param <E> el tipo elemento
 * @implements 
 */
@Repository
public abstract class GenericoDaoImpl <E> implements GenericoDao<E> {

    /** Referencia al EntityManager. */
	@PersistenceContext
	private EntityManager entityManager;
	
	
	/** Tipo generico de la clase. */
	private Class<E> type;

    /**
     * Método abstracto de almacenamiento que delega el comportamiento a la clase subclasificadora.
     * @param oParam Objeto a persistir.
     * @return Referencia al objeto persistido.
     */
    public final E save(E oParam) {
    	return defaultSave(oParam);    	
    }


    /**
     * Implementación por defecto del guardado del objeto pasado por parámetro
     * en el almacenamiento persistente.
     * @param oParam el objeto a persistir.
     * @return el objeto persistido.
     */
    protected final E defaultSave(E oParam) {
    		this.entityManager.persist(oParam);
    		this.entityManager.flush();
    	return oParam;
	}



    /**
     * Almacena todos los objetos pasados en la lista en el medio persistente.
     * @param oListParam - Lista de objetos a persistir.
     * @return La lista de objetos persistida.
     */
    public final List<E> saveAll(List<E> oListParam) {
    	for (E object : oListParam) {
			this.save(object);
		}
		return oListParam;
    }

    /**
     * Actualiza el almacenamiento persistente con los datos del objeto pasado
     * por parámetro.
     * @param oParam el objeto a actualizar.
     * @return el objeto actualizado.
     */
    public final E update(E oParam) {
		E e = null;
		try {
			e = this.entityManager.merge(oParam);
		} catch (PersistenceException ex) {
			//TODO throw new AuesRuntimeExcepcion(ex, TrazasAues.ERR_PERSISTIENDO_DATOS);
			System.out.println("SA ERROR al actualizar el objeto:" + ex.getMessage());
		}
		return e;
	}

    /**
     * Actualiza el almacenamiento persistente con los datos de los objetos pasados
     * en la lista.
     * @param oListParam - Lista de objetos a modificar.
     * @return La lista de objetos modificados.
     */
	public final List<E> updateAll(List<E> oListParam) {
	    for (E object : oListParam) {
			this.update(object);
		}
		return oListParam;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto
	 * pasado por parámetro.
	 * @param oParam El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 */
	
	public final E delete(E oParam) {
		try {
			this.entityManager.remove(entityManager.merge(oParam));
		} catch (PersistenceException ex) {
			//TODO throw new AuesRuntimeExcepcion(ex, TrazasAues.ERR_BORRANDO_DATOS);
			System.out.println("SA ERROR al borrar el objeto:" + ex.getMessage());
			ex.printStackTrace();
		}
		return oParam;
	}
	
	/**
	 * Ejecuta una query pasada por parámetros.
	 * @param namedQueryParam sql
	 * @param parametersParam parametros.
	 */
	protected final void executeUpdateSQL(final String namedQueryParam , final Map<String,Object> parametersParam){
		Query query = this.entityManager.createNamedQuery(namedQueryParam);
		if (parametersParam!=null){
			// Establecemos en el objeto Query el valor de los parámetros recibidos.
			for(String p: parametersParam.keySet()){
				query.setParameter(p, parametersParam.get(p));
			}
			query.executeUpdate();
		}
	}
	

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir de la Query y un mapa de parámetros.
	 * @param queryParam la query a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 */
	public final List<E> find(Query queryParam, Map<String, Object> parametersParam) throws SAExcepcion {
		List<E> resultList = doFind(queryParam, parametersParam);
		return resultList;
	}


	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir de la Query y un mapa de parámetros.
	 *
	 * @param <S> el tipo de los objetos de la lista de retorno.
	 * @param query la query a ejecutar.
	 * @param params los parámetros de la query.
	 *
	 * @return La lista de objetos genéricos resultado de la query.
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	private <S> List<S> doFind(Query query, Map<String, Object> params) throws SAExcepcion {
		if (params!=null)
			// Establecemos en el objeto Query el valor de los parámetros recibidos.
			for(String p: params.keySet())
				query.setParameter(p, params.get(p));
		List<S> resultList = null;
		try {
			resultList = query.getResultList();
			//limpiamos la cache
			this.entityManager.getEntityManagerFactory().getCache().evictAll();
		} catch (PersistenceException ex) {
			  throw new SAExcepcion("TrazasSA.ERR_CONSULTANDO_DATOS");
		}
		return resultList;
	}

	/**
	 * Configura el índice y el número de elementos de la consulta pasada por parámetro.
	 *
	 * @param query la query a ejecutar.
	 * @param startIndex índice inicial de búsqueda.
	 * @param maxResults máximo número de resultados a devolver.
	 */
	private void configurarIndicesBusqueda(Query query, int startIndex, int maxResults) {
		query.setFirstResult(startIndex);
		query.setMaxResults(maxResults);
	}


	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir del nombre de la query (Named Query) y un mapa de parámetros.
	 *
	 * @param namedQueryParam el nombre de la NamedQuery a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 * @see #findNamed(java.lang.String, java.util.Map)
	 */
	public final List<E> findNamed(final String namedQueryParam ,
            final Map<String,Object> parametersParam) throws SAExcepcion {
		Query query = this.entityManager.createNamedQuery(namedQueryParam);
		return this.find(query, parametersParam);
	}

    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object.
     *
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     *
     * @return Lista de objetos de tipo Object.
     */
	public final List<?> findNativeAsObjects(String nativeQueryParam, Class<?> mapperClassParam) {
		Query nativeQuery = this.entityManager.createNativeQuery(nativeQueryParam, mapperClassParam);
		return nativeQuery.getResultList();
	}

	/**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object.
     * Recibe el índice del primer resultado a devolver y el número máximo de
	 * resultados deseados.
     *
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
     *
     * @return Lista de objetos de tipo Object.
     */
	public final List<?> findNativeAsObjects(String nativeQueryParam, Class<?> mapperClassParam, int startIndexParam, int maxResultsParam) {
		Query nativeQuery = this.entityManager.createNativeQuery(nativeQueryParam, mapperClassParam);
		this.configurarIndicesBusqueda(nativeQuery, startIndexParam, maxResultsParam);
		return nativeQuery.getResultList();
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir del nombre de la query (Named Query) y un mapa de parámetros.
	 * Recibe el índice del primer resultado a devolver y el número máximo de
	 * resultados deseados.
	 *
	 * @param namedQueryParam el nombre de la NamedQuery a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
	 *
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 *
	 */
	public final List <E> findNamed(final String namedQueryParam ,
            final Map<String,Object> parametersParam, int startIndexParam, int maxResultsParam) throws SAExcepcion {
		Query query = this.entityManager.createNamedQuery(namedQueryParam);
		this.configurarIndicesBusqueda(query, startIndexParam, maxResultsParam);
		return this.find(query, parametersParam);
	}


	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir de la query en formato String.
	 * Recibe el índice del primer resultado a devolver y el número máximo de
	 * resultados deseados.
	 *
	 * @param queryParam la query a ejecutar.
	 * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
	 *
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #find(java.lang.String, java.util.Map, int, int)
	 */
	public final List <E> find(final String queryParam , int startIndexParam, int maxResultsParam) throws SAExcepcion {
		Query query = this.entityManager.createQuery(queryParam);
		this.configurarIndicesBusqueda(query, startIndexParam, maxResultsParam);
		return this.find(query,null);
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir del nombre de la query en formato String y un mapa de parámetros.
	 * Recibe el índice del primer resultado a devolver y el número máximo de
	 * resultados deseados.
	 *
	 * @param queryParam la query a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
	 *
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #find(java.lang.String, java.util.Map, int, int)
	 */
	public final List <E> find(final String queryParam , Map<String, Object> parametersParam, int startIndexParam, int maxResultsParam) throws SAExcepcion {
		Query query = this.entityManager.createQuery(queryParam);
		this.configurarIndicesBusqueda(query, startIndexParam, maxResultsParam);
		return this.find(query, parametersParam);
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de entidades
	 * a partir del nombre de la query en formato String y un mapa de parámetros.
	 *
	 * @param queryParam la query a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 *
	 * @return La lista de entidades resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #find(java.lang.String, java.util.Map)
	 */
	public final List <E> find(final String queryParam, Map<String, Object> parametersParam) throws SAExcepcion {
		Query query = this.entityManager.createQuery(queryParam);
		return this.find(query,parametersParam);
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de objetos
	 * a partir de la query en formato String.
	 *
	 * @param queryParam la query a ejecutar.
	 *
	 * @return el objeto list
	 * @throws SAExcepcion 
	 *
	 * @see #findAsObjects(java.lang.String, java.util.Map)
	 */
	public final List<Object> findAsObjects(String queryParam) throws SAExcepcion {
		return findAsObjects(queryParam, null);
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de objetos
	 * a partir de la query en formato String y un mapa de parámetros.
	 *
	 * @param queryParam la query a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 *
	 * @return La lista de objetos resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #findAsObjects(java.lang.String, java.util.Map)
	 */
	public final List<Object> findAsObjects(String queryParam, Map<String, Object> parametersParam) throws SAExcepcion {
		Query query = this.entityManager.createQuery(queryParam);
		List<Object> resultList = doFind(query, parametersParam);
		return resultList;
	}

	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de objetos
	 * a partir de la query en formato String y un mapa de parámetros.
	 * Recibe el índice del primer resultado a devolver y el número máximo de
	 * resultados deseados.
	 *
	 * @param queryParam la query a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
	 *
	 * @return La lista de objetos resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #findAsObjects(java.lang.String, java.util.Map, int, int)
	 */
	public final List<Object> findAsObjects(String queryParam, Map<String, Object> parametersParam, int startIndexParam, int maxResultsParam) throws SAExcepcion {
		Query query = this.entityManager.createQuery(queryParam);
		this.configurarIndicesBusqueda(query, startIndexParam, maxResultsParam);
		List<Object> resultList = this.doFind(query, parametersParam);
		return resultList;
	}

   
	/**
	 * Devuelve el resultado de una Query JPQL en forma de lista de objetos
	 * a partir del nombre de la query (NamedQuery) y un mapa de parámetros.
	 *
	 * @param namedQueryParam el nombre de la NamedQuery a ejecutar.
	 * @param parametersParam los parámetros de la query.
	 *
	 * @return La lista de objetos resultado de la query.
	 * @throws SAExcepcion 
	 *
	 * @see #findNamedAsObjects(java.lang.String, java.util.Map)
	 */
	public final List<Object> findNamedAsObjects(String namedQueryParam, Map<String, Object> parametersParam) throws SAExcepcion {
		List<Object> resultList = null;
		Query query = getEntityManager().createNamedQuery(namedQueryParam);
		resultList = this.doFind(query, parametersParam);
		return resultList;
	}
	
	
	/**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object.
     * @author RONIE
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     *
     * @return Lista de objetos de tipo Object.
	 * @throws SAExcepcion 
     */
	public final List<?> findNativeAsObjectsPropio(String nativeQueryParam, Class<?> mapperClassParam,Map<String, Object> parametersParam) throws SAExcepcion {
		TypedQuery <?> nativeQuery= this.entityManager.createNamedQuery(nativeQueryParam,mapperClassParam);
		return this.find(nativeQuery, parametersParam);
	}

	/**
	 * Establece el campo entity manager.
	 * @param managerParam el nuevo campo entity manager
	 */
	
	public void setEntityManager(EntityManager managerParam) {
		this.entityManager = managerParam;
	}	


	/**
     * Obtiene el campo entity manager.
     * @return el campo entity manager
     */
	
    public EntityManager getEntityManager() {
    	return this.entityManager;
	}

	/**
	 * Transforma un objeto en long.
	 * Si es un BigDecimal, se obtiene su valor Long, si
	 * es de cualquier otro tipo se realiza un casting.
	 *
	 * @param idParam el objeto a convertir en Long.
	 *
	 * @return el resultado de la transformación del objeto de entrada a Long.
	 */
	protected final Long idToLong(Object idParam) {
		Long idLong;
		if (idParam instanceof BigDecimal)
			idLong = ((BigDecimal)idParam).longValue();
		else
			idLong = (Long) idParam;
		return idLong;
	}
	
	/**
	 * Obtiene los datos necesarios para la extracción de DWH.
	 * @param query String con la consulta a obtener.
	 * @return List con el resultado de la consulta.
	 */
	@SuppressWarnings("unchecked")
	public final List<Object> findDataWareHouse(String query) {
		Query consulta;
		consulta = getEntityManager().createNativeQuery(query, String.class);
		getEntityManager().clear();
		return consulta.getResultList();	
	}
	

	/**
	* Método abstracto de búsqueda por PK.
	* @param pk clave a buscar.
	* @return Objeto por clave primaria.
	*/

	public final E findPk(Object pk) {
		return entityManager.find(this.type, pk);
	}

	/**
	* Get del tipo de clase genérico.
	* @return tipo de la clase.
	*/
	public final Class<E> getType() {
	return type;
	}

	/**
	* Set del tipo de clase genérico.
	* @param clazz tipo de la clase.
	*/

	public final void setType(Class<E> clazz) {
	this.type = clazz;

	}
}