package com.ruribe.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.TipoProductoDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.TipoProducto;
import com.ruribe.util.entidades.jpa.interfaces.ITipoProductoDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de usuarios.
 * */


public class TipoProductoDaoImpl<T extends ITipoProductoDao> extends GenericoDaoImpl<T> implements TipoProductoDao<T> {
	
	/**
	 * retorna los tipos de documentos
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarTipoProducto() throws SAExcepcion {
		 List<T> tipoProductos=null;
		 tipoProductos=(List<T>) this.findNamedAsObjects(TipoProducto.QUERY_FIND_ALL_TIPO_PRODUCTO,null);
		 return tipoProductos;
	}

	@Override
	public List<T> buscarTipoProducto(T TipoProducto) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	/**
	 * Consulta un Tipo Producto por su identificador
	 * @param String cod_tipo_producto
	 * @return objeto T TipoProducto
	 * @throws SAExcepcion 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarProductoTipoProducto(String cod_tipo_producto) throws SAExcepcion {
		List<T> tipoProductos=null;
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("cod_tipo_producto","%"+cod_tipo_producto+"%");
	    tipoProductos = (List<T>) this.findNamedAsObjects(TipoProducto.QUERY_SELECT_ID_TIPO_PRODUCTO,hmap);
		return tipoProductos;
	}
	
}
