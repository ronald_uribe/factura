package com.ruribe.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.SalidaDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Salida;
import com.ruribe.util.entidades.jpa.interfaces.ISalidaDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Salidas.
 * */


public class SalidaDaoImpl<T extends ISalidaDao> extends GenericoDaoImpl<T> implements SalidaDao<T> {
	
	/**
	 * busqueda de Salida por llave primaria.
	 * @param String identificador de Remision
	 * @return remision
	 */
	@SuppressWarnings("unchecked")
	public T buscarSalida(String documento){
		 return this.findPk(Integer.parseInt(documento));
	}
 
	/**
	 * Crear una Salida
	 * @return 
	 */
	public T crearSalida(T iSalida) {
		
		T entrada=null;
		entrada =  this.save(iSalida);	
		return entrada;
			
	}
	
	/**
	 * editar Salida
	 */
	public void editarSalida(T iSalida) {
		this.update(iSalida);	
	}
	
	@Override
	public void eliminarSalida(T iSalida) {
		this.delete(iSalida);
		
	}
	
	/**
	 * obtiene las ultimas 10 remisiones
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	public List<T> buscarSalidas() throws SAExcepcion {
		List<T> entradaes=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
	     // los ultimos 10 
	     entradaes=(List<T>) this.findNamed(Salida.QUERY_FIND_ALL_SALIDAS,hmap,0,10);
		 return entradaes;
	}
	
	/**
	 * obtiene las remisiones de un cliente 
	 * @param String id_cliente
	 * @return lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarSalidasPorProveedor(String id_proveedor) throws SAExcepcion {
		List<T> salidas=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("idProveedor",id_proveedor);
		 salidas=(List<T>) this.findNamedAsObjects(Salida.QUERY_FIND_SALIDA_PROVEEDOR,hmap);
		 return salidas;
	}
	
	
	/**
	 * obtiene las remisiones entre un rango de fechas
	 * @param Date fincio
	 * @param Date ffinal
	 * @return List<T> lista de remision
	 * @throws SAExcepcion 
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarSalidasPorRangoFechas(Date fincio, Date ffinal) throws SAExcepcion {
		List<T> entradaes=new ArrayList<T>();
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("fechaInicial",fincio);
		 hmap.put("fechaCorte",ffinal);
		 entradaes=(List<T>) this.findNamedAsObjects(Salida.QUERY_FIND_RANGO_FECHAS,hmap);
		 return entradaes;
	}

	/**
	 * obtiene los articulos que tiene un que se le debe al proveedor hasta la fecha
	 * @return lista de objetos
	 * @throws SAExcepcion 
	 */
	@Override
	public List<Object> obtenerProductosPendientesPorProveedor(String idproveedor) throws SAExcepcion {
		List<Object> pendientes=null;
	    HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("codigoProveedor",idproveedor);
		pendientes= (List<Object>) this.findNamedAsObjects(Salida.QUERY_PRODUCTOS_PENDIENTES_PROVEEDOR,hmap);
		return  pendientes;
	}

	@SuppressWarnings("unchecked")
	public List<T> buscarSalidasPorProveedorYRangogoFecha(String idProveedor,Date fechaInicial, Date fechaFinal) throws SAExcepcion {
			List<T> salidas=new ArrayList<T>();
		    HashMap<String, Object> hmap = new HashMap<String, Object>();
		    hmap.put("idProveedor",idProveedor);
		    hmap.put("fechaInicial",fechaInicial);
			hmap.put("fechaCorte",fechaFinal);
			salidas=(List<T>) this.findNamedAsObjects(Salida.QUERY_FIND_SALIDA_PROVEEDOR_RANGO_FECHAS,hmap);
		return salidas;
	}

}
