package com.ruribe.dao.impl;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.ObraDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.interfaces.IObraDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de Obras.
 * */


public class ObraDaoImpl<T extends IObraDao> extends GenericoDaoImpl<T> implements ObraDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarObras() throws SAExcepcion {
		 List<T> proveedores=null;
		 proveedores=(List<T>) this.findNamedAsObjects("QUERY_FIND_ALL",null);
		System.out.println("Obras" + proveedores.size());
		 return proveedores;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarObras(T iObra) throws SAExcepcion {
		 List<T> Obras=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("id_cliente","%"+iObra.getCliente().getId_cliente()+"%");
		 Obras=(List<T>) this.findNamedAsObjects("QUERY_SELECT_ID_LIKE",hmap);
		 return Obras;
	}
	
	/**
	 * busqueda de Obra por llave primaria.
	 * String identificador de Obra
	 */
	public T buscarObra(int id_obra){
		T Obra=null;
		Obra = this.findPk(id_obra);
		return Obra;
		
	}

	public void vincularObra(T iObra) {
			this.save(iObra);	
	}

	
	public void desvincularObra(T iObra) {
		this.delete(iObra);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> obtenerObrasCliente(String id_cliente) throws SAExcepcion {
		List<T> listaobras=null;
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("id_cliente",id_cliente);
		listaobras=(List<T>) this.findNamedAsObjects(Obra.SELECT_OBRAS_CLIENTE,hmap);
		return listaobras;
	}

	public void editarObra(T iObra) {
		this.update(iObra);	
	}

}
