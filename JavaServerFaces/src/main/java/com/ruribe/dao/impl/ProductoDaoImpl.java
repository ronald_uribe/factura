package com.ruribe.dao.impl;


import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ruribe.dao.ProductoDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;

/**
 * Ruribe.
 * implementacion de acceso a la tabla de producto.
 * */


public class ProductoDaoImpl<T extends IProductoDao> extends GenericoDaoImpl<T> implements ProductoDao<T> {
		 
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarProductos() throws SAExcepcion {
		 
		 List<T> producto=null;
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_FIND_ALL_PRODUCTO,null);
		return producto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarProducto(T iProducto) throws SAExcepcion {
		 List<T> producto=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("codigo","%"+iProducto.getCodigo_producto()+"%");
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_SELECT_ID_LIKE,hmap);
		 return producto;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<IProductoDao> buscarProductoPorTipoProducto(int idTipoProducto) throws SAExcepcion {
		 List<T> producto=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("cod_tipo_producto",idTipoProducto);
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_FIND_ALL_PRODUCTO_POR_TIPO,hmap);
		 return (List<IProductoDao>) producto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IProductoDao> obtenerProductosByDescripcion(String filtro) throws SAExcepcion {
		 List<T> producto=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("filtro","%"+filtro+"%");
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_FIND_PRODUCTO_BY_DESCRIPCION,hmap);
		 return (List<IProductoDao>) producto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IProductoDao> buscarProductoPorTipoProductoConStock(int idTipoProducto) throws SAExcepcion {
		 List<T> producto=null;
	     HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("cod_tipo_producto",idTipoProducto);
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_FIND_PRODUCTO_POR_TIPO_CON_STOCK,hmap);
		 return (List<IProductoDao>) producto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IProductoDao> obtenerProductosPorTipoProveedor(HashMap<String, Object> hmap) throws SAExcepcion {
		 List<T> producto=null;
		 producto=(List<T>) this.findNamedAsObjects(Producto.QUERY_FIND_PRODUCTO_POR_TIPO_PROVEEDOR,hmap);
		 return (List<IProductoDao>) producto;
	}
	
	/**
	 * busqueda de Producto por llave primaria.
	 * String identificador de Producto
	 */
	public T buscarProducto(String cod_producto){
		T Producto=null;
		Producto = this.findPk(cod_producto);
		return Producto;
		
	}

	@Override
	public void crearProducto(T iProducto) {
		this.save(iProducto);	
	}
	
	/**
	 * editar Producto
	 */
	public void editarProducto(T iProducto) {
		this.update(iProducto);	
	}
	
	@Override
	public void eliminarProducto(T iProducto) {
		this.delete(iProducto);
		
	}


}
