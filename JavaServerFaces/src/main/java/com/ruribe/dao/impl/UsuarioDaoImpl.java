package com.ruribe.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.ruribe.dao.UsuarioDao;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.LoginBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Usuario;
import com.ruribe.util.entidades.jpa.interfaces.IUsuarioDao;

/**
 * Ruribe. implementacion de acceso a la tabla de usuarios.
 */

public class UsuarioDaoImpl<T extends IUsuarioDao> extends GenericoDaoImpl<T> implements UsuarioDao<T> {


	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarUser() throws SAExcepcion {

		List<T> usuarios = null;
		usuarios = (List<T>) this.findNamedAsObjects(Usuario.QUERY_FIND_ALL, null);
		return usuarios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> buscarUser(String usernameABuscar) throws SAExcepcion {
		List<T> usuarios = new ArrayList<T>();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("valor", "%" + usernameABuscar + "%");
		usuarios = (List<T>) this.findNamedAsObjects(Usuario.QUERY_SELECT_ID_LIKE, hmap);
		return usuarios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean login(String username, String password) throws SAExcepcion {
		List<T> usuario = new ArrayList<T>();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("username", username);
		hmap.put("clave", password);
		usuario = (List<T>) this.findNamedAsObjects(Usuario.QUERY_LOGIN, hmap);
		if (usuario == null || usuario.size() == 0) {
			Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "login invalido:" + username);
			return false;
		}
		Logger.getLogger(UsuarioDaoImpl.class.getName()).log(Level.INFO, "login correcto:" + username);
		return true;
	}

	/**
	 * busqueda de usuario por llave primaria. String identificador de usuario
	 */
	public T buscarUsuario(String documento) {
		T usuario = null;
		usuario = this.findPk(documento);
		return usuario;

	}

	@Override
	public void crearUsuario(T iusuario) {
		this.save(iusuario);
	}

	/**
	 * editar usuario
	 */
	public void editarUsuario(T iusuario) {
		this.update(iusuario);
	}

	@Override
	public void eliminarUsuario(Long iusuario) {
		this.delete(this.findPk(iusuario));
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean existeUsuario(String username) throws SAExcepcion {
		List<T> usuario = new ArrayList<T>();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("username", username);
		usuario = (List<T>) this.findNamedAsObjects(Usuario.QUERY_USERNAME, hmap);
		if (usuario == null || usuario.size() == 0) {
			Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "Username ya existe" + username);
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T findUserByUsername(String username) throws SAExcepcion {
		List<T> usuario = new ArrayList<T>();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("username", username);
		usuario = (List<T>) this.findNamedAsObjects(Usuario.QUERY_USERNAME, hmap);
		if (usuario != null || !usuario.isEmpty()) {
			return usuario.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean actualizarContrasena(String username, String generateMD5Signature) throws SAExcepcion {
		List<T> usuario = new ArrayList<T>();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("username", username);
		usuario = (List<T>) this.findNamedAsObjects(Usuario.QUERY_USERNAME, hmap);
		usuario.get(0).setClave(generateMD5Signature);
		if (this.update(usuario.get(0))!=null) {
			return true;
		}
		return false;
	}

	/**
	 * clases de prueba
	 */
	static public void main(String arg[]) {
		// comprobamos la conexion a bd sea correcta.
//		Statement st;
		try {
			// POSTGREE
//			Class.forName("org.postgresql.Driver");	
//		    System.out.println("Driver Loaded.");
//		    String url ="jdbc:postgresql://localhost:5432/RONIE";   	
//		    Connection conn = DriverManager.getConnection(url, "RONIE", "admin");
			// MYSQL
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver Loaded.");
			String url = "Jdbc:mysql://localhost:3306/soloandamios?serverTimezone=UTC";
			@SuppressWarnings("unused")
			Connection conn = DriverManager.getConnection(url, "root", "ruQHB499");

			System.out.println("MENSAJE DE PRUEBA");
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			System.out.println("2");
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			System.out.println("3");
			System.out.println("MENSAJE DE PRUEBA FIN");
			Cliente cliente = new Cliente();
			cliente = em.find(Cliente.class, "1298734567");
			Obra obra = new Obra();
			obra.setDireccion("direccion2");
			obra.setNombre("nombre2");
			obra.setTelefono("telefono2");
			obra.setCliente(cliente);
			// em.persist(obra);
			TypedQuery<Cliente> clientes = em.createNamedQuery(Cliente.QUERY_FIND_ALL_CLIENTE, Cliente.class);
			for (Cliente c : clientes.getResultList()) {
				System.out.println("Razon social:" + c.getRazon_social());
				for (Obra o : c.getListaObras()) {
					System.out.println("obra:" + o.getNombre());
				}
			}

			TypedQuery<Obra> listaobras = em.createNamedQuery("QUERY_SELECT_OBRAS_CLIENTE", Obra.class);
			listaobras.setParameter("id_cliente", "1234567");
			for (Obra c : listaobras.getResultList()) {
				System.out.println("nombre de obra:" + c.getNombre());

			}

			em.getTransaction().commit();
			em.close();
			emf.close();
			System.out.println("Got Connection.");
//				st = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

}
