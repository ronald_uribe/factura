package com.ruribe.dao;

import java.util.Date;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IReposicionDao;


public interface ReposicionDao<T  extends IReposicionDao> extends GenericoDao<T> {

	public T crearReposicion(T iReposicion);
	
	public void editarReposicion(T iReposicion);
	
	public void eliminarReposicion(T iReposicion);

	public List<Object> obtenerProductosPendientes(String idCliente,String idobra) throws SAExcepcion;
	
	/**
	 * obtiene las ultimas 10 reposiciones.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarReposiciones() throws SAExcepcion;
	
	/**
	 * obtiene una reposicion por su id.
	 * @param idReposicion
	 * @return T Reposicion
	 * @throws SAExcepcion 
	 */
	public T buscarReposicion(String idReposicion) throws SAExcepcion;
	/**
	 * obtiene las reposiciones de un cliente
	 * @param idCliente
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarReposicionesPorCliente(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las reposiciones de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarReposicionesPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	
	/**
	 * obtiene las reposiciones de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 * @throws SAExcepcion 
	 */
	public List<T> buscarReposicionesPorClienteYRangogoFecha(String idCliente, Date fechaInicial, Date fechaFinal) throws SAExcepcion;

	/**
	 * Obtiene una decolucion agrupada por remision
	 * @param idReposicion
	 * @return IReposicionDao
	 * @throws SAExcepcion 
	 */
	public IReposicionDao buscarReposicionPDF(int idReposicion) throws SAExcepcion;

	
	

}
