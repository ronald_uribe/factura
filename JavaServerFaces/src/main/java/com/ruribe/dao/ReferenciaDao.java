package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IReferenciaDao;

public interface ReferenciaDao<T  extends IReferenciaDao> extends GenericoDao<T> {
	
	/**busca todos los referencias.
	 * @throws SAExcepcion */
	public List<T> buscarReferencias() throws SAExcepcion;
	
	/**busca un referencia por su identificador.*/
	public T buscarReferencia(String codigo_interno);

	/**Crea una referencia.*/
	public void crearReferencia(T ireferencia);
	
	/**editar una referencia.*/
	public void editarReferencia(T ireferencia);
	
	/**eliminar una referencia.*/
	public void eliminarReferencia(T ireferencia);

	/**
	 * @param filtro descripcion de referencia
	 * @return List<T>
	 * @throws SAExcepcion
	 */
	public List<T> obtenerReferenciasByDescripcion(String filtro) throws SAExcepcion;

}
