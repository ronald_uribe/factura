package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.ITipoProductoDao;


public interface TipoProductoDao<T  extends ITipoProductoDao> extends GenericoDao<T> {
	
	/**busca todos los tipos de documento.
	 * @throws SAExcepcion */
	public List<T> buscarTipoProducto() throws SAExcepcion;
	/**busca todos los tipos de documento.*/
	public List<T> buscarTipoProducto(T tipoProducto);

}
