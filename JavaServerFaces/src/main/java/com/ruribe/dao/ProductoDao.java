package com.ruribe.dao;

import java.util.HashMap;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;


public interface ProductoDao<T  extends IProductoDao> extends GenericoDao<T> {
	
	/**busca todos los productos.
	 * @throws SAExcepcion */
	public List<T> buscarProductos() throws SAExcepcion;
	
	/**busca un producto por su identificador.*/
	public T buscarProducto(String codigo) throws SAExcepcion;

	public void crearProducto(T iproducto);
	
	public void editarProducto(T iproducto);
	
	public void eliminarProducto(T iproducto);

	public List<T> buscarProducto(T iproducto) throws SAExcepcion;

	public List<IProductoDao> buscarProductoPorTipoProducto(int idTipoProducto) throws SAExcepcion;
	
	public List<IProductoDao> buscarProductoPorTipoProductoConStock(int idTipoProducto) throws SAExcepcion;

	public List<IProductoDao> obtenerProductosPorTipoProveedor(HashMap<String, Object> hmap) throws SAExcepcion;

	public List<IProductoDao> obtenerProductosByDescripcion(String filtro) throws SAExcepcion;

}
