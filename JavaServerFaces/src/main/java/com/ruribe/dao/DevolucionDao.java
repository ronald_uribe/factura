package com.ruribe.dao;

import java.util.Date;
import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IDevolucionDao;


public interface DevolucionDao<T  extends IDevolucionDao> extends GenericoDao<T> {

	public T crearDevolucion(T iDevolucion);
	
	public void editarDevolucion(T iDevolucion);
	
	public void eliminarDevolucion(T iDevolucion);

	public List<Object> obtenerProductosPendientes(String idCliente,int idobra) throws SAExcepcion;
	
//	public List<T> obtenerproductosPendientesbyFecha(HashMap<String, Object> params);
	
	/**
	 * obtiene las ultimas 10 devoluciones.
	 * @return List<T>
	 * @throws SAExcepcion 
	 */
	public List<T> buscarDevoluciones() throws SAExcepcion;
	/**
	 * obtiene una remision por su id.
	 * @param idDevolucion
	 * @return
	 */
	public T buscarDevolucion(String idDevolucion) throws SAExcepcion;
	/**
	 * obtiene las remisiones de un cliente
	 * @param idCliente
	 * @return
	 */
	public List<T> buscarDevolucionesPorCliente(String idCliente) throws SAExcepcion;
	/**
	 * obtiene las remisiones de un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<T> buscarDevolucionesPorRangoFechas(Date fechaInicial, Date fechaFinal) throws SAExcepcion;
	
	/**
	 * obtiene las remisiones de un cliente entre un rango de fechas
	 * @param idCliente
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<T> buscarDevolucionesPorClienteYRangogoFecha(String idCliente, Date fechaInicial, Date fechaFinal) throws SAExcepcion;

	/**
	 * Obtiene una decolucion agrupada por remision
	 * @param idDevolucion
	 * @return IDevolucionDao
	 * @throws SAExcepcion 
	 */
	public IDevolucionDao buscarDevolucionPDF(int idDevolucion) throws SAExcepcion;

	public List<Object> consultarAllSaldosPendientes() throws SAExcepcion;

	public boolean devolucionIsPrinted(int idDevolucion);

	public void marcarDevolucionImpresa(int idDevolucion);

	
	

}
