package com.ruribe.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ruribe.util.SAExcepcion;

/**
 * Interfaz que define el comportamiento base que deben implementar todas las clases de acceso a datos. 
 * @author Ruribe
 * @param <E> el tipo de la entidad.
 */
public interface GenericoDao < E > {

    /**
     * Persiste una entidad en base de datos.
     * @param oParam - Objeto a persistir
     * @return Referencia del objeto persistido.
     */
    public E save(E oParam);
        
    /**
     * Persiste una coleccion de entidades en base de datos.
     * @param oListParam - Lista de objetos a persistir.
     * @return Referencias a los objetos persistidos.
     */ 
    public List<E> saveAll(List<E> oListParam);
    
    /**
     * Modifica los valores de un objeto existente en base de datos.
     * @param oParam - Objeto a modificar.
     * @return Referencia al objeto modificado.
     */
    public E update(E oParam);
    
    /**
     * Modifica los valores de una coleccion de objetos existentes en base de datos.
     * @param oListParam - Lista de objetos a modificar.
     * @return Referencias a los objetos modificados.
     */
	public List<E> updateAll(List<E> oListParam);
    
    /**
     * Elimina un objeto existente en base de datos.
     * @param oParam - Objeto a eliminar.
     * @return Referencia al objeto eliminado.
     */
    public E delete(E oParam);
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro.
     * @param queryParam - Consulta a realizar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @return Lista de objetos recuperados.
     * @throws SAExcepcion 
     */
    public List<E> find(Query queryParam, Map<String, Object> parametersParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     * @param namedQueryParam - Nombre de la consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @return Lista de objetos de tipo Object.
     * @throws SAExcepcion 
     */
    public List<Object> findNamedAsObjects(String namedQueryParam, Map<String, Object> parametersParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     * @return Lista de objetos de tipo Object.
     */
    public List<?> findNativeAsObjects(String nativeQueryParam, Class<?> mapperClassParam);
    
	/**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     * Recibe el índice del primer resultado a devolver y el número máximo de 
	 * resultados deseados.
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     * @param startIndexParam el indice inicial de búsqueda.
	 * @param maxResultsParam el número máximo de resultados de la búsqueda.
     * @return Lista de objetos de tipo Object.
     */
	public List<?> findNativeAsObjects(String nativeQueryParam, Class<?> mapperClassParam, int startIndexParam, int maxResultsParam);
    

    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro.
     * @param namedQueryParam - Nombre de la consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @return Lista de objetos.
     * @throws SAExcepcion 
     */
    public List <E> findNamed(String namedQueryParam , Map<String,Object> parametersParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro de forma paginada.
     * @param namedQueryParam - Nombre de la consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @param startParam - Indica la posicion inicial de los objetos a recuperar.
     * @param amountParam - Indica el incremento desde la posicion inicial de objeto a recuperar.
     * 
     * @return Lista de objetos.
     * @throws SAExcepcion 
     */
    public List <E> findNamed(String namedQueryParam , Map<String,Object> parametersParam, int startParam, int amountParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro de forma paginada.
     * @param queryParam - Consulta a ejecutar.
     * @param startParam - Indica la posicion inicial de los objetos a recuperar.
     * @param amountParam - Indica el incremento desde la posicion inicial de objeto a recuperar.
     * @return Lista de objetos.
     * @throws SAExcepcion 
     */
    public List <E> find(String queryParam , int startParam, int amountParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro de forma paginada.
     * @param queryParam - Consulta a ejecutar.
     * @param parametersParam - Parametros.
     * @param startParam - Indica la posicion inicial de los objetos a recuperar.
     * @param amountParam - Indica el incremento desde la posicion inicial de objeto a recuperar.
     * 
     * @return Lista de objetos.
     * @throws SAExcepcion 
     */
    public List <E> find(String queryParam ,Map<String, Object> parametersParam, int startParam, int amountParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro de forma paginada.
     * @param queryParam - Consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @return Lista de objetos.
     * @throws SAExcepcion 
     */
    public List <E> find(String queryParam , Map<String, Object> parametersParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     * @param queryParam - Consulta a ejecutar.
     * @return Lista de objetos de tipo Object.
     * @throws SAExcepcion 
     */
    public List<Object> findAsObjects(String queryParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     * @param queryParam - Nombre de la consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * 
     * @return Lista de objetos de tipo Object.
     * @throws SAExcepcion 
     */
    public List<Object> findAsObjects(String queryParam, Map<String, Object> parametersParam) throws SAExcepcion;
    
    /**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object. 
     *
     * @param queryParam - Nombre de la consulta a ejecutar.
     * @param parametersParam - Conjunto de parámetros correspondientes a la consulta.
     * @param startIndexParam - Indica la posicion inicial de los objetos a recuperar.
     * @param maxResultsParam - Indica el incremento desde la posicion inicial de objeto a recuperar.
     * 
     * @return Lista de objetos de tipo Object.
     * @throws SAExcepcion 
     */
    public List<Object> findAsObjects(String queryParam, Map<String, Object> parametersParam, int startIndexParam, int maxResultsParam) throws SAExcepcion;
    
    
	/**
	 * Obtiene los datos necesarios para la extracción de DWH.
	 * @param query String con la consulta a obtener.
	 * @return List con el resultado de la consulta.
	 */
	public List<Object> findDataWareHouse(String query);
	
	/**
     * Obtiene el campo entity manager.
     * @return el campo entity manager
     */
    public EntityManager getEntityManager();
    
    /**
	* Método abstracto de búsqueda por PK.
	* @param pk clave a buscar.
	* @return Objeto por clave primaria.
	*/

	public E findPk(Object pk);

	/**
	* Get del tipo de clase genérico.
	* @return tipo de la clase.
	*/
	public Class<E> getType();

	/**
	* Set del tipo de clase genérico.
	* @param clazz tipo de la clase.
	*/
	public void setType(Class<E> clazz);
    
    
	/**
     * Obtiene la lista de objetos que satisfacen la consulta pasada como parámetro como tipo de objetos
     * Object.
     * @author RONIE
     * @param nativeQueryParam - Consulta nativa a ejecutar.
     * @param mapperClassParam - Clase que mapeará los datos de respuesta obtenidos de la ejecución.
     *
     * @return Lista de objetos de tipo Object.
	 * @throws SAExcepcion 
     */
	public List<?> findNativeAsObjectsPropio(String nativeQueryParam, Class<?> mapperClassParam,Map<String, Object> parametersParam) throws SAExcepcion;
    

}