package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.ICiudadDao;


public interface CiudadDao<T  extends ICiudadDao> extends GenericoDao<T> {
	
	/**busca todas las ciudades.
	 * @throws SAExcepcion */
	public List<T> buscarCiudades() throws SAExcepcion;
	/**busca una ciudad por identificador.*/
	public T buscarCiudad(Integer codigo_ciudad) throws SAExcepcion;;

}
