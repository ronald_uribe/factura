package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IObraDao;


public interface ObraDao<T  extends IObraDao> extends GenericoDao<T> {
	
	/**busca todos los Obras.*/
	public List<T> buscarObras() throws SAExcepcion;
	
	/**busca un Obra por su identificador.*/
	public T buscarObra(int id_Obra) throws SAExcepcion;

	public void vincularObra(T iObra);
	
	public void editarObra(T iObra);

	public List<T> buscarObras(T iObra) throws SAExcepcion;

	public void desvincularObra(T iobra);

	public List<T> obtenerObrasCliente(String id_cliente) throws SAExcepcion;

}
