package com.ruribe.dao;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.entidades.jpa.interfaces.IRoleDao;


public interface RoleDao<T  extends IRoleDao> extends GenericoDao<T> {
	
	public T findRole(int id_role);

	List<T> findAllRoles() throws SAExcepcion;


}
