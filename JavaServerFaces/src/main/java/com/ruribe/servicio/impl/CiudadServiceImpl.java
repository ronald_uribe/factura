package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.ruribe.dao.CiudadDao;
import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.CiudadBean;
import com.ruribe.util.bean.interfaces.ICiudadBean;
import com.ruribe.util.entidades.jpa.interfaces.ICiudadDao;

@Named
public class CiudadServiceImpl implements CiudadService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	
	@Autowired
	private CiudadDao<ICiudadDao> asCiudadDao;

	
	/**
	 * devuelve todos las ciudades
	 * @throws SAExcepcion 
	 */
	@Override
	public List<ICiudadBean> obtenerCiudad() throws SAExcepcion {
		
		List<ICiudadDao> ciudadDao= asCiudadDao.buscarCiudades();
		 List<ICiudadBean> ciudades = new ArrayList<ICiudadBean>();
		
		for (int i =0; i< ciudadDao.size(); i++){
			ICiudadBean iu = (ICiudadBean) new CiudadBean();	
			iu.setNombre_ciudad(ciudadDao.get(i).getNombre_ciudad());
			iu.setCodigo_ciudad(ciudadDao.get(i).getCodigo_ciudad());
			ciudades.add(iu);		
		}		
		return ciudades;
	}

	/**
	 * devuelve una ciudad consultada por identificador
	 * @throws SAExcepcion 
	 */
	@Override
	public ICiudadBean obtenerCiudad(ICiudadBean iciudadBean) throws SAExcepcion {
		
			ICiudadDao dao= asCiudadDao.buscarCiudad(iciudadBean.getCodigo_ciudad());
			ICiudadBean bean = (ICiudadBean) new CiudadBean();	
			bean.setNombre_ciudad(dao.getNombre_ciudad());
			bean.setCodigo_ciudad(dao.getCodigo_ciudad());
		
			return bean;
	}

 
}