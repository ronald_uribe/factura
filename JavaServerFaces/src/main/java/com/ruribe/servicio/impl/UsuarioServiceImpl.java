package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.RoleDao;
import com.ruribe.dao.UsuarioDao;
import com.ruribe.servicio.interfaces.UsuarioService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.RoleBean;
import com.ruribe.util.bean.UsuarioBean;
import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUsuarioBean;
import com.ruribe.util.entidades.jpa.Role;
import com.ruribe.util.entidades.jpa.Usuario;
import com.ruribe.util.entidades.jpa.interfaces.IRoleDao;
import com.ruribe.util.entidades.jpa.interfaces.IUsuarioDao;

@Named
public class UsuarioServiceImpl implements UsuarioService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private UsuarioDao<IUsuarioDao> asUserDao;
	
	@Autowired
	private RoleDao<IRoleDao> asRoleDao;
	
	/**
	 * alta de usuario 
	 */
	@Transactional
	public void crearUsuario(IUsuarioBean iusuarioBean){
		IUsuarioDao iusuario = new Usuario();
		iusuario.setNombre(iusuarioBean.getNombre());
		iusuario.setApellido(iusuarioBean.getApellido());
		iusuario.setTelefono(iusuarioBean.getTelefono());
		iusuario.setUsername(iusuarioBean.getUsername());
		iusuario.setClave(iusuarioBean.getClave());
		iusuario.setEnabled(iusuarioBean.getEnabled());
		
		//asociamos las obras del cliente.
		for (String authority :iusuarioBean.getRolesSelect()){
			iusuario.getRoles().add((Role) asRoleDao.findPk(Long.parseLong(authority)));	
		}
				
		asUserDao.crearUsuario(iusuario);
	}
	
	@SuppressWarnings("unused")
	private Role IRoleBeanTOIRoleDao(IRoleBean iRoleBean) {
		IRoleDao dao = new Role();
		dao.setAuthority(iRoleBean.getAuthority());
		dao.setRole_id(iRoleBean.getId());
		return (Role) dao;
	}

	/**
	 * editar usuario
	 */
	@Transactional
	public void editarUsuario(IUsuarioBean iusuarioBean){
		IUsuarioDao iusuario = new Usuario();
		iusuario.setId(iusuarioBean.getCodigo());
		iusuario.setNombre(iusuarioBean.getNombre());
		iusuario.setApellido(iusuarioBean.getApellido());
		iusuario.setTelefono(iusuarioBean.getTelefono());
		iusuario.setUsername(iusuarioBean.getUsername());
		iusuario.setClave(iusuarioBean.getClave());
		iusuario.setEnabled(iusuarioBean.getEnabled());
		asUserDao.editarUsuario(iusuario);
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional
	public void borrarUsuario(IUsuarioBean iusuarioBean) {
		asUserDao.eliminarUsuario(iusuarioBean.getCodigo());
	}

	/**
	 * editar una lista de usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IUsuarioBean> editarUsuario(List<IUsuarioBean> Listaiusuario) throws SAExcepcion {
		asUserDao.buscarUser();
		return null;
	}

	/**
	 * devuelve todos los usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	@Transactional(readOnly = true)
	public List<IUsuarioBean> obtenerUsuarios() throws SAExcepcion {
		List<IUsuarioDao> dao= asUserDao.buscarUser();
		 List<IUsuarioBean> usuarios = new ArrayList<IUsuarioBean>();
		
		for (int i =0; i< dao.size(); i++){
			IUsuarioBean bean = (IUsuarioBean) new UsuarioBean();	
			bean.setCodigo(dao.get(i).getId());
			bean.setNombre(dao.get(i).getNombre());
			bean.setApellido(dao.get(i).getApellido());
			bean.setTelefono(dao.get(i).getTelefono());
			bean.setUsername(dao.get(i).getUsername());
			bean.setClave(dao.get(i).getClave());
			bean.setEnabled(dao.get(i).getEnabled());
			
			usuarios.add(bean);		
		};		
		return usuarios;
	}

	/**
	 * obtenerUsuario
	 * @param IUsuarioBean
	 * @return IUsuarioBean
	 * @throws SAExcepcion 
	 */
	@Transactional(readOnly = true)
	public List<IUsuarioBean> obtenerUsuario(IUsuarioBean iusuarioBean) throws SAExcepcion {
		
		List<IUsuarioDao> dao= asUserDao.buscarUser(iusuarioBean.getCodigoBuscado());
		List<IUsuarioBean> usuarios = new ArrayList<IUsuarioBean>();
		if (dao!=null){
			for (int i =0; i< dao.size(); i++){
				IUsuarioBean bean = (IUsuarioBean) new UsuarioBean();	
				bean.setCodigo(dao.get(i).getId());
				bean.setNombre(dao.get(i).getNombre());
				bean.setApellido(dao.get(i).getApellido());
				bean.setTelefono(dao.get(i).getTelefono());
				bean.setUsername(dao.get(i).getUsername());
				bean.setClave(dao.get(i).getClave());
				bean.setEnabled(dao.get(i).getEnabled());
				for (Role roleDao : dao.get(i).getRoles()) {
					bean.getRoles().add(new RoleBean(roleDao.getRole_id(),roleDao.getAuthority()));
				}
				
				usuarios.add(bean);			
			};
		}	
		return usuarios;
	}
	
	/**
	 * obtenerUsuario
	 * @param IUsuarioBean
	 * @return IUsuarioBean
	 * @throws SAExcepcion 
	 */
	@Transactional(readOnly = true)
	public IUsuarioBean obtenerUsuarioByUsername(String username) throws SAExcepcion {
		
		IUsuarioDao dao= asUserDao.findUserByUsername(username);
		IUsuarioBean usuario = (IUsuarioBean) new UsuarioBean();
		if (dao!=null){
			usuario.setCodigo(dao.getId());
			usuario.setNombre(dao.getNombre());
			usuario.setApellido(dao.getApellido());
			usuario.setTelefono(dao.getTelefono());
			usuario.setUsername(dao.getUsername());
			usuario.setEnabled(dao.getEnabled());
				for (Role roleDao : dao.getRoles()) {
					usuario.getRoles().add(new RoleBean(roleDao.getRole_id(),roleDao.getAuthority()));
				}
		}	
		return usuario;
	}

	@Transactional(readOnly = true)
	public boolean login(String username, String password) throws SAExcepcion{
		return asUserDao.login(username,  password);
	}

	@Override
	@Transactional(readOnly = true)
	public boolean validarUsername(String usuername) throws SAExcepcion {
		return asUserDao.existeUsuario(usuername);
	}

	@Override
	@Transactional
	public boolean modificarPassword(String username, String generateMD5Signature) throws SAExcepcion {
		return asUserDao.actualizarContrasena(username, generateMD5Signature);
	}
	
	/**
	 * devuelve todos los usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	@Transactional(readOnly = true)
	public List<IRoleBean> obtenerRoles() throws SAExcepcion {
		 List<IRoleBean> roles = new ArrayList<IRoleBean>();
		for (IRoleDao dao: asRoleDao.findAllRoles()){
			roles.add(new RoleBean(dao.getRole_id(),dao.getAuthority()));		
		};		
		return roles;
	}
 
}