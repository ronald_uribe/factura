package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.TransportadorDao;
import com.ruribe.servicio.interfaces.TransportadorService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.TransportadorBean;
import com.ruribe.util.bean.interfaces.ITransportadorBean;
import com.ruribe.util.entidades.jpa.Transportador;
import com.ruribe.util.entidades.jpa.interfaces.ITransportadorDao;

@Named
public class TransportadorServiceImpl implements TransportadorService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private TransportadorDao<ITransportadorDao> asTransportadorDao;
	
	/**
	 * alta de usuario 
	 * @param ITransportadorBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void crearTransportador(ITransportadorBean itransportadorBean){
		asTransportadorDao.crearTransportador(TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(itransportadorBean));
	}
	
	/**
	 * Convierte un ITransportadorBean a ITransportadorDao
	 * @param itransportadorBean
	 * @return ITransportadorDao
	 */
	public static ITransportadorDao itransportadorBeanTOitransportadorDAO(ITransportadorBean itransportadorBean){
		ITransportadorDao itransportador = new Transportador();
		itransportador.setCod_tipo_documento(itransportadorBean.getCod_tipo_documento());
		itransportador.setDireccion(itransportadorBean.getDireccion());
		itransportador.setNo_documento(itransportadorBean.getNo_documento());
		itransportador.setTelefono(itransportadorBean.getTelefono());
		itransportador.setCelular(itransportadorBean.getCelular());
		itransportador.setEmail(itransportadorBean.getEmail());
		itransportador.setDescripcion(itransportadorBean.getDescripcion());
		itransportador.setPlaca(itransportadorBean.getPlaca());
		itransportador.setNombre(itransportadorBean.getNombre());
		itransportador.setEnabled(itransportadorBean.isEnabled());
		return itransportador;
	}
	
	
	/**
	 * Convierte un ITransportadorDao a ITransportadorBean
	 * @param ITransportadorDao
	 * @return ITransportadorBean
	 */
	public static ITransportadorBean itransportadorDAOTOitransportadorBean(ITransportadorDao dao){
		ITransportadorBean bean = (ITransportadorBean) new TransportadorBean();	
		bean.setNombre(dao.getNombre());
		bean.setCod_tipo_documento(dao.getCod_tipo_documento());
		bean.setDireccion(dao.getDireccion());
		bean.setNo_documento(dao.getNo_documento());
		bean.setTelefono(dao.getTelefono());
		bean.setCelular(dao.getCelular());
		bean.setEmail(dao.getEmail());
		bean.setDescripcion(dao.getDescripcion());
		bean.setPlaca(dao.getPlaca());
		bean.setEnabled(dao.getEnabled());
		return bean;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarTransportador(ITransportadorBean itransportadorBean){
		asTransportadorDao.editarTransportador(TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(itransportadorBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarTransportador(ITransportadorBean itransportadorBean) {
		asTransportadorDao.eliminarTransportador(TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(itransportadorBean));
	}

	/**
	 * editar una lista de usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	public List<ITransportadorBean> editarTransportador(List<ITransportadorBean> Listaitransportador) throws SAExcepcion {
		asTransportadorDao.buscarTransportadores();
		return null;
	}

	/**
	 * devuelve todos los proveedores
	 * @throws SAExcepcion 
	 */
	@Override
	public List<ITransportadorBean> obtenerTransportadores() throws SAExcepcion {
		List<ITransportadorDao> transportadorDao= asTransportadorDao.buscarTransportadores();
		 List<ITransportadorBean> listaBean = new ArrayList<ITransportadorBean>();
		
		for (int i =0; i< transportadorDao.size(); i++){
			listaBean.add(itransportadorDAOTOitransportadorBean(transportadorDao.get(i)));		
		};		
		return listaBean;
	}

	/**
	 * devuelve una lista de transportadores con like
	 * @param identificador a buscar
	 * @return lsita de tranportadores
	 * @throws SAExcepcion 
	 */
	public List<ITransportadorBean> obtenerTransportador(ITransportadorBean itransportadorBean) throws SAExcepcion {
		
		List<ITransportadorDao> users= asTransportadorDao.buscarTransportadores(TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(itransportadorBean));
		List<ITransportadorBean> transportadores = new ArrayList<ITransportadorBean>();
		
		for (int i =0; i< users.size(); i++){
			ITransportadorBean iu = (ITransportadorBean) new TransportadorBean();	
//			iu.setDocumento(users.get(i).getDocumento());
//			iu.setNombre(users.get(i).getNombre());
//			iu.setApellido1(users.get(i).getApellido1());
//			iu.setApellido2(users.get(i).getApellido2());
//			iu.setTelefono(users.get(i).getTelefono());
			transportadores.add(iu);			
		};		
		return transportadores;
	}

	
 
}