package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.SalidaDao;
import com.ruribe.servicio.interfaces.SalidaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleSalidaBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.SalidaBean;
import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.ISalidaBean;
import com.ruribe.util.bean.interfaces.ISalidaConsultaBean;
import com.ruribe.util.entidades.jpa.DetalleSalida;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.Salida;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleSalidaDao;
import com.ruribe.util.entidades.jpa.interfaces.ISalidaDao;

@Named
public class SalidaServiceImpl implements SalidaService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private SalidaDao<ISalidaDao> asSalidaDao;
	
	/**
	 * alta de usuario 
	 * @param ISalidaBean
	 * @return 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public ISalidaBean crearSalida(ISalidaBean iSalidaBean){
		
		ISalidaDao dao = asSalidaDao.crearSalida(SalidaServiceImpl.iSalidaBeanTOiSalidaDAO(iSalidaBean));
		iSalidaBean.setIdSalida(dao.getIdSalida());
		return iSalidaBean;
	}
	
	/**
	 * Convierte un ISalidaBean a ISalidaDao
	 * @param iSalidaBean
	 * @return ISalidaDao
	 */
	public static ISalidaDao iSalidaBeanTOiSalidaDAO(ISalidaBean iSalidaBean){
		ISalidaDao iSalida = new Salida();
		iSalida.setProveedor((Proveedor) ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iSalidaBean.getIproveedorBean()));
		iSalida.setIdSalida(iSalidaBean.getIdSalida());
		iSalida.setFecha(iSalidaBean.getFecha());
		iSalida.setUsuario_creacion(iSalidaBean.getUsuario_creacion());
		iSalida.setEnabled(iSalidaBean.isEstado());
		List<DetalleSalida> detalleDao= new ArrayList<DetalleSalida>();
		//recorremos el detalle de remision
		//int productoId=0;
		Producto producto;
		for (int k =0; k < iSalidaBean.getDetalleSalidas().size();k++){
			IDetalleSalidaDao detalle= new DetalleSalida();
			detalle.setSalida((Salida)iSalida);
			producto= new Producto();
			producto.setCodigo_producto(iSalidaBean.getDetalleSalidas().get(k).getIproductoBean().getCodigo_producto());
			detalle.setProducto(producto);
			detalle.setCantidad(iSalidaBean.getDetalleSalidas().get(k).getCantidad());
			detalleDao.add((DetalleSalida) detalle);
		}
		iSalida.setDetalleSalidas(detalleDao);
		return iSalida;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarSalida(ISalidaBean iSalidaBean){
		asSalidaDao.editarSalida(SalidaServiceImpl.iSalidaBeanTOiSalidaDAO(iSalidaBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarSalida(ISalidaBean iSalidaBean) {
		asSalidaDao.eliminarSalida(SalidaServiceImpl.iSalidaBeanTOiSalidaDAO(iSalidaBean));
	}

	private ISalidaBean ISalidaDAOTOISalidaBean(ISalidaDao dao) {
		
		ISalidaBean bean = (ISalidaBean) new SalidaBean();	
		if (dao!=null){
			bean.setIdSalida(dao.getIdSalida());
			//hay que inicializar el bean de proveedor.
			bean.setIproveedorBean(ProveedorServiceImpl.iProveedorDaoTOIProveedorBean(dao.getProveedor()));
			bean.setIdSalida(dao.getIdSalida());
			bean.setFecha(dao.getFecha());
			bean.setEstado(dao.isEnabled());

			for (int k =0; k< dao.getDetalleSalidas().size();k++){
				IDetalleSalidaBean detalle =  new DetalleSalidaBean();	
				detalle.setCantidad(dao.getDetalleSalidas().get(k).getCantidad());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetalleSalidas().get(k).getProducto()));
				bean.getDetalleSalidas().add(detalle);
			}
		}	
		return bean;
	}

	/**
	 * obtiene una remision por su identificador.
	 */
	@Override
	public ISalidaBean consultarIDSalida(ISalidaBean iSalida) {
		ISalidaBean remision= ISalidaDAOTOISalidaBean(asSalidaDao.buscarSalida(String.valueOf(iSalida.getIdSalida())));
		return remision;
	}

	@Override
	public List<IDetalleSalidaBean> obtenerProductosPendientesPorProveedor(String idproveedorSelect) throws SAExcepcion {
		List<Object> dao= asSalidaDao.obtenerProductosPendientesPorProveedor(idproveedorSelect);
		List<IDetalleSalidaBean> listaBean = new ArrayList<IDetalleSalidaBean>();
		IDetalleSalidaBean detalle;
		 //recorremos los datos obtenidos de la consulta y rellenamos List<IDetalleSalidaBean> listaBean
		 
		for(int i=0;i<dao.size();i++){
		 	Object[] actual = (Object[]) dao.get(i);
		 	Utilidades.printResult(actual);
		 
		 	detalle =  new DetalleSalidaBean();	
			detalle.setCantidad(0);//inicialidando el formulario en cero
			IProductoBean iProductoBean =  new ProductoBean();	
			iProductoBean.setCodigo_producto((Integer) ((Object[])actual)[1]);
			iProductoBean.setDescripcion((String)((Object[])actual)[2]);
			detalle.setPendiente(((BigDecimal) ((Object[])actual)[3]).intValue());
			detalle.setIproductoBean(iProductoBean);
			listaBean.add(detalle);
			detalle=null; //liberamos el objeto
		 } 
		return listaBean;
	}
	
	/**
	 * permiste consultar salidas por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las salidas segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<ISalidaBean> consultarSalidas(ISalidaConsultaBean consultaBean) throws SAExcepcion {
		List<ISalidaDao> dao = new ArrayList<ISalidaDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdSalida()!=null && !consultaBean.getIdSalida().trim().equals("")){
				//consulta de remision por identidicador
				System.out.println("[INFO] consulta de remision por identidicador");
				ISalidaDao salida= asSalidaDao.buscarSalida(consultaBean.getIdSalida());
				if (salida!=null){
					dao.add(salida);
				} 
			}else if (consultaBean.getIdProveedor()!=null && !consultaBean.getIdProveedor().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de salida por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asSalidaDao.buscarSalidasPorProveedorYRangogoFecha(consultaBean.getIdProveedor(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdProveedor()!=null && !consultaBean.getIdProveedor().trim().equals("Seleccionar")){
				//consulta de salida por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asSalidaDao.buscarSalidasPorProveedor(consultaBean.getIdProveedor()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de salida por rango de fechas
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asSalidaDao.buscarSalidasPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asSalidaDao.buscarSalidas();
			}
		}
		
		List<ISalidaBean> listaBean = new ArrayList<ISalidaBean>();
		
		//recorremos la lista 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(ISalidaDAOTOISalidaBean(dao.get(i)));		
		};		
		return listaBean;
	}

}