package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.ProveedorDao;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.ProveedorBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.interfaces.IProveedorDao;

@Named
public class ProveedorServiceImpl implements ProveedorService, Serializable{
	
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private ProveedorDao<IProveedorDao> asProveedorDao;
	
	/**
	 * alta de usuario 
	 */
	@Transactional
	public void crearProveedor(IProveedorBean iproveedorBean){
		asProveedorDao.crearProveedor(ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iproveedorBean));
	}
	
	/**
	 * Convierte IProveedorBean a IProveedorDao.
	 * @param iproveedorBean
	 * @return IProveedorDao
	 */
	public static IProveedorDao iProveedorBeanTOiProveedorDao(IProveedorBean iproveedorBean){
		IProveedorDao iproveedor = new Proveedor();
		iproveedor.setCod_ciudad(iproveedorBean.getCod_ciudad());
		iproveedor.setCodigo_proveedor(iproveedorBean.getCodigo_proveedor());
		iproveedor.setCod_tipo_documento(iproveedorBean.getCod_tipo_documento());
		iproveedor.setDireccion(iproveedorBean.getDireccion().toUpperCase());
		iproveedor.setNo_documento(iproveedorBean.getNo_documento().toUpperCase());
		iproveedor.setRazon_social(iproveedorBean.getRazon_social().toUpperCase());
		iproveedor.setTelefono(iproveedorBean.getTelefono());
		iproveedor.setCelular(iproveedorBean.getCelular());
		iproveedor.setEmail(iproveedorBean.getEmail());
		iproveedor.setEnabled(iproveedorBean.isEnabled());
		return iproveedor;
	}
	
	/**
	 * Edita un usuario pasado por parametros.
	 * @param IProveedorBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarProveedor(IProveedorBean iproveedorBean){
		asProveedorDao.editarProveedor(ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iproveedorBean));
	}
	
	/**
	 * borrar un usuario apartir de identificador
	 * @param IProveedorBean 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarProveedor(IProveedorBean iproveedorBean) {
		asProveedorDao.eliminarProveedor(ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iproveedorBean));
	}

	/**
	 * editar una lista de usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IProveedorBean> editarProveedor(List<IProveedorBean> Listaiproveedor) throws SAExcepcion {
		asProveedorDao.buscarProveedores();
		return null;
	}

	/**
	 * devuelve una lista de todos los proveedores.
	 * @return List<IProveedorBean>
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IProveedorBean> obtenerProveedores() throws SAExcepcion {
		List<IProveedorDao> proveedorDao= asProveedorDao.buscarProveedores();
		 List<IProveedorBean> bean = new ArrayList<IProveedorBean>();
		
		for (int i =0; i< proveedorDao.size(); i++){
			bean.add(ProveedorServiceImpl.iProveedorDaoTOIProveedorBean(proveedorDao.get(i)));		
		};		
		return bean;
	}
	
	/**
	 * convierte iProveedorDao a iProveedorBean
	 * @param dao
	 * @return
	 */
	public static IProveedorBean iProveedorDaoTOIProveedorBean(IProveedorDao dao){
		IProveedorBean bean = (IProveedorBean) new ProveedorBean();	
		bean.setCodigo_proveedor(dao.getCodigo_proveedor());
		bean.setCod_ciudad(dao.getCod_ciudad());
		bean.setCod_tipo_documento(dao.getCod_tipo_documento());
		bean.setDireccion(dao.getDireccion());
		bean.setNo_documento(dao.getNo_documento());
		bean.setTelefono(dao.getTelefono());
		bean.setCelular(dao.getCelular());
		bean.setEmail(dao.getEmail());
		bean.setRazon_social(dao.getRazon_social());
		bean.setEnabled(dao.isEnabled());
		return bean;
	}

	@Override
	public List<IProveedorBean> obtenerProveedor(IProveedorBean iproveedorBean) throws SAExcepcion {
		
		List<IProveedorDao> users= asProveedorDao.buscarProveedores(ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iproveedorBean));
		List<IProveedorBean> usuarios = new ArrayList<IProveedorBean>();
		
		for (int i =0; i< users.size(); i++){
			IProveedorBean iu = (IProveedorBean) new ProveedorBean();	
//			iu.setDocumento(users.get(i).getDocumento());
//			iu.setNombre(users.get(i).getNombre());
//			iu.setApellido1(users.get(i).getApellido1());
//			iu.setApellido2(users.get(i).getApellido2());
//			iu.setTelefono(users.get(i).getTelefono());
			usuarios.add(iu);			
		};		
		return usuarios;
	}
 
}