package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruribe.dao.TipoDocumentoDao;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.TipoDocumentoBean;
import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;
import com.ruribe.util.entidades.jpa.TipoDocumento;
import com.ruribe.util.entidades.jpa.interfaces.ITipoDocumentoDao;

@Named
public class TipoDocumentoServiceImpl implements TipoDocumentoService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	
	@Autowired
	private TipoDocumentoDao<ITipoDocumentoDao> asTipoDocumentoDao;

	
	/**
	 * devuelve todos los tipos de documento
	 * @throws SAExcepcion 
	 */
	@Override
	public List<ITipoDocumentoBean> obtenerTipoDocumentos() throws SAExcepcion {
		List<ITipoDocumentoDao> users= asTipoDocumentoDao.buscarTipoDocumento();
		 List<ITipoDocumentoBean> usuarios = new ArrayList<ITipoDocumentoBean>();
		
		for (int i =0; i< users.size(); i++){
			ITipoDocumentoBean iu = (ITipoDocumentoBean) new TipoDocumentoBean();	
			iu.setDescripcion(users.get(i).getDescripcion());
			iu.setIdTipoDocumento(users.get(i).getId_tipo_documento());
			usuarios.add(iu);		
		};		
		return usuarios;
	}

	/**
	 * devuelve un tipos de documento
	 */
	@Override
	public List<ITipoDocumentoBean> obtenerTipoDocumento(ITipoDocumentoBean iTipoDocumentoBean) {
		ITipoDocumentoDao iTipoDocumentoDao = new TipoDocumento();
		iTipoDocumentoDao.setDescripcion(iTipoDocumentoBean.getDescripcion());
		iTipoDocumentoDao.setId_tipo_documento(iTipoDocumentoBean.getIdTipoDocumento());
		List<ITipoDocumentoDao> docs= asTipoDocumentoDao.buscarTipoDocumento(iTipoDocumentoDao);
		 List<ITipoDocumentoBean> tipoDocumentos = new ArrayList<ITipoDocumentoBean>();
		
		for (int i =0; i< docs.size(); i++){
			ITipoDocumentoBean iu = (ITipoDocumentoBean) new TipoDocumentoBean();	
			iu.setDescripcion(docs.get(i).getDescripcion());
			iu.setIdTipoDocumento(docs.get(i).getId_tipo_documento());
			tipoDocumentos.add(iu);			
		};		
		return tipoDocumentos;
	}

 
}