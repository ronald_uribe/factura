package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.EntradaDao;
import com.ruribe.servicio.interfaces.EntradaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.DetalleEntradaBean;
import com.ruribe.util.bean.EntradaBean;
import com.ruribe.util.bean.interfaces.IDetalleEntradaBean;
import com.ruribe.util.bean.interfaces.IEntradaBean;
import com.ruribe.util.bean.interfaces.IEntradaConsultaBean;
import com.ruribe.util.entidades.jpa.DetalleEntrada;
import com.ruribe.util.entidades.jpa.Entrada;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleEntradaDao;
import com.ruribe.util.entidades.jpa.interfaces.IEntradaDao;

@Named
public class EntradaServiceImpl implements EntradaService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private EntradaDao<IEntradaDao> asEntradaDao;
	private final Logger log = Logger.getLogger(EntradaServiceImpl.class.getName());
	
	/**
	 * alta de usuario 
	 * @param IEntradaBean
	 * @return 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IEntradaBean crearEntrada(IEntradaBean iEntradaBean){
		
		IEntradaDao dao = asEntradaDao.crearEntrada(EntradaServiceImpl.iEntradaBeanTOiEntradaDAO(iEntradaBean));
		log.info("INSERTANDO ACTUALIZADO REMISION:" + dao.getIdEntrada());
		iEntradaBean.setIdEntrada(dao.getIdEntrada());
		return iEntradaBean;
	}
	
	/**
	 * Convierte un IEntradaBean a IEntradaDao
	 * @param iEntradaBean
	 * @return IEntradaDao
	 */
	public static IEntradaDao iEntradaBeanTOiEntradaDAO(IEntradaBean iEntradaBean){
		IEntradaDao iEntrada = new Entrada();
		iEntrada.setProveedor((Proveedor) ProveedorServiceImpl.iProveedorBeanTOiProveedorDao(iEntradaBean.getIproveedorBean()));
		iEntrada.setIdEntrada(iEntradaBean.getIdEntrada());
		iEntrada.setFecha(iEntradaBean.getFecha());
		iEntrada.setUsuario_creacion(iEntradaBean.getUsuario_creacion());
		iEntrada.setEnabled(iEntradaBean.isEstado());
		List<DetalleEntrada> detalleDao= new ArrayList<DetalleEntrada>();
		//recorremos el detalle de remision
		//int productoId=0;
		for (int k =0; k < iEntradaBean.getDetalleEntradas().size();k++){
			IDetalleEntradaDao detalle= new DetalleEntrada();
			detalle.setEntrada((Entrada)iEntrada);
			detalle.setProducto((Producto)(ProductoServiceImpl.iProductoBeanToiProductoDao(iEntradaBean.getDetalleEntradas().get(k).getIproductoBean())));
			detalle.setCantidad(iEntradaBean.getDetalleEntradas().get(k).getCantidad());
			detalleDao.add((DetalleEntrada) detalle);
		}
		iEntrada.setDetalleEntradas(detalleDao);
		return iEntrada;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarEntrada(IEntradaBean iEntradaBean){
		asEntradaDao.editarEntrada(EntradaServiceImpl.iEntradaBeanTOiEntradaDAO(iEntradaBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarEntrada(IEntradaBean iEntradaBean) {
		asEntradaDao.eliminarEntrada(EntradaServiceImpl.iEntradaBeanTOiEntradaDAO(iEntradaBean));
	}

	private IEntradaBean IEntradaDAOTOIEntradaBean(IEntradaDao dao) {
		
		IEntradaBean bean = (IEntradaBean) new EntradaBean();	
		if (dao!=null){
			bean.setIdEntrada(dao.getIdEntrada());
			//hay que inicializar el bean de proveedor.
			bean.setIproveedorBean(ProveedorServiceImpl.iProveedorDaoTOIProveedorBean(dao.getProveedor()));
			bean.setIdEntrada(dao.getIdEntrada());
			bean.setFecha(dao.getFecha());
			bean.setEstado(dao.isEnabled());

			for (int k =0; k< dao.getDetalleEntradas().size();k++){
				IDetalleEntradaBean detalle =  new DetalleEntradaBean();	
				detalle.setCantidad(dao.getDetalleEntradas().get(k).getCantidad());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetalleEntradas().get(k).getProducto()));
				bean.getDetalleEntradas().add(detalle);
			}
		}	
		return bean;
	}

	/**
	 * obtiene una remision por su identificador.
	 * @throws SAExcepcion 
	 */
	@Override
	public IEntradaBean consultarIDEntrada(IEntradaBean iEntrada){
		IEntradaBean remision= IEntradaDAOTOIEntradaBean(asEntradaDao.buscarEntrada(iEntrada.getIdEntrada()));
		return remision;
	}
	
	/**
	 * permiste consultar Entradas por rango de fechas, idEntrada, cliente.
	 * @return devuelve todas las Entradas segun parametros de consulta
	 * IEntradaConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<IEntradaBean> consultarEntradas(IEntradaConsultaBean consultaBean) throws SAExcepcion {
		List<IEntradaDao> dao = new ArrayList<IEntradaDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdEntrada()!=null && !consultaBean.getIdEntrada().trim().equals("")){
				//consulta de Entrada por identidicador
				log.info("Consulta de entrada por identidicador");
				IEntradaDao entrada= asEntradaDao.buscarEntrada(Integer.parseInt(consultaBean.getIdEntrada()));
				if (entrada!=null){
					dao.add(entrada);
				} 
			}else if (consultaBean.getIdProveedor()!=null && !consultaBean.getIdProveedor().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de Entrada por cliente
				log.info("consulta de Entrada por identidicador");
				dao.addAll(asEntradaDao.buscarEntradasPorProveedorYRangogoFecha(consultaBean.getIdProveedor(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdProveedor()!=null && !consultaBean.getIdProveedor().trim().equals("Seleccionar")){
				//consulta de Entrada por cliente
				log.info("consulta de Entrada por proveedor");
				dao.addAll(asEntradaDao.buscarEntradasPorProveedor(consultaBean.getIdProveedor()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de Entrada por rango de fechas
				log.info("consulta de Entrada por rango de fechas");
				dao.addAll(asEntradaDao.buscarEntradasPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asEntradaDao.buscarEntradas();
			}
		}
		
		List<IEntradaBean> listaBean = new ArrayList<IEntradaBean>();
		
		//recorremos la lista 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IEntradaDAOTOIEntradaBean(dao.get(i)));		
		};		
		return listaBean;
	}

}