package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.ClienteDao;
import com.ruribe.dao.ObraDao;
import com.ruribe.servicio.interfaces.ClienteService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.interfaces.IClienteDao;
import com.ruribe.util.entidades.jpa.interfaces.IObraDao;

@Named
public class ClienteServiceImpl implements ClienteService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private ClienteDao<IClienteDao> asClienteDao;
	@Autowired
	private ObraDao<IObraDao> asObraDao;
	
	
	/**
	 * alta de usuario 
	 */
	@Transactional
	public void crearCliente(IClienteBean iclienteBean){
		asClienteDao.crearCliente(IClienteBeanTOIClienteDAO(iclienteBean));
	}
	
	/**
	 * IClienteBeanTOIClienteDAO
	 */
	public static IClienteDao IClienteBeanTOIClienteDAO(IClienteBean iclienteBean){
			IClienteDao icliente = new Cliente();
			icliente.setCod_tipo_documento(iclienteBean.getCod_tipo_documento());
			icliente.setCod_ciudad(iclienteBean.getCod_ciudad());
			icliente.setDireccion(iclienteBean.getDireccion().toUpperCase());
			icliente.setId_cliente(iclienteBean.getId_cliente().toUpperCase());
			icliente.setTelefono(iclienteBean.getTelefono());
			icliente.setCelular(iclienteBean.getCelular());
			icliente.setEmail(iclienteBean.getEmail());
			icliente.setRazon_social(iclienteBean.getRazon_social());
			icliente.setEnabled(iclienteBean.getEnabled());
			
			//asociamos las obras del cliente.
			for (int i=0;i<iclienteBean.getListaObras().size();i++){
				icliente.getListaObras().add((Obra) IObraBeanTOIObraDao(iclienteBean.getListaObras().get(i)));	
			}
			return icliente;
	} 
	
	/**
	 * IClienteBeanTOIClienteDAO
	 */
	public static IClienteBean IClienteDAOTOIClienteBean(IClienteDao clienteDao){
		IClienteBean iu = (IClienteBean) new ClienteBean();	
		if (clienteDao!=null){
			iu.setRazon_social(clienteDao.getRazon_social());
			iu.setCod_tipo_documento(clienteDao.getCod_tipo_documento());
			iu.setCod_ciudad(clienteDao.getCod_ciudad());
			iu.setDireccion(clienteDao.getDireccion());
			iu.setId_cliente(clienteDao.getId_cliente());
			iu.setTelefono(clienteDao.getTelefono());
			iu.setCelular(clienteDao.getCelular());
			iu.setEmail(clienteDao.getEmail());
			iu.setEnabled(clienteDao.getEnabled());
			for (int k =0; k< clienteDao.getListaObras().size();k++){
				IObraBean io = (IObraBean) new ObraBean();	
				io.setIdObra(clienteDao.getListaObras().get(k).getIdObra());
				io.setDireccion(clienteDao.getListaObras().get(k).getDireccion());
				io.setNombre(clienteDao.getListaObras().get(k).getNombre());
				io.setTelefono(clienteDao.getListaObras().get(k).getTelefono());
				io.setDireccion(clienteDao.getListaObras().get(k).getDireccion());
				io.setContacto(clienteDao.getListaObras().get(k).getContacto());
				io.setEnabled(clienteDao.getListaObras().get(k).getEnabled());
				io.setIclientebean(iu);
				iu.setListaObras(io);
			}
		}	
		return iu;
	}
	
	
	/**
	 * editar usuario
	 */
	@Transactional
	public void editarCliente(IClienteBean iclienteBean){
		asClienteDao.editarCliente(IClienteBeanTOIClienteDAO(iclienteBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional
	public void borrarCliente(IClienteBean iclienteBean) {
		asClienteDao.eliminarCliente(IClienteBeanTOIClienteDAO(iclienteBean));
	}

	/**
	 * editar una lista de usuarios
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IClienteBean> editarCliente(List<IClienteBean> Listaicliente) throws SAExcepcion {
		asClienteDao.buscarClientes();
		return null;
	}

	/**
	 * @return devuelve todos los proveedores
	 * @throws SAExcepcion 
	 */
	@Transactional(readOnly = true)
	public List<IClienteBean> obtenerClientes() throws SAExcepcion {
		List<IClienteDao> clienteDao= asClienteDao.buscarClientes();
		List<IClienteBean> listaBean = new ArrayList<IClienteBean>();
		
		//recorremos la lista de clientes 
		for (int i =0; i< clienteDao.size(); i++){
			listaBean.add(IClienteDAOTOIClienteBean(clienteDao.get(i)));		
		};		
		return listaBean;
	}

	@Override
	public List<IClienteBean> obtenerCliente(IClienteBean iclienteBean) {
		return null;
	}

	/**
	 * asigna una obra a un cliente seleccionado
	 */
	@Transactional
	public void vincularObraCliente(IObraBean bean) {
		asObraDao.vincularObra(IObraBeanTOIObraDao(bean));
	}
	
	/**
	 * Elimina una obra de un cliente seleccionado
	 * @param IObraBean objeto a eliminar
	 */
	@Transactional
	public void desvincularObra(IObraBean bean) {
		asObraDao.desvincularObra(IObraBeanTOIObraDao(bean));
	}
	
	/**
	 * Modifica la obra de un cliente.
	 * @param IObraBean objeto obra
	 */
	@Transactional
	public void modificaObraCliente(IObraBean bean) {
		asObraDao.editarObra(IObraBeanTOIObraDao(bean));
	}
	
	/**
	 * convierte un IObraBean a IObraDao
	 * @param iObraBean
	 * @return IObraDao
	 */
	public static IObraDao IObraBeanTOIObraDao(IObraBean iObraBean){
		IObraDao iobra = new Obra();
		IClienteDao icliente = new Cliente();
		icliente.setCod_tipo_documento(iObraBean.getIclientebean().getCod_tipo_documento());
		icliente.setCod_ciudad(iObraBean.getIclientebean().getCod_ciudad());
		icliente.setDireccion(iObraBean.getIclientebean().getDireccion());
		icliente.setId_cliente(iObraBean.getIclientebean().getId_cliente());
		icliente.setTelefono(iObraBean.getIclientebean().getTelefono());
		icliente.setCelular(iObraBean.getIclientebean().getCelular());
		icliente.setEmail(iObraBean.getIclientebean().getEmail());
		icliente.setRazon_social(iObraBean.getIclientebean().getRazon_social());
		icliente.setEnabled(iObraBean.getIclientebean().getEnabled());
		
		iobra.setCliente((Cliente) icliente);
		iobra.setIdObra(iObraBean.getIdObra());
		iobra.setNombre(iObraBean.getNombre().toUpperCase());
		iobra.setDireccion(iObraBean.getDireccion().toUpperCase());
		iobra.setTelefono(iObraBean.getTelefono());
		iobra.setContacto(iObraBean.getContacto());
		iobra.setEnabled(iObraBean.getEnabled());
		
		return iobra;
	}
	
	/**
	 * Obtiene las obras de un cliente 
	 * @param String id_cliente identificador del cliente
	 * @return devuelve una lista List<IObraBean>
	 * @throws SAExcepcion 
	 */
	@Transactional(readOnly = true)
	public List<IObraBean> obtenerObrasCliente(String id_cliente) throws SAExcepcion {
		List<IObraDao> obras=asObraDao.obtenerObrasCliente(id_cliente);
		List<IObraBean> listaobras = new ArrayList<IObraBean>();
		for (int k =0; k< obras.size();k++){
			IObraBean io = (IObraBean) new ObraBean();	
			io.setIdObra(obras.get(k).getIdObra());
			io.setDireccion(obras.get(k).getDireccion());
			io.setNombre(obras.get(k).getNombre());
			io.setTelefono(obras.get(k).getTelefono());
			io.setDireccion(obras.get(k).getDireccion());
			io.setContacto(obras.get(k).getContacto());
			io.setEnabled(obras.get(k).getEnabled());
			listaobras.add(io);
			io=null;
		}
		return listaobras;
	}

	/**
	 * Consulta un cliente por identidicados
	 * @param String id_cliente
	 * @return IClienteBean;
	 */
	@Transactional(readOnly = true)
	public IClienteBean obtenerClientePK(String id_cliente) {
		return IClienteDAOTOIClienteBean(asClienteDao.buscarCliente(id_cliente));
	}
	
	/**
	 * Consulta un cliente por identidicados
	 * @param String id_cliente
	 * @return IClienteBean;
	 * @throws SAExcepcion 
	 */
	@Transactional(readOnly = true)
	public IObraBean obtenerObraPK(int id_obra) throws SAExcepcion {
		return IObraDAOTOIObraBean(asObraDao.buscarObra(id_obra));
	}

	public static IObraBean IObraDAOTOIObraBean(IObraDao obraDao) {
		IObraBean io = (IObraBean) new ObraBean();	
		io.setIdObra(obraDao.getIdObra());
		io.setDireccion(obraDao.getDireccion());
		io.setNombre(obraDao.getNombre());
		io.setTelefono(obraDao.getTelefono());
		io.setDireccion(obraDao.getDireccion());
		io.setContacto(obraDao.getContacto());
		io.setEnabled(obraDao.getEnabled());
		io.setIclientebean(IClienteDAOTOIClienteBean(obraDao.getCliente()));
		return io;
	}

	@Override
	public List<IClienteBean> obtenerClientesByRazonSocial(String razonSocial) throws SAExcepcion {
		List<IClienteDao> clienteDao= asClienteDao.obtenerClientesByRazonSocial(razonSocial);
		List<IClienteBean> listaBean = new ArrayList<IClienteBean>();
		
		//recorremos la lista de clientes 
		for (int i =0; i< clienteDao.size(); i++){
			listaBean.add(IClienteDAOTOIClienteBean(clienteDao.get(i)));		
		};		
		return listaBean;
	}
 
}