package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.ReferenciaDao;
import com.ruribe.servicio.interfaces.ReferenciaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.TipoProductoBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;
import com.ruribe.util.entidades.jpa.Referencia;
import com.ruribe.util.entidades.jpa.TipoProducto;
import com.ruribe.util.entidades.jpa.interfaces.IReferenciaDao;
import com.ruribe.util.entidades.jpa.interfaces.ITipoProductoDao;

@Named
public class ReferenciaServiceImpl implements ReferenciaService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private ReferenciaDao<IReferenciaDao> asReferenciaDao;
	
	/**
	 * alta de Referencia 
	 * @param IReferenciaBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void crearReferencia(IReferenciaBean iReferenciaBean){
		asReferenciaDao.crearReferencia(ReferenciaServiceImpl.iReferenciaBeanToiReferenciaDao(iReferenciaBean));
	}
	
	
	/**
	 * editar Referencia
	 * @param IReferenciaBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarReferencia(IReferenciaBean iReferenciaBean){
		asReferenciaDao.editarReferencia(ReferenciaServiceImpl.iReferenciaBeanToiReferenciaDao(iReferenciaBean));
	}
	
	/**
	 * borrar un Referencia apartir de identificador
	 * @param IReferenciaBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarReferencia(IReferenciaBean iReferenciaBean) {
		asReferenciaDao.eliminarReferencia(ReferenciaServiceImpl.iReferenciaBeanToiReferenciaDao(iReferenciaBean));
	}

	/**
	 * Edita una lista de Referencias
	 * @paramList<IReferenciaBean>
	 * @return List<IReferenciaBean>
	 * @throws SAExcepcion 
	 */
	public List<IReferenciaBean> editarReferencia(List<IReferenciaBean> ListaiReferencia) throws SAExcepcion {
		asReferenciaDao.buscarReferencias();
		return null;
	}

	/**
	 * devuelve todos los Referencias
	 * @return List<IReferenciaBean>
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IReferenciaBean> obtenerReferencias() throws SAExcepcion {
		List<IReferenciaDao> listaReferenciaDao= asReferenciaDao.buscarReferencias();
		 List<IReferenciaBean> listaReferenciasBean = new ArrayList<IReferenciaBean>();
		
		for (int i =0; i< listaReferenciaDao.size(); i++){
			listaReferenciasBean.add(ReferenciaServiceImpl.iReferenciaDaoTOiReferenciaBean(listaReferenciaDao.get(i)));		
		};		
		return listaReferenciasBean;
	}
	
	@Override
	public List<IReferenciaBean> obtenerReferenciasByDescripcion(String filtro) throws SAExcepcion {
		List<IReferenciaDao> listaReferenciaDao= asReferenciaDao.obtenerReferenciasByDescripcion(filtro);
		 List<IReferenciaBean> listaReferenciasBean = new ArrayList<IReferenciaBean>();
		
		for (int i =0; i< listaReferenciaDao.size(); i++){
			listaReferenciasBean.add(ReferenciaServiceImpl.iReferenciaDaoTOiReferenciaBean(listaReferenciaDao.get(i)));		
		};		
		return listaReferenciasBean;
	}
	
	
	/**
	 * convierte un IReferenciaDao a IReferenciaBean
	 * @param dao
	 * @return IReferenciaBean;
	 */
	public static IReferenciaBean iReferenciaDaoTOiReferenciaBean(IReferenciaDao dao){
		IReferenciaBean bean = (IReferenciaBean) new ReferenciaBean();	
		bean.setCodigo_interno(dao.getCodigo_interno());
		bean.setDescripcion(dao.getDescripcion());
		bean.setPrecio_alquiler(dao.getPrecio_alquiler());
		bean.setPrecio_reposicion(dao.getPrecio_reposicion());
		bean.setPeso(dao.getPeso());
		ITipoProductoBean iTipo = new TipoProductoBean();
		iTipo.setCod_tipo_producto(dao.getTipo_producto().getCod_tipo_producto());
		iTipo.setDescripcion(dao.getTipo_producto().getDescripcion());
		bean.setTipo_producto(iTipo);
		return bean;
	}
	
	/**
	 * Transforma un IReferenciaBean a IReferenciaDao
	 * @param iReferenciaBean
	 * @return IReferenciaDao
	 */
	public static IReferenciaDao iReferenciaBeanToiReferenciaDao(IReferenciaBean iReferenciaBean){
		IReferenciaDao dao = new Referencia();
		dao.setCodigo_interno(iReferenciaBean.getCodigo_interno());
		dao.setDescripcion(iReferenciaBean.getDescripcion());
		dao.setPeso(iReferenciaBean.getPeso());
		dao.setPrecio_alquiler(iReferenciaBean.getPrecio_alquiler());
		dao.setPrecio_reposicion(iReferenciaBean.getPrecio_reposicion());
		ITipoProductoDao tipo = new TipoProducto();
		tipo.setCod_tipo_producto(iReferenciaBean.getTipo_producto().getCod_tipo_producto());
		dao.setTipo_producto((TipoProducto) tipo);
		return dao;
	}
		

	@Override
	public List<IReferenciaBean> obtenerReferencia(IReferenciaBean iReferencia) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IReferenciaBean obtenerReferenciaPK(String idReferenciaSelect) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
	}


	


}