package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.DevolucionDao;
import com.ruribe.servicio.interfaces.DevolucionService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.DetalleDevolucionBean;
import com.ruribe.util.bean.DevolucionBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionConsultaBean;
import com.ruribe.util.bean.interfaces.IObraBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleDevolucion;
import com.ruribe.util.entidades.jpa.Devolucion;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Remision;
import com.ruribe.util.entidades.jpa.Transportador;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleDevolucionDao;
import com.ruribe.util.entidades.jpa.interfaces.IDevolucionDao;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;
import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;

@Named
public class DevolucionServiceImpl implements DevolucionService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private DevolucionDao<IDevolucionDao> asDevolucionDao;
	
	/**
	 * alta de usuario 
	 * @param IDevolucionBean
	 * @return 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IDevolucionBean crearDevolucion(IDevolucionBean iDevolucionBean){
		IDevolucionDao dao = asDevolucionDao.crearDevolucion(DevolucionServiceImpl.iDevolucionBeanTOiDevolucionDAO(iDevolucionBean));
		iDevolucionBean.setIdDevolucion(dao.getIdDevolucion());
		return iDevolucionBean;
	}
	
	/**
	 * Convierte un IDevolucionBean a IDevolucionDao
	 * @param iDevolucionBean
	 * @return IDevolucionDao
	 */
	public static IDevolucionDao iDevolucionBeanTOiDevolucionDAO(IDevolucionBean iDevolucionBean){
		IDevolucionDao iDevolucion = new Devolucion();
		iDevolucion.setCliente((Cliente) ClienteServiceImpl.IClienteBeanTOIClienteDAO(iDevolucionBean.getIclienteBean()));
		iDevolucion.setIdDevolucion(iDevolucionBean.getIdDevolucion());
		iDevolucion.setTransportador((Transportador)TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(iDevolucionBean.getItransportadorBean()));
		iDevolucion.setObra((Obra)ClienteServiceImpl.IObraBeanTOIObraDao(iDevolucionBean.getIobraBean()));
		iDevolucion.setFecha(iDevolucionBean.getFecha());
		iDevolucion.setFecha_entrega(iDevolucionBean.getFecha_entrega());
		iDevolucion.setTransporte(iDevolucionBean.getTransporte());
		iDevolucion.setObservaciones(iDevolucionBean.getObservaciones());
		iDevolucion.setUsuario_creacion(iDevolucionBean.getUsuario_creacion());
		iDevolucion.setEnabled(iDevolucionBean.isEstado());
		iDevolucion.setPrinted(iDevolucionBean.isPrinted());
		List<DetalleDevolucion> detalleDao= new ArrayList<DetalleDevolucion>();
		//recorremos el detalle de remision
		
//		for (IDetalleDevolucionBean d :iDevolucionBean.getDetalleDevoluciones()){
//			System.out.println("SERVICE getIdRemision:" + d.getIdRemision());
//			System.out.println("SERVICE getCodigo_producto:" + d.getIproductoBean().getCodigo_producto());
//			System.out.println("SERVICE getDescripcion:" + d.getIproductoBean().getDescripcion());
//			System.out.println("SERVICE getCantidad:" + d.getCantidad());	
//			System.out.println("SERVICE getPendiente:" + d.getPendiente());	
//			System.out.println("--------------------");
//		}
		
		for (int k =0; k < iDevolucionBean.getDetalleDevoluciones().size();k++){
			IDetalleDevolucionDao detalle= new DetalleDevolucion();
			detalle.setDevolucion((Devolucion)iDevolucion);
			IProductoDao producto = new Producto();
			producto.setCodigo_producto(iDevolucionBean.getDetalleDevoluciones().get(k).getIproductoBean().getCodigo_producto());
			detalle.setProducto((Producto)producto);
			detalle.setCantidad(iDevolucionBean.getDetalleDevoluciones().get(k).getCantidad());
			IRemisionDao remision= new Remision();
			remision.setIdRemision(iDevolucionBean.getDetalleDevoluciones().get(k).getIdRemision());
//			detalle.setRemision((Remision)remision);
			//solo guardamos si hay productos
			if (detalle.getCantidad()>0){
				detalleDao.add((DetalleDevolucion) detalle);
			}
			detalle=null;
		}
		iDevolucion.setDetalleDevolucions(detalleDao);
		return iDevolucion;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarDevolucion(IDevolucionBean iDevolucionBean){
		asDevolucionDao.editarDevolucion(DevolucionServiceImpl.iDevolucionBeanTOiDevolucionDAO(iDevolucionBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarDevolucion(IDevolucionBean iDevolucionBean) {
		asDevolucionDao.eliminarDevolucion(DevolucionServiceImpl.iDevolucionBeanTOiDevolucionDAO(iDevolucionBean));
	}
	
	/**
	 * @return devuelve todos los proveedores
	 * @throws SAExcepcion 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<IDevolucionBean> consultarDevoluciones() throws SAExcepcion {
		List<IDevolucionDao> dao= asDevolucionDao.buscarDevoluciones();
		List<IDevolucionBean> listaBean = new ArrayList<IDevolucionBean>();
		
		//recorremos la lista de clientes 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IDevolucionDAOTOIDevolucionBean(dao.get(i)));		
		};		
		return listaBean;
	}
	/**
	 * transforma un IDevolucionDao A IDevolucionBean
	 * @param IDevolucionDao 
	 * @return IDevolucionBean
	 */
	private IDevolucionBean IDevolucionDAOTOIDevolucionBean(IDevolucionDao dao) {
		
		IDevolucionBean bean = (IDevolucionBean) new DevolucionBean();	
		IDetalleDevolucionBean detalle ;
		if (dao!=null){
			bean.setIdDevolucion(dao.getIdDevolucion());
			//hay que inicializar el bean de cliente.
			bean.setIclienteBean(ClienteServiceImpl.IClienteDAOTOIClienteBean(dao.getCliente()));
			bean.setItransportadorBean(TransportadorServiceImpl.itransportadorDAOTOitransportadorBean(dao.getTransportador()));
			bean.setIobraBean(ClienteServiceImpl.IObraDAOTOIObraBean(dao.getObra()));
			bean.setFecha(dao.getFecha());
			bean.setFecha_entrega(dao.getFecha_entrega());
			bean.setTransporte(dao.getTransporte());
			bean.setObservaciones(dao.getObservaciones());
			bean.setEstado(dao.isEnabled());
			bean.setPrinted(dao.isPrinted());
			//detalle de devolucion
			for (int k =0; k< dao.getDetalleDevoluciones().size();k++){
				detalle =  new DetalleDevolucionBean();	
//				detalle.setIdRemision(dao.getDetalleDevoluciones().get(k).getRemision().getIdRemision());
				detalle.setCantidad(dao.getDetalleDevoluciones().get(k).getCantidad());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetalleDevoluciones().get(k).getProducto()));
				bean.getDetalleDevoluciones().add(detalle);
				detalle=null;
			}
		}	
		return bean;
	}

	/**
	 * obtiene los productos pendientes de un cliente agrupado por remisiones.
	 * @param identificador del cliente
	 * @return  List<IDetalleDevolucionBean> productos pendientes por devolver
	 */
	/*
	@Override
	public List<IDetalleDevolucionBean> obtenerProductosPendientesPorCliente(String idCliente) {
		List<PendientesDTO> dao= asDevolucionDao.obtenerproductosPendientes(idCliente);
		List<IDetalleDevolucionBean> listaBean = new ArrayList<IDetalleDevolucionBean>();
		 IDetalleDevolucionBean detalle;
		for(PendientesDTO p:dao){
			// System.out.println("cliente :" + ((Object[])valor)[0]+" obra:" + ((Object[])valor)[1]+ " producto:" + ((Object[])valor)[2]+" cantidad:" + ((Object[])valor)[3]);
			//Utilidades.printResult(valor);
			 System.out.println("clienteDTO:"+p.getId_cliente());
			 System.out.println("descripcionDTO:"+p.getDescripcion());
			 System.out.println("getId_remisionDTO:"+p.getId_remision());
			 System.out.println("getCantidadDTO:"+p.getCantidad());
			 System.out.println("getCantidadDevueltaDTO:"+p.getCantidadDevuelta());
			 System.out.println("descripcionDTO:"+p.getDescripcion());
			 System.out.println("----------------:");
			  detalle =  new DetalleDevolucionBean();	
				detalle.setCantidad(0);//inicialidando el formulario en cero
				
				detalle.setPendiente(p.getCantidad()- ((p.getCantidadDevuelta()==null)?0:p.getCantidadDevuelta().intValue())-((p.getCantidadRepuesta()==null)?0:p.getCantidadRepuesta().intValue()));
				detalle.setIdRemision(p.getId_remision());
				 IProductoBean iProductoBean =  new ProductoBean();	
				 iProductoBean.setCodigo_producto(p.getCodigo_producto());
				 iProductoBean.setDescripcion(p.getDescripcion());
				detalle.setIproductoBean(iProductoBean);
				//solo guardamos aquellos productos que tiene pendiente.
				if (detalle.getPendiente()>0){
					listaBean.add(detalle);
				}
				detalle=null;
		 } 
		return listaBean;
	}*/
	
	/**
	 * obtiene los productos pendientes de un cliente agrupado por remisiones.
	 * @param identificador del cliente
	 * @return  List<IDetalleDevolucionBean> productos pendientes por devolver
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IDetalleDevolucionBean> obtenerProductosPendientesPorCliente(String idCliente,int idobra) throws SAExcepcion {
		List<Object> dao= asDevolucionDao.obtenerProductosPendientes(idCliente,idobra);
		List<IDetalleDevolucionBean> listaBean = new ArrayList<IDetalleDevolucionBean>();
		 IDetalleDevolucionBean detalle;
		 //recorremos los datos obtenidos de la consulta y rellenamos List<IDetalleDevolucionBean> listaBean
		 for(int i=0;i<dao.size();i++){
		 	Object[] actual = (Object[]) dao.get(i);
		 	Utilidades.printResult(actual);
		 
		 	detalle =  new DetalleDevolucionBean();	
			detalle.setCantidad(0);//inicialidando el formulario en cero
		//	detalle.setIdRemision((Integer) ((Object[])actual)[0]);
			IProductoBean iProductoBean =  new ProductoBean();	
			iProductoBean.setCodigo_producto((Integer) ((Object[])actual)[2]);
			iProductoBean.setDescripcion((String)((Object[])actual)[3]);
			detalle.setPendiente(((BigDecimal) ((Object[])actual)[4]).intValue());
			IReferenciaBean referencia =  new ReferenciaBean();	
			referencia.setCodigo_interno((Integer) ((Object[])actual)[5]);
			referencia.setDescripcion((String)((Object[])actual)[6]);
			referencia.setPeso(((BigDecimal)((Object[])actual)[7]).doubleValue());
			iProductoBean.setReferenciaBean(referencia);
			detalle.setIproductoBean(iProductoBean);
			listaBean.add(detalle);
			detalle=null; //liberamos el objeto
		 } 
		return listaBean;
	}
	
	
	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<IDevolucionBean> consultarDevoluciones(IDevolucionConsultaBean consultaBean) throws SAExcepcion {
		List<IDevolucionDao> dao = new ArrayList<IDevolucionDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdDevolucion()!=null && !consultaBean.getIdDevolucion().trim().equals("")){
				//consulta de remision por identidicador
				System.out.println("[INFO] consulta de remision por identidicador");
				IDevolucionDao devolucion= asDevolucionDao.buscarDevolucion(consultaBean.getIdDevolucion());
				if (devolucion!=null){
					dao.add(devolucion);
				} 
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asDevolucionDao.buscarDevolucionesPorClienteYRangogoFecha(consultaBean.getIdCliente(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar")){
				//consulta de remision por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asDevolucionDao.buscarDevolucionesPorCliente(consultaBean.getIdCliente()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por rango de fechas
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asDevolucionDao.buscarDevolucionesPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asDevolucionDao.buscarDevoluciones();
			}
		}
		
		List<IDevolucionBean> listaBean = new ArrayList<IDevolucionBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IDevolucionDAOTOIDevolucionBean(dao.get(i)));		
		};		
		return listaBean;
	}
	
	/**
	 * obtiene los productos pendientes de un cliente agrupados.
	 * @param identificador del cliente
	 * @return  List<IDetalleDevolucionBean> productos pendientes por devolver
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IDevolucionBean> consultarAllSaldosPendientes() throws SAExcepcion {
		List<Object> dao= asDevolucionDao.consultarAllSaldosPendientes();
		List<IDevolucionBean> listaBean = new ArrayList<IDevolucionBean>();
		 IDevolucionBean devolucion;
		 //recorremos los datos obtenidos de la consulta y rellenamos List<IDetalleDevolucionBean> listaBean
		 for(int i=0;i<dao.size();i++){
		 	Object[] actual = (Object[]) dao.get(i);
		 	Utilidades.printResult(actual);
		 	devolucion =  new DevolucionBean();	
		 	IObraBean iobraBean =  new ObraBean();	
		 	IClienteBean iclienteBean =  new ClienteBean();	
		 	iclienteBean.setId_cliente((String) ((Object[])actual)[0]);
		 	iclienteBean.setRazon_social((String)((Object[])actual)[1]);
		 	iobraBean.setIdObra((Integer) (((Object[])actual)[2]));
			iobraBean.setNombre((String)((Object[])actual)[3]);
			iobraBean.setTelefono((String)((Object[])actual)[4]);
			iobraBean.setContacto((String)((Object[])actual)[5]);
			iobraBean.setDireccion((String)((Object[])actual)[6]);
			iclienteBean.setDireccion((String)((Object[])actual)[7]);
			iobraBean.setIclientebean(iclienteBean);
			devolucion.setIobraBean(iobraBean);
			devolucion.setIclienteBean(iclienteBean);
			listaBean.add(devolucion);
			devolucion=null; //liberamos el objeto
		 } 
		return listaBean;
	}

	@Override
	public DevolucionBean consultarDevolucionesPDF(int idDevolucion) throws SAExcepcion {
		List<IDevolucionDao> dao = new ArrayList<IDevolucionDao>();
		if (idDevolucion!=0){
				//consulta de remision por identidicador
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.add(asDevolucionDao.buscarDevolucionPDF(idDevolucion));
		}
	
		
		List<IDevolucionBean> listaBean = new ArrayList<IDevolucionBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IDevolucionDAOTOIDevolucionBean(dao.get(i)));		
		};		
		return  (DevolucionBean) listaBean.get(0);
	}
	
	@Override
	@Transactional(readOnly = true)
	public boolean devolucionIsPrinted(int idDevolucion) {
		return asDevolucionDao.devolucionIsPrinted(idDevolucion);
	}
	
	@Override
	@Transactional
	public void marcarDevolucionImpresa(int idDevolucion) {
		this.asDevolucionDao.marcarDevolucionImpresa(idDevolucion);
	}
	
	
}