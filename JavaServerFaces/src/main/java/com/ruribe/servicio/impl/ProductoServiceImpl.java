package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.ProductoDao;
import com.ruribe.dao.TipoProductoDao;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.TipoProductoBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.Referencia;
import com.ruribe.util.entidades.jpa.TipoProducto;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;
import com.ruribe.util.entidades.jpa.interfaces.IProveedorDao;
import com.ruribe.util.entidades.jpa.interfaces.IReferenciaDao;
import com.ruribe.util.entidades.jpa.interfaces.ITipoProductoDao;

@Named
public class ProductoServiceImpl implements ProductoService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private ProductoDao<IProductoDao> asProductoDao;
	@Autowired
	private TipoProductoDao<ITipoProductoDao> asTipoProductoDao;
	
	/**
	 * alta de Producto 
	 * @param IProductoBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void crearProducto(IProductoBean iProductoBean){
		asProductoDao.crearProducto(ProductoServiceImpl.iProductoBeanToiProductoDao(iProductoBean));
	}
	
	
	/**
	 * editar Producto
	 * @param IProductoBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarProducto(IProductoBean iProductoBean){
		asProductoDao.editarProducto(ProductoServiceImpl.iProductoBeanToiProductoDao(iProductoBean));
	}
	
	/**
	 * borrar un Producto apartir de identificador
	 * @param IProductoBean
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarProducto(IProductoBean iProductoBean) {
		asProductoDao.eliminarProducto(ProductoServiceImpl.iProductoBeanToiProductoDao(iProductoBean));
	}

	/**
	 * Edita una lista de Productos
	 * @paramList<IProductoBean>
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	public List<IProductoBean> editarProducto(List<IProductoBean> ListaiProducto) throws SAExcepcion {
		asProductoDao.buscarProductos();
		return null;
	}

	/**
	 * devuelve todos los Productos
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IProductoBean> obtenerProductos() throws SAExcepcion {
		List<IProductoDao> productosDao= asProductoDao.buscarProductos();
		 List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));		
		};		
		return Productos;
	}
	
	@Override
	public List<IProductoBean> obtenerProductosByDescripcion(String filtro) throws SAExcepcion {
		List<IProductoDao> productosDao= asProductoDao.obtenerProductosByDescripcion(filtro);
		 List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));		
		};		
		return Productos;
	}
	
	
	/**
	 * convierte un IProductoDao a IProductoBean
	 * @param dao
	 * @return IProductoBean;
	 */
	public static IProductoBean iProductoDaoTOiProductoBean(IProductoDao dao){
		IProductoBean bean = (IProductoBean) new ProductoBean();	
		
		bean.setCodigo_producto(dao.getCodigo_producto());
		IReferenciaBean referenciaBean = new ReferenciaBean();
		referenciaBean.setCodigo_interno(dao.getReferencia().getCodigo_interno());
		referenciaBean.setDescripcion(dao.getReferencia().getDescripcion());
		referenciaBean.setPrecio_alquiler(dao.getReferencia().getPrecio_alquiler());
		referenciaBean.setPrecio_reposicion(dao.getReferencia().getPrecio_reposicion());
		referenciaBean.setPeso(dao.getReferencia().getPeso());
		
		ITipoProductoBean iTipo = new TipoProductoBean();
		iTipo.setCod_tipo_producto(dao.getTipo_producto().getCod_tipo_producto());
		iTipo.setDescripcion(dao.getTipo_producto().getDescripcion());
		bean.setTipo_producto(iTipo);
		
		referenciaBean.setTipo_producto(iTipo);
		bean.setReferenciaBean(referenciaBean);
		bean.setDescripcion(dao.getDescripcion());
		bean.setStock(dao.getStock());
		bean.setMaximo(dao.getMaximo());
		bean.setSubarriendo(dao.getSubarriendo());
		bean.setMinimo(dao.getMinimo());
		bean.setProveedor(ProveedorServiceImpl.iProveedorDaoTOIProveedorBean(dao.getProveedor()));
		
		
		return bean;
	}
	
	/**
	 * Transforma un IProductoBean a IProductoDao
	 * @param iProductoBean
	 * @return IProductoDao
	 */
	public static IProductoDao iProductoBeanToiProductoDao(IProductoBean iProductoBean){
		
		IProductoDao iProducto = new Producto();
		iProducto.setCodigo_producto(iProductoBean.getCodigo_producto());
		IReferenciaDao referencia = new Referencia(); 
		referencia.setCodigo_interno(iProductoBean.getReferenciaBean().getCodigo_interno());
		referencia.setPeso(iProductoBean.getReferenciaBean().getPeso());
		iProducto.setReferencia((Referencia) referencia);
		
		iProducto.setDescripcion(iProductoBean.getDescripcion());
//		iProducto.setFecha_ingreso(Utilidades.convertJavaDateToSqlDate(iProductoBean.getFecha_ingreso()));
//		iProducto.setPrecio_alquiler(iProductoBean.getPrecio_alquiler());
//		iProducto.setPrecio_reposicion(iProductoBean.getPrecio_reposicion());
		iProducto.setStock(iProductoBean.getStock());
		iProducto.setMaximo(iProductoBean.getMaximo());
		iProducto.setSubarriendo(iProductoBean.getSubarriendo());
		iProducto.setMinimo(iProductoBean.getMinimo());
		
		IProveedorDao iProveedor = new Proveedor();
		iProveedor.setCodigo_proveedor(iProductoBean.getProveedor().getCodigo_proveedor());
		iProveedor.setNo_documento(iProductoBean.getProveedor().getNo_documento());
		iProducto.setProveedor((Proveedor)iProveedor);
		
		ITipoProductoDao iTipo = new TipoProducto();
		iTipo.setCod_tipo_producto(iProductoBean.getTipo_producto().getCod_tipo_producto());
		iProducto.setTipo_producto((TipoProducto)iTipo);
		
		return iProducto;
	}

	
	/**
	 * devuelve una lista de productos cuyo codigo empiece por un valor pasado por parametro
	 * @param IProductoBean identificador a buscar
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	public List<IProductoBean> obtenerProducto(IProductoBean iProductoBean) throws SAExcepcion {
		
		List<IProductoDao> productosDao= asProductoDao.buscarProducto(ProductoServiceImpl.iProductoBeanToiProductoDao(iProductoBean));
		List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));				
		};		
		return Productos;
	}

	@Override
	public List<ITipoProductoBean> obtenerTipoProducto() throws SAExcepcion {
		List<ITipoProductoDao> tipoproductosDao= asTipoProductoDao.buscarTipoProducto();
		 List<ITipoProductoBean> tipoproductos = new ArrayList<ITipoProductoBean>();
		
		for (int i =0; i< tipoproductosDao.size(); i++){
			ITipoProductoBean bean = (ITipoProductoBean) new TipoProductoBean();	
			bean.setDescripcion(tipoproductosDao.get(i).getDescripcion());
			bean.setCod_tipo_producto(tipoproductosDao.get(i).getCod_tipo_producto());
			tipoproductos.add(bean);		
		};		
		return tipoproductos;
	}
	
	/**
	 * obtenerProductosPorTipo cuyo stock mayor que cero
	 * @param int idTipoProducto identidicador del tipo producto
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	public List<IProductoBean> buscarProductoPorTipoProductoConStock(int idTipoProducto) throws SAExcepcion {
		List<IProductoDao> productosDao= asProductoDao.buscarProductoPorTipoProductoConStock(idTipoProducto);
		List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));				
		};		
		return Productos;
	}	
	
	/**
	 * obtenerProductosPorTipo
	 * @param int idTipoProducto identidicador del tipo producto
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	public List<IProductoBean> obtenerProductosPorTipo(int idTipoProducto) throws SAExcepcion {
		List<IProductoDao> productosDao= asProductoDao.buscarProductoPorTipoProducto(idTipoProducto);
		List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));				
		};		
		return Productos;
	}	
	
	/**
	 * obtenerProductosPorTipo
	 * @param int idTipoProducto identidicador del tipo producto
	 * @return List<IProductoBean>
	 * @throws SAExcepcion 
	 */
	public List<IProductoBean> obtenerProductosPorTipoProveedor(int idTipoProducto, String codigo_proveedor) throws SAExcepcion {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		 hmap.put("cod_tipo_producto",idTipoProducto);
		 hmap.put("codigoProveedor",codigo_proveedor);
		List<IProductoDao> productosDao= asProductoDao.obtenerProductosPorTipoProveedor(hmap);
		List<IProductoBean> Productos = new ArrayList<IProductoBean>();
		for (int i =0; i< productosDao.size(); i++){
			Productos.add(ProductoServiceImpl.iProductoDaoTOiProductoBean(productosDao.get(i)));				
		};		
		return Productos;
	}	
	


	@Override
	public IProductoBean obtenerProductosPK(String idProductoSelect) {
		// TODO Auto-generated method stub
		return null;
	}
	
		/**
		 * clases de prueba
		 */
		static public void main (String arg[]){
			// comprobamos la conexion a bd sea correcta.		
			try {
			    			    
				 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
				 EntityManager em = emf.createEntityManager();
				 em.getTransaction( ).begin( );
				 Producto iProducto = new Producto();
				 Proveedor iproveedor= new Proveedor();
				 TipoProducto itipo= new TipoProducto();
				 itipo.setCod_tipo_producto(1);
				 iproveedor.setNo_documento("72345968");
				 	iProducto.setProveedor(iproveedor);
					iProducto.setDescripcion("pruebaaaaaaaaaaaaa5");
//					iProducto.setFecha_ingreso(Utilidades.convertJavaDateToSqlDate(new Date()));
//					iProducto.setPrecio_alquiler(10);
					iProducto.setStock(10);
					//iProducto.setPeso(10.00);
					iProducto.setMaximo(10);
					iProducto.setSubarriendo(10);
					iProducto.setMinimo(10);
					iProducto.setTipo_producto(itipo);
				em.persist(iProducto);
				TypedQuery <Producto> productos= em.createNamedQuery(Producto.QUERY_FIND_ALL_PRODUCTO,Producto.class);
				 for(Producto c:productos.getResultList()){
					 System.out.println("Nombre:" + c.getDescripcion());
					 System.out.println("proveedor:" + c.getProveedor().getRazon_social());
				 } 
				 
//				 TypedQuery <Obra> listaobras= em.createNamedQuery("QUERY_SELECT_OBRAS_CLIENTE",Obra.class);
//				 listaobras.setParameter("id_cliente", "1234567");
//				 for(Obra c:listaobras.getResultList()){
//					 System.out.println("nombre de obra:" + c.getNombre());
//					
//				 } 
				 
				 em.getTransaction().commit();
			      em.close();
			      emf.close();
			    System.out.println("Got Connection.");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}


}