package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.ReposicionDao;
import com.ruribe.servicio.interfaces.ReposicionService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleReposicionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ReposicionBean;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionConsultaBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleReposicion;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Remision;
import com.ruribe.util.entidades.jpa.Reposicion;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleReposicionDao;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;
import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;
import com.ruribe.util.entidades.jpa.interfaces.IReposicionDao;

@Named
public class ReposicionServiceImpl implements ReposicionService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private ReposicionDao<IReposicionDao> asReposicionDao;
	
	/**
	 * alta de usuario 
	 * @param IReposicionBean
	 * @return 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IReposicionBean crearReposicion(IReposicionBean iReposicionBean){
		IReposicionDao dao = asReposicionDao.crearReposicion(ReposicionServiceImpl.iReposicionBeanTOiReposicionDAO(iReposicionBean));
		iReposicionBean.setIdReposicion(dao.getIdReposicion());
		return iReposicionBean;
	}
	
	/**
	 * Convierte un IReposicionBean a IReposicionDao
	 * @param iReposicionBean
	 * @return IReposicionDao
	 */
	public static IReposicionDao iReposicionBeanTOiReposicionDAO(IReposicionBean iReposicionBean){
		IReposicionDao iReposicion = new Reposicion();
		iReposicion.setCliente((Cliente) ClienteServiceImpl.IClienteBeanTOIClienteDAO(iReposicionBean.getIclienteBean()));
		iReposicion.setIdReposicion(iReposicionBean.getIdReposicion());
		iReposicion.setObra((Obra)ClienteServiceImpl.IObraBeanTOIObraDao(iReposicionBean.getIobraBean()));
		iReposicion.setFecha(iReposicionBean.getFecha());
		iReposicion.setObservaciones(iReposicionBean.getObservaciones());
		iReposicion.setUsuario_creacion(iReposicionBean.getUsuario_creacion());
		List<DetalleReposicion> detalleDao= new ArrayList<DetalleReposicion>();
		//recorremos el detalle de remision
		
//		for (IDetalleReposicionBean d :iReposicionBean.getDetalleReposiciones()){
//			System.out.println("SERVICE getIdRemision:" + d.getIdRemision());
//			System.out.println("SERVICE getCodigo_producto:" + d.getIproductoBean().getCodigo_producto());
//			System.out.println("SERVICE getDescripcion:" + d.getIproductoBean().getDescripcion());
//			System.out.println("SERVICE getCantidad:" + d.getCantidad());	
//			System.out.println("SERVICE getPendiente:" + d.getPendiente());	
//			System.out.println("--------------------");
//		}
		
		for (int k =0; k < iReposicionBean.getDetalleReposiciones().size();k++){
			IDetalleReposicionDao detalle= new DetalleReposicion();
			detalle.setReposicion((Reposicion)iReposicion);
			IProductoDao producto = new Producto();
			producto.setCodigo_producto(iReposicionBean.getDetalleReposiciones().get(k).getIproductoBean().getCodigo_producto());
			detalle.setProducto((Producto)producto);
			detalle.setCantidad(iReposicionBean.getDetalleReposiciones().get(k).getCantidad());
			detalle.setPrecio_reposicion(iReposicionBean.getDetalleReposiciones().get(k).getPrecio_reposicion());
			IRemisionDao remision= new Remision();
			remision.setIdRemision(iReposicionBean.getDetalleReposiciones().get(k).getIdRemision());
			//solo guardamos si hay productos
			if (detalle.getCantidad()>0){
				detalleDao.add((DetalleReposicion) detalle);
			}
			detalle=null;
		}
		iReposicion.setDetalleReposicions(detalleDao);
		return iReposicion;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarReposicion(IReposicionBean iReposicionBean){
		asReposicionDao.editarReposicion(ReposicionServiceImpl.iReposicionBeanTOiReposicionDAO(iReposicionBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarReposicion(IReposicionBean iReposicionBean) {
		asReposicionDao.eliminarReposicion(ReposicionServiceImpl.iReposicionBeanTOiReposicionDAO(iReposicionBean));
	}
	
	/**
	 * @return devuelve todos los proveedores
	 * @throws SAExcepcion 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<IReposicionBean> consultarReposiciones() throws SAExcepcion {
		List<IReposicionDao> dao= asReposicionDao.buscarReposiciones();
		List<IReposicionBean> listaBean = new ArrayList<IReposicionBean>();
		
		//recorremos la lista de clientes 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IReposicionDAOTOIReposicionBean(dao.get(i)));		
		};		
		return listaBean;
	}
	/**
	 * transforma un IReposicionDao A IReposicionBean
	 * @param IReposicionDao 
	 * @return IReposicionBean
	 */
	private IReposicionBean IReposicionDAOTOIReposicionBean(IReposicionDao dao) {
		
		IReposicionBean bean = (IReposicionBean) new ReposicionBean();	
		IDetalleReposicionBean detalle ;
		if (dao!=null){
			bean.setIdReposicion(dao.getIdReposicion());
			//hay que inicializar el bean de cliente.
			bean.setIclienteBean(ClienteServiceImpl.IClienteDAOTOIClienteBean(dao.getCliente()));
			bean.setIobraBean(ClienteServiceImpl.IObraDAOTOIObraBean(dao.getObra()));
			bean.setFecha(dao.getFecha());
			bean.setObservaciones(dao.getObservaciones());
			//detalle de devolucion
			for (int k =0; k< dao.getDetalleReposiciones().size();k++){
				detalle =  new DetalleReposicionBean();	
				detalle.setCantidad(dao.getDetalleReposiciones().get(k).getCantidad());
				detalle.setPrecio_reposicion(dao.getDetalleReposiciones().get(k).getPrecio_reposicion());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetalleReposiciones().get(k).getProducto()));
				bean.getDetalleReposiciones().add(detalle);
				bean.setTotal(bean.getTotal()+detalle.getCantidad()*detalle.getPrecio_reposicion());
				detalle=null;
			}
		}	
		return bean;
	}

	
	/**
	 * obtiene los productos pendientes de un cliente agrupado por remisiones.
	 * @param identificador del cliente
	 * @return  List<IDetalleReposicionBean> productos pendientes por devolver
	 * @throws SAExcepcion 
	 */
	@Override
	public List<IDetalleReposicionBean> obtenerProductosPendientesPorCliente(String idCliente,String idobra) throws SAExcepcion {
		List<Object> dao= asReposicionDao.obtenerProductosPendientes(idCliente,idobra);
		List<IDetalleReposicionBean> listaBean = new ArrayList<IDetalleReposicionBean>();
		 IDetalleReposicionBean detalle;
		 //recorremos los datos obtenidos de la consulta y rellenamos List<IDetalleReposicionBean> listaBean
		 for(int i=0;i<dao.size();i++){
		 	Object[] actual = (Object[]) dao.get(i);
		 	Utilidades.printResult(actual);
		 
		 	detalle =  new DetalleReposicionBean();	
			detalle.setCantidad(0);//inicialidando el formulario en cero
			IProductoBean iProductoBean =  new ProductoBean();	
			iProductoBean.setCodigo_producto((Integer) ((Object[])actual)[2]);
			iProductoBean.setDescripcion((String)((Object[])actual)[3]);
			detalle.setPendiente(((BigDecimal) ((Object[])actual)[4]).intValue());
			detalle.setPrecio_reposicion((Integer) ((Object[])actual)[5]);
			detalle.setIproductoBean(iProductoBean);
			listaBean.add(detalle);
			detalle=null; //liberamos el objeto
		 } 
		return listaBean;
	}
	
	
	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public List<IReposicionBean> consultarReposiciones(IReposicionConsultaBean consultaBean) throws SAExcepcion {
		List<IReposicionDao> dao = new ArrayList<IReposicionDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdReposicion()!=null && !consultaBean.getIdReposicion().trim().equals("")){
				//consulta de remision por identidicador
				System.out.println("[INFO] consulta de remision por identidicador");
				IReposicionDao reposicion= asReposicionDao.buscarReposicion(consultaBean.getIdReposicion());
				if (reposicion!=null){
					dao.add(reposicion);
				} 
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asReposicionDao.buscarReposicionesPorClienteYRangogoFecha(consultaBean.getIdCliente(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar")){
				//consulta de remision por cliente
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asReposicionDao.buscarReposicionesPorCliente(consultaBean.getIdCliente()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por rango de fechas
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.addAll(asReposicionDao.buscarReposicionesPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asReposicionDao.buscarReposiciones();
			}
		}
		
		List<IReposicionBean> listaBean = new ArrayList<IReposicionBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IReposicionDAOTOIReposicionBean(dao.get(i)));		
		};		
		return listaBean;
	}

	@Override
	public ReposicionBean consultarReposicionesPDF(int idReposicion) throws SAExcepcion {
		List<IReposicionDao> dao = new ArrayList<IReposicionDao>();
		if (idReposicion!=0){
				//consulta de remision por identidicador
				System.out.println("[INFO] consulta de remision por identidicador");
				dao.add(asReposicionDao.buscarReposicionPDF(idReposicion));
		}
	
		
		List<IReposicionBean> listaBean = new ArrayList<IReposicionBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IReposicionDAOTOIReposicionBean(dao.get(i)));		
		};		
		return  (ReposicionBean) listaBean.get(0);
	}
	
	
}