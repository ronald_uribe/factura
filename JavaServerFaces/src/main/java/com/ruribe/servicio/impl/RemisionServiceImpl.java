package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.RemisionDao;
import com.ruribe.servicio.interfaces.RemisionService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.DetalleRemisionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.RemisionBean;
import com.ruribe.util.bean.interfaces.IDetalleRemisionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.IRemisionBean;
import com.ruribe.util.bean.interfaces.IRemisionConsultaBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleRemision;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Remision;
import com.ruribe.util.entidades.jpa.Transportador;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleRemisionDao;
import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;

@Named
public class RemisionServiceImpl implements RemisionService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private RemisionDao<IRemisionDao> asRemisionDao;
	
	/**
	 * alta de usuario 
	 * @param IRemisionBean
	 * @return 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IRemisionBean crearRemision(IRemisionBean iRemisionBean){
		
		IRemisionDao dao = asRemisionDao.crearRemision(RemisionServiceImpl.iRemisionBeanTOiRemisionDAO(iRemisionBean));
		System.out.println("INSERTANDO ACTUALIZADO REMISION:" + dao.getIdRemision());
		iRemisionBean.setIdRemision(dao.getIdRemision());
		return iRemisionBean;
	}
	
	/**
	 * Convierte un IRemisionBean a IRemisionDao
	 * @param iRemisionBean
	 * @return IRemisionDao
	 */
	public static IRemisionDao iRemisionBeanTOiRemisionDAO(IRemisionBean iRemisionBean){
		IRemisionDao iRemision = new Remision();
		iRemision.setCliente((Cliente) ClienteServiceImpl.IClienteBeanTOIClienteDAO(iRemisionBean.getIclienteBean()));
		iRemision.setIdRemision(iRemisionBean.getIdRemision());
		iRemision.setTransportador((Transportador)TransportadorServiceImpl.itransportadorBeanTOitransportadorDAO(iRemisionBean.getItransportadorBean()));
		iRemision.setObra((Obra)ClienteServiceImpl.IObraBeanTOIObraDao(iRemisionBean.getIobraBean()));
		iRemision.setFecha(iRemisionBean.getFecha());
		iRemision.setFecha_entrega(iRemisionBean.getFecha_entrega());
		iRemision.setTransporte(iRemisionBean.getTransporte());
		iRemision.setObservaciones(iRemisionBean.getObservaciones());
		iRemision.setUsuario_creacion(iRemisionBean.getUsuario_creacion());
		iRemision.setEnabled(iRemisionBean.isEstado());
		iRemision.setPrinted(iRemisionBean.isPrinted());
		List<DetalleRemision> detalleDao= new ArrayList<DetalleRemision>();
		//recorremos el detalle de remision
		//int productoId=0;
		for (int k =0; k < iRemisionBean.getDetalleRemisions().size();k++){
			IDetalleRemisionDao detalle= new DetalleRemision();
			detalle.setRemision((Remision)iRemision);
			detalle.setProducto((Producto)(ProductoServiceImpl.iProductoBeanToiProductoDao(iRemisionBean.getDetalleRemisions().get(k).getIproductoBean())));
			detalle.setCantidad(iRemisionBean.getDetalleRemisions().get(k).getCantidad());
			detalleDao.add((DetalleRemision) detalle);
		}
		iRemision.setDetalleRemisions(detalleDao);
		return iRemision;
	}
	
	/**
	 * editar usuario
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void editarRemision(IRemisionBean iRemisionBean){
		asRemisionDao.editarRemision(RemisionServiceImpl.iRemisionBeanTOiRemisionDAO(iRemisionBean));
	}
	
	/**
	 * borrar usuario apartir de identificador
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void borrarRemision(IRemisionBean iRemisionBean) {
		asRemisionDao.eliminarRemision(RemisionServiceImpl.iRemisionBeanTOiRemisionDAO(iRemisionBean));
	}
	
	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(readOnly = true)
	public List<IRemisionBean> consultarRemisiones(IRemisionConsultaBean consultaBean) throws SAExcepcion {
		List<IRemisionDao> dao = new ArrayList<IRemisionDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdRemision()!=null && !consultaBean.getIdRemision().trim().equals("")){
				//consulta de remision por identidicador
				 Logger.getLogger(RemisionServiceImpl.class.getName()).log(Level.INFO, "consulta de remision por identidicador");
				IRemisionDao remision= asRemisionDao.buscarRemision(consultaBean.getIdRemision());
				if (remision!=null){
					dao.add(remision);
				} 
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por cliente
				Logger.getLogger(RemisionServiceImpl.class.getName()).log(Level.INFO, "consulta de remision por cliente y rango de fechas");
				dao.addAll(asRemisionDao.buscarRemisionesPorClienteYRangogoFecha(consultaBean.getIdCliente(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar")){
				//consulta de remision por cliente
				 Logger.getLogger(RemisionServiceImpl.class.getName()).log(Level.INFO, "consulta de remision por cliente");
				dao.addAll(asRemisionDao.buscarRemisionesPorCliente(consultaBean.getIdCliente()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				//consulta de remision por rango de fechas
				 Logger.getLogger(RemisionServiceImpl.class.getName()).log(Level.INFO, "consulta de remision por rango de fechas");
				dao.addAll(asRemisionDao.buscarRemisionesPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asRemisionDao.buscarRemisiones();
			}
		}
		
		List<IRemisionBean> listaBean = new ArrayList<IRemisionBean>();
		
		//recorremos la lista de clientes 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IRemisionDAOTOIRemisionBean(dao.get(i)));		
		};		
		return listaBean;
	}

	private IRemisionBean IRemisionDAOTOIRemisionBean(IRemisionDao dao) {
		
		IRemisionBean bean = (IRemisionBean) new RemisionBean();	
		if (dao!=null){
			bean.setIdRemision(dao.getIdRemision());
			//hay que inicializar el bean de cliente.
			bean.setIclienteBean(ClienteServiceImpl.IClienteDAOTOIClienteBean(dao.getCliente()));
			bean.setIdRemision(dao.getIdRemision());
			bean.setItransportadorBean(TransportadorServiceImpl.itransportadorDAOTOitransportadorBean(dao.getTransportador()));
			bean.setIobraBean(ClienteServiceImpl.IObraDAOTOIObraBean(dao.getObra()));
			bean.setFecha(dao.getFecha());
			bean.setFecha_entrega(dao.getFecha_entrega());
			bean.setTransporte(dao.getTransporte());
			bean.setObservaciones(dao.getObservaciones());
			bean.setEstado(dao.isEnabled());
			bean.setPrinted(dao.isPrinted());
			for (int k =0; k< dao.getDetalleRemisions().size();k++){
				IDetalleRemisionBean detalle =  new DetalleRemisionBean();	
				detalle.setCantidad(dao.getDetalleRemisions().get(k).getCantidad());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetalleRemisions().get(k).getProducto()));
				bean.getDetalleRemisions().add(detalle);
			}
		}	
		return bean;
	}

	/**
	 * consulta una remision agrupando por codigo interno
	 * @throws SAExcepcion 
	 */
	@Override
	@Transactional(readOnly = true)
	public IRemisionBean consultarRemisionPDFCliente(IRemisionBean remision) throws SAExcepcion {
		List<Object> dao;
		IRemisionBean remisionBean = null;
		if (remision.getIdRemision()!=0){
				//consulta de remision por identidicador
				 Logger.getLogger(RemisionServiceImpl.class.getName()).log(Level.INFO, "consulta de remision agrupado por codigo interno");
				dao= asRemisionDao.buscarRemisionPDFCliente(String.valueOf(remision.getIdRemision()));
				if (dao!=null){
						remisionBean = new RemisionBean();
						remisionBean.setIdRemision(remision.getIdRemision());
						remisionBean.setIclienteBean(remision.getIclienteBean());
						remisionBean.setIobraBean(remision.getIobraBean());
						remisionBean.setTransporte(remision.getTransporte());
						remisionBean.setItransportadorBean(remision.getItransportadorBean());
						remisionBean.setObservaciones(remision.getObservaciones());
						remisionBean.setFecha(remision.getFecha());
						remisionBean.setFecha_entrega(remision.getFecha_entrega());
						IDetalleRemisionBean detalleRemision;
						for(int i=0;i<dao.size();i++){
							Object[] detalle = (Object[]) dao.get(i);
							detalleRemision = new DetalleRemisionBean();
							IProductoBean producto = new ProductoBean();
							// le asignamos el codigo interno de la consulta para reaprovechar el pdf
							producto.setCodigo_producto((Integer) ((Object[])detalle)[8]);
							// le asignamos la descripcion de la referencia para reaprovechar el pdf
							producto.setDescripcion( ((Object[])detalle)[10].toString());
							detalleRemision.setCantidad(((BigDecimal) ((Object[])detalle)[7]).intValue());
							//referencia
							IReferenciaBean referencia = new ReferenciaBean();
							referencia.setCodigo_interno((Integer) ((Object[])detalle)[8]);
							referencia.setPeso( ((BigDecimal)((Object[])detalle)[11]).doubleValue());
							producto.setReferenciaBean(referencia);
							//producto
							detalleRemision.setIproductoBean(producto);
							remisionBean.setDetalleRemisions(detalleRemision);
							detalleRemision=null;
						}	
				}
		}		
		
		
				
		return remisionBean;	
	}

	/**
	 * obtiene una remision por su identificador.
	 */
	@Override
	@Transactional(readOnly = true)
	public IRemisionBean consultarIDRemision(IRemisionBean iRemision) {
		IRemisionBean remision= IRemisionDAOTOIRemisionBean(asRemisionDao.buscarRemision(String.valueOf(iRemision.getIdRemision())));
		return remision;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean remisionIsPrinted(int idRemision) {
		return asRemisionDao.remisionIsPrinted(idRemision);
	}
	
	@Override
	@Transactional
	public void marcarRemisionImpresa(int idRemision) {
		this.asRemisionDao.marcarRemisionImpresa(idRemision);
	}

}