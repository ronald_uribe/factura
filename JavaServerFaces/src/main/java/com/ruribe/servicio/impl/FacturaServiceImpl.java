package com.ruribe.servicio.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ruribe.dao.FacturaDao;
import com.ruribe.servicio.interfaces.FacturaService;
import com.ruribe.util.Constantes;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleFacturaBean;
import com.ruribe.util.bean.FacturaBean;
import com.ruribe.util.bean.interfaces.IDetalleFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaConsultaBean;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleFactura;
import com.ruribe.util.entidades.jpa.Factura;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.interfaces.IClienteDao;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleFacturaDao;
import com.ruribe.util.entidades.jpa.interfaces.IFacturaDao;
import com.ruribe.util.entidades.jpa.interfaces.IObraDao;
import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;

@Named
public class FacturaServiceImpl implements FacturaService, Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4840713400439728994L;
	@Autowired
	private FacturaDao<IFacturaDao> asFacturaDao; 
	
	private final Logger log = Logger.getLogger(FacturaServiceImpl.class.getName());
	
	/**
	 * alta de factura 
	 * @param IFacturaBean
	 * @return 
	 */

	@Transactional
	public IFacturaBean crearFactura(IFacturaBean iFacturaBean) {
		IFacturaDao dao =asFacturaDao.crearFactura(IFacturaBeanTOIFacturaDAO(iFacturaBean));
		//obtenemos el numero de factura generado y lo almacenamos en el bean
		iFacturaBean.setIdFactura(dao.getIdfactura());
		return iFacturaBean;
	}
	

	/**
	 * convierte un bean factura a un dao
	 * IFacturaBeanTOIFacturaDAO
	 * @return devuelve la factura guardada.
	 */
	public static IFacturaDao IFacturaBeanTOIFacturaDAO(IFacturaBean iFacturaBean){
			IFacturaDao ifactura = new Factura();
			//cliente
			IClienteDao cliente = new Cliente();
			cliente.setId_cliente(iFacturaBean.getIclienteBean().getId_cliente());
			ifactura.setCliente((Cliente) cliente);
			//obra
			IObraDao obra = new Obra();
			obra.setIdObra(iFacturaBean.getIobraBean().getIdObra());
			ifactura.setObra((Obra) obra);
			//transportador
			ifactura.setTransporte(iFacturaBean.getTransporte().intValue());
			ifactura.setObservaciones(iFacturaBean.getObservaciones().trim());
			ifactura.setTransporte(iFacturaBean.getTransporte().intValue());
			ifactura.setIva(Constantes.IVA);
			if (iFacturaBean.getDescuento()!=null) {
				ifactura.setDescuento(iFacturaBean.getDescuento());
			}
			ifactura.setReposicion(iFacturaBean.getReposicion());
			ifactura.setFecha(iFacturaBean.getFecha_factura());
			ifactura.setFecha_inicio(iFacturaBean.getFecha_inicio());
			ifactura.setFecha_corte(iFacturaBean.getFecha_corte());
			ifactura.setUsuario_creacion(iFacturaBean.getUsuario());
			ifactura.setEnabled(iFacturaBean.isEstado());
			List<DetalleFactura> detalleDao= new ArrayList<DetalleFactura>();
			for (int i =0;i<iFacturaBean.getListadetalleFactura().size();i++){
				IDetalleFacturaDao detalle= new DetalleFactura();
				detalle.setFactura((Factura)ifactura);
				detalle.setItem(i);
				iFacturaBean.getListadetalleFactura().get(i).getDescripcion();
				iFacturaBean.getListadetalleFactura().get(i).getFecha_inicial();
				detalle.setNumeroDias(iFacturaBean.getListadetalleFactura().get(i).getNo_dias());
				detalle.setValorUnitario(iFacturaBean.getListadetalleFactura().get(i).getVr_unitario());
				IProductoDao producto = new Producto();
				producto.setCodigo_producto(iFacturaBean.getListadetalleFactura().get(i).getCodigo_producto());
				detalle.setProducto((Producto) producto);
				detalle.setCantidad(iFacturaBean.getListadetalleFactura().get(i).getCantidad());
				detalle.setFechaInicio(iFacturaBean.getListadetalleFactura().get(i).getFecha_inicial());
				detalle.setFechaCorte(iFacturaBean.getListadetalleFactura().get(i).getFecha_final());
				detalle.setTipo(iFacturaBean.getListadetalleFactura().get(i).getTipo());
				detalle.setCantidad_gestionada(iFacturaBean.getListadetalleFactura().get(i).getCantidad_gestionada());
				detalleDao.add((DetalleFactura) detalle);
			}
			ifactura.setDetallefactura(detalleDao);
			return ifactura;
	}

	/**
	 * procesarFactura
	 * @param IFacturaBean fecha de inicio,fecha corte, id_cliente
	 * @return IFacturaBean lista de items a facturar.
	 * @throws SAExcepcion 
	 */
	@Override
	public IFacturaBean procesarFactura(IFacturaBean ifactura) throws SAExcepcion {
		
		HashMap<String, Object> params = new HashMap<String, Object> ();
		//params.put("corte", new SimpleDateFormat("dd/MM/yyyy").format(ifactura.getFecha_corte()));
		params.put("fechainicio", new SimpleDateFormat("yyyy-MM-dd").format(ifactura.getFecha_inicio()));
		params.put("fechacorte", new SimpleDateFormat("yyyy-MM-dd").format(ifactura.getFecha_corte()));
		params.put("idcliente", ifactura.getIclienteBean().getId_cliente());
		params.put("idobra", ifactura.getIobraBean().getIdObra());
		//ifactura.
		List<Object>detalle = asFacturaDao.consultarCorte(params);
		IDetalleFacturaBean items;// inicializacion del detalle de factura que rellenaremos con la consulta
		int vr_unitario;
		int cantidad = 0;
		
		for(int i=0;i<detalle.size();i++){
			Object[] actual = (Object[]) detalle.get(i);
			Object[] siguiente;
			Object[] anterior;
			cantidad = 0; // Variable para calcular si se debe restar o sumar dependiento el tipo de registro.
			Utilidades.printResult(actual);
			//asignamos el siguiente valor 
			if(i<detalle.size()-1){ 
				siguiente = (Object[]) detalle.get(i+1);
			}else{
				siguiente=null;
			}
			//asignamos el anterior valor 
			if(i>0){ 
				anterior = (Object[]) detalle.get(i-1);
			}else{
				anterior=null;
			}
			
			items = new DetalleFacturaBean(); 
			items.setIdFactura(0);
			items.setCodigo_producto((Integer) ((Object[])actual)[1]);
			items.setDescripcion(((Object[])actual)[4].toString());
			items.setTipo(((Object[])actual)[3].toString());
			items.setCantidad_gestionada(((BigDecimal) ((Object[])actual)[2]).intValue());
			
			
			// TIPO DE CONSULTA, DEVOLUCION, REMISION, ACUMULADO, RESPOSICION.
			// el tipo de dato acumulado la fecha inicial siempre sera la recibida por parametro
			String tipo=((Object[])actual)[3].toString();
			if (tipo != null && tipo.equals("ACUMULADO") ){
				items.setFecha_inicial(ifactura.getFecha_inicio());
				cantidad=((BigDecimal) ((Object[])actual)[2]).intValue();
			}else{
				if (anterior!=null){// a la fecha inicial le asignamos la fecha final del anterior item.
					if (items.getCodigo_producto()==(Integer) ((Object[])anterior)[1])
					{
							items.setFecha_inicial(Utilidades.sumarRestarDiasFecha(ifactura.getListadetalleFactura().get(i-1).getFecha_final(),1));
							if (tipo != null && tipo.equals("REMISION") ){
								cantidad = ifactura.getListadetalleFactura().get(i-1).getCantidad()+ ((BigDecimal) ((Object[])actual)[2]).intValue();
							}else 
							if (tipo != null && tipo.equals("DEVOLUCION") ){
								cantidad = ifactura.getListadetalleFactura().get(i-1).getCantidad()- ((BigDecimal) ((Object[])actual)[2]).intValue();
							}
							if (tipo != null && tipo.equals("REPOSICION") ){
								cantidad = ifactura.getListadetalleFactura().get(i-1).getCantidad()- ((BigDecimal) ((Object[])actual)[2]).intValue();
							}
					}else{// sino es igual asignamos fecha actual
							try {
								items.setFecha_inicial(Utilidades.fechaObjectToDate(((Object[])actual)[0]));
								cantidad=((BigDecimal) ((Object[])actual)[2]).intValue();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	
				 }else{// if anterior
					 try {
						 	cantidad=((BigDecimal) ((Object[])actual)[2]).intValue();
						// 	result instanceof String
							items.setFecha_inicial(Utilidades.fechaObjectToDate(((Object[])actual)[0]));
						} catch (ParseException e) {
							 log.log(Level.SEVERE, "No ha sido posible parsear la fecha fechaObjectToDate ", e.getMessage());
					          
							e.printStackTrace();
						}
				 }
			}	
			// preguntamos si el siguiente producto es una devolucion de esa remision y producto
			if (siguiente!=null){
				if (items.getCodigo_producto()==(Integer) ((Object[])siguiente)[1])
				{
					try {//si la fecha de devolucion es igual que a la fecha de remision no deberia incrementar. 
						if (Utilidades.fechaObjectToDate(((Object[])siguiente)[0])==Utilidades.fechaObjectToDate(((Object[])actual)[0])){
							items.setFecha_final(Utilidades.fechaObjectToDate(((Object[])actual)[0]));
						}else{
							items.setFecha_final(Utilidades.fechaObjectToDate(((Object[])siguiente)[0]));
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{	// significa que es un producto diferente
						items.setFecha_final(ifactura.getFecha_corte());
				}
			}else{
				//cantidad=((BigDecimal) ((Object[])actual)[3]).intValue();
				items.setFecha_final(ifactura.getFecha_corte());
			}
			
			items.setCantidad(cantidad);
			// si la fecha corte es mayor que la inicial asignamos 0 al numero de dias
			if (!Utilidades.fechaesAMayorOIgualfechaB(items.getFecha_final(),items.getFecha_inicial())){
				items.setNo_dias(Utilidades.restarDosFechas(items.getFecha_inicial(), items.getFecha_final())+1);
			}else{
				items.setNo_dias(0);
			}
			vr_unitario=(Integer) ((Object[])actual)[5];
			if (tipo != null && tipo.equals("DEVOLUCION") ){
				items.setIdDevolucion(Integer.parseInt(((Object[])  actual)[6].toString()));
			}
			if (tipo != null && tipo.equals("REPOSICION") ){
				items.setIdReposicion(Integer.parseInt(((Object[])  actual)[6].toString()));
			}
			items.setVr_unitario(vr_unitario);
			items.setVr_total((double) (items.getNo_dias()*items.getCantidad() * vr_unitario));
			ifactura.setVr_parcial(ifactura.getVr_parcial()+items.getVr_total()); // valor parcial
			
			ifactura.getListadetalleFactura().add(items);
			items=null;
		}// fin for detalle.size()

//		// calculamos el transporte para el periodo de corte
		ifactura.setTransporte(this.calcularTransportePeriodoFacturacion(ifactura));
		ifactura.setIva((((ifactura.getVr_parcial()+ifactura.getTransporte())*Constantes.IVA)/100));
		ifactura.setTotal((ifactura.getTransporte()+ifactura.getIva()+ifactura.getVr_parcial()));
		// fin de la facturacion
		
		//eliminamos los items cuya cantidad es cero
		for (int i=0;i<ifactura.getListadetalleFactura().size();i++){
			
			if (ifactura.getListadetalleFactura().get(i).getCantidad()==0){
//				ifactura.getListadetalleFactura().remove(ifactura.getListadetalleFactura().get(i));
//				i=i-1;// descontamos el valor borrado al tamano de la lista
			}// SI LA FECHA INICIAL ES MAYOR QUE LA FECHA DE CORTE DESCARTAMOS EL ITEM, SIGNIFICA QUE ES UN CONTENIDO PENDIENTE.
			else if (Utilidades.fechaesAMayorfechaB(ifactura.getFecha_corte(),ifactura.getListadetalleFactura().get(i).getFecha_inicial())){
				//	ifactura.getListadetalleFactura().remove(ifactura.getListadetalleFactura().get(i));
				//	i=i-1; // descontamos el valor borrado al tamano de la lista
				}
		}
		return ifactura;
		
	}
	
	
	/**
	 * Calcula el Transporte en un Periodo de Facturacion
	 * @param IFacturaBean ifactura fechainicio, fechacorte, idcliente
	 * @return Long valor del transporte
	 * @throws SAExcepcion 
	 */
	@Override
	public Long calcularTransportePeriodoFacturacion(IFacturaBean ifactura) throws SAExcepcion {
		HashMap<String, Object> params = new HashMap<String, Object> ();
		params.put("fechainicio", new SimpleDateFormat("yyyy/MM/dd").format(ifactura.getFecha_inicio()));
		params.put("fechacorte", new SimpleDateFormat("yyyy/MM/dd").format(ifactura.getFecha_corte()));
		params.put("idcliente", ifactura.getIclienteBean().getId_cliente());
		
		Long transporte = (long) 0;
		List<Object>detalle = asFacturaDao.consultarTransportePeriodoFacturacion(params);
		for(Object c:detalle){
			Utilidades.printResult(c);
			if (c!=null){
				transporte=((BigDecimal) c).longValue();
			}
		}
		return transporte;
	}
	
	
	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * List<IFacturaBean>
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(readOnly = true)
	public List<IFacturaBean> consultarFacturas(IFacturaConsultaBean consultaBean) throws SAExcepcion {
		List<IFacturaDao> dao = new ArrayList<IFacturaDao>();
		if (consultaBean!=null){
			if (consultaBean.getIdFactura()!=null && !consultaBean.getIdFactura().trim().equals("")){
				log.info("consulta de factura por identidicador");
				IFacturaDao factura= asFacturaDao.buscarFactura(Integer.parseInt(consultaBean.getIdFactura()));
				if (factura!=null){
					dao.add(factura);
				} 
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar") && consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				log.info("consulta de factura por identidicador");
				dao.addAll(asFacturaDao.buscarFacturaPorClienteYRangogoFecha(consultaBean.getIdCliente(),consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else if (consultaBean.getIdCliente()!=null && !consultaBean.getIdCliente().trim().equals("Seleccionar")){
				log.info("consulta de factura por identidicador");
				dao.addAll(asFacturaDao.buscarFacturaPorCliente(consultaBean.getIdCliente()));
			}else if (consultaBean.getFechaInicial()!=null && consultaBean.getFechaFinal()!=null){
				log.info("consulta de factura por identidicador");
				dao.addAll(asFacturaDao.buscarFacturaPorRangoFechas(consultaBean.getFechaInicial(),consultaBean.getFechaFinal()));
			}else{
				dao= asFacturaDao.buscarFacturas();
			}
		}
		
		List<IFacturaBean> listaBean = new ArrayList<IFacturaBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IFacturaDAOTOIFacturaBean(dao.get(i)));		
		};		
		return listaBean;
	}

	
	/**
	 * transforma un IDevolucionDao A IDevolucionBean
	 * @param IDevolucionDao 
	 * @return IDevolucionBean
	 */ 
	private IFacturaBean IFacturaDAOTOIFacturaBean(IFacturaDao dao) {
		IFacturaBean bean = (IFacturaBean) new FacturaBean();	
		if (dao!=null){
			bean.setIdFactura(dao.getIdfactura());
			//hay que inicializar el bean de cliente.
			bean.setIclienteBean(ClienteServiceImpl.IClienteDAOTOIClienteBean(dao.getCliente()));
			bean.setIobraBean(ClienteServiceImpl.IObraDAOTOIObraBean(dao.getObra()));
			bean.setFecha_factura(dao.getFecha());
			bean.setFecha_corte(dao.getFecha_corte());
			bean.setFecha_inicio(dao.getFecha_inicio());
			bean.setTransporte((long) dao.getTransporte());
			bean.setObservaciones(dao.getObservaciones());
			bean.setDescuento(dao.getDescuento());
			bean.setReposicion((int) dao.getReposicion());
			bean.setEstado(dao.isEnabled());
			
			for (int k =0; k< dao.getDetallefactura().size();k++){
				IDetalleFacturaBean detalle =  new DetalleFacturaBean();	
				detalle.setCantidad(dao.getDetallefactura().get(k).getCantidad());
				detalle.setNo_dias(dao.getDetallefactura().get(k).getNumeroDias());
				detalle.setCodigo_producto(dao.getDetallefactura().get(k).getProducto().getCodigo_producto());
				detalle.setFecha_final(dao.getDetallefactura().get(k).getFechaCorte());
				detalle.setFecha_inicial(dao.getDetallefactura().get(k).getFechaInicio());
				detalle.setIdFactura(dao.getDetallefactura().get(k).getIdFactura());
				detalle.setVr_unitario(dao.getDetallefactura().get(k).getValorUnitario());
				detalle.setVr_total(dao.getDetallefactura().get(k).calcularImporte());
				detalle.setDescripcion(dao.getDetallefactura().get(k).getProducto().getDescripcion());
				detalle.setTipo(dao.getDetallefactura().get(k).getTipo());
				detalle.setCantidad_gestionada(dao.getDetallefactura().get(k).getCantidad_gestionada());
				detalle.setIproductoBean(ProductoServiceImpl.iProductoDaoTOiProductoBean(dao.getDetallefactura().get(k).getProducto()));
				bean.getListadetalleFactura().add(detalle);
			}
			double iva = ((dao.getVr_parcial()-dao.getDescuento()+bean.getTransporte())*19)/100;
			bean.setIva(iva);
			bean.setVr_parcial(dao.getVr_parcial());
			bean.setTotal(dao.getTotal());
		}	
		return bean;
	}

	/**
	 * Edita la factura recibida por parametro
	 * @param IFacturaBean
	 * @return la misma factura
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IFacturaBean editarFactura(IFacturaBean ifacturaBean) {
		asFacturaDao.editarFactura(IFacturaBeanTOIFacturaDAO(ifacturaBean));
		return ifacturaBean;
	}
	
	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * @throws SAExcepcion 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IFacturaBean obtenerUltimaFactura(IFacturaBean consultaBean) throws SAExcepcion {
		List<IFacturaDao> dao = new ArrayList<IFacturaDao>();
		if (consultaBean!=null){
			if (consultaBean.getIclienteBean().getId_cliente()!=null && !consultaBean.getIclienteBean().getId_cliente().trim().equals("Seleccionar")){
				//consulta de remision por cliente
				System.out.println("[INFO] consulta ultima factura de un cliente para una determinada obra");
				dao.addAll(asFacturaDao.buscarLastFacturaClienteObra(consultaBean.getIclienteBean().getId_cliente(),consultaBean.getIobraBean().getIdObra()));
			}
		}
		
		List<IFacturaBean> listaBean = new ArrayList<IFacturaBean>();
		//recorremos la lista de devolucion 
		for (int i =0; i< dao.size(); i++){
			listaBean.add(IFacturaDAOTOIFacturaBean(dao.get(i)));		
		};
		//si la consulta es null devolvemos vacio
		if  (listaBean.isEmpty()){
			return (IFacturaBean) new FacturaBean();	
		}
		return listaBean.get(0);
	}
	
	/**
	 * agrupar filas  para reporte de cliente PDF
	 * @throws SAExcepcion 
	 */
	@Override
	public IFacturaBean consultarFacturaPDFCliente(IFacturaBean iFactura) throws SAExcepcion {
		IFacturaBean nuevaFactura=iFactura;
		nuevaFactura.getListadetalleFactura().clear();
		
		HashMap<String, Object> params = new HashMap<String, Object> ();
		params.put("idfactura", iFactura.getIdFactura());
		//ifactura.
		List<Object>detalle = asFacturaDao.consultarFacturaIdPDF(params);
		IDetalleFacturaBean items;// inicializacion del detalle de factura que rellenaremos con la consulta
		int vr_unitario;
		int cantidad=0;
		int no_dias=0;
		for (int i=0;i<detalle.size();i++){
			Object[] actual = (Object[]) detalle.get(i);
			Object[] anterior;
			Object[] siguiente;

			Utilidades.printResult(actual);
			
			//asignamos el anterior valor 
			if(i>0){ 
				anterior = (Object[]) detalle.get(i-1);
			}else{
				anterior=null;
			}
			//asignamos el siguiente valor 
			if(i<detalle.size()-1){ 
				siguiente = (Object[]) detalle.get(i+1);
			}else{
				siguiente=null;
			}
			
			items = new DetalleFacturaBean(); 
			//items.setIdFactura(0);
			//items.setIdRemision((Integer) ((Object[])actual)[1]);
			Date finicialActual=null;
			try { 
				finicialActual = (Utilidades.fechaObjectToDate(((Object[])actual)[2]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			items.setTipo(((Object[])actual)[3].toString());
//			items.setIdDevolucion((Integer) ((Object[])actual)[3]);
//			items.setIdReposicion((Integer) ((Object[])actual)[4]);
			int productoActual=(Integer) ((Object[])actual)[0];
			
					
			if (siguiente!=null){
						
					Date finicialSiguiente=null;
					try {
						finicialSiguiente = (Utilidades.fechaObjectToDate(((Object[])siguiente)[2]));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					int productoSiguiente =(Integer) ((Object[])siguiente)[0];
					if (anterior==null){
						cantidad+=((BigDecimal) ((Object[])actual)[5]).intValue();
						
					}else{
						
						int productoAnterior =(Integer) ((Object[])anterior)[0];
						if (productoActual==productoAnterior){
							// toca ver si es devolucion o reposicion
							if (items.getTipo().equals("DEVOLUCION") || items.getTipo().equals("REPOSICION"))  {// restamos
								cantidad+=-((BigDecimal) ((Object[])actual)[5]).intValue();
							}else {
								cantidad+=((BigDecimal) ((Object[])actual)[5]).intValue();
							}	
						}else{
							cantidad=0;
							cantidad=((BigDecimal) ((Object[])actual)[5]).intValue();
						}
					}
					
					if (productoActual==productoSiguiente){
						items.setFecha_final(Utilidades.sumarRestarDiasFecha(finicialSiguiente,-1));
					}else{
						items.setFecha_final(iFactura.getFecha_corte());
					}
				}else{
					if (anterior==null){
						cantidad=((BigDecimal) ((Object[])actual)[5]).intValue();
					}else{
						int productoAnterior =(Integer) ((Object[])anterior)[0];
						if (productoActual==productoAnterior){
							// toca ver si es devolucion o reposicion
							if (items.getTipo().equals("DEVOLUCION") || items.getTipo().equals("REPOSICION"))  {// es una devolucion o reposicion restamos
								cantidad+=-((BigDecimal) ((Object[])actual)[5]).intValue();
							}else {
								cantidad+=((BigDecimal) ((Object[])actual)[5]).intValue();
							}	
						}else{
							cantidad=0;
							cantidad=((BigDecimal) ((Object[])actual)[5]).intValue();
						}
					}	
					items.setFecha_final(iFactura.getFecha_corte());
				}
			
			items.setCantidad(cantidad);
			items.setCodigo_producto((Integer) ((Object[])actual)[0]);
			
			items.setDescripcion(((Object[])actual)[1].toString());
			items.setFecha_inicial(finicialActual);
			no_dias = Utilidades.restarDosFechas(items.getFecha_inicial(), items.getFecha_final())+1;
			items.setNo_dias(no_dias);
			vr_unitario=(Integer) ((Object[])actual)[4];
			items.setVr_unitario(vr_unitario);
			items.setVr_total((double) (items.getVr_unitario()*items.getNo_dias()*items.getCantidad()));
			if (items.getVr_total()!=0){
				nuevaFactura.getListadetalleFactura().add(items);
			}	
			items=null;
			
			
		} // fin for
		
		return nuevaFactura;
	}
	

	/**
	 * permiste consultar remisiones por rango de fechas, idRemision, cliente.
	 * @return devuelve todas las remisiones segun parametros de consulta
	 * IRemisionConsultaBean consultaBean 
	 * 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public IFacturaBean consultarFactura(int idFactura) {
		//consulta de remision por identidicador
		Logger.getLogger(FacturaService.class.getName()).log(Level.INFO, "consulta de factura por id" +idFactura);
		IFacturaDao factura= asFacturaDao.buscarFactura(idFactura);
		return IFacturaDAOTOIFacturaBean(factura);
	}
	
}