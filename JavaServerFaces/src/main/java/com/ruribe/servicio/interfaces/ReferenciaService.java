package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IReferenciaBean;

/**
 * @author RONIE
 */

public interface ReferenciaService{
 
	public void crearReferencia(IReferenciaBean iReferencia);

	public void editarReferencia(IReferenciaBean iReferencia);
	
	public List<IReferenciaBean> editarReferencia(List<IReferenciaBean> ListaiReferencia) throws SAExcepcion;
	
	public List<IReferenciaBean> obtenerReferencias() throws SAExcepcion;

	public List<IReferenciaBean> obtenerReferencia(IReferenciaBean iReferencia);

	public void borrarReferencia(IReferenciaBean iReferencia);

	public IReferenciaBean obtenerReferenciaPK(String idReferenciaSelect);

	public List<IReferenciaBean> obtenerReferenciasByDescripcion(String filtro) throws SAExcepcion;;

 
}