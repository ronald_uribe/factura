package com.ruribe.servicio.interfaces;


import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.DevolucionBean;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionConsultaBean;

public interface DevolucionService{

	public IDevolucionBean crearDevolucion(IDevolucionBean iDevolucion);

	public void editarDevolucion(IDevolucionBean iDevolucion);
	
	public void borrarDevolucion(IDevolucionBean iDevolucion);

	public List<IDevolucionBean> consultarDevoluciones() throws SAExcepcion;
	
	public List<IDevolucionBean> consultarDevoluciones(IDevolucionConsultaBean consultaBean) throws SAExcepcion;

	public List<IDetalleDevolucionBean> obtenerProductosPendientesPorCliente(String idclienteSelect, int idobraSelect) throws SAExcepcion;

	public DevolucionBean consultarDevolucionesPDF(int idDevolucion) throws SAExcepcion;

	List<IDevolucionBean> consultarAllSaldosPendientes() throws SAExcepcion;

	public boolean devolucionIsPrinted(int idDevolucion);
	
	public void marcarDevolucionImpresa(int idDevolucion);
 
}