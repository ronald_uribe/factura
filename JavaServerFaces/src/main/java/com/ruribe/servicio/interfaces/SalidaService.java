package com.ruribe.servicio.interfaces;


import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.ISalidaBean;
import com.ruribe.util.bean.interfaces.ISalidaConsultaBean;

public interface SalidaService{

	public ISalidaBean crearSalida(ISalidaBean iSalida);

	public void editarSalida(ISalidaBean iSalida);
	
	public void borrarSalida(ISalidaBean iSalida);

	public List<ISalidaBean> consultarSalidas(ISalidaConsultaBean consultaBean) throws SAExcepcion;

	public ISalidaBean consultarIDSalida(ISalidaBean iSalida);

	public List<IDetalleSalidaBean> obtenerProductosPendientesPorProveedor(String idproveedorSelect) throws SAExcepcion;
 
}