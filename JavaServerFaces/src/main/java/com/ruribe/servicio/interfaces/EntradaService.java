package com.ruribe.servicio.interfaces;


import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IEntradaBean;
import com.ruribe.util.bean.interfaces.IEntradaConsultaBean;

public interface EntradaService{

	public IEntradaBean crearEntrada(IEntradaBean iEntrada);

	public void editarEntrada(IEntradaBean iEntrada);
	
	public void borrarEntrada(IEntradaBean iEntrada);

	public List<IEntradaBean> consultarEntradas(IEntradaConsultaBean consultaBean) throws SAExcepcion;

	public IEntradaBean consultarIDEntrada(IEntradaBean iEntrada);
 
}