package com.ruribe.servicio.interfaces;


import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.ReposicionBean;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionConsultaBean;

public interface ReposicionService{

	public IReposicionBean crearReposicion(IReposicionBean iReposicion);

	public void editarReposicion(IReposicionBean iReposicion);
	
	public void borrarReposicion(IReposicionBean iReposicion);

	public List<IReposicionBean> consultarReposiciones() throws SAExcepcion;
	
	public List<IReposicionBean> consultarReposiciones(IReposicionConsultaBean consultaBean) throws SAExcepcion;

	public List<IDetalleReposicionBean> obtenerProductosPendientesPorCliente(String idclienteSelect, String idobraSelect) throws SAExcepcion;

	public ReposicionBean consultarReposicionesPDF(int idReposicion) throws SAExcepcion;
 
}