package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.ICiudadBean;

public interface CiudadService{
 
	/**
	 * devuelve todas las ciudades
	 * @return List ITipoDocumentoBean
	 * @throws SAExcepcion 
	 */
	public List<ICiudadBean> obtenerCiudad() throws SAExcepcion;
	
	/**
	 * devuelve una ciudad por identificador
	 * @return ICiudadBean
	 * @throws SAExcepcion 
	 */
	public ICiudadBean obtenerCiudad(ICiudadBean iciudad) throws SAExcepcion;


}