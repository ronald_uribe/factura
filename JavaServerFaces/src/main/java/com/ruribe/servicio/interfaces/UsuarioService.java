package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUsuarioBean;

public interface UsuarioService{
 
	public void crearUsuario(IUsuarioBean iusuario);

	public void editarUsuario(IUsuarioBean iusuario);
	
	public List<IUsuarioBean> editarUsuario(List<IUsuarioBean> Listaiusuario) throws SAExcepcion;
	
	public List<IUsuarioBean> obtenerUsuarios() throws SAExcepcion;

	public List<IUsuarioBean> obtenerUsuario(IUsuarioBean iusuario) throws SAExcepcion;
	
	public IUsuarioBean obtenerUsuarioByUsername(String username) throws SAExcepcion;

	public void borrarUsuario(IUsuarioBean iusuario);

	public boolean login(String username, String password) throws SAExcepcion;

	public boolean validarUsername(String valor) throws SAExcepcion;
 
	public boolean modificarPassword(String username, String generateMD5Signature) throws SAExcepcion;

	/**
	 * devuelve todos los usuarios
	 * @throws SAExcepcion 
	 */
	List<IRoleBean> obtenerRoles() throws SAExcepcion;
 
}