package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.ITransportadorBean;

public interface TransportadorService{

	public void crearTransportador(ITransportadorBean itransportador);

	public void editarTransportador(ITransportadorBean itransportador);
	
	public List<ITransportadorBean> editarTransportador(List<ITransportadorBean> Listaitransportador) throws SAExcepcion;
	
	public List<ITransportadorBean> obtenerTransportadores() throws SAExcepcion;

	public List<ITransportadorBean> obtenerTransportador(ITransportadorBean itransportador) throws SAExcepcion;

	public void borrarTransportador(ITransportadorBean itransportador);
 
}