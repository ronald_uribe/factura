package com.ruribe.servicio.interfaces;



import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaConsultaBean;

public interface FacturaService{

	public IFacturaBean crearFactura(IFacturaBean iFactura);
	
	/*** carga los datos de una factura por una fecha de corte
	 * @throws SAExcepcion */
	public IFacturaBean procesarFactura(IFacturaBean iFactura) throws SAExcepcion;

	/**calcula el transporte de un periodo de facturacion para un cliente
	 * @throws SAExcepcion */
	public Long calcularTransportePeriodoFacturacion(IFacturaBean ifactura) throws SAExcepcion;
	
	/**
	 * consultar facturas segun parametros de entrada. id, rango de fechas, cliente.
	 * @param consultaBean
	 * @return List<IDevolucionBean>
	 * @throws SAExcepcion 
	 */
	public List<IFacturaBean> consultarFacturas(IFacturaConsultaBean consultaBean) throws SAExcepcion;

	/**
	 * editar una factura
	 * @param ifacturaBean
	 * @return la factura modificada
	 */
	public IFacturaBean editarFactura(IFacturaBean ifacturaBean);

	/**
	 * obtiene la ultima factura de un cliente
	 * @param ifacturaBean
	 * @return IFacturaBean
	 * @throws SAExcepcion 
	 */
	public IFacturaBean obtenerUltimaFactura(IFacturaBean ifacturaBean) throws SAExcepcion;

	public IFacturaBean consultarFacturaPDFCliente(IFacturaBean iFacturaBean) throws SAExcepcion;

	public IFacturaBean consultarFactura(int idFactura);

}