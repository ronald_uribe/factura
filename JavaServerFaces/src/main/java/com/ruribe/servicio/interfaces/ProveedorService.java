package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IProveedorBean;

public interface ProveedorService{

	public void crearProveedor(IProveedorBean iproveedor);

	public void editarProveedor(IProveedorBean iproveedor);
	
	public List<IProveedorBean> editarProveedor(List<IProveedorBean> Listaiproveedor) throws SAExcepcion;
	
	public List<IProveedorBean> obtenerProveedores() throws SAExcepcion;

	public List<IProveedorBean> obtenerProveedor(IProveedorBean iproveedor) throws SAExcepcion;

	public void borrarProveedor(IProveedorBean iproveedor);

}