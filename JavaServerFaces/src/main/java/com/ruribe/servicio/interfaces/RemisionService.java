package com.ruribe.servicio.interfaces;


import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IRemisionBean;
import com.ruribe.util.bean.interfaces.IRemisionConsultaBean;

public interface RemisionService{

	public IRemisionBean crearRemision(IRemisionBean iRemision);

	public void editarRemision(IRemisionBean iRemision);
	
	public void borrarRemision(IRemisionBean iRemision);

	public List<IRemisionBean> consultarRemisiones(IRemisionConsultaBean consultaBean) throws SAExcepcion;

	public IRemisionBean consultarRemisionPDFCliente(IRemisionBean remisionBean) throws SAExcepcion;
	
	public IRemisionBean consultarIDRemision(IRemisionBean iRemision);

	public boolean remisionIsPrinted(int idRemision);

	public void marcarRemisionImpresa(int idRemision);
 
}