package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;

public interface ProductoService{
 
	public void crearProducto(IProductoBean iProducto);

	public void editarProducto(IProductoBean iProducto);
	
	public List<IProductoBean> editarProducto(List<IProductoBean> ListaiProducto) throws SAExcepcion;
	
	public List<IProductoBean> obtenerProductos() throws SAExcepcion;

	public List<IProductoBean> obtenerProducto(IProductoBean iProducto) throws SAExcepcion;

	public void borrarProducto(IProductoBean iProducto);

	public List<ITipoProductoBean> obtenerTipoProducto() throws SAExcepcion;

	public List<IProductoBean> obtenerProductosPorTipo(int idTipoProductoSelect) throws SAExcepcion;
	
	public List<IProductoBean> buscarProductoPorTipoProductoConStock(int idTipoProductoSelect) throws SAExcepcion;
	

	public IProductoBean obtenerProductosPK(String idProductoSelect);
	
	/**obtiene los productos de un proveedor 
	 * @throws SAExcepcion */
	public List<IProductoBean> obtenerProductosPorTipoProveedor(int idTipoProductoSelect, String codigo_proveedor) throws SAExcepcion;

	public List<IProductoBean> obtenerProductosByDescripcion(String filtro) throws SAExcepcion;

 
}