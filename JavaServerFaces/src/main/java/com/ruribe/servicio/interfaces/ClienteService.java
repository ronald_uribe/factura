package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;

public interface ClienteService{
 
	public void crearCliente(IClienteBean iCliente);

	public void editarCliente(IClienteBean iCliente);
	
	public List<IClienteBean> editarCliente(List<IClienteBean> ListaiCliente) throws SAExcepcion;
	
	public List<IClienteBean> obtenerClientes() throws SAExcepcion;

	public List<IClienteBean> obtenerCliente(IClienteBean iCliente);

	public void borrarCliente(IClienteBean iCliente);
	
	public void vincularObraCliente(IObraBean iObra);

	public void desvincularObra(IObraBean iObra);

	public List<IObraBean> obtenerObrasCliente(String idclienteSelect) throws SAExcepcion;

	public void modificaObraCliente(IObraBean iObra);

	public IClienteBean obtenerClientePK(String idclienteSelect);

	public IObraBean obtenerObraPK(int idobraSelect) throws SAExcepcion;

	public List<IClienteBean> obtenerClientesByRazonSocial(String findcliente) throws SAExcepcion;
 
}