package com.ruribe.servicio.interfaces;

import java.util.List;

import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;

public interface TipoDocumentoService{
 
	/**
	 * devuelve todos los tipos de documentos
	 * @return List ITipoDocumentoBean
	 * @throws SAExcepcion 
	 */
	public List<ITipoDocumentoBean> obtenerTipoDocumentos() throws SAExcepcion;
	
	/**
	 * devuelve un tipo de documento por identificador
	 * @return ITipoDocumentoBean
	 */
	public List<ITipoDocumentoBean> obtenerTipoDocumento(ITipoDocumentoBean iusuario);


}