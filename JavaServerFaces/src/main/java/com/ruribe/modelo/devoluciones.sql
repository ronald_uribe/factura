-- lo que el cliente se ha llevado a fecha de hoy menos devoluciones y resposiciones
-- se ha encontrado un error en esta SQL, cuando hay reposiciones.
select  d.id_remision,r.id_cliente,r.id_obra,d.codigo_producto,d.cantidad , coalesce(sum(dev.cantidad),0) , coalesce(sum(rep.cantidad),0) as debe
from remision r 
inner join detalle_remision d on (r.id_remision = d.id_remision and r.fecha <= '2019-12-30'
                                  )
left outer join detalle_devolucion dev on (d.id_remision = dev.id_remision and 
										   d.codigo_producto = dev.codigo_producto)
left outer join detalle_reposicion rep on (d.id_remision = rep.id_remision and 
										   d.codigo_producto = rep.codigo_producto)
where   
	  r.id_cliente ='123456' 
group by d.id_remision,r.id_cliente,r.id_obra,d.codigo_producto,d.cantidad
having debe > 0
order by d.codigo_producto;




