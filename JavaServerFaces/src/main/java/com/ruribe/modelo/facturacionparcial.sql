SELECT distinct remision.id_remision,remision.codigo_producto,remision.descripcion,remision.precio_alquiler,remision.cantidad,coalesce(sum(devolucion.cantidad),0) as dev, remision.cantidad-coalesce(sum(devolucion.cantidad),0)-coalesce(sum(reposicion.cantidad),0) AS debe,coalesce(sum(reposicion.cantidad),0)     
FROM (
		SELECT distinct r.id_remision,r.fecha,dr.codigo_producto,dr.cantidad,p.descripcion,p.precio_alquiler
		FROM remision r inner join 
			 detalle_remision dr on (r.id_remision=dr.id_remision AND
									 r.fecha <= '2020-01-01' and 
                                     r.id_cliente ='Y2252797F' )
			 left outer join producto p on (dr.codigo_producto = p.codigo_producto)                       
	 ) as remision 
	LEFT OUTER JOIN 
		(SELECT d.fecha,dd.codigo_producto,dd.id_remision,dd.cantidad
		 FROM devolucion d inner join 
			  detalle_devolucion dd on (d.id_devolucion=dd.id_devolucion 
               -- AND  d.fecha >= '2019-01-14' 
              AND d.fecha < '2020-01-30')
		 ) as devolucion 
	ON (remision.id_remision = devolucion.id_remision and 
		remision.codigo_producto = devolucion.codigo_producto)
 LEFT OUTER JOIN (SELECT rp.id_reposicion,rp.fecha,drp.codigo_producto,drp.id_remision,drp.cantidad
		 FROM reposicion rp inner join 
			  detalle_reposicion drp on (rp.id_reposicion=drp.id_reposicion AND 
				rp.fecha <= '2020-01-30')
		 ) as reposicion 
	 ON (remision.id_remision = reposicion.id_remision and 
		 remision.codigo_producto = reposicion.codigo_producto )     
GROUP BY remision.id_remision,remision.codigo_producto,remision.cantidad,remision.descripcion,remision.precio_alquiler       
 having debe > 0 
order by remision.descripcion;