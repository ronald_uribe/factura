-- productos pendientes a fecha inicial de la fecha de facturacion ACUMULACION
-- todos los productos que se ha llevado entre la fecha inicial y final REMISION
-- todos los productos que ha devuelto a fecha de hoy DEVOLUCIONES
-- REPOSICIONES
SELECT factura.fecha, 
factura.id_remision, 
factura.codigo_producto,factura.cantidad, factura.tipo, P.DESCRIPCION,P.PRECIO_ALQUILER, factura.id
FROM (
	SELECT 	
		r.fecha,
		dr.id_remision,
        dr.codigo_producto,
        dr.cantidad- 
        (SELECT coalesce(SUM(cantidad),0)
						FROM DEVOLUCION d INNER JOIN 
						 DETALLE_DEVOLUCION DD on (d.id_devolucion =  dd.id_devolucion AND d.fecha < '2019-12-30')
						WHERE  DD.ID_REMISION = dr.id_remision AND
								DD.codigo_producto = Dr.codigo_producto
                                ) 
		as cantidad, 'ACUMULADO' as tipo,
        '' as id
FROM remision r INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision)
WHERE r.id_cliente = '123456' 
AND r.fecha < '2019-12-01'
AND dr.cantidad > (SELECT coalesce(SUM(cantidad),0)
					FROM DEVOLUCION d INNER JOIN 
						 DETALLE_DEVOLUCION DD on (d.id_devolucion =  dd.id_devolucion AND d.fecha < '2019-12-01' )
                    WHERE  DD.ID_REMISION = dr.id_remision AND
							DD.codigo_producto = Dr.codigo_producto )
UNION 
select r.fecha,dr.id_remision,dr.codigo_producto,cantidad,'REMISION',r.id_remision  
	from remision r INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision)
	WHERE r.id_cliente = '123456'  
    AND r.fecha >= '2019-12-01' and r.fecha <= '2019-12-30'   
UNION 
select d.fecha,dd.id_remision,dd.codigo_producto,cantidad,'DEVOLUCION',d.id_devolucion  
	from devolucion d INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion)
	WHERE d.id_cliente = '123456'  
    AND d.fecha >= '2019-12-01' and d.fecha <= '2019-12-30'
UNION    
SELECT rp.fecha,drp.id_remision,drp.codigo_producto,cantidad,'REPOSICION',rp.id_reposicion  
FROM reposicion rp INNER JOIN detalle_reposicion drp ON (rp.id_reposicion = drp.id_reposicion)
WHERE rp.id_cliente = '123456'  
AND rp.fecha >= '2019-12-01' and rp.fecha <= '2019-12-30'
) FACTURA  LEFT OUTER JOIN producto p ON (p.codigo_producto=FACTURA.codigo_producto)
ORDER BY p.descripcion, id_remision, FACTURA.codigo_producto,fecha asc;


	
