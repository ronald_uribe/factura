-- productos pendientes a fecha inicial de la fecha de facturacion
-- todos los productos que se ha llevado entre la fecha inicial y final
-- todos los productos que ha devuelto a fecha de hoy
-- falta calcular los productos pendientes a la fecha final.
SELECT factura.fecha, 
factura.id_remision, 
factura.codigo_producto,factura.cantidad, factura.tipo, P.DESCRIPCION,P.PRECIO_ALQUILER
FROM (
	SELECT 	
		r.fecha,
		dr.id_remision,
        dr.codigo_producto,
        dr.cantidad- 
        (SELECT coalesce(SUM(cantidad),0)
						FROM DEVOLUCION d INNER JOIN 
						 DETALLE_DEVOLUCION DD on (d.id_devolucion =  dd.id_devolucion AND d.fecha < '2019-01-01')
						WHERE  DD.ID_REMISION = dr.id_remision AND
								DD.codigo_producto = Dr.codigo_producto
                                ) 
		as cantidad, 'ACUMULADO' as tipo
FROM remision r INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision)
WHERE r.id_cliente = 'Y2252797F' 
AND r.fecha < '2019-01-01'
AND dr.cantidad > (SELECT coalesce(SUM(cantidad),0)
					FROM DEVOLUCION d INNER JOIN 
						 DETALLE_DEVOLUCION DD on (d.id_devolucion =  dd.id_devolucion AND d.fecha < '2019-01-01' )
                    WHERE  DD.ID_REMISION = dr.id_remision AND
							DD.codigo_producto = Dr.codigo_producto )
UNION 
select r.fecha,dr.id_remision,dr.codigo_producto,cantidad,'REMISION'  
	from remision r INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision)
	WHERE r.id_cliente = 'Y2252797F'  
    AND r.fecha >= '2019-01-01' and r.fecha <= '2019-01-30'   
UNION 
select d.fecha,dd.id_remision,dd.codigo_producto,cantidad,'DEVOLUCION'  
	from devolucion d INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion)
	WHERE d.id_cliente = 'Y2252797F'  
    AND d.fecha >= '2019-01-01' and d.fecha <= '2019-01-30'
UNION    
select rp.fecha,drp.id_reposicion,drp.codigo_producto,cantidad,'REPOSICION'  
from reposicion rp INNER JOIN detalle_reposicion drp ON (rp.id_reposicion = drp.id_reposicion)
WHERE rp.id_cliente = 'Y2252797F'  
AND rp.fecha >= '2019-01-01' and rp.fecha <= '2019-01-30'
) FACTURA  LEFT OUTER JOIN producto p ON (p.codigo_producto=FACTURA.codigo_producto)
ORDER BY p.descripcion, id_remision, FACTURA.codigo_producto,fecha asc;


	
