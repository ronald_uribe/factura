SELECT  r.codigo_proveedor, 
dr.codigo_producto, 
p.descripcion,
sum(dr.cantidad) - coalesce((SELECT sum(dd.cantidad ) 
							 FROM salida d INNER JOIN detalle_salida dd ON (d.id_salida = dd.id_salida) 
                             WHERE 	d.codigo_proveedor = r.codigo_proveedor and 
                                    dr.codigo_producto=dd.codigo_producto 
							GROUP BY dd.codigo_producto),0)
	AS debe 
FROM entrada r inner join detalle_entrada dr on (r.id_entrada = dr.id_entrada)  
LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto) 
WHERE r.codigo_proveedor = 'RU'
GROUP BY r.codigo_proveedor, dr.codigo_producto, p.descripcion 
HAVING debe > 0 
ORDER BY p.descripcion;
