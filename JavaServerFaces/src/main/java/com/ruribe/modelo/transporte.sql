-- Transporte de un periodo de facturacion, ida y regreso
SELECT SUM(t.transporte) 
FROM (	SELECT  SUM(r.transporte) AS transporte
		FROM remision r 
		WHERE  DATE_FORMAT(r.fecha,'%d/%m/%Y') >= '2019-01-14' and DATE_FORMAT(r.fecha,'%d/%m/%Y') <= '2019-01-21' AND id_cliente ='830.509.497-4'
		UNION 
		SELECT  SUM(d.transporte) AS transporte
		FROM devolucion d 
		WHERE  	d.fecha>= '2019-01-14' 	AND  d.fecha <= '2019-01-21 00:01:00' AND id_cliente ='830.509.497-4') as t;
	 