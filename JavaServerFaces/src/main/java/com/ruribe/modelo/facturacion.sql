-- periodo de facturacion a esto hay que anadirle los pendientes
SELECT dr.codigo_producto,r.id_remision,
p.descripcion,dr.cantidad,dd.cantidad,
coalesce(DATE_FORMAT(r.fecha,'%d/%m/%Y'),null) as inicio,
coalesce(DATE_FORMAT(d.fecha,'%d/%m/%Y'),null) as corte,
p.precio_alquiler,d.id_devolucion
FROM remision r 
INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision and 
								   -- r.fecha>='2020-01-01 00:01:00' and 
								   r.fecha<='2020-01-30 00:01:00' and 
                                   r.id_cliente= 'Y2252797F') 
LEFT OUTER JOIN  detalle_devolucion dd on (dr.id_remision = dd.id_remision 
											and dr.codigo_producto = dd.codigo_producto)
LEFT JOIN devolucion d ON (d.id_devolucion = dd.id_devolucion and 
								   d.fecha<='2020-01-30 00:01:00')
LEFT OUTER JOIN producto p ON (p.codigo_producto=dr.codigo_producto)
ORDER BY p.descripcion,r.id_remision,d.fecha,r.fecha