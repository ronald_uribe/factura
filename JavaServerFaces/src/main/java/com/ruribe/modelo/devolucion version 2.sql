-- lo que el cliente se ha llevado a fecha de hoy menos devoluciones y resposiciones
select 	r.id_remision,
		r.id_cliente,
        r.id_obra,
		dr.codigo_producto,
        p.descripcion,
        dr.cantidad - coalesce((select sum(dd.cantidad )
								from devolucion d inner join detalle_devolucion dd on (d.id_devolucion = dd.id_devolucion)
								where d.id_cliente = '123456' and 
									dr.id_remision=dd.id_remision and 
									dr.codigo_producto=dd.codigo_producto
								group by dd.id_remision,dd.codigo_producto),0)- 
						coalesce((select sum(drp.cantidad) 
									from reposicion rp inner join detalle_reposicion drp on (rp.id_reposicion = drp.id_reposicion)
									where rp.id_cliente = '123456' and 
										dr.id_remision=drp.id_remision and 
										dr.codigo_producto=drp.codigo_producto
									group by drp.id_remision,drp.codigo_producto ),0) AS debe
from remision r inner join detalle_remision dr on (r.id_remision = dr.id_remision) 
left outer join producto p on (p.codigo_producto=dr.codigo_producto)
where r.id_cliente = '123456'
group by r.id_remision,dr.codigo_producto
having debe > 0
