SELECT 	r.id_remision, 
		r.id_cliente, 
        r.id_obra,
        dr.codigo_producto, 
        p.descripcion,
        dr.cantidad - coalesce((SELECT sum(dd.cantidad ) 
								FROM devolucion d 
                                INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion) 
                                WHERE 	d.id_cliente = r.id_cliente and 
										r.id_obra = r.id_obra and 
                                        dr.id_remision=dd.id_remision and 
                                        dr.codigo_producto=dd.codigo_producto 
								GROUP BY dd.id_remision,dd.codigo_producto),0)
					- coalesce((SELECT sum(drp.cantidad) 
								FROM reposicion rp 
                                INNER JOIN detalle_reposicion drp ON (rp.id_reposicion = drp.id_reposicion) 
                                WHERE 	rp.id_cliente = r.id_cliente  and 
										r.id_obra = r.id_obra and 
                                        dr.id_remision=drp.id_remision and 
                                        dr.codigo_producto=drp.codigo_producto 
								GROUP BY drp.id_remision,drp.codigo_producto ),0) 
		AS debe 
FROM remision r 
INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision)  
LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto) 
-- WHERE r.id_cliente = '901.027.298-3' and r.id_obra = '68' 
GROUP BY r.id_remision,dr.codigo_producto 
HAVING debe > 0 
ORDER BY r.id_cliente,p.descripcion,r.id_remision
-- bind => [901.027.298-3, 68, 901.027.298-3, 68, 901.027.298-3, 68]