SELECT r.id_remision,r.id_cliente,r.fecha,r.id_transportador,r.transporte,r.observaciones,r.id_obra,sum(d.cantidad),p.codigo_interno,ref.precio_alquiler,ref.descripcion, ref.peso 
FROM soloandamios.remision r 
inner join detalle_remision d on (r.id_remision=d.id_remision )
inner join producto p on (d.codigo_producto = p.codigo_producto)
inner join referencia ref on (p.codigo_interno=ref.codigo_interno)
where r.id_remision = 153
group by id_remision,id_cliente,fecha,id_transportador,transporte,observaciones,id_obra,codigo_interno,precio_alquiler,descripcion ;
