package com.ruribe.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
 
@Configuration 
@PropertySource("classpath:ficherosConfiguracion/configuracion.properties")
@ComponentScan(basePackages = {"ficherosConfiguracion"})
public class Propiedades {
	static final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Propiedades.class);
//	@Autowired
//	Environment enviroment;
//	
//	
//	 @Bean
//     public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
//        return new PropertySourcesPlaceholderConfigurer();
//     }
	
//	 Constructor
		public Propiedades() {
			
		}
		
		/**
		 * Lee una propiedad
		 * @param miClave
		 * @return Valor de la propiedad
		 * @throws FileNotFoundException
		 * @throws IOException
		 */
		public static String leerPropiedad(String miClave){
			return applicationContext.getEnvironment().getProperty(miClave);
		}
		
	public static void main(String[] args) throws IOException{
		
		Properties prop = new Properties();
		InputStream is = null;
	
		
		try {
			//is = new FileInputStream("classpath:ficherosConfiguracion/configuracion.properties");
			URL resource = ClassLoader.getSystemResource("ficherosConfiguracion/configuracion.properties");
		    prop.load(new FileReader(new File(resource.getFile())));
		    System.out.println("JDBC URL: " + prop.get("SHARED_REMISION"));
			is=ClassLoader.getSystemResourceAsStream("ficherosConfiguracion/configuracion.properties");
			prop.load(is);
		} catch(IOException e) {
			System.out.println(e.toString());
		}
 
		// Acceder a las propiedades por su nombre
		System.out.println("Propiedades por nombre:");
		System.out.println("-----------------------");
		System.out.println(prop.getProperty("SHARED_REMISION"));
		
		// Recorrer todas sin conocer los nombres de las propiedades
		System.out.println("Recorrer todas las propiedades:");
		System.out.println("-------------------------------");
 
		for (Enumeration<?> e = prop.keys(); e.hasMoreElements() ; ) {
			// Obtenemos el objeto
			Object obj = e.nextElement();
			System.out.println(obj + ": " + prop.getProperty(obj.toString()));
		}
	}



}
