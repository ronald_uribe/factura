package com.ruribe.util;

/**
 * Interfaz que define los metodos necesarios para que una clase pueda ser objeto de traza.
 * @author ruribe
 *
 */
public interface ITrazableSA {

		
		/**
		 * Devuelve el codigo del elemento trazable.
		 * @return Codigo del elemento trazable.
		 */
		public String getCodigo();
		
		/**
		 * Devuelve el mensaje del elemento trazable.
		 * @return Mensaje del elemento trazable.
		 */
		public String getMensaje();
		
		/**
		 * Devuelve el mensaje del elemento trazable para ser logado.
		 * @return Mensaje del elemento trazable para ser logado.
		 */
		public String getMensajeLog();	
		
		/**
		 * Devuelve el enumerado del elemento trazable.
		 * @return TrazasEval del elemento trazable.
		 */
		public TrazasSA getTrazasSA();
}
