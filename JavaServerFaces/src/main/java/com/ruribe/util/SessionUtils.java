package com.ruribe.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

	public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	public static String getUserName() {
		return getSession().getAttribute("username").toString();
	}

	public static String getUserId() {
		HttpSession session = getSession();
		if (session != null)
			return (String) session.getAttribute("userid");
		else
			return null;
	}
	/**
	 * 
	 * @param mensaje
	 */
	public static void addWarn (String mensaje){
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,mensaje,mensaje));
	}
	/**
	 * 
	 * @param mensaje
	 */
	public static void addInfo (String mensaje){
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,mensaje,mensaje));
	}
	
	/**
	 * 
	 * @param mensaje
	 */
	public static void addError (String mensaje){
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,mensaje,mensaje));
	}
	
	/**
	 * 
	 * @param mensaje
	 */
	public static void addFatalError (String mensaje){
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,mensaje,mensaje));
	}
	
	/**
	 * 
	 * @param mensaje
	 */
	public static void addError (Severity severityInfo,String mensaje){
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(severityInfo,mensaje,mensaje));
	}
}
