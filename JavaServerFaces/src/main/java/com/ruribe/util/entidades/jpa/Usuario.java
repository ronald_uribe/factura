package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import com.ruribe.util.entidades.jpa.interfaces.IUsuarioDao;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL",
			   query="SELECT u FROM Usuario u"),
	@NamedQuery(name="QUERY_SELECT_ID",
				query="select a from Usuario a where a.id = :id"),
	@NamedQuery(name="QUERY_USERNAME",
	query="select a from Usuario a where a.username = :username"),
	@NamedQuery(name="QUERY_SELECT_ID_LIKE",
				query="select a from Usuario a where a.username like :valor or a.nombre like :valor"),
	@NamedQuery(name="QUERY_LOGIN", query="select u from Usuario u where u.username = :username and u.clave = :clave and u.enabled=1")
})
public class Usuario implements IUsuarioDao, Serializable { 
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL = "QUERY_FIND_ALL";
	public static final String QUERY_SELECT_ID ="QUERY_SELECT_ID";
	public static final String QUERY_SELECT_ID_LIKE ="QUERY_SELECT_ID_LIKE";
	public static final String QUERY_LOGIN = "QUERY_LOGIN";
	public static final String QUERY_USERNAME = "QUERY_USERNAME";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String apellido;
	private String nombre;
	private String telefono;
	@Column(nullable = false, unique = true)
	private String username;
	@Column(nullable = false)
	private String clave;
	private Boolean enabled;
		
	@ManyToMany
	  @JoinTable(name="USUARIO_ROLE", 
	  joinColumns=@JoinColumn(name="USUARIO_ID",referencedColumnName="id"),
	  inverseJoinColumns=@JoinColumn(name="ROLE_ID",referencedColumnName="idrole"))
	private List<Role> roles;

	
	public Usuario() {
		roles= new ArrayList<Role>();
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

}