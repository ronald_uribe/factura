package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the detalle_remision database table.
 * 
 */
@Embeddable
public class DetalleSalidaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_salida", insertable=false, updatable=false )
	private int idSalida;

	@Column(name="codigo_producto", insertable=false, updatable=false )
	private int codigoProducto;

	public DetalleSalidaPK() {
	}
	
	/**
	 * @param idSalida
	 * @param codigoProducto
	 */
	public DetalleSalidaPK(int idSalida, int codigoProducto) {
		super();
		this.idSalida = idSalida;
		this.codigoProducto = codigoProducto;
	}

	public int getIdSalida() {
		return this.idSalida;
	}
	public void setIdSalida(int idSalida) {
		this.idSalida = idSalida;
	}
	public int getCodigoProducto() {
		return this.codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigoProducto;
		result = prime * result + idSalida;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleSalidaPK other = (DetalleSalidaPK) obj;
		if (codigoProducto != other.codigoProducto)
			return false;
		if (idSalida != other.idSalida)
			return false;
		return true;
	}
}