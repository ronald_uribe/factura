package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import com.ruribe.util.entidades.jpa.interfaces.IClienteDao;

/**
 * The persistent class for the products database table.
 * 
 */
@Entity
@Table(name="cliente")

@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_CLIENTE", query="SELECT u FROM Cliente u ORDER BY u.razon_social") ,
    @NamedQuery(name="QUERY_FIND_RAZON_SOCIAL", query="SELECT u FROM Cliente u where u.razon_social like :razonSocial ORDER BY u.razon_social")
})
public class Cliente implements IClienteDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL_CLIENTE = "QUERY_FIND_ALL_CLIENTE";
	public static final String QUERY_FIND_RAZON_SOCIAL="QUERY_FIND_RAZON_SOCIAL";

	@Id
	private String id_cliente;
	
	private int cod_tipo_documento;
	private String razon_social;
	private String direccion;
	private int cod_ciudad;
	private String telefono;
	private String celular;
	private String email;
	private Boolean enabled;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="cliente",cascade={CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.REFRESH},orphanRemoval=true)
	private List<Obra> listaObras;
	
	public Cliente() {
		super();
		listaObras = new ArrayList<Obra>();
	}

	/**
	 * @return the id_cliente
	 */
	public String getId_cliente() {
		return id_cliente;
	}


	/**
	 * @param id_cliente the id_cliente to set
	 */
	public void setId_cliente(String id_cliente) {
		this.id_cliente = id_cliente;
	}


	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}


	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}


	/**
	 * @return the razon_social
	 */
	public String getRazon_social() {
		return razon_social;
	}


	/**
	 * @param razon_social the razon_social to set
	 */
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}


	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}


	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	/**
	 * @return the cod_ciudad
	 */
	public int getCod_ciudad() {
		return cod_ciudad;
	}


	/**
	 * @param cod_ciudad the cod_ciudad to set
	 */
	public void setCod_ciudad(int cod_ciudad) {
		this.cod_ciudad = cod_ciudad;
	}


	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}


	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}


	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the listaObras
	 */
	public List<Obra> getListaObras() {
		return listaObras;
	}

	/**
	 * @param listaObras the listaObras to set
	 */
	public void setListaObras(Obra listaObras) {
		this.listaObras.add(listaObras);
	}

	/**
	 * @param listaObras the listaObras to set
	 */
	public void setListaObras(List<Obra> listaObras) {
		this.listaObras = listaObras;
	}


	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((celular == null) ? 0 : celular.hashCode());
		result = prime * result + cod_ciudad;
		result = prime * result + cod_tipo_documento;
		result = prime * result
				+ ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((id_cliente == null) ? 0 : id_cliente.hashCode());
//		result = prime * result
//				+ ((listaObras == null) ? 0 : listaObras.hashCode());
		result = prime * result
				+ ((razon_social == null) ? 0 : razon_social.hashCode());
		result = prime * result
				+ ((telefono == null) ? 0 : telefono.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (celular == null) {
			if (other.celular != null)
				return false;
		} else if (!celular.equals(other.celular))
			return false;
		if (cod_ciudad != other.cod_ciudad)
			return false;
		if (cod_tipo_documento != other.cod_tipo_documento)
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id_cliente == null) {
			if (other.id_cliente != null)
				return false;
		} else if (!id_cliente.equals(other.id_cliente))
			return false;
		if (listaObras == null) {
			if (other.listaObras != null)
				return false;
		} else if (!listaObras.equals(other.listaObras))
			return false;
		if (razon_social == null) {
			if (other.razon_social != null)
				return false;
		} else if (!razon_social.equals(other.razon_social))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Cliente [id_cliente=" + id_cliente + ", cod_tipo_documento="
				+ cod_tipo_documento + ", razon_social=" + razon_social
				+ ", direccion=" + direccion + ", cod_ciudad=" + cod_ciudad
				+ ", telefono=" + telefono + ", celular=" + celular
				+ ", email=" + email + ", listaObras=" + listaObras + "]";
	}

	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			
			
			// crear main para consulta de obras 

			TypedQuery <Cliente> clientes= em.createNamedQuery(Cliente.QUERY_FIND_ALL_CLIENTE,Cliente.class);
			 for(Cliente c:clientes.getResultList()){
				 System.out.println("ID:" + c.getId_cliente());
				 System.out.println("Cliente:" + c.getRazon_social());
			 
				 for(Obra o:c.getListaObras()){
					 System.out.println("nombre de obra:" + o.getNombre());
				 } 
			 }
			 
			 em.getTransaction().commit();
		      em.close();
		      emf.close();
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}



}