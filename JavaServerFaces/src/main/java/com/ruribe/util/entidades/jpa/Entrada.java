package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Persistence;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.ruribe.util.entidades.jpa.interfaces.IEntradaDao;


/**
 * The persistent class for the entrada database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Entrada.findAll", query="SELECT r FROM Entrada r order by r.idEntrada desc"),
	@NamedQuery(name="Entrada.pk", query="SELECT r FROM Entrada r join r.detalleEntradas d where r.idEntrada =:id_entrada order by d.producto.descripcion"),
	@NamedQuery(name="Entrada.rangofechas", query="SELECT r FROM Entrada r where r.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Entrada.proveedor.fecha", query="SELECT r FROM Entrada r where r.proveedor.codigo_proveedor =:idProveedor and r.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Entrada.proveedor", query="SELECT r FROM Entrada r where r.proveedor.codigo_proveedor =:idProveedor")})

public class Entrada implements Serializable, IEntradaDao {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL_ENTRADAS = "Entrada.findAll";
	public static final String QUERY_FIND_RANGO_FECHAS = "Entrada.rangofechas";
	public static final String QUERY_FIND_ENTRADA_PROVEEDOR = "Entrada.proveedor";
	public static final String QUERY_FIND_PK_ENTRADA = "Entrada.pk";
	public static final String QUERY_FIND_ENTRADA_PROVEEDOR_RANGO_FECHAS = "Entrada.proveedor.fecha";
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_entrada")
	private int idEntrada;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@OneToOne
	@JoinColumn(name="codigo_proveedor")
	private Proveedor proveedor;
	
	private String usuario_creacion;
	
	private boolean enabled;
		
	//bi-directional many-to-one association to DetalleEntrada
	@OneToMany(mappedBy="entrada",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	@OrderBy("producto ASC")
	private List<DetalleEntrada> detalleEntradas;

	public Entrada() {
		detalleEntradas = new ArrayList<DetalleEntrada>();
	}

	public int getIdEntrada() {
		return this.idEntrada;
	}

	public void setIdEntrada(int idEntrada) {
		this.idEntrada = idEntrada;
	}
	
	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<DetalleEntrada> getDetalleEntradas() {
		return this.detalleEntradas;
	}

	public void setDetalleEntradas(List<DetalleEntrada> detalleEntradas) {
		this.detalleEntradas = detalleEntradas;
	}

	public DetalleEntrada addDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().add(detalleEntrada);
		return detalleEntrada;
	}

	public DetalleEntrada removeDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().remove(detalleEntrada);
		return detalleEntrada;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			 Entrada entrada = new Entrada();
			 
			 Proveedor proveedor = new Proveedor();
			 proveedor.setCodigo_proveedor("RU");
			 entrada.setProveedor(proveedor);
			 entrada.setFecha(new Date());
			 entrada.setUsuario_creacion("ronald");
			 Producto producto = new Producto();
			 producto.setCodigo_producto(6);
			 DetalleEntrada detalle = new DetalleEntrada();
			 detalle.setCantidad(3);
			 detalle.setEntrada(entrada);
			 detalle.setProducto(producto);
			 entrada.addDetalleEntrada(detalle);
			 em.persist(entrada); 
			 em.flush();
			 //return abc.getId();
			 System.out.println("#######id de entrada:" + entrada.getIdEntrada());
			 em.getTransaction().commit();
			 em.close();
		     emf = Persistence.createEntityManagerFactory("JavaServerFaces");
		     em = emf.createEntityManager();
		     em.getTransaction( ).begin( );
			TypedQuery <Entrada> entradaes= em.createNamedQuery(Entrada.QUERY_FIND_ALL_ENTRADAS,Entrada.class);
			 for(Entrada c:entradaes.getResultList()){
				System.out.println("ID ENTRADA:"+ c.getIdEntrada());
			 } 
			 em.getTransaction().commit();
			 em.close();
		     emf.close();
			 
		      
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}
}