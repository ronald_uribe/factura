/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import java.util.List;

import com.ruribe.util.entidades.jpa.Role;

/**
 *Interfaz del entity Usuario.
 * @author RONI
 *
 */
public interface IUsuarioDao {
	public Long getId();
	public void setId(Long id);
	public String getApellido();
	public void setApellido(String apellido1);
	public String getNombre();
	public void setNombre(String nombre);
	public String getTelefono();
	public void setTelefono(String telefono);
	public String getUsername();
	public void setUsername(String username);
	public String getClave();
	public void setClave(String clave);
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);
	public List<Role> getRoles();
	public void setRoles(List<Role> roles);


}
