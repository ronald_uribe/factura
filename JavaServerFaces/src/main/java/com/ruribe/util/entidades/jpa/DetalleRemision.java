package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.ruribe.util.entidades.jpa.interfaces.IDetalleRemisionDao;


/**
 * The persistent class for the detalle_remision database table.
 * 
 */
@Entity
@Table(name="detalle_remision")
@NamedQuery(name="DetalleRemision.findAll", query="SELECT d FROM DetalleRemision d")
public class DetalleRemision implements IDetalleRemisionDao, Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleRemisionPK detalleRemisionPK;

	private int cantidad;
	
	//bi-directional many-to-one association to Remision
	@ManyToOne
	@JoinColumn(name="id_remision")
	private Remision remision;
	
	//bi-directional many-to-one association to Remision
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto;
	
//	@OneToMany(mappedBy="detalleremision",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
//	private List<DetalleDevolucion> listaDetdevoluciones;
//	
//	@OneToMany(mappedBy="detalleremision",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
//	private List<DetalleReposicion> listaDetreposiciones;

	public DetalleRemision() {
		detalleRemisionPK = new DetalleRemisionPK();
		remision = new Remision();
		producto = new Producto();
	}
	public DetalleRemision(int remisionId, int productoId) {
		detalleRemisionPK = new DetalleRemisionPK(remisionId, productoId);
    }
	
	public DetalleRemisionPK getId() {
		return this.detalleRemisionPK;
	}

	public void setId(DetalleRemisionPK detalleRemisionPK) {
		this.detalleRemisionPK = detalleRemisionPK;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	
	public Remision getRemision() {
		return this.remision;
	}

	public void setRemision(Remision remision) {
		this.remision = remision;
	}
	
	@Override
	public final int getIdRemision() {
		return detalleRemisionPK.getIdRemision();
	}
	@Override
	public final void setIdRemision(int idRemision) {
		this.detalleRemisionPK.setIdRemision(idRemision);
		
	}
	@Override
	public final int getCodigoProducto() {
		
		return detalleRemisionPK.getCodigoProducto();
	}
	@Override
	public final void setCodigoProducto(int codigoProducto) {
		this.detalleRemisionPK.setCodigoProducto(codigoProducto);
		
	}
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
//	/**
//	 * @return the listaDetdevoluciones
//	 */
//	public List<DetalleDevolucion> getListaDetdevoluciones() {
//		return listaDetdevoluciones;
//	}
//	/**
//	 * @param listaDetdevoluciones the listaDetdevoluciones to set
//	 */
//	public void setListaDetdevoluciones(List<DetalleDevolucion> listaDetdevoluciones) {
//		this.listaDetdevoluciones = listaDetdevoluciones;
//	}

}