/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.Proveedor;
import com.ruribe.util.entidades.jpa.Referencia;
import com.ruribe.util.entidades.jpa.TipoProducto;


/**
 *Interfaz del entity Usuario.
 * @author RONI
 *
 */
public interface IProductoDao {
	
	
	public int getCodigo_producto();
	public void setCodigo_producto(int codigo_producto);
	public  String getDescripcion();
	public  void setDescripcion(String descripcion);
	public Referencia getReferencia();
	public void setReferencia(Referencia referencia);
	public  int getStock();
	public  void setStock(int stock);
	public int getSubarriendo();
	public void setSubarriendo(int subarriendo);
	public TipoProducto getTipo_producto();
	public void setTipo_producto(TipoProducto tipo_producto);
	public int getMaximo();
	public void setMaximo(int maximo);
	public int getMinimo();
	public void setMinimo(int minimo);
	public Proveedor getProveedor();
	public void setProveedor(Proveedor proveedor);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);

}
