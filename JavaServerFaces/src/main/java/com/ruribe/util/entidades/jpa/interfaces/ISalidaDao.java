package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;

import com.ruribe.util.entidades.jpa.DetalleSalida;
import com.ruribe.util.entidades.jpa.Proveedor;


/**
 *Interfaz del entity entrada.
 * @author RONI
 *
 */
public interface ISalidaDao {

	public int getIdSalida();
	public void setIdSalida(int idSalida);
	public Proveedor getProveedor();
	public void setProveedor(Proveedor proveedor);
	public Date getFecha();
	public void setFecha(Date fecha);
	public List<DetalleSalida> getDetalleSalidas();
	public void setDetalleSalidas(List<DetalleSalida> detalleSalidas);
	public DetalleSalida addDetalleSalida(DetalleSalida detalleSalida);
	public DetalleSalida removeDetalleSalida(DetalleSalida detalleSalida);
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);

}
