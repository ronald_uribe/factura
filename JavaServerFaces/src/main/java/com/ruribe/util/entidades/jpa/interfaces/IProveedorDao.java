/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity Proveedor.
 * @author RONI
 *
 */
public interface IProveedorDao {
	
	public String getNo_documento();
	public void setNo_documento(String no_documento);
	public int getCod_tipo_documento();
	public void setCod_tipo_documento(int cod_tipo_documento);
	public String getRazon_social();
	public void setRazon_social(String razon_social);
	public String getDireccion();
	public void setDireccion(String direccion);
	public int getCod_ciudad();
	public void setCod_ciudad(int cod_ciudad);
	public String getTelefono();
	public void setTelefono(String telefono);
	public String getCelular();
	public void setCelular(String celular);
	public String getEmail();
	public void setEmail(String email);
	public String getCodigo_proveedor();
	public void setCodigo_proveedor(String codigo);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);

}
