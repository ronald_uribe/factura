package com.ruribe.util.entidades.jpa.interfaces;

/**
 * Interfaz de Tipo de documento de usuario
 */

public interface ITipoDocumentoDao {
	
	public int getId_tipo_documento();
	public void setId_tipo_documento(int tipoDocumento);	
	public String getDescripcion();
	public void setDescripcion(String descripcion);

}
