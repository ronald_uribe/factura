/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.DetalleSalidaPK;
import com.ruribe.util.entidades.jpa.Salida;
import com.ruribe.util.entidades.jpa.Producto;


/**
 *Interfaz del entity IDetalleFacturaPK.
 * @author RONI
 *
 */
public interface IDetalleSalidaDao extends IDetalleSalidaPKDao {
	
	public DetalleSalidaPK getId();
	public void setId(DetalleSalidaPK detalleSalidaPK);
	public int getCantidad();
	public void setCantidad(int cantidad);
	public Salida getSalida();
	public void setSalida(Salida entrada);
	public int getIdSalida();
	public void setIdSalida(int idSalida);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);
	public Producto getProducto();
	public void setProducto(Producto producto);

}
