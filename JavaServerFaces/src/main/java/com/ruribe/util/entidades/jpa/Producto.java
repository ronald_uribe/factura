package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.UniqueConstraint;

import com.ruribe.util.entidades.jpa.interfaces.IProductoDao;


/**
 * The persistent class for the products database table.
 * 
 */
@Entity
@Table(name="producto", uniqueConstraints= {@UniqueConstraint(columnNames= {"referencia,proveedor"})})
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_PRODUCTO", query="SELECT u FROM Producto u order by u.descripcion"),
	@NamedQuery(name="QUERY_FIND_PRODUCTO_BY_DESCRIPCION", query="SELECT u FROM Producto u where u.descripcion like :filtro order by u.descripcion"),
	@NamedQuery(name="QUERY_SELECT_ID_PRODUCTO", query="select a from Producto a where a.codigo_producto = :codigo_producto order by a.descripcion"),
	@NamedQuery(name="QUERY_SELECT_ID_LIKE_PRODUCTO", query="select a from Producto a where a.codigo_producto like :codigo_producto order by a.descripcion"),
	@NamedQuery(name="QUERY_FIND_PRODUCTO_POR_TIPO_CON_STOCK", query="select a from Producto a join a.tipo_producto t where t.cod_tipo_producto like :cod_tipo_producto and a.stock >0 order by a.descripcion"),
	@NamedQuery(name="QUERY_FIND_ALL_PRODUCTO_POR_TIPO", query="select a from Producto a join a.tipo_producto t where t.cod_tipo_producto like :cod_tipo_producto order by a.descripcion"),
	@NamedQuery(name="QUERY_FIND_PRODUCTO_POR_TIPO_PROVEEDOR", query="select a from Producto a join a.tipo_producto t join a.proveedor pro where t.cod_tipo_producto like :cod_tipo_producto and pro.codigo_proveedor= :codigoProveedor order by a.descripcion")
})

public class Producto implements Serializable, IProductoDao { 
	private static final long serialVersionUID = 1L;
 
	public static final String QUERY_SELECT_ID_LIKE = "QUERY_SELECT_ID_LIKE_PRODUCTO";
	public static final String QUERY_FIND_PRODUCTO_POR_TIPO_CON_STOCK = "QUERY_FIND_PRODUCTO_POR_TIPO_CON_STOCK";
	public static final String QUERY_FIND_ALL_PRODUCTO_POR_TIPO = "QUERY_FIND_ALL_PRODUCTO_POR_TIPO";
	public static final String QUERY_FIND_PRODUCTO_POR_TIPO_PROVEEDOR = "QUERY_FIND_PRODUCTO_POR_TIPO_PROVEEDOR";

	public static final String QUERY_FIND_PRODUCTO_BY_DESCRIPCION = "QUERY_FIND_PRODUCTO_BY_DESCRIPCION";
	public static final String QUERY_FIND_ALL_PRODUCTO="QUERY_FIND_ALL_PRODUCTO";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigo_producto; 

	@ManyToOne
	@JoinColumn(name="cod_tipo_producto")
	private TipoProducto tipo_producto;
	
	private String descripcion;
	
	@ManyToOne
	@JoinColumn(name="codigo_proveedor")
	private Proveedor proveedor;
	
	@ManyToOne
	@JoinColumn(name="codigo_interno")
	private Referencia referencia; 
	private int stock; 
	private int maximo; 
	private int minimo; 
	private int subarriendo;
	private boolean enabled;
	
	/**
	 * Constructor 
	 */
	public Producto() {
		super();
	}
	
	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_producto() {
		return codigo_producto;
	}
	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}
	
	/**
	 * @return the referencia
	 */
	public Referencia getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the stock
	 */
	public final int getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public final void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * @return the tipo_producto
	 */
	public final TipoProducto getTipo_producto() {
		return tipo_producto;
	}
	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public final void setTipo_producto(TipoProducto tipo_producto) {
		this.tipo_producto = tipo_producto;
	}
	
	/**
	 * @return the maximo
	 */
	public final int getMaximo() {
		return maximo;
	}
	/**
	 * @param maximo the maximo to set
	 */
	public final void setMaximo(int maximo) {
		this.maximo = maximo;
	}
	/**
	 * @return the minimo
	 */
	public final int getMinimo() {
		return minimo;
	}
	/**
	 * @param minimo the minimo to set
	 */
	public final void setMinimo(int minimo) {
		this.minimo = minimo;
	}
	/**
	 * @return the proveedor
	 */
	public final Proveedor getProveedor() {
		return proveedor;
	}
	/**
	 * @param proveedor the proveedor to set
	 */
	public final void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	/**
	 * @return the subarriendo
	 */
	public final int getSubarriendo() {
		return subarriendo;
	}
	/**
	 * @param subarriendo the subarriendo to set
	 */
	public final void setSubarriendo(int subarriendo) {
		this.subarriendo = subarriendo;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			// TipoProducto tp = new TipoProducto();
			// em.persist(tp);
			// em.persist(detalle);

			TypedQuery <Producto> ps= em.createNamedQuery(Producto.QUERY_FIND_ALL_PRODUCTO_POR_TIPO,Producto.class);
			ps.setParameter("cod_tipo_producto", 1); 
			for(Producto c:ps.getResultList()){
				 System.out.println("cod:" + c.getCodigo_producto());
				 System.out.println("des:" + c.getDescripcion());
			 } 
			 
//			 TypedQuery <TipoProducto> tps1= em.createNamedQuery(TipoProducto.QUERY_SELECT_ID_TIPO_PRODUCTO,TipoProducto.class);
//			 tps1.setParameter("cod_tipo_producto", 1);
//			 for(TipoProducto c:tps1.getResultList()){
//				 System.out.println("cod:" + c.getCod_tipo_producto());
//				 System.out.println("des:" + c.getDescripcion());
//				 System.out.println("lista:" + c.getListaProductos().size());
//			 } 
			 
			 em.getTransaction().commit();
		     em.close();
		     emf.close();
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}

}