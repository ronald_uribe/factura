package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;

import com.ruribe.util.entidades.jpa.interfaces.IReferenciaDao;


/**
 * The persistent class for the products database table.
 * 
 */
@Entity
@Table(name="referencia")
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_REFERENCIA", query="SELECT r FROM Referencia r order by r.descripcion"),
	@NamedQuery(name="QUERY_FIND_REFERENCIA_BY_DESCRIPCION", query="SELECT r FROM Referencia r where r.descripcion like :filtro order by r.descripcion")
			 })

public class Referencia implements Serializable, IReferenciaDao { 
	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_ALL_REFERENCIA = "QUERY_FIND_ALL_REFERENCIA";

	public static final String QUERY_FIND_REFERENCIA_BY_DESCRIPCION = "QUERY_FIND_REFERENCIA_BY_DESCRIPCION";
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigo_interno; 
	@Column(unique=true)
	private String descripcion;
	private int precio_alquiler;
	private int precio_reposicion;
	private double peso; 
	
	@ManyToOne
	@JoinColumn(name="cod_tipo_producto")
	private TipoProducto tipo_producto;
	
	 
	/**
	 * Constructor 
	 */
	public Referencia() {
		super();
	}
	
	
	
	/**
	 * @return the codigo_interno
	 */
	public int getCodigo_interno() {
		return codigo_interno;
	}



	/**
	 * @param codigo_interno the codigo_interno to set
	 */
	public void setCodigo_interno(int codigo_interno) {
		this.codigo_interno = codigo_interno;
	}



	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	/**
	 * @return the precio_alquiler
	 */
	public int getPrecio_alquiler() {
		return precio_alquiler;
	}



	/**
	 * @param precio_alquiler the precio_alquiler to set
	 */
	public void setPrecio_alquiler(int precio_alquiler) {
		this.precio_alquiler = precio_alquiler;
	}



	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion() {
		return precio_reposicion;
	}



	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion) {
		this.precio_reposicion = precio_reposicion;
	}
	
	/**
	 * @return the peso
	 */
	public final double getPeso() {
		return peso;
	}
	/**
	 * @param peso the peso to set
	 */
	public final void setPeso(double peso) {
		this.peso = peso;
	}
	
	/**
	 * @return the tipo_producto
	 */
	public final TipoProducto getTipo_producto() {
		return tipo_producto;
	}
	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public final void setTipo_producto(TipoProducto tipo_producto) {
		this.tipo_producto = tipo_producto;
	}



	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction().begin();
			 System.out.println("Got Connection."); 
			 //Referencia ref =  em.find(Referencia.class, 1);
			 Referencia ref =  new Referencia();
			 ref.setCodigo_interno(1);
			 TipoProducto tipo = new TipoProducto();
			 tipo.setCod_tipo_producto(1);
			 ref.setTipo_producto(tipo);
			 ref.setDescripcion("ronald uribe");
			 System.out.println("ref:"+ref.getDescripcion());
			 ref.setPrecio_alquiler(2500);
			 ref.setPrecio_reposicion(12850);
			 ref.setPeso(10.0);
			 em.merge(ref);
			 em.getTransaction().commit();
		     em.close();
		     emf.close();
		     System.out.println("Got update."); 
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}

}