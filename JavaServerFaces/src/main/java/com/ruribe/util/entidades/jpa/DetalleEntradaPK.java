package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the detalle_remision database table.
 * 
 */
@Embeddable
public class DetalleEntradaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_entrada", insertable=false, updatable=false )
	private int idEntrada;

	@Column(name="codigo_producto", insertable=false, updatable=false )
	private int codigoProducto;

	public DetalleEntradaPK() {
	}
	
	/**
	 * @param idEntrada
	 * @param codigoProducto
	 */
	public DetalleEntradaPK(int idEntrada, int codigoProducto) {
		super();
		this.idEntrada = idEntrada;
		this.codigoProducto = codigoProducto;
	}

	public int getIdEntrada() {
		return this.idEntrada;
	}
	public void setIdEntrada(int idEntrada) {
		this.idEntrada = idEntrada;
	}
	public int getCodigoProducto() {
		return this.codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigoProducto;
		result = prime * result + idEntrada;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleEntradaPK other = (DetalleEntradaPK) obj;
		if (codigoProducto != other.codigoProducto)
			return false;
		if (idEntrada != other.idEntrada)
			return false;
		return true;
	}
}