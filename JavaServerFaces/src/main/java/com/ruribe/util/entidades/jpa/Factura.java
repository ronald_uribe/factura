package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ruribe.util.entidades.jpa.interfaces.IFacturaDao;

/**
 * The persistent class for the factura database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Factura.findAll", query="SELECT f FROM Factura f order by f.idfactura desc"),
	@NamedQuery(name="Factura.pk", query="SELECT f FROM Factura f where f.idfactura =:id_factura"),
	@NamedQuery(name="Factura.lastFacturaClienteObra", query="SELECT f FROM Factura f where f.cliente.id_cliente =:idCliente and f.obra.idObra =:idObra order by f.idfactura desc"),
	@NamedQuery(name="Factura.rangofechas", query="SELECT f FROM Factura f where f.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Factura.cliente.fecha", query="SELECT f FROM Factura f where f.cliente.id_cliente =:idCliente and f.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Factura.cliente", query="SELECT f FROM Factura f where f.cliente.id_cliente =:idCliente")})

@NamedNativeQueries({
	@NamedNativeQuery(name = "procesarCorte",query = 
						"select  factura.fecha,"
						+ " factura.codigo_producto, "
						+ "factura.debe,"
						+ "factura.tipo,"
						+ "P.DESCRIPCION,"
						+ "ref.PRECIO_ALQUILER, "
						+ "factura.id "
						+ "FROM "
								+ "(select ?fechainicio AS fecha,"
								+ "dr.codigo_producto,"
								+ " coalesce(SUM(dr.cantidad),0) - "
									+ "coalesce((select SUM(dd.cantidad) "
									+ "FROM devolucion d "
									+ "INNER JOIN detalle_devolucion dd  ON (d.id_devolucion=dd.id_devolucion) "
									+ "where d.fecha < ?fechainicio "
									+ "AND dd.codigo_producto=dr.codigo_producto "
									+ "AND d.id_cliente=r.id_cliente "
									+ "AND d.id_obra=r.id_obra "
									+ "GROUP BY dd.codigo_producto),0) " 
				                    + "- "
									+ "	 coalesce((select SUM(drp.cantidad) "  
									+ "	from reposicion rp  "
									+ "	inner join detalle_reposicion drp  on (rp.id_reposicion=drp.id_reposicion)  "
									+ "	where rp.fecha < ?fechainicio  "
				                    + " and drp.codigo_producto=dr.codigo_producto "
									+ "	and rp.id_cliente=r.id_cliente "
				                    + "    and rp.id_obra=r.id_obra "
									+ "	group by drp.codigo_producto),0) "
				+ "	as debe , 'ACUMULADO' as tipo, '0' as id "
				+ "    from remision r  "
				+ "    inner join detalle_remision dr  on (r.id_remision=dr.id_remision) " 
				+ "    where r.fecha < ?fechainicio and r.id_obra=?idobra and r.id_cliente=?idcliente "
				+ "    group by dr.codigo_producto  "
				+ "    having debe > 0 "
			+ "	UNION  select r.fecha,dr.codigo_producto,sum(cantidad),'REMISION','0' as id  "  	
			+ "			from remision r  "
			+ "			INNER JOIN detalle_remision dr ON (r.id_remision = dr.id_remision) 	 "   
			+ "			where r.fecha >= ?fechainicio and r.fecha <= ?fechacorte  "
			+ "	        and r.id_obra=?idobra and r.id_cliente=?idcliente "
			+ "			GROUP BY r.fecha,dr.codigo_producto "
			+ "	UNION  select d.fecha,dd.codigo_producto,sum(cantidad),'DEVOLUCION', '0' as id "   	
			+ "			from devolucion d "
			+ "			INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion) 	  "  
			+ "			where d.fecha >= ?fechainicio and d.fecha <= ?fechacorte  "
			+ "	        and d.id_obra=?idobra and d.id_cliente=?idcliente "
			+ "			group by d.fecha,dd.codigo_producto"
			+ "	UNION  select rp.fecha,drp.codigo_producto,sum(cantidad),'REPOSICION'  , '0' as id 	 "
			+ "			from reposicion rp "
			+ "			INNER JOIN detalle_reposicion drp ON (rp.id_reposicion = drp.id_reposicion)  "	   
			+ "			where rp.fecha >= ?fechainicio and rp.fecha <= ?fechacorte  "
			+ "	        and rp.id_obra=?idobra and rp.id_cliente=?idcliente "
			+ "			group by rp.fecha,drp.codigo_producto"
			+ "	) factura  "
			+ "	LEFT OUTER JOIN producto p ON (p.codigo_producto=factura.codigo_producto)   "
			+ "	LEFT OUTER JOIN referencia ref ON (p.codigo_interno = ref.codigo_interno) "
			+ "	ORDER BY p.descripcion, factura.codigo_producto,fecha asc  "),
	@NamedNativeQuery(name="Factura.PDFCliente", query ="select 	"
				+ "p.codigo_interno,"
				+ "ref.descripcion,"
				+ "d.fecha_inicio,"
				+ "d.tipo, "
				+ "d.valor_unitario,"
				+ "sum(d.cantidad_gestionada) "
				+ "FROM soloandamios.detalle_factura d "
				+ "inner join producto p on (d.codigo_producto = p.codigo_producto) "
				+ "inner join referencia ref on (ref.codigo_interno = p.codigo_interno) "
				+ "where id_factura= ?idfactura "
				+ "group by p.codigo_interno,d.fecha_inicio, d.tipo,d.valor_unitario "
				+ "order by codigo_interno,fecha_inicio"),
	@NamedNativeQuery(name = "procesarCorte1",query = "select d.id_remision, DATE_FORMAT(r.fecha,'%d/%m/%Y') as inicio,"
				+ "coalesce(DATE_FORMAT(de.fecha,'%d/%m/%Y'),null) as corte,"
				+ "r.id_cliente,r.id_obra,p.descripcion,d.cantidad, p.precio_alquiler,d.codigo_producto, sum(dev.cantidad)"
			+ "from remision r "
			+ "left outer join detalle_remision d on (r.id_remision = d.id_remision) "
			+ "left outer join detalle_devolucion dev on (d.id_remision = dev.id_remision and d.codigo_producto = dev.codigo_producto) "
			+ "left outer join producto p on (d.codigo_producto = p.codigo_producto) "
			+ "left outer join devolucion de on (dev.id_devolucion = de.id_devolucion and de.fecha < ?fechacorte) "
			+ "where r.fecha between ?fechainicio and ?fechacorte and r.id_cliente =?idcliente "
			+ "group by d.id_remision,r.fecha,de.fecha,r.id_cliente,r.id_obra,d.codigo_producto,d.cantidad "
			+ "order by p.descripcion,r.fecha;"),
	@NamedNativeQuery(name="transporte_periodo_facturacion", query="SELECT SUM(t.transporte) "
			+ "FROM (	SELECT  SUM(r.transporte) AS transporte "
					+ "FROM remision r "
					+ "WHERE  DATE_FORMAT(r.fecha,'%Y/%m/%d') >= ?fechainicio AND DATE_FORMAT(r.fecha,'%Y/%m/%d')<= ?fechacorte AND id_cliente =?idcliente "
					+ "UNION  "
					+ "SELECT  SUM(d.transporte) AS transporte "
					+ "FROM devolucion d  "
					+ "WHERE  	DATE_FORMAT(d.fecha,'%Y/%m/%d')>= ?fechainicio 	AND  "
					+ "DATE_FORMAT(d.fecha,'%Y/%m/%d') <= ?fechacorte AND "
					+ "id_cliente =?idcliente) as t;"),
	@NamedNativeQuery(name="periodo_facturacion_parcial", 
	query="SELECT remision.id_remision,remision.codigo_producto,remision.descripcion,remision.precio_alquiler,remision.cantidad-coalesce(sum(devolucion.cantidad),0)- coalesce(sum(reposicion.cantidad),0) AS debe,coalesce(sum(reposicion.cantidad),0),DATE_FORMAT(remision.fecha,'%d/%m/%Y') "
			+ "FROM (SELECT distinct r.id_remision,r.fecha,dr.codigo_producto,dr.cantidad,p.descripcion,p.precio_alquiler "
					+ "FROM remision r inner join "
					+ "detalle_remision dr on (r.id_remision=dr.id_remision AND "
											+ "DATE_FORMAT(r.fecha,'%Y/%m/%d')= ?fechainicio and r.id_cliente =?idcliente )"
					+ " left outer join producto p on (dr.codigo_producto = p.codigo_producto)                       "
					+ ") as remision "
			+ "LEFT OUTER JOIN "
				+ "(SELECT d.id_devolucion,d.fecha,dd.codigo_producto,dd.id_remision,dd.cantidad "
				+ " FROM devolucion d inner join "
				+ "detalle_devolucion dd on (d.id_devolucion=dd.id_devolucion AND DATE_FORMAT(d.fecha,'%Y/%m/%d') < ?fechacorte) "
			+ ") as devolucion "
			+ "ON (remision.id_remision = devolucion.id_remision and "
			+ "remision.codigo_producto = devolucion.codigo_producto) "
			+ "LEFT OUTER JOIN (SELECT rp.id_reposicion,rp.fecha,drp.codigo_producto,drp.id_remision,drp.cantidad"
			+ " FROM reposicion rp inner join "
			+ "detalle_reposicion drp on (rp.id_reposicion=drp.id_reposicion AND "
			+ "DATE_FORMAT(rp.fecha,'%Y/%m/%d') <= ?fechacorte)"
			+ " ) as reposicion "
			+ " ON (remision.id_remision = reposicion.id_remision and "
			+ "remision.codigo_producto = reposicion.codigo_producto )     "
			+ "GROUP BY remision.id_remision,remision.codigo_producto,remision.cantidad,remision.descripcion,remision.precio_alquiler,remision.fecha "
			+ "having debe > 0 order by remision.descripcion;")				
	})
public class Factura implements IFacturaDao, Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_ALL_FACTURAS = "Factura.findAll";
	public static final String QUERY_PROCESAR_CORTE = "procesarCorte";
	public static final String QUERY_TRANSPORTE_PERIODO_FACT = "transporte_periodo_facturacion";
	public static final String QUERY_FACT_PARCIAL = "periodo_facturacion_parcial";

	public static final String QUERY_FIND_FACTURA_CLIENTE_OBRA = "Factura.lastFacturaClienteObra";

	public static final String QUERY_PROCESAR_PDF = "Factura.PDFCliente";
	
	public static String QUERY_FIND_PK_FACTURA="Factura.pk";
	public static String QUERY_FIND_RANGO_FECHAS="Factura.rangofechas";
	public static String QUERY_FIND_FACTURA_CLIENTE="Factura.cliente";
	public static String QUERY_FIND_FACTURA_CLIENTE_RANGO_FECHAS="Factura.cliente.fecha";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idfactura;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_inicio;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_corte;

	@OneToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	@OneToOne
	@JoinColumn(name="id_obra")
	private Obra obra;

	private double iva;

	private String observaciones;

	private double descuento;
	private double reposicion;

	private int transporte;
	private boolean enabled;
	private String usuario_creacion;
	
	//bi-directional many-to-one association to DetalleDevolucion
	@OneToMany(mappedBy="factura",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	@OrderBy("producto ASC, fechaInicio ASC, fechaCorte ASC")
	private List<DetalleFactura> detallefactura;

	public Factura() {
		detallefactura = new ArrayList<DetalleFactura>();
	}

	public int getIdfactura() {
		return this.idfactura;
	}

	public void setIdfactura(int idfactura) {
		this.idfactura = idfactura;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	/**
	 * @return the fecha_inicio
	 */
	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	/**
	 * @param fecha_inicio the fecha_inicio to set
	 */
	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	/**
	 * @return the fecha_corte
	 */
	public Date getFecha_corte() {
		return fecha_corte;
	}

	/**
	 * @param fecha_corte the fecha_corte to set
	 */
	public void setFecha_corte(Date fecha_corte) {
		this.fecha_corte = fecha_corte;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the obra
	 */
	public Obra getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(Obra obra) {
		this.obra = obra;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public int getTransporte() {
		return this.transporte;
	}

	public void setTransporte(int transporte) {
		this.transporte = transporte;
	}
	
	@Override
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion=usuario_creacion;
	}

	@Override
	public String getUsuario_creacion() {
		return this.usuario_creacion;
	}
	
	/**
	 * @return the detallefactura
	 */
	public List<DetalleFactura> getDetallefactura() {
		return detallefactura;
	}

	/**
	 * @param detallefactura the detallefactura to set
	 */
	public void setDetallefactura(List<DetalleFactura> detallefactura) {
		this.detallefactura = detallefactura;
	}
	
	public DetalleFactura addDetalleDevolucion(DetalleFactura detalleFactura) {
		getDetallefactura().add(detalleFactura);

		return detalleFactura;
	}

	public DetalleFactura removeDetalleDevolucion(DetalleFactura detalleFactura) {
		getDetallefactura().remove(detalleFactura);

		return detalleFactura;
	}

	/**
	 * @return the descuento
	 */
	public double getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return the reposicion
	 */
	public double getReposicion() {
		return reposicion;
	}

	/**
	 * @param reposicion the reposicion to set
	 */
	public void setReposicion(double reposicion) {
		this.reposicion = reposicion;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * calculo del importe total decuentos, IVA, ...
	 * @return Double valor total de la factura.
	 */
	public Double getTotal() {
		Double total = 0.0;

		int size = detallefactura.size();

		for (int i = 0; i < size; i++) {
			total += detallefactura.get(i).calcularImporte();
		}
		return total+transporte+iva-descuento;
		//bean.setTotal((long) (bean.getVr_parcial()+bean.getTransporte()+iva-dao.getDescuento()));
	}
	
	
	/**
	 * calculo del importe total decuentos, IVA, ...
	 * @return Double valor total de la factura.
	 */
	public Double getVr_parcial() {
		Double total = 0.0;

		int size = detallefactura.size();

		for (int i = 0; i < size; i++) {
			total += detallefactura.get(i).calcularImporte();
		}
		return total;
	}
	

	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			 Factura factura = new Factura();
			 Cliente cliente = new Cliente();
			 cliente.setId_cliente("900.539.520-8");
			 factura.setCliente(cliente);
			 factura.setTransporte(512000);
			 factura.setFecha(new Date());
			 factura.setIva(21);
			 factura.setObservaciones("observaciones");
			
			 DetalleFactura detalle = new DetalleFactura();
			 detalle.setCantidad(5);
			 detalle.setFechaCorte(new Date());
			 detalle.setFechaInicio(new Date());
			 detalle.setValorUnitario(2000);
			// detalle.setCodigoProducto(6);
			 detalle.setFactura(factura);
			 factura.addDetalleDevolucion(detalle);
			// em.persist(factura); 
			// em.flush();
			System.out.println("#######id de factura:" + factura.getIdfactura());
			 em.getTransaction().commit();
			 em.close();
		      emf.close();
		      
		       emf = Persistence.createEntityManagerFactory("JavaServerFaces");
				  em = emf.createEntityManager();
				 em.getTransaction( ).begin( );
				 Query q = em.createNamedQuery(Factura.QUERY_PROCESAR_CORTE);
				 
				 q.setParameter("corte", "2019-01-25");
				 q.setParameter("fechainicio", "2019-01-01");
				 q.setParameter("fechacorte", "2019-01-25");
				 q.setParameter("idcliente", "830.509.497-4");

//				 List<Object[]> facturas = q.getResultList();
//			 for(Object c:facturas){
//				 System.out.println("remision:" + ((Object[])c)[0]);
//				 
//			 } 
			 em.getTransaction().commit();
			 em.close();
		      emf.close();
			 
		      
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}



}