package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the detalle_remision database table.
 * 
 */
@Embeddable
public class DetalleDevolucionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_devolucion", insertable=false, updatable=false )
	private int idDevolucion;

	@Column(name="codigo_producto", insertable=false, updatable=false )
	private int codigoProducto;
	
	public DetalleDevolucionPK() {
	}
	
	/**
	 * @param idDevolucion
	 * @param codigoProducto
	 */
	public DetalleDevolucionPK(int idDevolucion, int codigoProducto) {
		super();
		this.idDevolucion = idDevolucion;
		this.codigoProducto = codigoProducto;
	}

	public int getIdDevolucion() {
		return this.idDevolucion;
	}
	public void setIdDevolucion(int idDevolucion) {
		this.idDevolucion = idDevolucion;
	}
	public int getCodigoProducto() {
		return this.codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigoProducto;
		result = prime * result + idDevolucion;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleDevolucionPK other = (DetalleDevolucionPK) obj;
		if (codigoProducto != other.codigoProducto)
			return false;
		if (idDevolucion != other.idDevolucion)
			return false;
		return true;
	}

}