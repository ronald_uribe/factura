package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.IDetalleEntradaDao;

/**
 * The persistent class for the detalle_entrada database table.
 * 
 */
@Entity
@Table(name="detalle_entrada")
@NamedQuery(name="DetalleEntrada.findAll", query="SELECT d FROM DetalleEntrada d")
public class DetalleEntrada implements Serializable, IDetalleEntradaDao {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleEntradaPK detalleEntradaPK;
	private int cantidad;
	
	//bi-directional many-to-one association to Entrada
	@ManyToOne
	@JoinColumn(name="id_entrada")
	private Entrada entrada;
	
	//bi-directional many-to-one association to Entrada
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto;

	public DetalleEntrada() {
		detalleEntradaPK = new DetalleEntradaPK();
		entrada = new Entrada();
		producto = new Producto();
	}
	public DetalleEntrada(int entradaId, int productoId) {
		detalleEntradaPK = new DetalleEntradaPK(entradaId, productoId);
    }
	
	public DetalleEntradaPK getId() {
		return this.detalleEntradaPK;
	}

	public void setId(DetalleEntradaPK detalleEntradaPK) {
		this.detalleEntradaPK = detalleEntradaPK;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public Entrada getEntrada() {
		return this.entrada;
	}

	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}
	
	public final int getIdEntrada() {
		return detalleEntradaPK.getIdEntrada();
	}
	public final void setIdEntrada(int idEntrada) {
		this.detalleEntradaPK.setIdEntrada(idEntrada);
		
	}
	public final int getCodigoProducto() {
		
		return detalleEntradaPK.getCodigoProducto();
	}
	public final void setCodigoProducto(int codigoProducto) {
		this.detalleEntradaPK.setCodigoProducto(codigoProducto);
		
	}
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}