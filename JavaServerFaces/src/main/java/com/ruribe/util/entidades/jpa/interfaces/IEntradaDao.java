package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;

import com.ruribe.util.entidades.jpa.DetalleEntrada;
import com.ruribe.util.entidades.jpa.Proveedor;


/**
 *Interfaz del entity entrada.
 * @author RONI
 *
 */
public interface IEntradaDao {

	public int getIdEntrada();
	public void setIdEntrada(int idEntrada);
	public Proveedor getProveedor();
	public void setProveedor(Proveedor proveedor);
	public Date getFecha();
	public void setFecha(Date fecha);
	public List<DetalleEntrada> getDetalleEntradas();
	public void setDetalleEntradas(List<DetalleEntrada> detalleEntradas);
	public DetalleEntrada addDetalleEntrada(DetalleEntrada detalleEntrada);
	public DetalleEntrada removeDetalleEntrada(DetalleEntrada detalleEntrada);
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);

}
