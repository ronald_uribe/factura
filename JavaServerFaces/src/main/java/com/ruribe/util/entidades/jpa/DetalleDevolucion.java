package com.ruribe.util.entidades.jpa;
import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.ruribe.util.entidades.jpa.interfaces.IDetalleDevolucionDao;


/**
 * The persistent class for the detalle_devolucion database table.
 * 
 */
@Entity
@Table(name="detalle_devolucion")
@NamedQuery(name="DetalleDevolucion.findAll", query="SELECT d FROM DetalleDevolucion d")
public class DetalleDevolucion implements IDetalleDevolucionDao, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleDevolucionPK detalleDevolucionPK;

	private int cantidad;
	
	//bi-directional many-to-one association to Devolucion
	@ManyToOne
	@JoinColumn(name="id_devolucion")
	private Devolucion devolucion;
	
	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto;

//	@ManyToOne(cascade={CascadeType.PERSIST})
//	@JoinColumns({
//	    @JoinColumn(name="id_remision", referencedColumnName="id_remision",insertable=false, updatable=false),
//	    @JoinColumn(name="codigo_producto", referencedColumnName="codigo_producto",insertable=false, updatable=false)
//	})
//	private DetalleRemision detalleremision;
	
//	@ManyToOne
//	@JoinColumn(name="id_remision")
//	private Remision remision;
	
	public DetalleDevolucion() {
		detalleDevolucionPK = new DetalleDevolucionPK();
	}
	public DetalleDevolucion(int devolucionId, int productoId, int remisionId) {
		detalleDevolucionPK = new DetalleDevolucionPK(devolucionId, productoId);
    }
	
	public DetalleDevolucionPK getId() {
		return this.detalleDevolucionPK;
	}

	public void setId(DetalleDevolucionPK detalleDevolucionPK) {
		this.detalleDevolucionPK = detalleDevolucionPK;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Devolucion getDevolucion() {
		return this.devolucion;
	}

	public void setDevolucion(Devolucion devolucion) {
		this.devolucion = devolucion;
	}
	
	public final int getIdDevolucion() {
		return detalleDevolucionPK.getIdDevolucion();
	}
	public final void setIdDevolucion(int idDevolucion) {
		this.detalleDevolucionPK.setIdDevolucion(idDevolucion);
		
	}
	public final int getCodigoProducto() {
		
		return detalleDevolucionPK.getCodigoProducto();
	}
	public final void setCodigoProducto(int codigoProducto) {
		this.detalleDevolucionPK.setCodigoProducto(codigoProducto);
	}
	
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
//	/**
//	 * @return the detalleremision
//	 */
//	public DetalleRemision getDetalleremision() {
//		return detalleremision;
//	}
//	/**
//	 * @param detalleremision the detalleremision to set
//	 */
//	public void setDetalleremision(DetalleRemision detalleremision) {
//		this.detalleremision = detalleremision;
//	}
//	/**
//	 * @return the remision
//	 */
//	public Remision getRemision() {
//		return remision;
//	}
//	/**
//	 * @param remision the remision to set
//	 */
//	public void setRemision(Remision remision) {
//		this.remision = remision;
//	}

	
	

}