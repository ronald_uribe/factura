/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity IDetalleDevolucionPK.
 * @author RONI
 *
 */
public interface IDetalleDevolucionPKDao {
	
	public int getIdDevolucion();
	public void setIdDevolucion(int idDevolucion);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);

}
