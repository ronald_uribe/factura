/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import java.util.List;

import com.ruribe.util.entidades.jpa.Obra;

/**
 *Interfaz del entity Cliente.
 * @author RONI
 *
 */
public interface IClienteDao{
	/**
	 * @return the id_cliente
	 */
	public String getId_cliente();
	/**
	 * @param id_cliente the id_cliente to set
	 */
	public void setId_cliente(String id_cliente);
	public String getTelefono();
	public String getCelular();
	public String getDireccion();
	public String getRazon_social();
	public String getEmail();
	public void setCod_tipo_documento(int cod_tipo_documento);
	public int getCod_tipo_documento();
	public int getCod_ciudad();
	public void setCod_ciudad(int cod_ciudad);
	public void setDireccion(String direcion);
	public void setEmail(String email);
	public void setRazon_social(String razonSocial);
	public void setTelefono(String telefono);
	public void setCelular(String celular);
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);
	/**
	 * @return the listaObras
	 */
	public List<Obra> getListaObras() ;

	/**
	 * @param listaObras the listaObras to set
	 */
	public void setListaObras(Obra listaObras);

}
