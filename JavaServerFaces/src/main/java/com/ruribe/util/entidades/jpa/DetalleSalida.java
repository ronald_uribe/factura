package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.IDetalleSalidaDao;

/**
 * The persistent class for the detalle_salida database table.
 * 
 */
@Entity
@Table(name="detalle_salida")
@NamedQuery(name="DetalleSalida.findAll", query="SELECT d FROM DetalleSalida d")
public class DetalleSalida implements Serializable, IDetalleSalidaDao {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleSalidaPK detalleSalidaPK;
	private int cantidad;
	
	//bi-directional many-to-one association to Salida
	@ManyToOne
	@JoinColumn(name="id_salida")
	private Salida salida;
	
	//bi-directional many-to-one association to Salida
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto;

	public DetalleSalida() {
		detalleSalidaPK = new DetalleSalidaPK();
		salida = new Salida();
		producto = new Producto();
	}
	public DetalleSalida(int salidaId, int productoId) {
		detalleSalidaPK = new DetalleSalidaPK(salidaId, productoId);
    }
	
	public DetalleSalidaPK getId() {
		return this.detalleSalidaPK;
	}

	public void setId(DetalleSalidaPK detalleSalidaPK) {
		this.detalleSalidaPK = detalleSalidaPK;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public Salida getSalida() {
		return this.salida;
	}

	public void setSalida(Salida salida) {
		this.salida = salida;
	}
	
	public final int getIdSalida() {
		return detalleSalidaPK.getIdSalida();
	}
	public final void setIdSalida(int idSalida) {
		this.detalleSalidaPK.setIdSalida(idSalida);
		
	}
	public final int getCodigoProducto() {
		
		return detalleSalidaPK.getCodigoProducto();
	}
	public final void setCodigoProducto(int codigoProducto) {
		this.detalleSalidaPK.setCodigoProducto(codigoProducto);
		
	}
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}