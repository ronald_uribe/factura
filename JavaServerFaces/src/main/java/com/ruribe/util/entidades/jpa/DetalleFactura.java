package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.IDetalleFacturaDao;


/**
 * The persistent class for the detalle_factura database table.
 * 
 */
@Entity
@Table(name="detalle_factura")
@NamedQuery(name="DetalleFactura.findAll", query="SELECT d FROM DetalleFactura d")
public class DetalleFactura implements IDetalleFacturaDao, Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleFacturaPK detalleFacturaPK;
	
	//bi-directional many-to-one association to Devolucion
	@ManyToOne
	@JoinColumn(name="id_factura")
	private Factura factura;

	//bi-directional many-to-one association to Remision
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto;
	
	private int cantidad;
	private int item;
	private int cantidad_gestionada;
	private String tipo;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_corte")
	private Date fechaCorte;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_inicio")
	private Date fechaInicio;

	@Column(name="valor_unitario")
	private int valorUnitario;
	
	@Column(name="numero_dias")
	private int numeroDias;


	public DetalleFactura() {
	}

	public DetalleFacturaPK getDetalleFacturaPK() {
		return this.detalleFacturaPK;
	}

	public void setDetalleFacturaPK(DetalleFacturaPK detalleFacturaPK) {
		this.detalleFacturaPK = detalleFacturaPK;
	}
	
	/**
	 * @return the item
	 */
	public int getItem() {
		return this.item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item=item;
	}

	/**
	 * @return the factura
	 */
	public Factura getFactura() {
		return factura;
	}

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(Factura factura) {
		this.factura = factura;
	}
	
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the cantidad_gestionada
	 */
	public int getCantidad_gestionada() {
		return cantidad_gestionada;
	}

	/**
	 * @param cantidad_gestionada the cantidad_gestionada to set
	 */
	public void setCantidad_gestionada(int cantidad_gestionada) {
		this.cantidad_gestionada = cantidad_gestionada;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFechaCorte() {
		return this.fechaCorte;
	}

	public void setFechaCorte(Date date) {
		this.fechaCorte = date;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getValorUnitario() {
		return this.valorUnitario;
	}

	public void setValorUnitario(int valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	@Override
	public int getIdFactura() {
		return detalleFacturaPK.getIdFactura();
	}

	@Override
	public void setIdFactura(int idfactura) {
		detalleFacturaPK.setIdFactura(idfactura);	
	}

	/**
	 * @return the valorTotal
	 */
	public Double calcularImporte() {
		return (double) (cantidad * numeroDias * producto.getReferencia().getPrecio_alquiler());
	}

	
	/**
	 * @return the numeroDias
	 */
	public int getNumeroDias() {
		return numeroDias;
	}

	/**
	 * @param numeroDias the numeroDias to set
	 */
	public void setNumeroDias(int numeroDias) {
		this.numeroDias = numeroDias;
	}

}