/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.DetalleReposicionPK;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Reposicion;

/**
 *Interfaz del entity IDetalleReposicionPK.
 * @author RONI
 *
 */
public interface IDetalleReposicionDao extends IDetalleReposicionPKDao {
	
	public DetalleReposicionPK getId();
	public void setId(DetalleReposicionPK detalleReposicionPK);
	public int getCantidad();
	public void setCantidad(int cantidad);
	public Reposicion getReposicion();
	public void setReposicion(Reposicion reposicion);
	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion();
	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion);
	/**
	 * @return the producto
	 */
	public Producto getProducto();
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto);

}
