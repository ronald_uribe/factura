package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.ITipoDocumentoDao;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@Table(name = "tipo_documento") 
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_TIPO_DOCUMENTO",
			   query="SELECT u FROM TipoDocumento u"),
})
public class TipoDocumento implements ITipoDocumentoDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL_TIPO_DOCUMENTO = "QUERY_FIND_ALL_TIPO_DOCUMENTO";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_tipo_documento ;

	private String descripcion;

	public TipoDocumento(){
		
	}

	/**
	 * @return the id_tipo_documento
	 */
	public final int getId_tipo_documento() {
		return id_tipo_documento;
	}

	/**
	 * @param id_tipo_documento the id_tipo_documento to set
	 */
	public final void setId_tipo_documento(int id_tipo_documento) {
		this.id_tipo_documento = id_tipo_documento;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}