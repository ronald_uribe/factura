package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.ruribe.util.entidades.jpa.interfaces.IRoleDao;

@Entity
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_ROLES", query="SELECT r FROM Role r order by r.authority"),
})
public class Role implements IRoleDao , Serializable {
	
	public static final String QUERY_FIND_ALL_ROLES="QUERY_FIND_ALL_ROLES";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="idrole")
	private Long role_id;
	
	@Column(name ="descripcion", unique=true, nullable=false) 
	private String authority;
	
	@ManyToMany
	private Set<Usuario> usuarios = new HashSet<Usuario>();


	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	private static final long serialVersionUID = 1L;

}
