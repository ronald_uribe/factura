/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity Proveedor.
 * @author RONI
 *
 */
public interface ITransportadorDao {
	
	/**
	 * @return the no_documento
	 */
	public String getNo_documento();
	public void setNo_documento(String no_documento);
	public int getCod_tipo_documento();
	public void setCod_tipo_documento(int cod_tipo_documento);
	public String getNombre();
	public void setNombre(String nombre);
	public String getDireccion();
	public void setDireccion(String direccion);
	public String getTelefono();
	public void setTelefono(String telefono);
	public String getCelular();
	public void setCelular(String celular);
	public String getEmail();
	public void setEmail(String email);
	public String getPlaca();
	public void setPlaca(String placa);
	public String getDescripcion();
	public void setDescripcion(String descripcion);
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);

}
