package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the detalle_factura database table.
 * 
 */
@Embeddable
public class DetalleFacturaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_factura", insertable=false, updatable=false )
	private int idFactura;

	@Column(name="item", insertable=false, updatable=false )
	private int item;
	
	/**
	 * @return the item
	 */
	public int getItem() {
		return item;
	}
	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item = item;
	}
	public DetalleFacturaPK() {
	}
	public int getIdFactura() {
		return this.idFactura;
	}
	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}
	

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DetalleFacturaPK)) {
			return false;
		}
		DetalleFacturaPK castOther = (DetalleFacturaPK)other;
		return 
			(this.idFactura == castOther.idFactura)
			&& (this.item == castOther.item);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idFactura;
		hash = hash * prime + this.item;
		
		return hash;
	}
}