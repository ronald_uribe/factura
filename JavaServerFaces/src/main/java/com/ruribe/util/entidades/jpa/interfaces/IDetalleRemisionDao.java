/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.DetalleRemisionPK;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Remision;

/**
 *Interfaz del entity IDetalleRemisionPK.
 * @author RONI
 *
 */
public interface IDetalleRemisionDao extends IDetalleRemisionPKDao {
	
	public DetalleRemisionPK getId();
	public void setId(DetalleRemisionPK detalleRemisionPK);
	public int getCantidad();
	public void setCantidad(int cantidad);
	public Remision getRemision();
	public void setRemision(Remision remision);
	/**
	 * @return the producto
	 */
	public Producto getProducto();
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto);

}
