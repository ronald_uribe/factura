package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.IReposicionDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the reposicion database table.
 * 
 */
@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name = "Reposicion.contenidospPendientes",query = 
			"SELECT "
				+ "r.id_cliente, "
				+ "r.id_obra,"
				+ "dr.codigo_producto, "
				+ "p.descripcion,"
				+ "sum(dr.cantidad) - coalesce((SELECT sum(dd.cantidad ) "
					+ "							FROM devolucion d INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion) "
					+ "							WHERE d.id_cliente = r.id_cliente and d.id_obra = r.id_obra  "
					+ "									and dr.codigo_producto=dd.codigo_producto "
					+ "							GROUP BY dd.codigo_producto),0)- "
					+ "					coalesce((SELECT sum(drp.cantidad) "
					+ "								FROM reposicion rp inner join detalle_reposicion drp on (rp.id_reposicion = drp.id_reposicion) "
					+ "								WHERE rp.id_cliente = r.id_cliente and rp.id_obra = r.id_obra 	"
					+ "									 and 	dr.codigo_producto=drp.codigo_producto "
					+ "								GROUP BY drp.codigo_producto ),0) AS debe, "
				+ "ref.precio_reposicion "
			+ "FROM remision r inner join detalle_remision dr on (r.id_remision = dr.id_remision)  "
							+ "LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto) "
							+ " LEFT OUTER JOIN referencia ref ON (p.codigo_interno = ref.codigo_interno) "
			+ "WHERE r.id_cliente = ?cliente and r.id_obra = ?idobra "
			+ "GROUP BY r.id_cliente,r.id_obra, dr.codigo_producto, p.descripcion  "
			+ "HAVING debe > 0 "
			+ "ORDER BY p.descripcion")})
@NamedQueries({
				@NamedQuery(name="Reposicion.pk.groupby.remision", query="SELECT d FROM Reposicion d where d.idReposicion =:id_reposicion"),
				@NamedQuery(name="Reposicion.findAll", query="SELECT d FROM Reposicion d"),
				@NamedQuery(name="Reposicion.pk", query="SELECT d FROM Reposicion d where d.idReposicion =:id_reposicion "),
				@NamedQuery(name="Reposicion.rangofechas", query="SELECT d FROM Reposicion d where d.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Reposicion.cliente.fecha", query="SELECT d FROM Reposicion d where d.cliente.id_cliente =:idCliente and d.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Reposicion.cliente", query="SELECT d FROM Reposicion d where d.cliente.id_cliente =:idCliente")})

public class Reposicion implements IReposicionDao, Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_PK_GROUP_BY_REMISION_REPOSICION = "Reposicion.pk.groupby.remision";
	public static String QUERY_FIND_ALL_REPOSICIONES="Reposicion.findAll";
	public static String QUERY_FIND_PK_REPOSICION="Reposicion.pk";
	public static String QUERY_FIND_RANGO_FECHAS="Reposicion.rangofechas";
	public static String QUERY_FIND_REPOSICION_CLIENTE="Reposicion.cliente";
	public static String QUERY_FIND_REPOSICION_CLIENTE_RANGO_FECHAS="Reposicion.cliente.fecha";
	public static String QUERY_PRODUCTOS_PENDIENTES_CLIENTE="Reposicion.contenidospPendientes";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reposicion")
	private int idReposicion;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	@OneToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	@OneToOne
	@JoinColumn(name="id_obra")
	private Obra obra;
	
	//bi-directional many-to-one association to DetalleReposicion
	@OneToMany(mappedBy="reposicion",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	private List<DetalleReposicion> detalleReposiciones;

	private String observaciones;
	private int descuento;
	private String usuario_creacion;
	private boolean enabled;
	
	public Reposicion() {
		detalleReposiciones = new ArrayList<DetalleReposicion>();
	}

	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion() {
		return idReposicion;
	}

	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion) {
		this.idReposicion = idReposicion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<DetalleReposicion> getDetalleReposiciones() {
		return this.detalleReposiciones;
	}

	public void setDetalleReposicions(List<DetalleReposicion> detalleReposiciones) {
		this.detalleReposiciones = detalleReposiciones;
	}

	public DetalleReposicion addDetalleReposicion(DetalleReposicion detalleReposicion) {
		getDetalleReposiciones().add(detalleReposicion);
	//	DetalleReposicion.setRemision(this);

		return detalleReposicion;
	}

	public DetalleReposicion removeDetalleReposicion(DetalleReposicion detalleReposicion) {
		getDetalleReposiciones().remove(detalleReposicion);

		return detalleReposicion;
	}

	
	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the obra
	 */
	public Obra getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(Obra obra) {
		this.obra = obra;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	/**
	 * @return the descuento
	 */
	public int getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		
	}



}