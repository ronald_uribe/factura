/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.DetalleDevolucionPK;
import com.ruribe.util.entidades.jpa.Producto;
import com.ruribe.util.entidades.jpa.Devolucion;

/**
 *Interfaz del entity IDetalleDevolucionPK.
 * @author RONI
 *
 */
public interface IDetalleDevolucionDao extends IDetalleDevolucionPKDao {
	
	public DetalleDevolucionPK getId();
	public void setId(DetalleDevolucionPK detalleDevolucionPK);
	public int getCantidad();
	public void setCantidad(int cantidad);
	public Devolucion getDevolucion();
	public void setDevolucion(Devolucion devolucion);
	/**
	 * @return the producto
	 */
	public Producto getProducto();
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto);
//	
//	/**
//	 * @return the detalleremision
//	 */
//	public DetalleRemision getDetalleremision();
//	/**
//	 * @param detalleremision the detalleremision to set
//	 */
//	public void setDetalleremision(DetalleRemision detalleremision);
//	/**
//	 * @return the remision
//	 */
//	public Remision getRemision();
//	/**
//	 * @param remision the remision to set
//	 */
//	public void setRemision(Remision remision);

}
