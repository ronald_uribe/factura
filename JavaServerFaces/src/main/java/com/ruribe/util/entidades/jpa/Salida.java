package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Persistence;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.ruribe.util.entidades.jpa.interfaces.ISalidaDao;


/**
 * The persistent class for the salida database table.
 * 
 */
@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name = "Salida.contenidospPendientes",query = 
						"SELECT  r.codigo_proveedor,  " + 
						"dr.codigo_producto,  " + 
						"p.descripcion, " + 
						"sum(dr.cantidad) - coalesce((SELECT sum(dd.cantidad )  " + 
						"							 FROM salida d INNER JOIN detalle_salida dd ON (d.id_salida = dd.id_salida)  " + 
						"                             WHERE 	d.codigo_proveedor = r.codigo_proveedor and  " + 
						"                                    dr.codigo_producto=dd.codigo_producto  " + 
						"							GROUP BY dd.codigo_producto),0) " + 
						"	AS debe  " + 
						"FROM entrada r inner join detalle_entrada dr on (r.id_entrada = dr.id_entrada)   " + 
						"LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto)  " + 
						"WHERE r.codigo_proveedor = ?codigoProveedor " + 
						"GROUP BY r.codigo_proveedor, dr.codigo_producto, p.descripcion  " + 
						"HAVING debe > 0  " + 
						"ORDER BY p.descripcion")})

@NamedQueries({
	@NamedQuery(name="Salida.findAll", query="SELECT r FROM Salida r order by r.idSalida desc"),
	@NamedQuery(name="Salida.pk", query="SELECT r FROM Salida r join r.detalleSalidas d where r.idSalida =:id_salida order by d.producto.descripcion"),
	@NamedQuery(name="Salida.rangofechas", query="SELECT r FROM Salida r where r.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Salida.proveedor.fecha", query="SELECT r FROM Salida r where r.proveedor.codigo_proveedor =:idProveedor and r.fecha between :fechaInicial and :fechaCorte"),
	@NamedQuery(name="Salida.proveedor", query="SELECT r FROM Salida r where r.proveedor.codigo_proveedor =:idProveedor")})

public class Salida implements Serializable, ISalidaDao {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL_SALIDAS = "Salida.findAll";
	public static final String QUERY_FIND_RANGO_FECHAS = "Salida.rangofechas";
	public static final String QUERY_FIND_SALIDA_PROVEEDOR = "Salida.proveedor";
	public static final String QUERY_FIND_PK_SALIDA = "Salida.pk";
	public static final String QUERY_PRODUCTOS_PENDIENTES_PROVEEDOR = "Salida.contenidospPendientes";
	public static final String QUERY_FIND_SALIDA_PROVEEDOR_RANGO_FECHAS = "Salida.proveedor.fecha";
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_salida")
	private int idSalida;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	@OneToOne
	@JoinColumn(name="codigo_proveedor")
	private Proveedor proveedor;
	private String usuario_creacion;
		
	//bi-directional many-to-one association to DetalleSalida
	@OneToMany(mappedBy="salida",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	@OrderBy("producto ASC")
	private List<DetalleSalida> detalleSalidas;
	private boolean enabled;

	public Salida() {
		detalleSalidas = new ArrayList<DetalleSalida>();
	}

	public int getIdSalida() {
		return this.idSalida;
	}

	public void setIdSalida(int idSalida) {
		this.idSalida = idSalida;
	}
	
	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<DetalleSalida> getDetalleSalidas() {
		return this.detalleSalidas;
	}

	public void setDetalleSalidas(List<DetalleSalida> detalleSalidas) {
		this.detalleSalidas = detalleSalidas;
	}

	public DetalleSalida addDetalleSalida(DetalleSalida detalleSalida) {
		getDetalleSalidas().add(detalleSalida);
		return detalleSalida;
	}

	public DetalleSalida removeDetalleSalida(DetalleSalida detalleSalida) {
		getDetalleSalidas().remove(detalleSalida);
		return detalleSalida;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			 Salida salida = new Salida();
			 
			 Proveedor proveedor = new Proveedor();
			 proveedor.setCodigo_proveedor("RU");
			 salida.setProveedor(proveedor);
			 salida.setFecha(new Date());
			 salida.setUsuario_creacion("ronald");
			 Producto producto = new Producto();
			 producto.setCodigo_producto(6);
			 DetalleSalida detalle = new DetalleSalida();
			 detalle.setCantidad(3);
			 detalle.setSalida(salida);
			 detalle.setProducto(producto);
			 salida.addDetalleSalida(detalle);
			 em.persist(salida); 
			 em.flush();
			 //return abc.getId();
			 System.out.println("#######id de salida:" + salida.getIdSalida());
			 em.getTransaction().commit();
			 em.close();
		     emf = Persistence.createEntityManagerFactory("JavaServerFaces");
		     em = emf.createEntityManager();
		     em.getTransaction( ).begin( );
			TypedQuery <Salida> salidaes= em.createNamedQuery(Salida.QUERY_FIND_ALL_SALIDAS,Salida.class);
			 for(Salida c:salidaes.getResultList()){
				System.out.println("ID SALIDA:"+ c.getIdSalida());
			 } 
			 em.getTransaction().commit();
			 em.close();
		     emf.close();
			 
		      
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}
}