/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity IDetalleRemisionPK.
 * @author RONI
 *
 */
public interface IDetalleRemisionPKDao {
	
	public int getIdRemision();
	public void setIdRemision(int idRemision);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);

}
