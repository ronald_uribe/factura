package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import com.ruribe.util.entidades.jpa.interfaces.ITipoProductoDao;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@Table(name = "tipo_producto") 
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_TIPO_PRODUCTO",
			   query="SELECT u FROM TipoProducto u"),
			   @NamedQuery(name="QUERY_SELECT_ID_TIPO_PRODUCTO",
				query="select a from TipoProducto a where a.cod_tipo_producto = :cod_tipo_producto")
})
public class TipoProducto implements ITipoProductoDao, Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_ALL_TIPO_PRODUCTO = "QUERY_FIND_ALL_TIPO_PRODUCTO";
	public static final String QUERY_SELECT_ID_TIPO_PRODUCTO = "QUERY_SELECT_ID_TIPO_PRODUCTO";

	@Id
	private int cod_tipo_producto ;
	private String descripcion;
	
	@OneToMany(mappedBy="tipo_producto",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	@OrderBy("descripcion asc")
	private List<Producto> listaProductos;
	
	public TipoProducto(){
		
	}
	
	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	/**
	 * @return the codigo_tipo_producto
	 */
	public final int getCod_tipo_producto() {
		return cod_tipo_producto;
	}

	/**
	 * @param codigo_tipo_producto the codigo_tipo_producto to set
	 */
	public final void setCod_tipo_producto(int cod_tipo_producto) {
		this.cod_tipo_producto = cod_tipo_producto;
	}
	
	/**
	 * @return the listaProductos
	 */
	public final List<Producto> getListaProductos() {
		return listaProductos;
	}

	/**
	 * @param listaProductos the listaProductos to set
	 */
	public final void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			 //TipoProducto tp = new TipoProducto();
			// em.persist(tp);
			// em.persist(detalle);

			TypedQuery <TipoProducto> tps= em.createNamedQuery(TipoProducto.QUERY_FIND_ALL_TIPO_PRODUCTO,TipoProducto.class);
			 for(TipoProducto c:tps.getResultList()){
				 System.out.println("cod:" + c.getCod_tipo_producto());
				 System.out.println("des:" + c.getDescripcion());
			 } 
			 
			 TypedQuery <TipoProducto> tps1= em.createNamedQuery(TipoProducto.QUERY_SELECT_ID_TIPO_PRODUCTO,TipoProducto.class);
			 tps1.setParameter("cod_tipo_producto", 1);
			 for(TipoProducto c:tps1.getResultList()){
				 System.out.println("cod:" + c.getCod_tipo_producto());
				 System.out.println("des:" + c.getDescripcion());
				 System.out.println("lista:" + c.getListaProductos().size());
			 } 
			 
			 em.getTransaction().commit();
		     em.close();
		     emf.close();
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}

}