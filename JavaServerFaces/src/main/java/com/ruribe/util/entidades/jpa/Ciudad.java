package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;
import com.ruribe.util.entidades.jpa.interfaces.ICiudadDao;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@Table(name = "ciudad") 
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_CIUDADES",
			   query="SELECT u FROM Ciudad u"),
})
public class Ciudad implements ICiudadDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL_CIUDADES = "QUERY_FIND_ALL_CIUDADES";

	@Id
	private int codigo_ciudad ;
	private String nombre_ciudad;

	public Ciudad(){
		
	}

	/**
	 * @return the codigo_ciudad
	 */
	public int getCodigo_ciudad() {
		return codigo_ciudad;
	}

	/**
	 * @param codigo_ciudad the codigo_ciudad to set
	 */
	public void setCodigo_ciudad(int codigo_ciudad) {
		this.codigo_ciudad = codigo_ciudad;
	}

	/**
	 * @return the nombre_ciudad
	 */
	public String getNombre_ciudad() {
		return nombre_ciudad;
	}

	/**
	 * @param nombre_ciudad the nombre_ciudad to set
	 */
	public void setNombre_ciudad(String nombre_ciudad) {
		this.nombre_ciudad = nombre_ciudad;
	}

	
}