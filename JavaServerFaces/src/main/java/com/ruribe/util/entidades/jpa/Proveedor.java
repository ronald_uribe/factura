package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.IProveedorDao;



/**
 * The persistent class for the Proveedor database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_PROVEEDOR",
			   query="SELECT u FROM Proveedor u order by u.razon_social"),
	@NamedQuery(name="QUERY_SELECT_ID_PROVEEDOR",
				query="select a from Proveedor a where a.codigo_proveedor = :documento"),
	@NamedQuery(name="QUERY_SELECT_ID_LIKE_PROVEEDOR",
				query="select a from Proveedor a where a.codigo_proveedor like :documento")
})

public class Proveedor implements Serializable, IProveedorDao {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL = "QUERY_FIND_ALL_PROVEEDOR";
	public static final String QUERY_SELECT_ID ="QUERY_SELECT_ID_PROVEEDOR";
	public static final String QUERY_SELECT_ID_LIKE ="QUERY_SELECT_ID_LIKE_PROVEEDOR";

	@Id
	private String 	codigo_proveedor;
	
	private String no_documento;
	private int cod_tipo_documento;
	private String razon_social;
	private String direccion;
	private int cod_ciudad;
	private String telefono;
	private String celular;
	private String email;
	private boolean enabled;

	public Proveedor() {
	}


	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the no_documento
	 */
	public String getNo_documento() {
		return no_documento;
	}
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento) {
		this.no_documento = no_documento;
	}
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}
	/**
	 * @return the razon_social
	 */
	public String getRazon_social() {
		return razon_social;
	}
	/**
	 * @param razon_social the razon_social to set
	 */
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the cod_ciudad
	 */
	public int getCod_ciudad() {
		return cod_ciudad;
	}
	/**
	 * @param cod_ciudad the cod_ciudad to set
	 */
	public void setCod_ciudad(int cod_ciudad) {
		this.cod_ciudad = cod_ciudad;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email=email;
	}


	/**
	 * @return the codigo
	 */
	public String getCodigo_proveedor() {
		return codigo_proveedor;
	}


	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo_proveedor(String codigo) {
		this.codigo_proveedor = codigo;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}