package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.ruribe.dao.dto.PendientesDTO;
import com.ruribe.util.entidades.jpa.interfaces.IDevolucionDao;

/**
 * The persistent class for the devolucion database table.
 * 
 */
@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name = "clientesConSaldosPendientes",query = "SELECT DISTINCT " 
			+"saldo.id_cliente, "
			+"c.razon_social, saldo.id_obra, nombre, o.telefono, o.contacto, o.direccion, c.direccion "
			+"FROM  ( "
			+"	SELECT  r.id_cliente, " 
			+"			r.id_obra, "
//			+"			dr.codigo_producto, " 
//			+"			p.descripcion, "
			+"				sum(dr.cantidad) - coalesce((SELECT sum(dd.cantidad )  "
			+"											FROM devolucion d INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion)  "
			+"											WHERE d.id_cliente = r.id_cliente and d.id_obra = r.id_obra   "
			+"													and dr.codigo_producto=dd.codigo_producto  "
			+"											GROUP BY dd.codigo_producto),0)-  "
			+"									coalesce((SELECT sum(drp.cantidad)  "
			+"												FROM reposicion rp inner join detalle_reposicion drp on (rp.id_reposicion = drp.id_reposicion)  "
			+"												WHERE rp.id_cliente = r.id_cliente and rp.id_obra = r.id_obra 	 "
			+"													 and 	dr.codigo_producto=drp.codigo_producto  "
			+"												GROUP BY drp.codigo_producto ),0) AS debe  "
			+"				FROM remision r inner join detalle_remision dr on (r.id_remision = dr.id_remision)   "
			+"				LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto)  "
//			+"			-- 	WHERE r.id_cliente = ?cliente and r.id_obra = ?idobra  "
			+"				GROUP BY r.id_cliente,r.id_obra, dr.codigo_producto, p.descripcion  "
			+"				HAVING debe > 0 ) saldo inner join cliente c on (c.id_cliente=saldo.id_cliente) "
			+"               inner join obra o ON (o.id_obra=saldo.id_obra) order by saldo.id_cliente"
			                ),
	@NamedNativeQuery(name = "Devolucion.contenidospPendientes",query = 
			"SELECT  r.id_cliente, "
			+ "r.id_obra,"
			+ "dr.codigo_producto, "
			+ "p.descripcion,"
			+ "sum(dr.cantidad) - coalesce((SELECT sum(dd.cantidad ) "
			+ "							FROM devolucion d INNER JOIN detalle_devolucion dd ON (d.id_devolucion = dd.id_devolucion) "
			+ "							WHERE d.id_cliente = r.id_cliente and d.id_obra = r.id_obra  "
			+ "									and dr.codigo_producto=dd.codigo_producto "
			+ "							GROUP BY dd.codigo_producto),0)- "
			+ "					coalesce((SELECT sum(drp.cantidad) "
			+ "								FROM reposicion rp inner join detalle_reposicion drp on (rp.id_reposicion = drp.id_reposicion) "
			+ "								WHERE rp.id_cliente = r.id_cliente and rp.id_obra = r.id_obra 	"
			+ "									 and 	dr.codigo_producto=drp.codigo_producto "
			+ "								GROUP BY drp.codigo_producto ),0) "
			+ "AS debe, "
			+ "p.codigo_interno, "
			+ "ref.descripcion,"
			+ "ref.peso "
			+ "FROM remision r inner join detalle_remision dr on (r.id_remision = dr.id_remision)  "
			+ "LEFT OUTER JOIN producto p on (p.codigo_producto=dr.codigo_producto) "
			+ "LEFT OUTER JOIN referencia ref on (p.codigo_interno=ref.codigo_interno) "
			+ "WHERE r.id_cliente = ?cliente and r.id_obra = ?idobra "
			+ "GROUP BY r.id_cliente,r.id_obra, dr.codigo_producto, p.descripcion "
			+ "HAVING debe > 0 "
			+ "ORDER BY p.descripcion")})
@NamedQueries({
				@NamedQuery(name="Devolucion.pk.groupby.remision", query="SELECT d FROM Devolucion d where d.idDevolucion =:id_devolucion"),
				@NamedQuery(name="Devolucion.findAll", query="SELECT d FROM Devolucion d order by d.idDevolucion desc"),
				@NamedQuery(name="Devolucion.pk", query="SELECT d FROM Devolucion d where d.idDevolucion =:id_devolucion "),
				@NamedQuery(name="Devolucion.rangofechas", query="SELECT d FROM Devolucion d where d.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Devolucion.cliente.fecha", query="SELECT d FROM Devolucion d where d.cliente.id_cliente =:idCliente and d.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Devolucion.cliente", query="SELECT d FROM Devolucion d where d.cliente.id_cliente =:idCliente")})
//				@NamedQuery(name="Devolucion.contenidospPendientesDTO", 
//				query="SELECT new com.ruribe.dao.dto.PendientesDTO(d.remision.idRemision,"
//																+ "r.cliente.id_cliente,"
//																+ "r.obra.idObra,d.producto.codigo_producto,"
//																+ "d.producto.descripcion, "
//																+ "d.cantidad,"
//																+ "SUM(dev.cantidad),"
//																+ "SUM(rep.cantidad) ) "
//						+ "FROM Remision r "
//								+ "LEFT OUTER JOIN r.detalleRemisions d "
//								+ "LEFT OUTER JOIN d.listaDetdevoluciones dev "
//								+ "LEFT OUTER JOIN d.listaDetreposiciones rep "
//						+ "WHERE r.idRemision=d.remision.idRemision AND "
////						+ "d.remision.idRemision=dev.remision.idRemision AND "
//						+ "r.fecha<= :fecha AND "
//						+ "r.cliente.id_cliente= :cliente "
//						+ "GROUP BY d.remision.idRemision,r.cliente.id_cliente,r.obra.idObra,d.producto.codigo_producto,d.cantidad "
//						+ "order by d.producto.descripcion")})

public class Devolucion implements IDevolucionDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_PK_GROUP_BY_REMISION_DEVOLUCION = "Devolucion.pk.groupby.remision";
	public static String QUERY_FIND_ALL_DEVOLUCIONES="Devolucion.findAll";
	//public static String QUERY_FIND_DEVOLUCIONES_BY_DATES="Devolucion.returnbydate";
	public static String QUERY_PRODUCTOS_PENDIENTES_CLIENTE_DTO="Devolucion.contenidospPendientesDTO";
	public static String QUERY_PRODUCTOS_PENDIENTES_CLIENTE="Devolucion.contenidospPendientes";
	public static String QUERY_ALL_PENDIENTES_CLIENTE="clientesConSaldosPendientes";
	
	public static String QUERY_FIND_PK_DEVOLUCION="Devolucion.pk";
	public static String QUERY_FIND_RANGO_FECHAS="Devolucion.rangofechas";
	public static String QUERY_FIND_DEVOLUCION_CLIENTE="Devolucion.cliente";
	public static String QUERY_FIND_DEVOLUCION_CLIENTE_RANGO_FECHAS="Devolucion.cliente.fecha";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_devolucion")
	private int idDevolucion;

	private boolean enabled;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_entrega;

	@OneToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	@OneToOne
	@JoinColumn(name="id_obra")
	private Obra obra;
	
	@OneToOne
	@JoinColumn(name="id_transportador")
	private Transportador transportador;

	private int transporte;
	private boolean printed;
	
	private String usuario_creacion;
	
	//bi-directional many-to-one association to DetalleDevolucion
	@OneToMany(mappedBy="devolucion",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	private List<DetalleDevolucion> detalleDevoluciones;

	private String observaciones;
	
	public Devolucion() {
		detalleDevoluciones = new ArrayList<DetalleDevolucion>();
	}

	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion() {
		return idDevolucion;
	}

	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion) {
		this.idDevolucion = idDevolucion;
	}



	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the fecha_entrega
	 */
	public Date getFecha_entrega() {
		return fecha_entrega;
	}
	
	

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @param fecha_entrega the fecha_entrega to set
	 */
	public void setFecha_entrega(Date fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}

	public int getTransporte() {
		return this.transporte;
	}

	public void setTransporte(int transporte) {
		this.transporte = transporte;
	}

	public List<DetalleDevolucion> getDetalleDevoluciones() {
		return this.detalleDevoluciones;
	}

	public void setDetalleDevolucions(List<DetalleDevolucion> detalleDevoluciones) {
		this.detalleDevoluciones = detalleDevoluciones;
	}

	public DetalleDevolucion addDetalleDevolucion(DetalleDevolucion detalleDevolucion) {
		getDetalleDevoluciones().add(detalleDevolucion);
	//	DetalleDevolucion.setRemision(this);

		return detalleDevolucion;
	}

	public DetalleDevolucion removeDetalleDevolucion(DetalleDevolucion detalleDevolucion) {
		getDetalleDevoluciones().remove(detalleDevolucion);
	//	DetalleDevolucion.setRemision(null);

		return detalleDevolucion;
	}

	
	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the obra
	 */
	public Obra getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(Obra obra) {
		this.obra = obra;
	}
	
	/**
	 * @return the transportador
	 */
	public Transportador getTransportador() {return transportador;}
	public void setTransportador(Transportador transportador) {	this.transportador = transportador;}
	
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	
	public boolean isPrinted() {return printed;	}
	public void setPrinted(boolean printed) {this.printed = printed;}

	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction().begin( );
			 Devolucion devolucion = new Devolucion();
			 Cliente cliente = new Cliente();
			 Transportador transportador=new Transportador();
			 Obra obra = new Obra();
			 obra.setIdObra(51);
			 cliente.setId_cliente("830.509.497-4");//830.509.497-4
			 devolucion.setTransporte(512000);
			 devolucion.setFecha(new Date());
			 devolucion.setCliente(cliente);
			
			 transportador.setNo_documento("123456789");
			 devolucion.setTransportador(transportador);
			 devolucion.setObra(obra);
			 devolucion.setEnabled(true);
			 
			 Producto producto = new Producto();
			 producto.setCodigo_producto(2);
			
			 DetalleDevolucion detalle = new DetalleDevolucion();
			 detalle.setCantidad(1);
			 ///DetalleRemision d = new DetalleRemision();
			// Id_remision(81);
			 //detalle.setDetalleremision(d);
			 detalle.setDevolucion(devolucion);
			// detalle.setCodigoProducto(2);
			 detalle.setProducto(producto);
			 //detalle.getDetalleremision().setIdRemision(82);
			 Remision remision= new Remision();
			 remision.setIdRemision(82);
//			 detalle.setRemision(remision);
			 devolucion.addDetalleDevolucion(detalle);
		//	 em.persist(devolucion); 
//			 
			 em.flush();
			 
			System.out.println("#######id de devolucion:" + devolucion.getIdDevolucion());
			 em.getTransaction().commit();
			 em.close();
		     emf.close();
		      
		     emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
//			TypedQuery <Devolucion> devoluciones= em.createNamedQuery(Devolucion.QUERY_FIND_ALL_DEVOLUCIONES,Devolucion.class);
//			 for(Devolucion c:devoluciones.getResultList()){
//				 System.out.println("devolucion:" + c.getIdDevolucion());
//				 System.out.println("Cliente:" + c.getCliente().getId_cliente());
//				 System.out.println("Cliente:" + c.getCliente().getRazon_social());
//				 System.out.println("obra:" + c.getObra().getIdObra());
//				 System.out.println("nombre obra:" + c.getObra().getNombre());
//				 System.out.println("nombre tranportador:" + c.getTransportador().getNombre());
//			 } 
			 
			 System.out.println("INICIO ");
				TypedQuery <PendientesDTO> remisiones= em.createNamedQuery(Devolucion.QUERY_PRODUCTOS_PENDIENTES_CLIENTE_DTO,PendientesDTO.class);
				remisiones.setParameter("fecha", new Date()); 
				remisiones.setParameter("cliente", "830.509.497-4"); 
				
//				for(Object valor:remisiones.getResultList()){
//					// System.out.println("cliente :" + ((Object[])valor)[0]+" obra:" + ((Object[])valor)[1]+ " producto:" + ((Object[])valor)[2]+" cantidad:" + ((Object[])valor)[3]);
//					Utilidades.printResult(valor);
//					 
//				 } 
				
				for(PendientesDTO p:remisiones.getResultList()){
					// System.out.println("cliente :" + ((Object[])valor)[0]+" obra:" + ((Object[])valor)[1]+ " producto:" + ((Object[])valor)[2]+" cantidad:" + ((Object[])valor)[3]);
					//Utilidades.printResult(valor);
					 System.out.println("clienteDTO:"+p.getId_cliente());
					 System.out.println("descripcionDTO:"+p.getDescripcion());
					 System.out.println("getId_remisionDTO:"+p.getId_remision());
					 System.out.println("getCantidadDTO:"+p.getCantidad());
					 System.out.println("getCantidadDevueltaDTO:"+p.getCantidadDevuelta());
					 System.out.println("descripcionDTO:"+p.getDescripcion());
					 System.out.println("----------------:");
				 } 
			 
			 em.getTransaction().commit();
			 em.close();
		     emf.close();
			 
		      
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}


}