/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity IDetalleDevolucionPK.
 * @author RONI
 *
 */
public interface IDetalleFacturaPKDao {
	
	public int getIdFactura();
	public void setIdFactura(int idfactura);
	public int getItem();
	public void setItem(int item);



}
