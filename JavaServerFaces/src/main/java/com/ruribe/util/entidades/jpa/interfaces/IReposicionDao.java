package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;
import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleReposicion;
import com.ruribe.util.entidades.jpa.Obra;

/**
 *Interfaz del entity Reposicion.
 * @author RONI
 *
 */
public interface IReposicionDao {

	public int getIdReposicion();
	public void setIdReposicion(int idReposicion);
	public Date getFecha();
	public void setFecha(Date fecha);
	public List<DetalleReposicion> getDetalleReposiciones();
	public void setDetalleReposicions(List<DetalleReposicion> detalleReposiciones);
	public DetalleReposicion addDetalleReposicion(DetalleReposicion detalleReposicion);
	public DetalleReposicion removeDetalleReposicion(DetalleReposicion detalleReposicion);
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	public Cliente getCliente();
	public void setCliente(Cliente cliente);
	public Obra getObra();
	public void setObra(Obra obra);
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);
	
}
