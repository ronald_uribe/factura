/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

/**
 *Interfaz del entity IDetalleReposicionPK.
 * @author RONI
 *
 */
public interface IDetalleReposicionPKDao {
	
	public int getIdReposicion();
	public void setIdReposicion(int idReposicion);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);

}
