package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;

import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleDevolucion;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Transportador;

/**
 *Interfaz del entity Deolucion.
 * @author RONI
 *
 */
public interface IDevolucionDao {

	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion();


	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion);


	public boolean isEnabled();
	public void setEnabled(boolean enabled);

	public Date getFecha();
	public void setFecha(Date fecha);

	public Date getFecha_entrega();
	public void setFecha_entrega(Date fecha_entrega);

	public int getTransporte();
	public void setTransporte(int transporte);

	public List<DetalleDevolucion> getDetalleDevoluciones();
	public void setDetalleDevolucions(List<DetalleDevolucion> detalleDevoluciones);

	public DetalleDevolucion addDetalleDevolucion(DetalleDevolucion detalleDevolucion);
	public DetalleDevolucion removeDetalleDevolucion(DetalleDevolucion detalleDevolucion);

	public String getObservaciones();
	public void setObservaciones(String observaciones);
	
	public Cliente getCliente();
	public void setCliente(Cliente cliente);

	public Obra getObra();
	public void setObra(Obra obra);
	
	public Transportador getTransportador();
	public void setTransportador(Transportador transportador);
	
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	
	public boolean isPrinted();
	public void setPrinted(boolean printed);
}
