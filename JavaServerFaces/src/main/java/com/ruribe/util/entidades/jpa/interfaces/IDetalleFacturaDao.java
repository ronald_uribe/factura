/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;

import com.ruribe.util.entidades.jpa.Factura;
import com.ruribe.util.entidades.jpa.Producto;

/**
 *Interfaz del entity IDetalleFacturaPK.
 * @author RONI
 *
 */
public interface IDetalleFacturaDao extends IDetalleFacturaPKDao {
	
	public Factura getFactura();

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(Factura factura);

	public int getCantidad();

	public void setCantidad(int cantidad);
	/**
	 * @return the cantidad_gestionada
	 */
	public int getCantidad_gestionada();

	/**
	 * @param cantidad_gestionada the cantidad_gestionada to set
	 */
	public void setCantidad_gestionada(int cantidad_gestionada);

	/**
	 * @return the tipo
	 */
	public String getTipo();

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo);


	public Date getFechaCorte();

	public void setFechaCorte(Date date);

	public Date getFechaInicio();

	public void setFechaInicio(Date fechaInicio);

	public int getValorUnitario();

	public void setValorUnitario(int valorUnitario);

	public Double calcularImporte();

	public int getNumeroDias();
	
	public void setNumeroDias(int numeroDias);
	
	public Producto getProducto();

	public void setProducto(Producto producto);

	public void setItem(int item);
	
	public int getItem();

}
