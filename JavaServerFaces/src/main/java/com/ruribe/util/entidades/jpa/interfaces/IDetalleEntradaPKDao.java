package com.ruribe.util.entidades.jpa.interfaces;

public interface IDetalleEntradaPKDao {
	
	public int getIdEntrada();
	public void setIdEntrada(int idEntrada);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);
	
}
