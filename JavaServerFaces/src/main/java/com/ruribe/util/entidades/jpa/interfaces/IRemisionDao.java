/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;

import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleRemision;
import com.ruribe.util.entidades.jpa.Obra;
import com.ruribe.util.entidades.jpa.Transportador;

/**
 *Interfaz del entity Proveedor.
 * @author RONI
 *
 */
public interface IRemisionDao {
	
	public int getIdRemision();
	public void setIdRemision(int idRemision);
	public boolean isEnabled();
	public void setEnabled(boolean enabled);
	public Date getFecha();
	public void setFecha(Date fecha);
	public Date getFecha_entrega();
	public void setFecha_entrega(Date fecha_entrega);
	public int getTransporte();
	public void setTransporte(int transporte);
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	public List<DetalleRemision> getDetalleRemisions();
	public void setDetalleRemisions(List<DetalleRemision> detalleRemisions);
	public DetalleRemision addDetalleRemision(DetalleRemision detalleRemision);
	public DetalleRemision removeDetalleRemision(DetalleRemision detalleRemision);
	public Cliente getCliente();
	public void setCliente(Cliente cliente);
	public Obra getObra();
	public void setObra(Obra obra);
	public Transportador getTransportador();
	public void setTransportador(Transportador transportador);
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	public boolean isPrinted();
	public void setPrinted(boolean printed);

}
