/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.TipoProducto;


/**
 *Interfaz del entity Usuario.
 * @author RONI
 *
 */
public interface IReferenciaDao {
	
	
	/**
	 * @return the codigo_interno
	 */
	public int getCodigo_interno();



	/**
	 * @param codigo_interno the codigo_interno to set
	 */
	public void setCodigo_interno(int codigo_interno);



	/**
	 * @return the descripcion
	 */
	public String getDescripcion();



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);
	
	/**
	 * @return the precio_alquiler
	 */
	public int getPrecio_alquiler();



	/**
	 * @param precio_alquiler the precio_alquiler to set
	 */
	public void setPrecio_alquiler(int precio_alquiler);



	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion();



	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion);
	
	/**
	 * @return the peso
	 */
	public double getPeso();
	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso);
	
	/**
	 * @return the tipo_producto
	 */
	public TipoProducto getTipo_producto();
	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public void setTipo_producto(TipoProducto tipo_producto);
	
}
