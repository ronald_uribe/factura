package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Set;

import com.ruribe.util.entidades.jpa.Usuario;

public interface IRoleDao {
	
	public Long getRole_id();
	public void setRole_id(Long role_id);

	public String getAuthority();
	public void setAuthority(String authority);

	public Set<Usuario> getUsuarios();
	public void setUsuarios(Set<Usuario> usuarios);
}
