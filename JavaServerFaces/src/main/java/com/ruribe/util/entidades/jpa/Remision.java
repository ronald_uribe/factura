package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.ruribe.util.entidades.jpa.interfaces.IRemisionDao;


/**
 * The persistent class for the remision database table.
 * 
 */
@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name ="Remision.PDFCliente",query = "SELECT r.id_remision,r.id_cliente,r.fecha,r.id_transportador,r.transporte,r.observaciones,r.id_obra,sum(d.cantidad),p.codigo_interno,ref.precio_alquiler,ref.descripcion, ref.peso "
			+ "FROM soloandamios.remision r  "
			+ "inner join detalle_remision d on (r.id_remision=d.id_remision ) "
			+ "inner join producto p on (d.codigo_producto = p.codigo_producto) "
			+ "inner join referencia ref on (p.codigo_interno=ref.codigo_interno) "
			+ "where r.id_remision = ?id_remision "
			+ "group by id_remision,id_cliente,fecha,id_transportador,transporte,observaciones,id_obra,codigo_interno,precio_alquiler,descripcion")})
@NamedQueries({
				@NamedQuery(name="Remision.findAll", query="SELECT r FROM Remision r order by r.idRemision desc"),
				@NamedQuery(name="Remision.pk", query="SELECT r FROM Remision r join r.detalleRemisions d where r.idRemision =:id_remision order by d.producto.descripcion"),
				@NamedQuery(name="Remision.rangofechas", query="SELECT r FROM Remision r where r.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Remision.cliente.fecha", query="SELECT r FROM Remision r where r.cliente.id_cliente =:idCliente and r.fecha between :fechaInicial and :fechaCorte"),
				@NamedQuery(name="Remision.cliente", query="SELECT r FROM Remision r where r.cliente.id_cliente =:idCliente")})



public class Remision implements IRemisionDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static String QUERY_PDF_REMISION_PDF_CLIENTE="Remision.PDFCliente";
	public static String QUERY_FIND_ALL_REMISIONES="Remision.findAll";
	public static String QUERY_FIND_PK_REMISION="Remision.pk";
	public static String QUERY_FIND_RANGO_FECHAS="Remision.rangofechas";
	public static String QUERY_FIND_REMISION_CLIENTE="Remision.cliente";
	public static String QUERY_FIND_REMISION_CLIENTE_RANGO_FECHAS="Remision.cliente.fecha";
	
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_remision")
	private int idRemision;

	private boolean enabled;
	private boolean printed;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_entrega;

	@OneToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	
	@OneToOne
	@JoinColumn(name="id_obra")
	private Obra obra;
	
	private String usuario_creacion;
	
	@OneToOne
	@JoinColumn(name="id_transportador")
	private Transportador transportador;

	private int transporte;
	
	//bi-directional many-to-one association to DetalleRemision
	@OneToMany(mappedBy="remision",cascade={CascadeType.PERSIST,CascadeType.REMOVE},orphanRemoval=true)
	private List<DetalleRemision> detalleRemisions;

	private String observaciones;
	
	public Remision() {
		detalleRemisions = new ArrayList<DetalleRemision>();
	}

	public int getIdRemision() {return this.idRemision;}
	public void setIdRemision(int idRemision) {	this.idRemision = idRemision;}

	public boolean isEnabled() {return enabled;}
	public void setEnabled(boolean enabled) {this.enabled = enabled;}
	
	public boolean isPrinted() {return printed;}
	public void setPrinted(boolean printed) {this.printed = printed;}

	public Date getFecha() {return this.fecha;}
	public void setFecha(Date fecha) {	this.fecha = fecha;}

	public Date getFecha_entrega() {return fecha_entrega;}
	public void setFecha_entrega(Date fecha_entrega) {this.fecha_entrega = fecha_entrega;}

	public int getTransporte() {return this.transporte;}
	public void setTransporte(int transporte) {this.transporte = transporte;}

	public List<DetalleRemision> getDetalleRemisions() {return this.detalleRemisions;}

	public void setDetalleRemisions(List<DetalleRemision> detalleRemisions) {
		this.detalleRemisions = detalleRemisions;
	}

	public DetalleRemision addDetalleRemision(DetalleRemision detalleRemision) {
		getDetalleRemisions().add(detalleRemision);
	//	detalleRemision.setRemision(this);

		return detalleRemision;
	}

	public DetalleRemision removeDetalleRemision(DetalleRemision detalleRemision) {
		getDetalleRemisions().remove(detalleRemision);
	//	detalleRemision.setRemision(null);

		return detalleRemision;
	}

	
	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the obra
	 */
	public Obra getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(Obra obra) {
		this.obra = obra;
	}
	
	/**
	 * @return the transportador
	 */
	public Transportador getTransportador() {
		return transportador;
	}

	/**
	 * @param transportador the transportador to set
	 */
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	
	/**
	 * clases de prueba
	 */
	static public void main (String arg[]){
		// comprobamos la conexion a bd sea correcta.		
		try {
		    			    
			 EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaServerFaces");
			 EntityManager em = emf.createEntityManager();
			 em.getTransaction( ).begin( );
			 Remision remision = new Remision();
			 Cliente cliente = new Cliente();
			 Transportador transportador=new Transportador();
			 Obra obra = new Obra();
			 obra.setIdObra(51);
			 cliente.setId_cliente("900.539.520-8");
			 remision.setTransporte(512000);
			 remision.setFecha(new Date());
			 remision.setCliente(cliente);
			
			 transportador.setNo_documento("123456789");
			 remision.setTransportador(transportador);
			 remision.setObra(obra);
			 remision.setEnabled(true);
			 
			 Producto producto = new Producto();
			 producto.setCodigo_producto(2);
			
			 DetalleRemision detalle = new DetalleRemision();
			 detalle.setCantidad(5);
			 detalle.setRemision(remision);
			// detalle.setCodigoProducto(2);
			 detalle.setProducto(producto);
			 remision.addDetalleRemision(detalle);
			 em.persist(remision); 
			 
			 em.flush();
			 
			 //return abc.getId();
			System.out.println("#######id de remision:" + remision.getIdRemision());
			 em.getTransaction().commit();
			 em.close();
		      emf.close();
		      
		       emf = Persistence.createEntityManagerFactory("JavaServerFaces");
				  em = emf.createEntityManager();
				 em.getTransaction( ).begin( );
			TypedQuery <Remision> remisiones= em.createNamedQuery(Remision.QUERY_FIND_ALL_REMISIONES,Remision.class);
			 for(Remision c:remisiones.getResultList()){
				 System.out.println("remision:" + c.getIdRemision());
				 System.out.println("Cliente:" + c.getCliente().getId_cliente());
				 System.out.println("Cliente:" + c.getCliente().getRazon_social());
				 System.out.println("obra:" + c.getObra().getIdObra());
				 System.out.println("nombre obra:" + c.getObra().getNombre());
				 System.out.println("nombre tranportador:" + c.getTransportador().getNombre());
			 } 
			 em.getTransaction().commit();
			 em.close();
		      emf.close();
			 
		      
		    System.out.println("Got Connection.");
			} catch (Exception e) {
			    System.out.println("ERROR " + e.getMessage());
				e.printStackTrace();
			}
	}

}