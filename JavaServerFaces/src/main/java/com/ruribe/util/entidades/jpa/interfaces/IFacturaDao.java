package com.ruribe.util.entidades.jpa.interfaces;

import java.util.Date;
import java.util.List;

import com.ruribe.util.entidades.jpa.Cliente;
import com.ruribe.util.entidades.jpa.DetalleFactura;
import com.ruribe.util.entidades.jpa.Obra;

/**
 *Interfaz del entity Deolucion.
 * @author RONI
 *
 */
public interface IFacturaDao {

	public int getIdfactura();
	public void setIdfactura(int idfactura);
	public Date getFecha();
	public void setFecha(Date fecha);
	public Date getFecha_inicio();
	public void setFecha_inicio(Date fecha_inicio);
	public Date getFecha_corte();
	public void setFecha_corte(Date fecha_corte);
	public Cliente getCliente();
	public void setCliente(Cliente cliente);
	public Obra getObra();
	public void setObra(Obra obra);
	public double getIva();
	public void setIva(double iva);
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	public int getTransporte();
	public void setTransporte(int transporte);
	public double getDescuento();
	public Double getTotal();
	public void setDescuento(double descuento);
	public double getReposicion();
	public void setReposicion(double reposicion);
	public List<DetalleFactura> getDetallefactura();
	public void setDetallefactura(List<DetalleFactura> detallefactura);
	public DetalleFactura addDetalleDevolucion(DetalleFactura detalleFactura);
	public DetalleFactura removeDetalleDevolucion(DetalleFactura detalleFactura);
	public void setUsuario_creacion(String usuario_creacion);
	public String getUsuario_creacion();
	public Double getVr_parcial();
	public boolean isEnabled();
	public void setEnabled(boolean enabled);

	
}
