/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.DetalleEntradaPK;
import com.ruribe.util.entidades.jpa.Entrada;
import com.ruribe.util.entidades.jpa.Producto;


/**
 *Interfaz del entity IDetalleFacturaPK.
 * @author RONI
 *
 */
public interface IDetalleEntradaDao extends IDetalleEntradaPKDao {
	
	public DetalleEntradaPK getId();
	public void setId(DetalleEntradaPK detalleEntradaPK);
	public int getCantidad();
	public void setCantidad(int cantidad);
	public Entrada getEntrada();
	public void setEntrada(Entrada entrada);
	public int getIdEntrada();
	public void setIdEntrada(int idEntrada);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);
	public Producto getProducto();
	public void setProducto(Producto producto);

}
