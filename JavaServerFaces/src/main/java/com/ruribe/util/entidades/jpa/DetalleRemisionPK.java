package com.ruribe.util.entidades.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the detalle_remision database table.
 * 
 */
@Embeddable
public class DetalleRemisionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_remision", insertable=false, updatable=false )
	private int idRemision;

	@Column(name="codigo_producto", insertable=false, updatable=false )
	private int codigoProducto;

	public DetalleRemisionPK() {
	}
	
	/**
	 * @param idRemision
	 * @param codigoProducto
	 */
	public DetalleRemisionPK(int idRemision, int codigoProducto) {
		super();
		this.idRemision = idRemision;
		this.codigoProducto = codigoProducto;
	}

	public int getIdRemision() {
		return this.idRemision;
	}
	public void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}
	public int getCodigoProducto() {
		return this.codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DetalleRemisionPK)) {
			return false;
		}
		DetalleRemisionPK castOther = (DetalleRemisionPK)other;
		return 
			(this.idRemision == castOther.idRemision)
			&& (this.codigoProducto == castOther.codigoProducto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idRemision;
		hash = hash * prime + this.codigoProducto;
		
		return hash;
	}
}