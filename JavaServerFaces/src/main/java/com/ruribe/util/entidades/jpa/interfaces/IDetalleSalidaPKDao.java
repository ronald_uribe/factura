package com.ruribe.util.entidades.jpa.interfaces;

public interface IDetalleSalidaPKDao {
	
	public int getIdSalida();
	public void setIdSalida(int idSalida);
	public int getCodigoProducto();
	public void setCodigoProducto(int codigoProducto);
	
}
