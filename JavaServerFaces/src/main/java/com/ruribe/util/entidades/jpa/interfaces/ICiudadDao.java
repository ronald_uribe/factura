package com.ruribe.util.entidades.jpa.interfaces;

/**
 * Interfaz de Tipo de documento de usuario
 */

public interface ICiudadDao {
	
	/**
	 * @return the codigo_ciudad
	 */
	public int getCodigo_ciudad();
	public void setCodigo_ciudad(int codigo_ciudad);
	public String getNombre_ciudad();
	public void setNombre_ciudad(String nombre_ciudad);

}
