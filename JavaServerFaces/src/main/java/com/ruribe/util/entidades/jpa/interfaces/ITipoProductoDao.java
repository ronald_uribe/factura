package com.ruribe.util.entidades.jpa.interfaces;

/**
 * Interfaz de Tipo de producto de usuario
 */

public interface ITipoProductoDao {
	
	public int getCod_tipo_producto();
	public void setCod_tipo_producto(int tipoproducto);	
	public String getDescripcion();
	public void setDescripcion(String descripcion);

}
