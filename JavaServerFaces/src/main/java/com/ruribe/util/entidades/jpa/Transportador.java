package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

import com.ruribe.util.entidades.jpa.interfaces.ITransportadorDao;

/**
 * The persistent class for the Transportador database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="QUERY_FIND_ALL_Transportador",
			   query="SELECT u FROM Transportador u order by u.nombre"),
	@NamedQuery(name="QUERY_SELECT_ID_Transportador",
				query="select a from Transportador a where a.no_documento = :documento"),
	@NamedQuery(name="QUERY_SELECT_ID_LIKE_Transportador",
				query="select a from Transportador a where a.no_documento like :documento")
})
public class Transportador implements ITransportadorDao, Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL = "QUERY_FIND_ALL_Transportador";
	public static final String QUERY_SELECT_ID ="QUERY_SELECT_ID_Transportador";
	public static final String QUERY_SELECT_ID_LIKE ="QUERY_SELECT_ID_LIKE_Transportador";

	@Id
	private String no_documento;
	
	private int cod_tipo_documento;
	private String nombre;
	private String direccion;
	private String telefono;
	private String celular;
	private String email;
	private String placa;
	private String descripcion;
	private Boolean enabled;
	/**
	 * @return the no_documento
	 */
	public String getNo_documento() {
		return no_documento;
	}
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento) {
		this.no_documento = no_documento;
	}
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}
	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}
	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	/**
	 * @return the descripcionVehiculo
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcionVehiculo the descripcionVehiculo to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


}