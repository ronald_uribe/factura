package com.ruribe.util.entidades.jpa;
import java.io.Serializable;

import javax.persistence.*;
import com.ruribe.util.entidades.jpa.interfaces.IDetalleReposicionDao;


/**
 * The persistent class for the detalle_reposicion database table.
 * 
 */
@Entity
@Table(name="detalle_reposicion")
@NamedQuery(name="DetalleReposicion.findAll", query="SELECT d FROM DetalleReposicion d")
public class DetalleReposicion implements IDetalleReposicionDao, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleReposicionPK detalleReposicionPK;

	private int cantidad;
	private int precio_reposicion;
	
	//bi-directional many-to-one association to Reposicion
	@ManyToOne
	@JoinColumn(name="id_reposicion")
	private Reposicion reposicion;
	
	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="codigo_producto")
	private Producto producto; 

//	@ManyToOne(cascade={CascadeType.PERSIST})
//	@JoinColumns({
//	    @JoinColumn(name="id_remision", referencedColumnName="id_remision",insertable=false, updatable=false),
//	    @JoinColumn(name="codigo_producto", referencedColumnName="codigo_producto",insertable=false, updatable=false)
//	})
//	private DetalleRemision detalleremision;
	
	public DetalleReposicion() {
		detalleReposicionPK = new DetalleReposicionPK();
	}
	public DetalleReposicion(int reposicionId, int productoId) {
		detalleReposicionPK = new DetalleReposicionPK(reposicionId, productoId);
    }
	
	public DetalleReposicionPK getId() {
		return this.detalleReposicionPK;
	}

	public void setId(DetalleReposicionPK detalleReposicionPK) {
		this.detalleReposicionPK = detalleReposicionPK;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Reposicion getReposicion() {
		return this.reposicion;
	}

	public void setReposicion(Reposicion reposicion) {
		this.reposicion = reposicion;
	}
	
	public final int getIdReposicion() {
		return detalleReposicionPK.getIdReposicion();
	}
	public final void setIdReposicion(int idReposicion) {
		this.detalleReposicionPK.setIdReposicion(idReposicion);
		
	}
	public final int getCodigoProducto() {
		
		return detalleReposicionPK.getCodigoProducto();
	}
	public final void setCodigoProducto(int codigoProducto) {
		this.detalleReposicionPK.setCodigoProducto(codigoProducto);
	}
	
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion() {
		return precio_reposicion;
	}
	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion) {
		this.precio_reposicion = precio_reposicion;
	}

}