/**
 * 
 */
package com.ruribe.util.entidades.jpa.interfaces;

import com.ruribe.util.entidades.jpa.Cliente;

/**
 *Interfaz del entity Proveedor.
 * @author RONI
 *
 */
public interface IObraDao {
	
	public int getIdObra();
	public void setIdObra(int idObra);
	public String getDireccion();
	public void setDireccion(String direccion);
	public String getNombre();
	public void setNombre(String nombre);
	public String getTelefono();
	public void setTelefono(String telefono);
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);
	public String getContacto();
	public void setContacto(String contacto);
	public Cliente getCliente();
	public void setCliente(Cliente cliente);

}
