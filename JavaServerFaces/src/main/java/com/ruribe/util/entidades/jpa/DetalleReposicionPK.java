package com.ruribe.util.entidades.jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the detalle_remision database table.
 * 
 */
@Embeddable
public class DetalleReposicionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_reposicion", insertable=false, updatable=false )
	private int idReposicion;

	@Column(name="codigo_producto", insertable=false, updatable=false )
	private int codigoProducto;
	
	public DetalleReposicionPK() {
	}
	
	/**
	 * @param idReposicion
	 * @param codigoProducto
	 */
	public DetalleReposicionPK(int idReposicion, int codigoProducto) {
		super();
		this.idReposicion = idReposicion;
		this.codigoProducto = codigoProducto;
	}

	public int getIdReposicion() {
		return this.idReposicion;
	}
	public void setIdReposicion(int idReposicion) {
		this.idReposicion = idReposicion;
	}
	public int getCodigoProducto() {
		return this.codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigoProducto;
		result = prime * result + idReposicion;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleReposicionPK other = (DetalleReposicionPK) obj;
		if (codigoProducto != other.codigoProducto)
			return false;
		if (idReposicion != other.idReposicion)
			return false;
		return true;
	}

}