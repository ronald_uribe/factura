package com.ruribe.util;

public class SAExcepcion extends Exception {

	private static final long serialVersionUID = 1L;
	/** Codigo del error. */
    private String codigo;

    /** Clave del mensaje error en el fichero de mensajes. */
    private String clave;

    /** Clave del mensaje de error para el log en el fichero de mensajes. */
    private String log;

    /** Parametros a sustituir en el mensaje de error. */
    private String parametros;
	private Throwable cause;


	public SAExcepcion() {
	}


	public SAExcepcion(String message, Throwable cause) {
		clave=message;
	}

	public SAExcepcion(String message) {
		this.clave=message;
	}

	public SAExcepcion(Throwable cause) {
		this.cause=cause;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getParametros() {
		return parametros;
	}

	public void setParametros(String parametros) {
		this.parametros = parametros;
	}


	public Throwable getCause() {
		return cause;
	}


	public void setCause(Throwable cause) {
		this.cause = cause;
	}
	
	

}
