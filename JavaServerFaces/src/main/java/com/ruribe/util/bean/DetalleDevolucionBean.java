package com.ruribe.util.bean;

import java.io.Serializable;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleDevolucionBean implements IDetalleDevolucionBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	private IProductoBean iproductoBean;
	private int cantidad;
	private int pendiente;
	private int idRemision;
	private int idDevolucion;

	public DetalleDevolucionBean() {
		super();
	}


	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the iproductoBean
	 */
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}


	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion() {
		return idDevolucion;
	}


	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion) {
		this.idDevolucion = idDevolucion;
	}


	/**
	 * @return the idRemision
	 */
	public int getIdRemision() {
		return idRemision;
	}


	/**
	 * @param idRemision the idRemision to set
	 */
	public void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}


	/**
	 * @return the pendiente
	 */
	public int getPendiente() {
		return pendiente;
	}


	/**
	 * @param pendiente the pendiente to set
	 */
	public void setPendiente(int pendiente) {
		this.pendiente = pendiente;
	}

	
}
