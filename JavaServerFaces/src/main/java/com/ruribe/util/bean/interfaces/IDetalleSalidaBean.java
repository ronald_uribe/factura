/**
 * 
 */
package com.ruribe.util.bean.interfaces;


/**
 * @author RONI
 *
 */
public interface IDetalleSalidaBean {
	

	/**
	 * @return the iobraBean
	 */
	public  IProductoBean getIproductoBean();

	/**
	 * @param iobraBean the iobraBean to set
	 */
	public  void setIproductoBean(IProductoBean iProductoBean);
	
	/**
	 * @return the cantidad
	 */
	public  int getCantidad();

	/**
	 * @param int the cantidad to set
	 */
	public  void setCantidad(int cantidad);
	
	/**
	 * @return the pendiente
	 */
	public int getPendiente();

	/**
	 * @param pendiente the pendiente to set
	 */
	public void setPendiente(int pendiente);
	
	

}
