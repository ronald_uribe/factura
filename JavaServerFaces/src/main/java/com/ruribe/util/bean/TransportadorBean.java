package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.ITransportadorBean;

public class TransportadorBean implements ITransportadorBean, Serializable{
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	private String no_documento;
	private int cod_tipo_documento;
	
	/**
	 * @param no_documento
	 * @param cod_tipo_documento
	 * @param nombre
	 * @param direccion
	 * @param telefono
	 * @param celular
	 * @param email
	 * @param placa
	 * @param descripcion
	 * @param editable
	 * @param activo
	 */
	public TransportadorBean(String no_documento, int cod_tipo_documento,
			String nombre, String direccion, String telefono, String celular,
			String email, String placa, String descripcion, boolean editable,
			boolean enabled) {
		super();
		this.no_documento = no_documento;
		this.cod_tipo_documento = cod_tipo_documento;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.email = email;
		this.placa = placa;
		this.descripcion = descripcion;
		this.editable = editable;
		this.enabled = enabled;
	}
	/**
	 * 
	 */
	public TransportadorBean() {
	}
	private String nombre;
	private String direccion;
	private String telefono;
	private String celular;
	private String email;
	private String placa;
	private String descripcion;
	private boolean editable;
	private String tipoDocumentoSelect;
	private boolean enabled;
	/**
	 * @return the no_documento
	 */
	public String getNo_documento() {
		return no_documento;
	}
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento) {
		this.no_documento = no_documento;
	}
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}
	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}
	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}
	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	/**
	 * @return the tipoDocumentoSelect
	 */
	public String getTipoDocumentoSelect() {
		return tipoDocumentoSelect;
	}
	/**
	 * @param tipoDocumentoSelect the tipoDocumentoSelect to set
	 */
	public void setTipoDocumentoSelect(String tipoDocumentoSelect) {
		this.tipoDocumentoSelect = tipoDocumentoSelect;
	}
	/**
	 * @return the activo
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * @param activo the activo to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


}
