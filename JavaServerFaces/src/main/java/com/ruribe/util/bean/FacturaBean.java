package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date; 
import java.util.List;

import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.util.bean.interfaces.IObraBean;

public class FacturaBean implements IFacturaBean, Serializable{


	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = 1L;
	private int idFactura;
	private IClienteBean iclienteBean;
	private IObraBean iobraBean;
	private Date fecha_inicio; 
	private Date fecha_corte; 
	private Date fecha_factura;
	private List<IDetalleFacturaBean> listadetalleFactura;
	private Double vr_parcial;
	private Double descuento;
	private int reposicion;
	private Long transporte;
	private Double total;
	private Double iva;
	private String usuario;
	private String observaciones;
	private boolean estado;

	public FacturaBean() {
		super();
		iclienteBean = new ClienteBean();
		iobraBean = new ObraBean();
		listadetalleFactura= new ArrayList<IDetalleFacturaBean>();
	}
	
	/**
	 * @return the idFactura
	 */
	public int getIdFactura() {
		return idFactura;
	}

	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	/**
	 * @return the fecha_inicio
	 */
	public Date getFecha_inicio() {
		return fecha_inicio;
	}


	/**
	 * @param fecha_inicio the fecha_inicio to set
	 */
	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}


	/**
	 * @return the fecha_corte
	 */
	public Date getFecha_corte() {
		return fecha_corte;
	}


	/**
	 * @param fecha_corte the fecha_corte to set
	 */
	public void setFecha_corte(Date fecha_corte) {
		this.fecha_corte = fecha_corte;
	}


	/**
	 * @return the fecha_factura
	 */
	public Date getFecha_factura() {
		return fecha_factura;
	}

	/**
	 * @param fecha_factura the fecha_factura to set
	 */
	public void setFecha_factura(Date fecha_factura) {
		this.fecha_factura = fecha_factura;
	}

	/**
	 * @return the iclienteBean
	 */
	public IClienteBean getIclienteBean() {
		return iclienteBean;
	}


	/**
	 * @param iclienteBean the iclienteBean to set
	 */
	public void setIclienteBean(IClienteBean iclienteBean) {
		this.iclienteBean = iclienteBean;
	}


	/**
	 * @return the iobraBean
	 */
	public IObraBean getIobraBean() {
		return iobraBean;
	}


	/**
	 * @param iobraBean the iobraBean to set
	 */
	public void setIobraBean(IObraBean iobraBean) {
		this.iobraBean = iobraBean;
	}

	/**
	 * @return the listadetalleFactura
	 */
	public List<IDetalleFacturaBean> getListadetalleFactura() {
		return listadetalleFactura;
	}

	/**
	 * @param listadetalleFactura the listadetalleFactura to set
	 */
	public void setListadetalleFactura(List<IDetalleFacturaBean> listadetalleFactura) {
		this.listadetalleFactura = listadetalleFactura;
	}
	
	/**
	 * @param listadetalleFactura the listadetalleFactura to set
	 */
	public void setListadetalleFactura(IDetalleFacturaBean detalleFactura) {
		this.listadetalleFactura.add(detalleFactura);
	}
	
	/**
	 * @return the vr_total
	 */
	@Override
	public Double getVr_parcial() {
		return vr_parcial;
	}


	/**
	 * @param vr_total the vr_total to set
	 */
	@Override
	public void setVr_parcial(Double parcial) {
		this.vr_parcial = parcial;
	}
	
	/**
	 * @return the transporte
	 */
	public Long getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(Long transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the iva
	 */
	public Double getIva() {
		return iva;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(Double iva) {
		this.iva = iva;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return the reposicion
	 */
	public int getReposicion() {
		return reposicion;
	}

	/**
	 * @param reposicion the reposicion to set
	 */
	public void setReposicion(int reposicion) {
		this.reposicion = reposicion;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	

}
