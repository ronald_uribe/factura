/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IDevolucionConsultaBean {
	
	/**
	 * @return the idDevolucion
	 */
	public String getIdDevolucion();
	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(String idDevolucion);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	/**
	 * @return the idCliente
	 */
	public String getIdCliente();
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente);
	
	/**
	 * @return the listaDevoluciones
	 */
	public List<IDevolucionBean> getListaDevoluciones();
	/**
	 * @param listaDevoluciones the listaDevoluciones to set
	 */
	public void setListaDevoluciones(List<IDevolucionBean> listaDevoluciones);
}
