package com.ruribe.util.bean.interfaces;

public interface IUser {

	public String getIdUsuario() ;
	public void setIdUsuario(String idUsuario);
	public String getClave() ;
	public void setClave(String clave);
	
	public boolean isLoggedIn();
 
    public void setLoggedIn(boolean loggedIn);
}
