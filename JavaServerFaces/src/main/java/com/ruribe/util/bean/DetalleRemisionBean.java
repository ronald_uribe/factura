package com.ruribe.util.bean;

import java.io.Serializable;
import com.ruribe.util.bean.interfaces.IDetalleRemisionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleRemisionBean implements IDetalleRemisionBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	private IProductoBean iproductoBean;
	private int cantidad;

	public DetalleRemisionBean() {
		super();
	}


	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the iproductoBean
	 */
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}

	
}
