package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.IReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionConsultaBean;

public class ReposicionConsultaBean implements IReposicionConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idReposicion;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idCliente;
	private List<IReposicionBean> listaReposiciones;
	
	/**
	 * @return the idReposicion
	 */
	public String getIdReposicion() {
		return idReposicion;
	}
	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(String idReposicion) {
		this.idReposicion = idReposicion;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return the listaReposiciones
	 */
	public List<IReposicionBean> getListaReposiciones() {
		return listaReposiciones;
	}
	/**
	 * @param listaReposiciones the listaReposiciones to set
	 */
	public void setListaReposiciones(List<IReposicionBean> listaReposiciones) {
		this.listaReposiciones = listaReposiciones;
	}
}
