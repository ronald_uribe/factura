/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IReposicionConsultaBean {
	
	/**
	 * @return the idReposicion
	 */
	public String getIdReposicion();
	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(String idReposicion);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	/**
	 * @return the idCliente
	 */
	public String getIdCliente();
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente);
	
	/**
	 * @return the listaReposiciones
	 */
	public List<IReposicionBean> getListaReposiciones();
	/**
	 * @param listaReposiciones the listaReposiciones to set
	 */
	public void setListaReposiciones(List<IReposicionBean> listaReposiciones);
}
