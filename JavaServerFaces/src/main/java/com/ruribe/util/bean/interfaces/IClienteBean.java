/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.List;

/**
 * @author RONI
 *
 */
public interface IClienteBean {
	
	
	public boolean isEditable();
	public String getTipoDocumentoSelect();
	public  void setEditable(boolean editable);
	public void setTipoDocumentoSelect(String tipoDocumentoSelect);
	public void setCiudadSelect(String string);
	public String getClienteSelect();
	public void setClienteSelect(String clienteSelect);
	public String getId_cliente();
	public void setId_cliente(String id_cliente);
	public int getCod_tipo_documento();
	public void setCod_tipo_documento(int cod_tipo_documento);
	public String getRazon_social();
	public void setRazon_social(String razon_social);
	public String getDireccion();
	public void setDireccion(String direccion);
	public int getCod_ciudad();
	public void setCod_ciudad(int cod_ciudad);
	public String getTelefono();
	public void setTelefono(String telefono);
	public String getCelular();
	public void setCelular(String celular);
	public String getEmail();
	public void setEmail(String email);
	public List<IObraBean> getListaObras();
	public void setListaObras(IObraBean obra);
	void setListaObras(List<IObraBean> obra);
	public String getCiudadSelect();
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);

}
