package com.ruribe.util.bean;

import java.io.Serializable;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleReposicionBean implements IDetalleReposicionBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	private IProductoBean iproductoBean;
	private int cantidad;
	private int pendiente;
	private int idRemision;
	private int idReposicion;
	private int precio_reposicion;

	public DetalleReposicionBean() {
		super();
	}


	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion() {
		return precio_reposicion;
	}


	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion) {
		this.precio_reposicion = precio_reposicion;
	}


	/**
	 * @return the iproductoBean
	 */
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}


	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion() {
		return idReposicion;
	}


	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion) {
		this.idReposicion = idReposicion;
	}


	/**
	 * @return the idRemision
	 */
	public int getIdRemision() {
		return idRemision;
	}


	/**
	 * @param idRemision the idRemision to set
	 */
	public void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}


	/**
	 * @return the pendiente
	 */
	public int getPendiente() {
		return pendiente;
	}


	/**
	 * @param pendiente the pendiente to set
	 */
	public void setPendiente(int pendiente) {
		this.pendiente = pendiente;
	}

	
}
