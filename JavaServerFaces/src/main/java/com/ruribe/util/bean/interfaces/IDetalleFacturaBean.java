package com.ruribe.util.bean.interfaces;

import java.util.Date;



public interface IDetalleFacturaBean{

	public int getIdFactura();

	public void setIdFactura(int idFactura);

	public int getCantidad();

	public void setCantidad(int cantidad);

	public IProductoBean getIproductoBean();

	public void setIproductoBean(IProductoBean iproductoBean);

	public int getIdRemision();

	public void setIdRemision(int idRemision);

	public Date getFecha_inicial();

	public void setFecha_inicial(Date fecha_inicial);

	public Date getFecha_final();
	
	public Double getVr_total();
	public void setVr_total(Double valor);

	public void setFecha_final(Date fecha_final);

	public int getNo_dias();

	public void setNo_dias(int no_dias);

	public int getVr_unitario();

	public void setVr_unitario(int vr_unitario);
	
	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto();


	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto);
	
	
	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);
	
	/**
	 * @return the devolucion
	 */
	public int getDevolucion();
	/**
	 * @param devolucion the devolucion to set
	 */
	public void setDevolucion(int devolucion);
	
	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion();

	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion);
	
	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion();

	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion);
	
	/**
	 * @return the tipo
	 */
	public String getTipo();


	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo);

	public void setCantidad_gestionada(int intValue);
	/**
	 * @return the cantidad_gestionada
	 */
	public int getCantidad_gestionada();
	
}
