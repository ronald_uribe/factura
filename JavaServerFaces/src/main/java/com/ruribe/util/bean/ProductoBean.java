package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;

public class ProductoBean implements IProductoBean, Serializable{

	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private int codigo_producto; 
	private IReferenciaBean referenciaBean;
	private ITipoProductoBean tipo_producto;
	private IProveedorBean proveedorBean;
	private String codProveedorSelect;
	private String descripcion;
	private int stock; 
	private boolean editable;
	private String codReferenciaSelect;
	private String codTipoProductoSelect;
	private IProveedorBean proveedor;
	private int maximo;
	private int minimo;
	private int subarriendo;
	
	public ProductoBean() {
		super();
		referenciaBean = new ReferenciaBean();
	}

	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_producto() {
		return codigo_producto;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}

	/**
	 * @return the referencia
	 */
	public IReferenciaBean getReferenciaBean() {
		return referenciaBean;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferenciaBean(IReferenciaBean referencia) {
		this.referenciaBean = referencia;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the stock
	 */
	public final int getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public final void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * @return the codProveedorSelect
	 */
	public final String getCodProveedorSelect() {
		return codProveedorSelect;
	}

	/**
	 * @param codProveedorSelect the codProveedorSelect to set
	 */
	public final void setCodProveedorSelect(String codProveedorSelect) {
		this.codProveedorSelect = codProveedorSelect;
	}

	/**
	 * @return the codTipoProductoSelect
	 */
	public final String getCodTipoProductoSelect() {
		return codTipoProductoSelect;
	}

	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public final void setCodTipoProductoSelect(String codTipoProductoSelect) {
		this.codTipoProductoSelect = codTipoProductoSelect;
	}

	/**
	 * @return the tipo_producto
	 */
	public final ITipoProductoBean getTipo_producto() {
		return tipo_producto;
	}

	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public final void setTipo_producto(ITipoProductoBean tipo_producto) {
		this.tipo_producto = tipo_producto;
	}

	/**
	 * @return the proveedor
	 */
	public final IProveedorBean getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public final void setProveedor(IProveedorBean proveedor) {
		this.proveedor = proveedor;
	}


	/**
	 * @return the maximo
	 */
	public final int getMaximo() {
		return maximo;
	}


	/**
	 * @param maximo the maximo to set
	 */
	public final void setMaximo(int maximo) {
		this.maximo = maximo;
	}


	/**
	 * @return the minimo
	 */
	public final int getMinimo() {
		return minimo;
	}


	/**
	 * @param minimo the minimo to set
	 */
	public final void setMinimo(int minimo) {
		this.minimo = minimo;
	}


	/**
	 * @return the subarriendo
	 */
	public final int getSubarriendo() {
		return subarriendo;
	}


	/**
	 * @param subarriendo the subarriendo to set
	 */
	public final void setSubarriendo(int subarriendo) {
		this.subarriendo = subarriendo;
	}



	/**
	 * @return the proveedorBean
	 */
	public final IProveedorBean getProveedorBean() {
		return proveedorBean;
	}



	/**
	 * @param proveedorBean the proveedorBean to set
	 */
	public final void setProveedorBean(IProveedorBean proveedorBean) {
		this.proveedorBean = proveedorBean;
	}
	
	/**
	 * @return the codReferenciaSelect
	 */
	public String getCodReferenciaSelect() {
		return codReferenciaSelect;
	}

	/**
	 * @param codReferenciaSelect the codReferenciaSelect to set
	 */
	public void setCodReferenciaSelect(String codReferenciaSelect) {
		this.codReferenciaSelect = codReferenciaSelect;
	}

}
