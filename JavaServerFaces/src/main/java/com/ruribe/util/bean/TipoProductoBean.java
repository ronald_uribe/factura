package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.List;

import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;

public class TipoProductoBean implements ITipoProductoBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private int cod_tipo_producto ;
	private String descripcion;
	private boolean editable;
	private List<IProductoBean> iproductoBean;
	
	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the codigo_tipo_producto
	 */
	public final int getCod_tipo_producto() {
		return cod_tipo_producto;
	}

	/**
	 * @param codigo_tipo_producto the codigo_tipo_producto to set
	 */
	public final void setCod_tipo_producto(int codigo_tipo_producto) {
		this.cod_tipo_producto = codigo_tipo_producto;
	}

	/**
	 * @return the iproductoBean
	 */
	public final List<IProductoBean> getIproductoBean() {
		return iproductoBean;
	}

	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(List<IProductoBean> iproductoBean) {
		this.iproductoBean = iproductoBean;
	}

}
