/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 * interfaz de DevolucionBean
 *
 */
public interface IDevolucionBean {
	
	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion();

	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion);

	/**
	 * @return the estado
	 */
	public boolean isEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(boolean estado);
	
	/**
	 * @return the iclienteBean
	 */
	public  IClienteBean getIclienteBean();
	public  void setIclienteBean(IClienteBean iclienteBean);

	/**
	 * @return the iobraBean
	 */
	public  IObraBean getIobraBean();
	public  void setIobraBean(IObraBean iobraBean);
	
	/**
	 * @return the producto
	 */
	public IProductoBean getProducto() ;
	public void setProducto(IProductoBean producto);

	/**
	 * @return the selectProducto
	 */
	public IProductoBean getSelectProducto();
	public void setSelectProducto(IProductoBean selectProducto);
	
	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto();
	public void setListaProducto(List<IProductoBean> listaProducto);
	
	
	/**
	 * @devuelve un IProductoBean
	 */
	public IProductoBean getProducto(int id) ;
	
	/**
	 * @return the cantidad
	 */
	public int getCantidad();
	public void setCantidad(int cantidad);
	
	/**
	 * @return the detalleDevolucions
	 */
	public List<IDetalleDevolucionBean> getDetalleDevoluciones();

	/**
	 * @param detalleDevolucions the detalleDevolucions to set
	 */
	public void setDetalleDevoluciones(List<IDetalleDevolucionBean> detalleDevolucions);
	public void setDetalleDevoluciones(IDetalleDevolucionBean detalle);
	
	public int getCodigo_producto();
	public void setCodigo_producto(int codigo_producto);

	public int getTransporte();
	public void setTransporte(int transporte);
	
	public Date getFecha();
	public void setFecha(Date fecha);
	
	public Date getFecha_entrega();
	public void setFecha_entrega(Date fecha_entrega);
	
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	
	public ITransportadorBean getItransportadorBean();
	public void setItransportadorBean(ITransportadorBean itransportadorBean);
	
	public String getUsuario_creacion();
	public void setUsuario_creacion(String usuario_creacion);
	
	public boolean isPrinted();
	public void setPrinted(boolean printed);
	
}
