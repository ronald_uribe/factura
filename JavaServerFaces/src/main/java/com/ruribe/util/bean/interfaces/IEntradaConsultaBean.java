/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IEntradaConsultaBean {
	
	/**
	 * @return the idEntrada
	 */
	public String getIdEntrada();
	/**
	 * @param idEntrada the idEntrada to set
	 */
	public void setIdEntrada(String idEntrada);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	/**
	 * @return the idProveedor
	 */
	public String getIdProveedor();
	/**
	 * @param idProveedor the idProveedor to set
	 */
	public void setIdProveedor(String idProveedor);
	
	/**
	 * @return the listaEntradas
	 */
	public List<IEntradaBean> getListaEntradas();
	/**
	 * @param listaEntradas the listaEntradas to set
	 */
	public void setListaEntradas(List<IEntradaBean> listaEntradas);
}
