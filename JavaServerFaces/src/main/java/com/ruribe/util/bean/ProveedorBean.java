package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IProveedorBean;

public class ProveedorBean implements Serializable,IProveedorBean {
	
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = 1L;
	private String no_documento;
	private String codigo_proveedor;
	private int cod_tipo_documento;
	private String razon_social;
	private String direccion;
	private int cod_ciudad;	
	private String telefono;
	private String celular;
	private String email;
	private boolean editable;
	private String ciudadSelect;
	private String tipoDocumentoSelect;
	private boolean enabled;

	public ProveedorBean() {
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the no_documento
	 */
	public String getNo_documento() {
		return no_documento;
	}
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento) {
		this.no_documento = no_documento;
	}
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}
	/**
	 * @return the razon_social
	 */
	public String getRazon_social() {
		return razon_social;
	}
	/**
	 * @param razon_social the razon_social to set
	 */
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the cod_ciudad
	 */
	public int getCod_ciudad() {
		return cod_ciudad;
	}
	/**
	 * @param cod_ciudad the cod_ciudad to set
	 */
	public void setCod_ciudad(int cod_ciudad) {
		this.cod_ciudad = cod_ciudad;
	}


	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}


	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the ciudadSelect
	 */
	public final String getCiudadSelect() {
		return ciudadSelect;
	}

	/**
	 * @param ciudadSelect the ciudadSelect to set
	 */
	public final void setCiudadSelect(String ciudadSelect) {
		this.ciudadSelect = ciudadSelect;
	}

	/**
	 * @return the tipoDocumentoSelect
	 */
	public final String getTipoDocumentoSelect() {
		return tipoDocumentoSelect;
	}

	/**
	 * @param tipoDocumentoSelect the tipoDocumentoSelect to set
	 */
	public final void setTipoDocumentoSelect(String tipoDocumentoSelect) {
		this.tipoDocumentoSelect = tipoDocumentoSelect;
	}

	/**
	 * @return the celular
	 */
	public final String getCelular() {
		return celular;
	}

	/**
	 * @param celular the celular to set
	 */
	public final void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @return the email
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public final void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo_proveedor() {
		return codigo_proveedor;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo_proveedor(String codigo) {
		this.codigo_proveedor = codigo;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
