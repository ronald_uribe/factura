package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleRemisionBean;
import com.ruribe.util.bean.interfaces.IObraBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IRemisionBean;
import com.ruribe.util.bean.interfaces.ITransportadorBean;

public class RemisionBean implements IRemisionBean, Serializable{
	
	/**
	 *serialVersionUID. 
	 */
		
	private static final long serialVersionUID = 1L;

	private int idRemision;
	private boolean estado;
	private boolean printed;
	private IClienteBean iclienteBean;
	private ITransportadorBean itransportadorBean;
	private IObraBean iobraBean;
	
	private Date fecha;
	private Date fecha_entrega;
	private int transporte;
	private int codigo_producto;
	private int cantidad;
	private String observaciones;
	private List<IDetalleRemisionBean> detalleRemisions;
	private IProductoBean producto;
	private IProductoBean selectProducto;
	private List<IProductoBean> listaProducto;
	private String usuario_creacion;
	
	public RemisionBean() {
		super();
		iclienteBean = new ClienteBean();
		iobraBean = new ObraBean();
		itransportadorBean = new TransportadorBean();
		detalleRemisions = new ArrayList<IDetalleRemisionBean>();
	}
	
	/**
	 * @return the iclienteBean
	 */
	public final IClienteBean getIclienteBean() {
		return iclienteBean;
	}

	/**
	 * @param iclienteBean the iclienteBean to set
	 */
	public final void setIclienteBean(IClienteBean iclienteBean) {
		this.iclienteBean = iclienteBean;
	}

	/**
	 * @return the iobraBean
	 */
	public final IObraBean getIobraBean() {
		return iobraBean;
	}

	/**
	 * @param iobraBean the iobraBean to set
	 */
	public final void setIobraBean(IObraBean iobraBean) {
		this.iobraBean = iobraBean;
	}

	/**
	 * @return the producto
	 */
	public final IProductoBean getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public final void setProducto(IProductoBean producto) {
		this.producto = producto;
	}

	/**
	 * @return the selectProducto
	 */
	public final IProductoBean getSelectProducto() {
		return selectProducto;
	}

	/**
	 * @param selectProducto the selectProducto to set
	 */
	public final void setSelectProducto(IProductoBean selectProducto) {
		this.selectProducto = selectProducto;
	}

	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto() {
		return listaProducto;
	}

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto) {
		this.listaProducto = listaProducto;
	}
	
	/**
	 * retorna un objeto de una lista de tipo IProductoBean
	 * @param String codigo_producto
	 * @return IProductoBean
	 */
	public IProductoBean getProducto(int id) {
        
        for (IProductoBean producto : listaProducto){
            if (id==producto.getCodigo_producto()){
                return producto;
            }
        }
        return null;
    }

	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the detalleRemisions
	 */
	public final List<IDetalleRemisionBean> getDetalleRemisions() {
		return detalleRemisions;
	}

	/**
	 * @param detalleRemisions the detalleRemisions to set
	 */
	public final void setDetalleRemisions(
			List<IDetalleRemisionBean> detalleRemisions) {
		this.detalleRemisions = detalleRemisions;
	}

	@Override
	public void setDetalleRemisions(IDetalleRemisionBean detalle) {
		this.detalleRemisions.add(detalle);
	}

	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_producto() {
		return codigo_producto;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}

	/**
	 * @return the transporte
	 */
	public final int getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public final void setTransporte(int transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the fecha
	 */
	public final Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public final void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the fecha_entrega
	 */
	public Date getFecha_entrega() {
		return fecha_entrega;
	}

	/**
	 * @param fecha_entrega the fecha_entrega to set
	 */
	public void setFecha_entrega(Date fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}

	/**
	 * @return the idRemision
	 */
	public final int getIdRemision() {
		return idRemision;
	}

	/**
	 * @param idRemision the idRemision to set
	 */
	public final void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}

	/**
	 * @return the estado
	 */
	public final boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public final void setEstado(boolean estado) {
		this.estado = estado;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the itransportadorBean
	 */
	public ITransportadorBean getItransportadorBean() {
		return itransportadorBean;
	}

	/**
	 * @param itransportadorBean the itransportadorBean to set
	 */
	public void setItransportadorBean(ITransportadorBean itransportadorBean) {
		this.itransportadorBean = itransportadorBean;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	public boolean isPrinted() {return printed;}
	public void setPrinted(boolean printed) {this.printed = printed;}
	
	
	
}
