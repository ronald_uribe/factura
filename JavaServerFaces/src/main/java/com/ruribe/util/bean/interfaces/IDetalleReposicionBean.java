/**
 * 
 */
package com.ruribe.util.bean.interfaces;


/**
 * @author RONI
 *
 */
public interface IDetalleReposicionBean {
	

	/**
	 * @return the iobraBean
	 */
	public  IProductoBean getIproductoBean();

	/**
	 * @param iobraBean the iobraBean to set
	 */
	public  void setIproductoBean(IProductoBean iProductoBean);
	
	/**
	 * @return the cantidad
	 */
	public  int getCantidad();

	/**
	 * @param int the cantidad to set
	 */
	public  void setCantidad(int cantidad);
	
	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion();


	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion);
	
	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion() ;


	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion);


	/**
	 * @return the idRemision
	 */
	public int getIdRemision();


	/**
	 * @param idRemision the idRemision to set
	 */
	public void setIdRemision(int idRemision);
	
	/**
	 * @return the pendiente
	 */
	public int getPendiente();


	/**
	 * @param pendiente the pendiente to set
	 */
	public void setPendiente(int pendiente);

}
