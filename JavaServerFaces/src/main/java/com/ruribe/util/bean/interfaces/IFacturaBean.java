package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;

public interface IFacturaBean {
	
	public int getIdFactura();
	public void setIdFactura(int idFactura);
	public Date getFecha_inicio();
	public void setFecha_inicio(Date fecha_inicio);
	public Date getFecha_corte();
	public void setFecha_corte(Date fecha_corte);
	public Date getFecha_factura();
	public void setFecha_factura(Date fecha_factura);
	public IClienteBean getIclienteBean();
	public void setIclienteBean(IClienteBean iclienteBean);
	public IObraBean getIobraBean();
	public void setIobraBean(IObraBean iobraBean);
	public List<IDetalleFacturaBean> getListadetalleFactura();
	public void setListadetalleFactura(List<IDetalleFacturaBean> listadetalleFactura);
	public void setListadetalleFactura(IDetalleFacturaBean detalleFactura);
	public Double getVr_parcial();
	public void setVr_parcial(Double parcial);
	public Long getTransporte() ;
	public void setTransporte(Long transporte);
	public Double getTotal();
	public void setTotal(Double valorTotal);
	public Double getIva();
	public void setIva(Double iva);
	public void setUsuario(String userName);
	public String getUsuario();
	public void setObservaciones(String observaciones);
	public String getObservaciones();
	public Double getDescuento();
	public void setDescuento(Double d);
	public int getReposicion();
	public void setReposicion(int reposicion);
	public boolean isEstado();
	public void setEstado(boolean estado);

}
