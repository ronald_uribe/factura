package com.ruribe.util.bean;

import java.io.Serializable;
import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleSalidaBean implements IDetalleSalidaBean, Serializable{
	
		
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private IProductoBean iproductoBean;
	private int cantidad;
	private int pendiente;

	public DetalleSalidaBean() {
		super();
	}


	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the pendiente
	 */
	public int getPendiente() {
		return pendiente;
	}


	/**
	 * @param pendiente the pendiente to set
	 */
	public void setPendiente(int pendiente) {
		this.pendiente = pendiente;
	}


	/**
	 * @return the iproductoBean
	 */
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}

	
}
