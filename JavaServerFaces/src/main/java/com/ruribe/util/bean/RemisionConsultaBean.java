package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.IRemisionBean;
import com.ruribe.util.bean.interfaces.IRemisionConsultaBean;

public class RemisionConsultaBean implements IRemisionConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idRemision;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idCliente;
	private List<IRemisionBean> listaRemisiones;
	
	/**
	 * @return the idRemision
	 */
	public String getIdRemision() {
		return idRemision;
	}
	/**
	 * @param idRemision the idRemision to set
	 */
	public void setIdRemision(String idRemision) {
		this.idRemision = idRemision;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return the listaRemisiones
	 */
	public List<IRemisionBean> getListaRemisiones() {
		return listaRemisiones;
	}
	/**
	 * @param listaRemisiones the listaRemisiones to set
	 */
	public void setListaRemisiones(List<IRemisionBean> listaRemisiones) {
		this.listaRemisiones = listaRemisiones;
	}
}
