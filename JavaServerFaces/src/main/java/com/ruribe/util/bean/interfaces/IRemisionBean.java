/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IRemisionBean {
	
	public int getIdRemision();
	public void setIdRemision(int idRemision);

	public boolean isEstado();
	public void setEstado(boolean estado);
	
	public  IClienteBean getIclienteBean();
	public  void setIclienteBean(IClienteBean iclienteBean);

	public  IObraBean getIobraBean();
	public  void setIobraBean(IObraBean iobraBean);
	
	public IProductoBean getProducto() ;
	public void setProducto(IProductoBean producto);

	public IProductoBean getSelectProducto();
	public void setSelectProducto(IProductoBean selectProducto);
	
	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto();

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto);
	
	
	/**
	 * @devuelve un IProductoBean
	 */
	public IProductoBean getProducto(int id) ;
	
	/**
	 * @return the cantidad
	 */
	public int getCantidad();

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad);
	
	/**
	 * @return the detalleRemisions
	 */
	public List<IDetalleRemisionBean> getDetalleRemisions();

	/**
	 * @param detalleRemisions the detalleRemisions to set
	 */
	public void setDetalleRemisions(List<IDetalleRemisionBean> detalleRemisions);

	public void setDetalleRemisions(IDetalleRemisionBean detalle);
	
	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto();

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto);

	/**
	 * @return the transporte
	 */
	public int getTransporte();

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(int transporte);
	
	/**
	 * @return the fecha
	 */
	public Date getFecha();

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha);
	
	/**
	 * @return the fecha_entrega
	 */
	public Date getFecha_entrega();

	/**
	 * @param fecha_entrega the fecha_entrega to set
	 */
	public void setFecha_entrega(Date fecha_entrega);
	
	
	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);
	
	/**
	 * @return the itransportadorBean
	 */
	public ITransportadorBean getItransportadorBean();

	/**
	 * @param itransportadorBean the itransportadorBean to set
	 */
	public void setItransportadorBean(ITransportadorBean itransportadorBean);
	
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion();

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion);

	public boolean isPrinted();
	public void setPrinted(boolean printed);
}
