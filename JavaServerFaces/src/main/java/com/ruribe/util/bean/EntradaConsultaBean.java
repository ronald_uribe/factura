
package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.IEntradaBean;
import com.ruribe.util.bean.interfaces.IEntradaConsultaBean;

public class EntradaConsultaBean implements IEntradaConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idEntrada;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idProveedor;
	private List<IEntradaBean> listaEntradas;
	
	/**
	 * @return the idEntrada
	 */
	public String getIdEntrada() {
		return idEntrada;
	}
	/**
	 * @param idEntrada the idEntrada to set
	 */
	public void setIdEntrada(String idEntrada) {
		this.idEntrada = idEntrada;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idProveedor
	 */
	public String getIdProveedor() {
		return idProveedor;
	}
	/**
	 * @param idProveedor the idProveedor to set
	 */
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	/**
	 * @return the listaEntradas
	 */
	public List<IEntradaBean> getListaEntradas() {
		return listaEntradas;
	}
	/**
	 * @param listaEntradas the listaEntradas to set
	 */
	public void setListaEntradas(List<IEntradaBean> listaEntradas) {
		this.listaEntradas = listaEntradas;
	}
}
