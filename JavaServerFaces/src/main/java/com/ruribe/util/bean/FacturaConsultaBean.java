package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaConsultaBean;

public class FacturaConsultaBean implements IFacturaConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idFactura;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idCliente;
	private List<IFacturaBean> listaFacturaes;
	
	/**
	 * @return the idFactura
	 */
	public String getIdFactura() {
		return idFactura;
	}
	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return the listaFacturaes
	 */
	public List<IFacturaBean> getListaFactura() {
		return listaFacturaes;
	}
	/**
	 * @param listaFacturaes the listaFacturaes to set
	 */
	public void setListaFactura(List<IFacturaBean> listaFactura) {
		this.listaFacturaes = listaFactura;
	}
}
