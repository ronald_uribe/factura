/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 *
 */
public interface ICiudadBean {
	
	
	public boolean isEditable();
	public  void setEditable(boolean editable);
	public int getCodigo_ciudad();
	public void setCodigo_ciudad(int codigo_ciudad);
	public String getNombre_ciudad();
	public void setNombre_ciudad(String nombre_ciudad);

}
