/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.List;

import com.ruribe.util.entidades.jpa.Role;

/**
 * @author RONI
 *
 */
public interface IUsuarioBean {
	
	public String getTelefono();
	public void setTelefono(String telefono);	
	
	public Long getCodigo();
	public void setCodigo(Long codigo);
	
	public String getCodigoBuscado();
	public void setCodigoBuscado(String codigoBuscado);
	
	public String getApellido();
	public void setApellido(String apellido);
	
	public String getNombre();
	public void setNombre(String nombre);

	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);
	
	public String getClave();
	public void setClave(String clave);

	public String getUsername();
	public void setUsername(String username);
	
	public boolean isEditable();
	public  void setEditable(boolean editable);
	

	public List<IRoleBean> getRoles();
	public void setRoles(List<IRoleBean> roles);
	
	public String[] getRolesSelect();
	public void setRolesSelect(String[] rolesSelect);

}
