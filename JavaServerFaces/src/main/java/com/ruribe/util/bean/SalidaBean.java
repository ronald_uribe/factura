package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.ISalidaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;

public class SalidaBean implements ISalidaBean, Serializable{
	
	/**
	 *serialVersionUID. 
	 */
		
	private static final long serialVersionUID = 1L;

	private int idSalida;
	private boolean estado;
	private IProveedorBean iproveedorBean;
	private String codProveedorSelect;
	private Date fecha;
	private int codigo_producto;
	private int cantidad;
	private String observaciones;
	private List<IDetalleSalidaBean> detalleSalidas;
	private IProductoBean producto;
	private String selectProducto;
	private String codTipoProductoSelect;
	private List<IProductoBean> listaProducto;
	private String usuario_creacion;
	
	public SalidaBean() {
		super();
		producto= new ProductoBean(); 
		iproveedorBean = new ProveedorBean();
		detalleSalidas = new ArrayList<IDetalleSalidaBean>();
	}
	
	/**
	 * @return the iproveedorBean
	 */
	public final IProveedorBean getIproveedorBean() {
		return iproveedorBean;
	}

	/**
	 * @param iproveedorBean the iproveedorBean to set
	 */
	public final void setIproveedorBean(IProveedorBean iproveedorBean) {
		this.iproveedorBean = iproveedorBean;
	}

	/**
	 * @return the producto
	 */
	public final IProductoBean getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public final void setProducto(IProductoBean producto) {
		this.producto = producto;
	}

	/**
	 * @return the selectProducto
	 */
	public final String getSelectProducto() {
		return selectProducto;
	}

	/**
	 * @param selectProducto the selectProducto to set
	 */
	public final void setSelectProducto(String selectProducto) {
		this.selectProducto = selectProducto;
	}

	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto() {
		return listaProducto;
	}

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto) {
		this.listaProducto = listaProducto;
	}
	
	/**
	 * retorna un objeto de una lista de tipo IProductoBean
	 * @param String codigo_producto
	 * @return IProductoBean
	 */
	public IProductoBean getProducto(int id) {
        
        for (IProductoBean producto : listaProducto){
            if (id==producto.getCodigo_producto()){
                return producto;
            }
        }
        return null;
    }

	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the detalleSalidas
	 */
	public final List<IDetalleSalidaBean> getDetalleSalidas() {
		return detalleSalidas;
	}

	/**
	 * @param detalleSalidas the detalleSalidas to set
	 */
	public final void setDetalleSalidas(
			List<IDetalleSalidaBean> detalleSalidas) {
		this.detalleSalidas = detalleSalidas;
	}

	@Override
	public void setDetalleSalidas(IDetalleSalidaBean detalle) {
		this.detalleSalidas.add(detalle);
	}

	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_producto() {
		return codigo_producto;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}

	/**
	 * @return the fecha
	 */
	public final Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public final void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codProveedorSelect
	 */
	public String getCodProveedorSelect() {
		return codProveedorSelect;
	}

	/**
	 * @param codProveedorSelect the codProveedorSelect to set
	 */
	public void setCodProveedorSelect(String codProveedorSelect) {
		this.codProveedorSelect = codProveedorSelect;
	}

	/**
	 * @return the idSalida
	 */
	public final int getIdSalida() {
		return idSalida;
	}

	/**
	 * @param idSalida the idSalida to set
	 */
	public final void setIdSalida(int idSalida) {
		this.idSalida = idSalida;
	}

	/**
	 * @return the estado
	 */
	public final boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public final void setEstado(boolean estado) {
		this.estado = estado;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the codTipoProductoSelect
	 */
	public String getCodTipoProductoSelect() {
		return this.codTipoProductoSelect;
	}

	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public void setCodTipoProductoSelect(String codTipoProductoSelect) {
		this.codTipoProductoSelect = codTipoProductoSelect;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	
}
