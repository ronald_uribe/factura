package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IUser;

public class User implements IUser, Serializable{
	
	
	/**serialVersionUID.*/
	private static final long serialVersionUID = 1188475657408871676L;
	private String idUsuario;
	private String clave;
	private boolean loggedIn;
	
	public User() {
	} 
	
	public User(String idUsuario, String clave) {
		this.idUsuario = idUsuario;
		this.clave = clave;
	}

	public final String getIdUsuario() {
		return idUsuario;
	}

	public final void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public final String getClave() {
		return clave;
	}

	public final void setClave(String clave) {
		this.clave = clave;
	}
	
	public boolean isLoggedIn() {
        return loggedIn;
    }
 
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

}
