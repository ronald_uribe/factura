package com.ruribe.util.bean;

import java.io.Serializable;
import com.ruribe.util.bean.interfaces.IDetalleEntradaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleEntradaBean implements IDetalleEntradaBean, Serializable{
	
		
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private IProductoBean iproductoBean;
	private int cantidad;

	public DetalleEntradaBean() {
		super();
	}


	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the iproductoBean
	 */
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}

	
}
