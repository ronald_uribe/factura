/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 *
 */
public interface ITipoDocumentoBean {
	
	
	/**
	 * @return the editable
	 */
	public boolean isEditable();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setEditable(boolean editable);
	
	/**
	 * @return the idTipoDocumento
	 */
	public int getIdTipoDocumento();

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param idTipoDocumento the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(int idTipoDocumento);


	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}
