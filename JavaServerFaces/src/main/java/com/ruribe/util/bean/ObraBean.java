package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;

public class ObraBean implements IObraBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private IClienteBean iclientebean;
	private int idObra;
	private String nombre;
	private String telefono;
	private String direccion;
	private String contacto;
	private Boolean enabled;
	
	
	public ObraBean() {
		super();
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}


	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}


	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	/**
	 * @return the iclientebean
	 */
	public IClienteBean getIclientebean() {
		return iclientebean;
	}


	/**
	 * @param iclientebean the iclientebean to set
	 */
	public void setIclientebean(IClienteBean iclientebean) {
		this.iclientebean = iclientebean;
	}


	/**
	 * @return the idObra
	 */
	public int getIdObra() {
		return idObra;
	}


	/**
	 * @param idObra the idObra to set
	 */
	public void setIdObra(int idObra) {
		this.idObra = idObra;
	}


	/**
	 * @return the contacto
	 */
	public String getContacto() {
		return contacto;
	}

	/**
	 * @param contacto the contacto to set
	 */
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

}
