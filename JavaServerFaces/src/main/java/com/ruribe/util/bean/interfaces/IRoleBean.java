package com.ruribe.util.bean.interfaces;

public interface IRoleBean {

	public Long getId();
	public void setId(Long id);

	public String getAuthority();
	public void setAuthority(String authority);

}
