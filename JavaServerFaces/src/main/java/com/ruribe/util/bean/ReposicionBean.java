package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionBean;
import com.ruribe.util.bean.interfaces.IObraBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.ITransportadorBean;

public class ReposicionBean implements IReposicionBean, Serializable{
	
	/**
	 *serialVersionUID. 
	 */
		
	private static final long serialVersionUID = 1L;

	private int idReposicion;
	private boolean estado;
	private IClienteBean iclienteBean;
	private ITransportadorBean itransportadorBean;
	private IObraBean iobraBean;
	private Date fecha;
	private int transporte;
	private int codigo_producto;
	private int cantidad;
	private int descuento;
	private int total;
	private String observaciones;
	private List<IDetalleReposicionBean> detalleReposiciones;
	private IProductoBean producto;
	private IProductoBean selectProducto;
	private List<IProductoBean> listaProducto;
	private String usuario_creacion;
	
	public ReposicionBean() {
		super();
		iclienteBean = new ClienteBean();
		iobraBean = new ObraBean();
		itransportadorBean = new TransportadorBean();
		detalleReposiciones = new ArrayList<IDetalleReposicionBean>();
	}
	
	/**
	 * @return the iclienteBean
	 */
	public final IClienteBean getIclienteBean() {
		return iclienteBean;
	}

	/**
	 * @param iclienteBean the iclienteBean to set
	 */
	public final void setIclienteBean(IClienteBean iclienteBean) {
		this.iclienteBean = iclienteBean;
	}

	/**
	 * @return the iobraBean
	 */
	public final IObraBean getIobraBean() {
		return iobraBean;
	}

	/**
	 * @param iobraBean the iobraBean to set
	 */
	public final void setIobraBean(IObraBean iobraBean) {
		this.iobraBean = iobraBean;
	}

	/**
	 * @return the producto
	 */
	public final IProductoBean getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public final void setProducto(IProductoBean producto) {
		this.producto = producto;
	}

	/**
	 * @return the selectProducto
	 */
	public final IProductoBean getSelectProducto() {
		return selectProducto;
	}

	/**
	 * @param selectProducto the selectProducto to set
	 */
	public final void setSelectProducto(IProductoBean selectProducto) {
		this.selectProducto = selectProducto;
	}

	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto() {
		return listaProducto;
	}

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto) {
		this.listaProducto = listaProducto;
	}
	
	/**
	 * retorna un objeto de una lista de tipo IProductoBean
	 * @param String codigo_producto
	 * @return IProductoBean
	 */
	public IProductoBean getProducto(int id) {
        
        for (IProductoBean producto : listaProducto){
            if (id==producto.getCodigo_producto()){
                return producto;
            }
        }
        return null;
    }

	/**
	 * @return the cantidad
	 */
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the descuento
	 */
	public int getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the detalleReposiciones
	 */
	public final List<IDetalleReposicionBean> getDetalleReposiciones() {
		return detalleReposiciones;
	}

	/**
	 * @param detalleReposiciones the detalleReposiciones to set
	 */
	public final void setDetalleReposiciones(
			List<IDetalleReposicionBean> detalleReposiciones) {
		this.detalleReposiciones = detalleReposiciones;
	}

	public void setDetalleReposiciones(IDetalleReposicionBean detalle) {
		this.detalleReposiciones.add(detalle);
	}

	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_producto() {
		return codigo_producto;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}

	/**
	 * @return the transporte
	 */
	public final int getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public final void setTransporte(int transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the fecha
	 */
	public final Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public final void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idReposicion
	 */
	public final int getIdReposicion() {
		return idReposicion;
	}

	/**
	 * @param idReposicion the idReposicion to set
	 */
	public final void setIdReposicion(int idReposicion) {
		this.idReposicion = idReposicion;
	}

	/**
	 * @return the estado
	 */
	public final boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public final void setEstado(boolean estado) {
		this.estado = estado;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the itransportadorBean
	 */
	public ITransportadorBean getItransportadorBean() {
		return itransportadorBean;
	}

	/**
	 * @param itransportadorBean the itransportadorBean to set
	 */
	public void setItransportadorBean(ITransportadorBean itransportadorBean) {
		this.itransportadorBean = itransportadorBean;
	}

	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}


}
