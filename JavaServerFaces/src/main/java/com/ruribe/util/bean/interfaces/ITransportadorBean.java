/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 *
 */
public interface ITransportadorBean {
	

	/**
	 * @return the telefono
	 */
	public  String getTelefono();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setTelefono(String telefono);
	/**
	 * @return the editable
	 */
	public boolean isEditable();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setEditable(boolean editable);
	/**
	 * @return the no_documento
	 */
	public String getNo_documento();
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento);
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento();
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento);
	/**
	 * @return the razon_social
	 */
	public String getNombre();
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre);
	/**
	 * @return the direccion
	 */
	public String getDireccion();
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion);
	
	/**
	 * @return the celular
	 */
	public String getCelular() ;

	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular);

	/**
	 * @return the email
	 */
	public String getEmail();

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email);

	/**
	 * @return the tipoDocumentoSelect
	 */
	public String getTipoDocumentoSelect();
	/**
	 * @param tipoDocumentoSelect the tipoDocumentoSelect to set
	 */
	public void setTipoDocumentoSelect(String tipoDocumentoSelect);
	
	/**
	 * @return descripcion de vehiculo
	 */
	public String getDescripcion();
	
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);
	/**
	 * @return placa
	 */
	public String getPlaca();
	
	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa);
	public boolean isEnabled();
	public void setEnabled(boolean activo);

}
