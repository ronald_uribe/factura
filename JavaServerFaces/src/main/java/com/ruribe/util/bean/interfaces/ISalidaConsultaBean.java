/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface ISalidaConsultaBean {
	
	/**
	 * @return the idSalida
	 */
	public String getIdSalida();
	/**
	 * @param idSalida the idSalida to set
	 */
	public void setIdSalida(String idSalida);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	
	/**
	 * @return the idProveedor
	 */
	public String getIdProveedor();
	
	/**
	 * @param idProveedor the idProveedor to set
	 */
	public void setIdProveedor(String idProveedor);
	
	/**
	 * @return the listaSalidas
	 */
	public List<ISalidaBean> getListaSalidas();
	/**
	 * @param listaSalidas the listaSalidas to set
	 */
	public void setListaSalidas(List<ISalidaBean> listaSalidas);
}
