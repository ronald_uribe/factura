/**
 * 
 */
package com.ruribe.util.bean.interfaces;


/**
 * @author RONI
 *
 */
public interface IObraBean {

	public IClienteBean getIclientebean();
	public void setIclientebean(IClienteBean iclientebean);
	public String getNombre();
	public void setNombre(String string);
	public String getTelefono();
	public void setTelefono(String telefono);
	public String getDireccion();
	public void setDireccion(String direccion);
	public int getIdObra();
	public void setIdObra(int idObra);
	public String getContacto();
	public void setContacto(String contacto);
	public Boolean getEnabled();
	public void setEnabled(Boolean enabled);

}
