package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUsuarioBean;

public class UsuarioBean implements IUsuarioBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private Long codigo;
	private String codigoBuscado;
	private String nombre;
	private String username;
	private String clave;
	private String apellido;
	private String telefono;
	private Boolean enabled;
	private boolean editable;
	private List<IRoleBean> roles;
	private String[] rolesSelect;
	
	
	public UsuarioBean() {
		super();
		roles = new ArrayList<IRoleBean>();
	}
	
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}


	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	/**
	 * @return the codigoBuscado
	 */
	public String getCodigoBuscado() {
		return codigoBuscado;
	}


	/**
	 * @param codigoBuscado the codigoBuscado to set
	 */
	public void setCodigoBuscado(String codigoBuscado) {
		this.codigoBuscado = codigoBuscado;
	}


	/**
	 * @return the apellido1
	 */
	public String getApellido() {
		return apellido;
	}
	/**
	 * @param apellido1 the apellido1 to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the telefono
	 */
	public final String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public final void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getEnabled() {
		return enabled;
	}


	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}


	public List<IRoleBean> getRoles() {
		return roles;
	}


	public void setRoles(List<IRoleBean> roles) {
		this.roles = roles;
	}
	public String[] getRolesSelect() {
		return rolesSelect;
	}

	public void setRolesSelect(String[] rolesSelect) {
		this.rolesSelect = rolesSelect;
	}
	
	
	
}
