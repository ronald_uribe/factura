/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface ISalidaBean {
	
	/**
	 * @return the idSalida
	 */
	public int getIdSalida();

	/**
	 * @param idSalida the idSalida to set
	 */
	public void setIdSalida(int idSalida);

	/**
	 * @return the estado
	 */
	public boolean isEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(boolean estado);
	
	/**
	 * @return the iproveedorBean
	 */
	public  IProveedorBean getIproveedorBean();

	/**
	 * @param iproveedorBean the iproveedorBean to set
	 */
	public  void setIproveedorBean(IProveedorBean iproveedorBean);

	/**
	 * @return the producto
	 */
	public IProductoBean getProducto() ;

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(IProductoBean producto);

	/**
	 * @return the selectProducto
	 */
	public String getSelectProducto();

	/**
	 * @param selectProducto the selectProducto to set
	 */
	public void setSelectProducto(String selectProducto);
	
	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto();

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto);
	
	
	/**
	 * @devuelve un IProductoBean
	 */
	public IProductoBean getProducto(int id) ;
	
	/**
	 * @return the cantidad
	 */
	public int getCantidad();

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad);
	
	/**
	 * @return the detalleSalidas
	 */
	public List<IDetalleSalidaBean> getDetalleSalidas();

	/**
	 * @param detalleSalidas the detalleSalidas to set
	 */
	public void setDetalleSalidas(List<IDetalleSalidaBean> detalleSalidas);

	public void setDetalleSalidas(IDetalleSalidaBean detalle);
	
	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto();

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto);

	/**
	 * @return the fecha
	 */
	public Date getFecha();

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha);
	
	/**
	 * @return the codProveedorSelect
	 */
	public String getCodProveedorSelect();
	/**
	 * @param codProveedorSelect the codProveedorSelect to set
	 */
	public void setCodProveedorSelect(String codProveedorSelect);
	
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion();

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion);

	public String getCodTipoProductoSelect();
	public void setCodTipoProductoSelect(String string);

}
