package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;

public class TipoDocumentoBean implements ITipoDocumentoBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private int idTipoDocumento;
	private String descripcion;
	private boolean editable;
	
	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the idTipoDocumento
	 */
	public final int getIdTipoDocumento() {
		return idTipoDocumento;
	}

	/**
	 * @param idTipoDocumento the idTipoDocumento to set
	 */
	public final void setIdTipoDocumento(int idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
