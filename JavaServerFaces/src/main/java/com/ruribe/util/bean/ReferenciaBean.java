package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;

public class ReferenciaBean implements IReferenciaBean, Serializable{

	/** 
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private int codigo_interno; 
	private String descripcion;
	private int precio_alquiler;
	private int precio_reposicion;
	private boolean editable;
	private double peso;
	private ITipoProductoBean tipo_producto;
	private String codTipoProductoSelect;

	
	public ReferenciaBean() {
		super();
	}

	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the codigo_producto
	 */
	public final int getCodigo_interno() {
		return codigo_interno;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public final void setCodigo_interno(int codigo_interno) {
		this.codigo_interno = codigo_interno;
	}

	

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the precio_alquiler
	 */
	public int getPrecio_alquiler() {
		return precio_alquiler;
	}

	/**
	 * @param precio_alquiler the precio_alquiler to set
	 */
	public void setPrecio_alquiler(int precio_alquiler) {
		this.precio_alquiler = precio_alquiler;
	}

	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion() {
		return precio_reposicion;
	}

	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion) {
		this.precio_reposicion = precio_reposicion;
	}

	/**
	 * @return the peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	/**
	 * @return the tipo_producto
	 */
	public final ITipoProductoBean getTipo_producto() {
		return tipo_producto;
	}

	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public final void setTipo_producto(ITipoProductoBean tipo_producto) {
		this.tipo_producto = tipo_producto;
	}

	/**
	 * @return the codTipoProductoSelect
	 */
	public String getCodTipoProductoSelect() {
		return codTipoProductoSelect;
	}

	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public void setCodTipoProductoSelect(String codTipoProductoSelect) {
		this.codTipoProductoSelect = codTipoProductoSelect;
	}

}
