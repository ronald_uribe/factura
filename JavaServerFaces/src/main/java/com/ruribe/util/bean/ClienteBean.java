package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;

public class ClienteBean implements IClienteBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private String id_cliente;
	private int cod_tipo_documento;
	private String telefono; 
	private String celular;
	private String direccion;
	private String obra;
	private String razon_social;
	private String email;
	private int cod_ciudad;	
	private String pais;
	private boolean editable;
	private Boolean enabled;
	private String ciudadSelect;
	private String tipoDocumentoSelect;
	private String clienteSelect;
	private List<IObraBean> listaObras;
	
	
	public ClienteBean() {
		super();
		 listaObras = new ArrayList<IObraBean>();
	}
	
	/**
	 * @param id_cliente
	 * @param cod_tipo_documento
	 * @param telefono
	 * @param celular
	 * @param direccion
	 * @param obra
	 * @param razonSocial
	 * @param email
	 * @param cod_ciudad
	 * @param pais
	 * @param editable
	 */
	public ClienteBean(String id_cliente, int cod_tipo_documento,
			String telefono, String celular, String direccion, String obra,
			String razon_social, String email, int cod_ciudad, String pais,
			boolean editable) {
		super();
		this.id_cliente = id_cliente;
		this.cod_tipo_documento = cod_tipo_documento;
		this.telefono = telefono;
		this.celular = celular;
		this.direccion = direccion;
		this.obra = obra;
		this.razon_social = razon_social;
		this.email = email;
		this.cod_ciudad = cod_ciudad;
		this.pais = pais;
		this.editable = editable;
	}

	/**
	 * @return the telefono
	 */
	public final String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public final void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * @param celular the celular to set
	 */
	public final void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public final void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the obra
	 */
	public String getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public final void setObra(String obra) {
		this.obra = obra;
	}

	/**
	 * @return the razon_social
	 */
	public String getRazon_social() {
		return razon_social;
	}

	/**
	 * @param razon_social the razon_social to set
	 */
	public final void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public final void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the ciudadSelect
	 */
	public String getCiudadSelect() {
		return ciudadSelect;
	}

	/**
	 * @param ciudadSelect the ciudadSelect to set
	 */
	public void setCiudadSelect(String ciudadSelect) {
		this.ciudadSelect = ciudadSelect;
	}

	/**
	 * @return the tipoDocumentoSelect
	 */
	public String getTipoDocumentoSelect() {
		return tipoDocumentoSelect;
	}

	/**
	 * @param tipoDocumentoSelect the tipoDocumentoSelect to set
	 */
	public void setTipoDocumentoSelect(String tipoDocumentoSelect) {
		this.tipoDocumentoSelect = tipoDocumentoSelect;
	}

	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento() {
		return cod_tipo_documento;
	}

	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento) {
		this.cod_tipo_documento = cod_tipo_documento;
	}

	/**
	 * @return the cod_ciudad
	 */
	public int getCod_ciudad() {
		return cod_ciudad;
	}

	/**
	 * @param cod_ciudad the cod_ciudad to set
	 */
	public void setCod_ciudad(int cod_ciudad) {
		this.cod_ciudad = cod_ciudad;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the id_cliente
	 */
	public String getId_cliente() {
		return id_cliente;
	}

	/**
	 * @param id_cliente the id_cliente to set
	 */
	public void setId_cliente(String id_cliente) {
		this.id_cliente = id_cliente;
	}

	/**
	 * @return the clienteSelect
	 */
	public String getClienteSelect() {
		return clienteSelect;
	}

	/**
	 * @param clienteSelect the clienteSelect to set
	 */
	public void setClienteSelect(String clienteSelect) {
		this.clienteSelect = clienteSelect;
	}


	/**
	 * @return the listaObras
	 */
	public List<IObraBean> getListaObras() {
		return listaObras;
	}

	/**
	 * @param listaObras the listaObras to set
	 */
	public void setListaObras(IObraBean listaObra) {
		this.listaObras.add(listaObra);
	}

	@Override
	public void setListaObras(List<IObraBean> list) {
		this.listaObras=list;
		
	}

	
	
}
