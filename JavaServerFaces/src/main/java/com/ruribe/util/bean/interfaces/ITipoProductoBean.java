/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 *
 */
public interface ITipoProductoBean {
	
	public boolean isEditable();
	public  void setEditable(boolean editable);	
	public int getCod_tipo_producto();
	public void setCod_tipo_producto(int tipoproducto);	
	public String getDescripcion();
	public void setDescripcion(String descripcion);


}
