/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 * Interfaz de producto.
 */
public interface IReferenciaBean {
	
	/**
	 * @return the editable
	 */
	public boolean isEditable();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setEditable(boolean editable);
	
	public int getCodigo_interno();

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_interno(int codigo_interno);

	/**
	 * @return the descripcion
	 */
	public  String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public  void setDescripcion(String descripcion);
	
	/**
	 * @return the precio_alquiler
	 */
	public int getPrecio_alquiler();

	/**
	 * @param precio_alquiler the precio_alquiler to set
	 */
	public void setPrecio_alquiler(int precio_alquiler);

	/**
	 * @return the precio_reposicion
	 */
	public int getPrecio_reposicion();

	/**
	 * @param precio_reposicion the precio_reposicion to set
	 */
	public void setPrecio_reposicion(int precio_reposicion);
	
	/**
	 * @return the peso
	 */
	public double getPeso();

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso);
	
	/**
	 * @return the tipo_producto
	 */
	public ITipoProductoBean getTipo_producto();

	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public void setTipo_producto(ITipoProductoBean tipo_producto);
	
	/**
	 * @return the codTipoProductoSelect
	 */
	public String getCodTipoProductoSelect();

	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public void setCodTipoProductoSelect(String codTipoProductoSelect);

	
}
