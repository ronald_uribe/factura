/**
 * 
 */
package com.ruribe.util.bean.interfaces;

/**
 * @author RONI
 *
 */
public interface IProveedorBean {
	

	/**
	 * @return the telefono
	 */
	public  String getTelefono();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setTelefono(String telefono);
	/**
	 * @return the editable
	 */
	public boolean isEditable();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setEditable(boolean editable);
	/**
	 * @return the no_documento
	 */
	public String getNo_documento();
	/**
	 * @param no_documento the no_documento to set
	 */
	public void setNo_documento(String no_documento);
	/**
	 * @return the cod_tipo_documento
	 */
	public int getCod_tipo_documento();
	/**
	 * @param cod_tipo_documento the cod_tipo_documento to set
	 */
	public void setCod_tipo_documento(int cod_tipo_documento);
	/**
	 * @return the razon_social
	 */
	public String getRazon_social();
	/**
	 * @param razon_social the razon_social to set
	 */
	public void setRazon_social(String razon_social);
	/**
	 * @return the direccion
	 */
	public String getDireccion();
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion);
	/**
	 * @return the cod_ciudad
	 */
	public int getCod_ciudad();
	/**
	 * @param cod_ciudad the cod_ciudad to set
	 */
	public void setCod_ciudad(int cod_ciudad);

	public String getCiudadSelect();
	
	/**
	 * @return the celular
	 */
	public String getCelular() ;

	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular);

	/**
	 * @return the email
	 */
	public String getEmail();

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email);

	/**
	 * @param ciudadSelect the ciudadSelect to set
	 */
	public void setCiudadSelect(String ciudadSelect);

	/**
	 * @return the tipoDocumentoSelect
	 */
	public String getTipoDocumentoSelect();
	/**
	 * @param tipoDocumentoSelect the tipoDocumentoSelect to set
	 */
	public void setTipoDocumentoSelect(String tipoDocumentoSelect);
	
	/**
	 * @return the codigo
	 */
	public String getCodigo_proveedor();

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo_proveedor(String codigo);
	
	public boolean isEnabled();

	public void setEnabled(boolean enabled);

}
