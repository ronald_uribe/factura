package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.ICiudadBean;

public class CiudadBean implements ICiudadBean, Serializable{
	
		
	/**
	 *serialVersionUID. 
	 */
	private static final long serialVersionUID = -7632577241265924249L;
	
	private int codigo_ciudad ;
	private String nombre_ciudad;
	private boolean editable;
	
	/**
	 * @return the editable
	 */
	public final boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public final void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the codigo_ciudad
	 */
	public final int getCodigo_ciudad() {
		return codigo_ciudad;
	}

	/**
	 * @param codigo_ciudad the codigo_ciudad to set
	 */
	public final void setCodigo_ciudad(int codigo_ciudad) {
		this.codigo_ciudad = codigo_ciudad;
	}

	/**
	 * @return the nombre_ciudad
	 */
	public final String getNombre_ciudad() {
		return nombre_ciudad;
	}

	/**
	 * @param nombre_ciudad the nombre_ciudad to set
	 */
	public final void setNombre_ciudad(String nombre_ciudad) {
		this.nombre_ciudad = nombre_ciudad;
	}
	
}
