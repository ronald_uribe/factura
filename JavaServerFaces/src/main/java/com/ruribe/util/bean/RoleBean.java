package com.ruribe.util.bean;

import java.io.Serializable;

import com.ruribe.util.bean.interfaces.IRoleBean;

public class RoleBean implements IRoleBean, Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long id;
	private String authority;
	
	public RoleBean() {
		
	}
	
	public RoleBean(Long id, String authority) {
		this.id = id;
		this.authority = authority;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	


}
