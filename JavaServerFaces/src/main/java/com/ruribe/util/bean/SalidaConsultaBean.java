package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.ISalidaBean;
import com.ruribe.util.bean.interfaces.ISalidaConsultaBean;

public class SalidaConsultaBean implements ISalidaConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idSalida;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idProveedor;
	private List<ISalidaBean> listaSalidas;
	
	/**
	 * @return the idSalida
	 */
	public String getIdSalida() {
		return idSalida;
	}
	/**
	 * @param idSalida the idSalida to set
	 */
	public void setIdSalida(String idSalida) {
		this.idSalida = idSalida;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	/**
	 * @return the idProveedor
	 */
	public String getIdProveedor() {
		return idProveedor;
	}
	/**
	 * @param idProveedor the idProveedor to set
	 */
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	/**
	 * @return the listaSalidas
	 */
	public List<ISalidaBean> getListaSalidas() {
		return listaSalidas;
	}
	/**
	 * @param listaSalidas the listaSalidas to set
	 */
	public void setListaSalidas(List<ISalidaBean> listaSalidas) {
		this.listaSalidas = listaSalidas;
	}
}
