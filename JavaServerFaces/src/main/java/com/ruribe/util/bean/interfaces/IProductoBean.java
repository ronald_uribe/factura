/**
 * 
 */
package com.ruribe.util.bean.interfaces;


/**
 * @author RONI
 * Interfaz de producto.
 */
public interface IProductoBean {
	
	/**
	 * @return the editable
	 */
	public boolean isEditable();
	/**
	 * @param telefono the telefono to set
	 */
	public  void setEditable(boolean editable);
	
	public int getCodigo_producto();

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto);
	

	/**
	 * @return the referencia
	 */
	public IReferenciaBean getReferenciaBean();

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferenciaBean(IReferenciaBean referencia);

	/**
	 * @return the descripcion
	 */
	public  String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public  void setDescripcion(String descripcion);


	/**
	 * @return the stock
	 */
	public  int getStock();

	/**
	 * @param stock the stock to set
	 */
	public  void setStock(int stock);

	/**
	 * @return the codProveedorSelect
	 */
	public String getCodProveedorSelect();

	/**
	 * @param codProveedorSelect the codProveedorSelect to set
	 */
	public void setCodProveedorSelect(String codProveedorSelect);

	/**
	 * @return the codTipoProductoSelect
	 */
	public String getCodTipoProductoSelect();
	
	/**
	 * @return the codReferenciaSelect
	 */
	public String getCodReferenciaSelect();

	/**
	 * @param codReferenciaSelect the codReferenciaSelect to set
	 */
	public void setCodReferenciaSelect(String codReferenciaSelect);

	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public void setCodTipoProductoSelect(String codTipoProductoSelect);
	/**
	 * @return the tipo_producto
	 */
	public ITipoProductoBean getTipo_producto();

	/**
	 * @param tipo_producto the tipo_producto to set
	 */
	public void setTipo_producto(ITipoProductoBean tipo_producto);

	/**
	 * @return the proveedor
	 */
	public IProveedorBean getProveedor();

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(IProveedorBean proveedor);
	
	public int getMaximo();
	public int getSubarriendo();
	public int getMinimo();
	public void setMaximo(int maximo);
	public void setSubarriendo(int subarriendo);
	public void setMinimo(int minimo);

}
