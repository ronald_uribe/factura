/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IRemisionConsultaBean {
	
	/**
	 * @return the idRemision
	 */
	public String getIdRemision();
	/**
	 * @param idRemision the idRemision to set
	 */
	public void setIdRemision(String idRemision);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	/**
	 * @return the idCliente
	 */
	public String getIdCliente();
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente);
	
	/**
	 * @return the listaRemisiones
	 */
	public List<IRemisionBean> getListaRemisiones();
	/**
	 * @param listaRemisiones the listaRemisiones to set
	 */
	public void setListaRemisiones(List<IRemisionBean> listaRemisiones);
}
