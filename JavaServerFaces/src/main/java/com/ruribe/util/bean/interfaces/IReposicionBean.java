/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 * interfaz de ReposicionBean
 *
 */
public interface IReposicionBean {
	
	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion();

	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion);

	/**
	 * @return the estado
	 */
	public boolean isEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(boolean estado);
	
	/**
	 * @return the iclienteBean
	 */
	public  IClienteBean getIclienteBean();

	/**
	 * @param iclienteBean the iclienteBean to set
	 */
	public  void setIclienteBean(IClienteBean iclienteBean);

	/**
	 * @return the iobraBean
	 */
	public  IObraBean getIobraBean();

	/**
	 * @param iobraBean the iobraBean to set
	 */
	public  void setIobraBean(IObraBean iobraBean);
	
	/**
	 * @return the producto
	 */
	public IProductoBean getProducto() ;

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(IProductoBean producto);

	/**
	 * @return the selectProducto
	 */
	public IProductoBean getSelectProducto();

	/**
	 * @param selectProducto the selectProducto to set
	 */
	public void setSelectProducto(IProductoBean selectProducto);
	
	/**
	 * @return the listaProducto
	 */
	public List<IProductoBean> getListaProducto();

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto);
	
	
	/**
	 * @devuelve un IProductoBean
	 */
	public IProductoBean getProducto(int id) ;
	
	/**
	 * @return the cantidad
	 */
	public int getCantidad();

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad);
	
	/**
	 * @return the descuento
	 */
	public int getDescuento();

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(int descuento);
	
	/**
	 * @return the detalleReposicions
	 */
	public List<IDetalleReposicionBean> getDetalleReposiciones();

	/**
	 * @param detalleReposicions the detalleReposicions to set
	 */
	public void setDetalleReposiciones(List<IDetalleReposicionBean> detalleReposicions);

	public void setDetalleReposiciones(IDetalleReposicionBean detalle);
	
	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto();

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto);

	/**
	 * @return the transporte
	 */
	public int getTransporte();

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(int transporte);
	
	/**
	 * @return the fecha
	 */
	public Date getFecha();

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha);
	
	
	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);
	
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion();

	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion);
	
	/**
	 * @return the total
	 */
	public int getTotal();

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total);
	
}
