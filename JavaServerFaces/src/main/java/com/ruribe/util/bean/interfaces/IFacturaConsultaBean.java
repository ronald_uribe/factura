/**
 * 
 */
package com.ruribe.util.bean.interfaces;

import java.util.Date;
import java.util.List;


/**
 * @author RONI
 *
 */
public interface IFacturaConsultaBean {
	
	/**
	 * @return the idFactura
	 */
	public String getIdFactura();
	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura(String idFactura);
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial();
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial);
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal);
	/**
	 * @return the idCliente
	 */
	public String getIdCliente();
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente);
	/**
	 * @return the listaFacturaes
	 */
	public List<IFacturaBean> getListaFactura();
	/**
	 * @param listaFacturaes the listaFacturaes to set
	 */
	public void setListaFactura(List<IFacturaBean> listaFactura);
	
}
