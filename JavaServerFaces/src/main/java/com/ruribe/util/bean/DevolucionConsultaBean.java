package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionConsultaBean;

public class DevolucionConsultaBean implements IDevolucionConsultaBean, Serializable{
	 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String idDevolucion;
	private Date fechaInicial;
	private Date fechaFinal;
	private String idCliente;
	private List<IDevolucionBean> listaDevoluciones;
	
	/**
	 * @return the idDevolucion
	 */
	public String getIdDevolucion() {
		return idDevolucion;
	}
	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(String idDevolucion) {
		this.idDevolucion = idDevolucion;
	}
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return the listaDevoluciones
	 */
	public List<IDevolucionBean> getListaDevoluciones() {
		return listaDevoluciones;
	}
	/**
	 * @param listaDevoluciones the listaDevoluciones to set
	 */
	public void setListaDevoluciones(List<IDevolucionBean> listaDevoluciones) {
		this.listaDevoluciones = listaDevoluciones;
	}
}
