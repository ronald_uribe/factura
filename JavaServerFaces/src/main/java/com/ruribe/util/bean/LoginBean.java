package com.ruribe.util.bean;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.UsuarioService;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUsuarioBean;
 
 
/**
 * Simple login bean.
 * 
 * @author ruribe
 */
@Named
@Scope("session")
public class LoginBean implements Serializable {
 
    private static final long serialVersionUID = 7765876811740798583L;
 
    @Inject
	UsuarioService usuarioService;
    
    private String username;
    private String password;
    private String newPassword;
    private String confirmarPassword;
    private boolean loggedIn;
    private boolean checkBox = false;
    private String virtualCheck;
    private IUsuarioBean usuarioLogado;
 
    @ManagedProperty(value="#{navigationBean}")
    private NavigationBean navigationBean;
    
    public LoginBean() {
        isChecked();
    }
    
    /**
     * Login operation.
     * @return
     */
    
    public String doLogin() {
        // Get every user from our sample database :)
            // Successful login
        try {    
        if (usuarioService.login(username,generateMD5Signature(password))) {
            loggedIn = true; // indicador si es usuario logado
           // creamos la sesion de usuario username para que sea recogida al registrar cada movimiento
            HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", username);
			usuarioLogado=usuarioService.obtenerUsuarioByUsername(username);
			session.setAttribute("usuarioLogado", usuarioLogado);
			FacesContext fc = FacesContext.getCurrentInstance();
	        if(checkBox == true) {
	            virtualCheck = "true"; 
	            Cookie cusername = new Cookie("cusername", username);
	            Cookie cPassword = new Cookie("cPassword", password);
	            Cookie cVirtualCheck = new Cookie("cVirtualCheck", virtualCheck);
	            cusername.setMaxAge(604800);
	            cPassword.setMaxAge(604800);
	            cVirtualCheck.setMaxAge(604800);
	            ((HttpServletResponse)(fc.getExternalContext().getResponse())).addCookie(cusername);
	            ((HttpServletResponse)(fc.getExternalContext().getResponse())).addCookie(cPassword);
	            ((HttpServletResponse)(fc.getExternalContext().getResponse())).addCookie(cVirtualCheck);
	        } else {
	            virtualCheck = "false";
	            Cookie cVirtualCheck = new Cookie("cVirtualCheck", virtualCheck);
	            ((HttpServletResponse)(fc.getExternalContext().getResponse())).addCookie(cVirtualCheck);
	        }
			return "INICIO";
        }
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("###error: "+e.getMessage());
            SessionUtils.addFatalError("Acceso Incorrecto, Intente mas tarde.");
            
        	return "LOGIN";
        
        }
        
        SessionUtils.addError("Comprueba que el usuario y la clave son correctos");
        // To to login page
        return "LOGIN";
         
    }
    
    public void actualizarContrasena() {
    	try {
    		 System.out.println("Nueva contrasena::::"+newPassword);
    		 if (usuarioService.login(username,generateMD5Signature(password))) {
	    		 if (newPassword!=null && StringUtils.isNotBlank(newPassword) && newPassword.equals(confirmarPassword)) {	    			
	    			 if (usuarioService.modificarPassword(username,generateMD5Signature(newPassword))) {
	    				 SessionUtils.addInfo("Su contraseña se ha modificado correctamente");
	    			 }
	    		 }else {
	    			 SessionUtils.addError("La nueva contraseña no coincide.");
	    		 }
    		 }else {
    			 SessionUtils.addError("Contraseña incorrecta.");
    		 }
    	}catch(Exception e) {
    		 SessionUtils.addError("Error al modificar la contraseña");
    	}
    }
     
    /**
     * Logout operation.
     * @return
     */
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;
         
        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
         
        return navigationBean.toLogin();
    }
    
    /** Método para generar la huella MD5 
     *  https://jdiezfoto.es/java-ee-seguridad-en-aplicaciones-web-i/
     *  @param String password
     *  @return clave encriptada
     * */
      public static String generateMD5Signature(String password) {
          try {
              //Cambiando MD5 por SHA-1 podríamos obtener la huella usando este otro algoritmo
              MessageDigest md = MessageDigest.getInstance("MD5");
              byte[] huella = md.digest(password.getBytes());
              return transformaAHexadecimal(huella);
          } catch (NoSuchAlgorithmException ex) {
              Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, "No se ha encontrado el algoritmo MD5", ex);
              return "-1";
          }
      }
      
    //Método para transformar el array de bytes en una cadena hexadecimal
      private static String transformaAHexadecimal(byte buf[]) {
          StringBuilder strbuf = new StringBuilder(buf.length * 2);
          for (int i = 0; i < buf.length; i++) {
              if (((int) buf[i] & 0xff) < 0x10) {
                  strbuf.append("0");
              }
              strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
          }
          return strbuf.toString();
      }
      
      private static String generarPassword(){
      	//Como argumento la longitud de la contraseña
      	String newPass = RandomStringUtils.randomAlphanumeric(5);
      	System.out.println("NEW PASS:"+newPass);
      	String newPassHash = generateMD5Signature(newPass);
      	//Asignar newPassHash al usuario
      	System.out.println(newPassHash);
      	return newPassHash;
      }
 
    // ------------------------------
    // Getters & Setters 
     
    public String getUsername() { return username;}
    public void setUsername(String username) { this.username = username;}
 
    public String getPassword() {return password; }
    public void setPassword(String password) { this.password = password; }
 
    public boolean isLoggedIn() {return loggedIn;  }
    public void setLoggedIn(boolean loggedIn) {this.loggedIn = loggedIn;}
 
    public String getNewPassword() {return newPassword;}
	public void setNewPassword(String newPassword) {this.newPassword = newPassword;}

	public String getConfirmarPassword() {return confirmarPassword;}
	public void setConfirmarPassword(String confirmarPassword) {this.confirmarPassword = confirmarPassword;}

	public void setNavigationBean(NavigationBean navigationBean) { this.navigationBean = navigationBean; }
    
    public static void main(String[] args) {

    	System.out.println("PASSWORD:"+generateMD5Signature("qwerty"));
    	System.out.println("PASSWORD ALEATORIO:"+generarPassword());
	}
    
    /**
     * otra forma de validar
     * https://mkyong.com/jsf2/multi-components-validator-in-jsf-2-0/?utm_source=mkyong.com&utm_medium=referral&utm_campaign=afterpost-related&utm_content=link11
     * @param event
     */
    public void validatePassword(ComponentSystemEvent event) {

  	  FacesContext fc = FacesContext.getCurrentInstance();
  	  UIComponent components = event.getComponent();
  	  // get password
  	  UIInput uiInputPassword = (UIInput) components.findComponent("newclave");
  	  String password = uiInputPassword.getLocalValue() == null ? "" : uiInputPassword.getLocalValue().toString();
  	  String passwordId = uiInputPassword.getClientId();

  	  // get confirm password
  	  UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirmarClave");
  	  String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
  		: uiInputConfirmPassword.getLocalValue().toString();

  	  // Let required="true" do its job.
  	  if (password.isEmpty() || confirmPassword.isEmpty()) {
  		return;
  	  }

  	  if (!password.equals(confirmPassword)) {

  		FacesMessage msg = new FacesMessage("La contraseña debe coincidir con la contraseña de confirmación");
  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
  		fc.addMessage(passwordId, msg);
  		fc.renderResponse();

  	  }

  	}
    
    public void isChecked() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Cookie[] cookiesArr = ((HttpServletRequest)(fc.getExternalContext().getRequest())).getCookies();
        if(cookiesArr != null && cookiesArr.length > 0)
            for(int i =0; i < cookiesArr.length; i++) {
                String cName = cookiesArr[i].getName();
                String cValue= cookiesArr[i].getValue();
                System.out.println("***cValue***"+cValue);
                if(cName.equals("cusername")) {
                	System.out.println("***seteando usuario***");
                    setUsername(cValue);
                } else if(cName.equals("cPassword")) {
                	System.out.println("***seteando password***");
                    setPassword(cValue);
                } else if(cName.equals("cVirtualCheck")) {
                	System.out.println("***seteando cVirtualCheck***");
                    setVirtualCheck(cValue);
                    if(getVirtualCheck().equals("false")) {
                        setCheckBox(false);
                        setUsername(null);
                        setPassword(null);
                    } else if(getVirtualCheck().equals("true")) {
                        System.out.println("Here in doLogin() line 99");
                        setCheckBox(true);
                    }
                }
            }

    }
    
   public boolean tienePermiso(final String permiso){
	   for (IRoleBean role:usuarioLogado.getRoles()) {
		   if (role!=null && role.getAuthority().equals(permiso)) {
			   return true;
		   }
		   
	   }
	   return false;
//        return usuarioLogado.getRoles().stream().filter(o -> o.getAuthority().equals(role)).findFirst().isPresent();
    }
   // #{loginBean.containsRole('administrador')}

	public boolean isCheckBox() {
		return checkBox;
	}

	public void setCheckBox(boolean checkBox) {
		this.checkBox = checkBox;
	}

	public String getVirtualCheck() {
		return virtualCheck;
	}

	public void setVirtualCheck(String virtualCheck) {
		this.virtualCheck = virtualCheck;
	}

	public IUsuarioBean getUsuarioLogado() {
		return usuarioLogado;
	}
    
     
}