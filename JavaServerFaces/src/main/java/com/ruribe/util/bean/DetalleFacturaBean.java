package com.ruribe.util.bean;

import java.io.Serializable;
import java.util.Date;

import com.ruribe.util.bean.interfaces.IDetalleFacturaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DetalleFacturaBean implements IDetalleFacturaBean, Serializable{
	
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *serialVersionUID. 
	 */

	private IProductoBean iproductoBean;
	private int codigo_producto;
	private String descripcion;
	private int cantidad;
	private int cantidad_gestionada;
	private int devolucion;
	private int idRemision;
	private int idFactura;
	private int idDevolucion;
	private int idReposicion;
	private Date fecha_inicial;
	private Date fecha_final;
	private int no_dias;
	private int vr_unitario;
	private Double vr_total;
	private String tipo;// identifica si es un DEVOLUCION, REMISION, REPOSICION, ACUMULADO
	

	public DetalleFacturaBean() {
		super();
	}


	
	/**
	 * @return the cantidad
	 */
	@Override
	public final int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	@Override
	public final void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	/**
	 * @return the cantidad_gestionada
	 */
	public int getCantidad_gestionada() {
		return cantidad_gestionada;
	}



	/**
	 * @param cantidad_gestionada the cantidad_gestionada to set
	 */
	public void setCantidad_gestionada(int cantidad_gestionada) {
		this.cantidad_gestionada = cantidad_gestionada;
	}



	/**
	 * @return the iproductoBean
	 */
	@Override
	public final IProductoBean getIproductoBean() {
		return iproductoBean;
	}


	/**
	 * @param iproductoBean the iproductoBean to set
	 */
	@Override
	public final void setIproductoBean(IProductoBean iproductoBean) {
		this.iproductoBean = iproductoBean;
	}

	/**
	 * @return the idRemision
	 */
	@Override
	public int getIdRemision() {
		return idRemision;
	}


	/**
	 * @param idRemision the idRemision to set
	 */
	@Override
	public void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}


	/**
	 * @return the fecha_inicial
	 */
	@Override
	public Date getFecha_inicial() {
		return fecha_inicial;
	}


	/**
	 * @param fecha_inicial the fecha_inicial to set
	 */
	@Override
	public void setFecha_inicial(Date fecha_inicial) {
		this.fecha_inicial = fecha_inicial;
	}


	/**
	 * @return the fecha_final
	 */
	@Override
	public Date getFecha_final() {
		return fecha_final;
	}


	/**
	 * @param fecha_final the fecha_final to set
	 */
	@Override
	public void setFecha_final(Date fecha_final) {
		this.fecha_final = fecha_final;
	}


	/**
	 * @return the no_dias
	 */
	@Override
	public int getNo_dias() {
		return no_dias;
	}


	/**
	 * @param no_dias the no_dias to set
	 */
	@Override
	public void setNo_dias(int no_dias) {
		this.no_dias = no_dias;
	}


	/**
	 * @return the vr_unitario
	 */
	@Override
	public int getVr_unitario() {
		return vr_unitario;
	}


	/**
	 * @param vr_unitario the vr_unitario to set
	 */
	@Override
	public void setVr_unitario(int vr_unitario) {
		this.vr_unitario = vr_unitario;
	}


	/**
	 * @return the vr_total
	 */
	@Override
	public Double getVr_total() {
		return vr_total;
	}


	/**
	 * @param vr_total the vr_total to set
	 */
	public void setVr_total(Double vr_total) {
		this.vr_total = vr_total;
	}


	@Override
	public int getIdFactura() {
		return idFactura;
	}


	/**
	 * @param idFactura the idFactura to set
	 */
	@Override
	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	/**
	 * @return the codigo_producto
	 */
	public int getCodigo_producto() {
		return codigo_producto;
	}

	/**
	 * @param codigo_producto the codigo_producto to set
	 */
	public void setCodigo_producto(int codigo_producto) {
		this.codigo_producto = codigo_producto;
	}
	
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * @return the devolucion
	 */
	public int getDevolucion() {
		return devolucion;
	}



	/**
	 * @param devolucion the devolucion to set
	 */
	public void setDevolucion(int devolucion) {
		this.devolucion = devolucion;
	}



	/**
	 * @return the idDevolucion
	 */
	public int getIdDevolucion() {
		return idDevolucion;
	}



	/**
	 * @param idDevolucion the idDevolucion to set
	 */
	public void setIdDevolucion(int idDevolucion) {
		this.idDevolucion = idDevolucion;
	}
	
	/**
	 * @return the idReposicion
	 */
	public int getIdReposicion() {
		return idReposicion;
	}

	/**
	 * @param idReposicion the idReposicion to set
	 */
	public void setIdReposicion(int idReposicion) {
		this.idReposicion = idReposicion;
	}



	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}



	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}

