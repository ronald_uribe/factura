package com.ruribe.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

public class Utilidades {
	
	

	public static java.util.Date convertFromSQLDateToJAVADate(java.sql.Date sqlDate) {
        java.util.Date javaDate = null;
        if (sqlDate != null) {
            javaDate = new Date(sqlDate.getTime());
        }
        return javaDate;
    }
	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
	    return new java.sql.Date(date.getTime());
	} 
	
	public static java.util.Date fechaObjectToDate (Object value) throws ParseException{
		Date dt = null;
		SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy");
		sdt.setTimeZone(TimeZone.getTimeZone("GMT+1"));
		
		if( value instanceof String ){
			sdt = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			dt = sdt.parse(((String) (value)));
		}else
			if( value instanceof java.sql.Timestamp ){ 
				dt=new Date(((java.sql.Timestamp)value).getTime());
				
		 }
		return dt;
	} 
	
	/**
	 * imprime el valor del objeto
	 * @param result objeto
	 */
	public static void printResult(Object result) {
	    if (result == null) {
	      System.out.print("NULL");
	    } else if (result instanceof Object[]) {
	      Object[] row = (Object[]) result;
	      System.out.print("[");
	      for (int i = 0; i < row.length; i++) {
	        printResult(row[i]);
	      }
	      System.out.print("]");
	    } else if (result instanceof Long || result instanceof Double
	        || result instanceof String || result instanceof Integer || 
	        result instanceof BigDecimal || result instanceof Date) {
	      System.out.print(result.getClass().getName() + ": " + result);
	    } else {
	      System.out.print(result);
	    }
	    System.out.println();
	  }
	
	/**
	 * comprueba si una fecha_final es > fecha_inicial
	 * @param Date fecha final
	 * @param Date fecha inicial
	 * @return boolean false si > true si es <
	 */
	public static boolean fechaesAMayorfechaB(Date fecha_final, Date fecha_inicial) {
		if ((int) ((fecha_final.getTime()-fecha_inicial.getTime())/86400000)>0){
			return false;
		}
		return true;
	}
	/**
	 * comprueba si una fecha_final es >= fecha_inicial
	 * @param Date fecha final
	 * @param Date fecha inicial
	 * @return boolean false si > true si es <
	 */
	public static boolean fechaesAMayorOIgualfechaB(Date fecha_final, Date fecha_inicial) {
		try {
			if ((int) ((fecha_final.getTime()-fecha_inicial.getTime())/86400000)>=0){
				System.out.println("ES FALSO"+(int) ((fecha_final.getTime()-fecha_inicial.getTime())/86400000));
				return false;
			}
		}catch(Exception e) {
			return true;
		}	
		System.out.println("ES TRUE"+(int) ((fecha_final.getTime()-fecha_inicial.getTime())/86400000));
		return true;
	}
	
	/**
	 * restamos X dias a una fecha determinada
	 * @param Date fecha 
	 * @param  int dias a restar
	 * @return Date fecha
	 */
	public static Date sumarRestarDiasFecha(Date fecha, int dias){
		      Calendar calendar = Calendar.getInstance();
		      calendar.setTime(fecha); // Configuramos la fecha que se recibe
		      calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
		      return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
		 }
	
	/**
	 * restar dos fechas.
	 * @param fechaIn
	 * @param fechaFinal
	 * @return int
	 */
	public static int restarFechas(Date fechaIn, Date fechaFinal) {
			System.out.println(fechaIn+"-"+fechaFinal);
			GregorianCalendar fechaInicio= new GregorianCalendar();
			fechaInicio.setTime(fechaIn);
			GregorianCalendar fechaFin= new GregorianCalendar();
			fechaFin.setTime(fechaFinal);
			int dias = 0;
			if(fechaFin.get(Calendar.YEAR)==fechaInicio.get(Calendar.YEAR)){
				dias =(fechaFin.get(Calendar.DAY_OF_YEAR)- fechaInicio.get(Calendar.DAY_OF_YEAR))+1;
			}else{
				int rangoAnyos = fechaFin.get(Calendar.YEAR) - fechaInicio.get(Calendar.YEAR);

				for(int i=0;i<=rangoAnyos;i++){
					int diasAnio = fechaInicio.isLeapYear(fechaInicio.get(Calendar.YEAR)) ? 366 : 365;
					if(i==0){
						dias=1+dias +(diasAnio- fechaInicio.get(Calendar.DAY_OF_YEAR));
					}else	if(i==rangoAnyos){
						dias=dias +fechaFin.get(Calendar.DAY_OF_YEAR);
					}else{
						dias=dias+diasAnio;
					}
				}
			}

			return dias;
	}
	
	/**
	 * resta 2 fechas recibidas por paramentro
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return int 
	 */
	public static int restarDosFechas(Date fechaInicial, Date fechaFinal){
		System.out.println("fechaInicial:"+fechaInicial+" - fechaFinal:"+fechaFinal);
		int dias=0;
		dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
		System.out.println("TOTAL DIAS:"+dias);
		return dias;
	}	
	
	/**
	 * separador de miles, con convierte un numero en formato 
	 * @param bd
	 * @return String 
	 */
	public static String separadorDeMiles(Integer valor){
		DecimalFormat df = new DecimalFormat("#,##0", new DecimalFormatSymbols(new Locale("es", "ES")));
		//BigDecimal value = new BigDecimal(123456.00);
		//System.out.println(df.format(value.floatValue()));
		//System.out.println(df.format(bd));
		return df.format(valor);
	}
	
	/**
	 * separador de miles, con convierte un numero en formato 
	 * @param bd
	 * @return String 
	 */
	public static String separadorDeMiles(Double valor){
		DecimalFormat df = new DecimalFormat("#,##0", new DecimalFormatSymbols(new Locale("es", "ES")));
		return df.format(valor);
	}
	
	@SuppressWarnings("deprecation")
	public static void main (String arg[]){
		
		
		 SimpleDateFormat sm = new SimpleDateFormat("mm-dd-yyyy");
		    Date myDate = new Date();
			// myDate is the java.util.Date in yyyy-mm-dd format
		    // Converting it into String using formatter
		    String strDate = sm.format(myDate );
		    //Converting the String back to java.util.Date
		    try {
				Date dt = sm.parse(strDate);
				System.out.println(dt);
				System.out.println(new SimpleDateFormat("MM/dd/yyyy").format(myDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.out.println("CONFIGURANDO HORA");
		    String fecha1="2020-01-25 00:00:00";
		    Date dt = null;
			SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy");
			sdt.setTimeZone(TimeZone.getTimeZone("GMT+1"));
			try {
				dt = sdt.parse(((String) (fecha1)));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			  System.out.println("FIN CONFIGURANDO HORA");
		    Timestamp ts=new Timestamp(System.currentTimeMillis());
		    Date date=new Date(ts.getTime());
		    System.out.println("FECHA:"+new SimpleDateFormat("dd/MM/yyyy").format(date));
		    Date fechaIn;
			try {
				fechaIn =  new Date("01/12/2019");
				Date fechaFinal = new Date("01/12/2019");
				System.out.println(restarFechas(fechaIn, fechaFinal));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaInicial;
			try {
				fechaInicial = dateFormat.parse("2019-12-01");
			
			Date fechaFinal=dateFormat.parse("2019-12-01");
	 
			int dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
	 
			System.out.println("Hay "+dias+" dias de diferencia");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(fechaesAMayorfechaB(new Date("31/12/2019"),	new Date("30/12/2019")));
			
			Integer bd = new Integer(30000000);
			NumberFormat formatter = NumberFormat.getInstance(new Locale("ES"));

			System.out.println(formatter.format(bd.longValue()));
			
			DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("es", "ES")));
			BigDecimal value = new BigDecimal(123456.00);

				    System.out.println(df.format(value.floatValue()));
				    System.out.println(df.format(bd));
	}
	
	/**
	 * lee un archivo desde una ruta y lo muestra en el navegador
	 * @param RUTA path donde se encuentra el archivo
	 * @param ARCHIVO nombre de archivo
	 * @throws IOException
	 */
	public static void descargarPDF(String RUTA,String ARCHIVO) throws IOException{
		File file = new File(RUTA+ARCHIVO);
        FacesContext facesContext = FacesContext.getCurrentInstance();  
        ExternalContext externalContext = facesContext.getExternalContext();  
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();  

        BufferedInputStream input = null;  
        BufferedOutputStream output = null;
        
        try {  
            // Open file.                                                                                          
            input = new BufferedInputStream(new FileInputStream(file));  

            // Init servlet response.  
            response.reset();             
            response.setHeader("Content-Type", "application/pdf"); 
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Content-Length", String.valueOf(file.length()));            
            response.setHeader("Content-Disposition", "inline; filename="+ARCHIVO);  
            output = new BufferedOutputStream(response.getOutputStream());  

            // Write file contents to response.  
            byte[] buffer = new byte[2000];  
            int length;  
            while ((length = input.read(buffer)) > 0) {  
                output.write(buffer, 0, length);  
            }              
             // Finalize task.  
            output.flush();                          
        }finally{  
        	if (output!=null){
        		output.close();
        	}if (input!=null){	
        		input.close(); 
        	}
        }         
        facesContext.responseComplete();
	}

}
