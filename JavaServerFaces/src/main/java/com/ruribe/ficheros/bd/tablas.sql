create table IF NOT EXISTS ciudad (
codigo_ciudad serial not null , 
nombre_ciudad varchar (30) not null,
primary key (codigo_ciudad)
);
select * from ciudad;
insert into ciudad values (DEFAULT,'Barranquilla');

create table IF NOT EXISTS tipo_documento (
id_tipo_documento serial not null , 
descripcion varchar (30) not null,
primary key (id_tipo_documento)
);

SELECT * FROM tipo_documento;
insert into tipo_documento values (DEFAULT,'CC');
insert into tipo_documento values (DEFAULT,'TI');
insert into tipo_documento values (DEFAULT,'RC');
insert into tipo_documento values (DEFAULT,'NIT');
insert into tipo_documento values (DEFAULT,'PASAPORTE');
insert into tipo_documento values (DEFAULT,'CE');
insert into tipo_documento values (DEFAULT,'RUT');


create table IF NOT EXISTS remision (
id_remision serial not null , 
id_cliente varchar (30) not null,
horaEntrega TIMESTAMP not null, 

primary key (id_remision)
);

drop table producto;
create table IF NOT EXISTS producto (
codigo_producto serial not null , 
codigo_tipo_producto int not null references tipoProducto(codigo_tipo_producto),
descripcion varchar (30) not null,
cod_proveedor varchar(30) not null,
precio_alquiler int,
stock int, 
fecha_ingreso date, 

primary key (codigo_producto)
);



create table IF NOT EXISTS tipoProducto (
codigo_tipo_producto serial not null , 
descripcion varchar (50) not null,

primary key (codigo_tipo_producto)
);

create table IF NOT EXISTS proveedor (
cod_proveedor varchar(30) not null,
cod_tipo_documento varchar(30) not null,
razon_social varchar (30) not null,
direccion varchar (30) not null,
cod_ciudad int,
telefono varchar(15) default null,
celular varchar(15) default null,
email varchar(30) default null,
primary key (cod_proveedor)
);

create table IF NOT EXISTS cliente (
id_cliente varchar(30) not null,
cod_tipo_documento int not null,
razon_social varchar (50) not null,
direccion varchar (30) not null,
cod_ciudad int,
telefono varchar(20) default null,
celular varchar(20) default null,
email varchar(50) default null,
primary key (id_cliente)
);

create table IF NOT EXISTS obra (
  id_obra INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  direccion VARCHAR(45) NOT NULL,
  telefono VARCHAR(45) NULL,
  id_cliente VARCHAR(45) NOT NULL references cliente(id_cliente),
  PRIMARY KEY (id_obra, id_cliente));
  
create table IF NOT EXISTS transportador (
no_documento varchar(30) not null,
cod_tipo_documento varchar(30) not null,
nombre varchar (30) not null,
placa varchar(30) default null,
descripcion varchar(50) default null,
direccion varchar (30) not null,
telefono varchar(15) default null,
celular varchar(15) default null,
email varchar(30) default null,
primary key (no_documento)
);




