package com.ruribe.web.reportes.pdf;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Phrase;
import com.ruribe.util.SessionUtils;

/**
 * Clase que maneja los eventos de pagina necesarios para agregar un encabezado y  conteo de paginas a un documento.
 * El encabezado, definido en onEndPage, consiste en una tabla con 3 celdas que contienen:
 * Frase del encabezado | pagina de | total de paginas, con una linea horizontal separando el
 * encabezado del texto
 *
 * Referencia: http://itextpdf.com/examples/iia.php?id=104
 * https://hashblogeando.wordpress.com/2013/08/09/creando-encabezados-y-conteo-de-paginas-en-archivos-pdf-con-java-e-itext/
 *
 * @author Ruribe
 */

public class CabeceraFacturaPDF extends PdfPageEventHelper {
    private String encabezado;
    private String tipoFactura;
	private Date fecha;
    private int id;
    PdfTemplate total;
    
    /**
     * Crea el objecto PdfTemplate el cual contiene el numero total de
     * paginas en el documento
     */
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30, 16);
    }
    
    /**
     * Esta es el metodo a llamar cuando ocurra el evento onEndPage, es en este evento
     * donde crearemos el encabeazado de la pagina con los elementos indicados.
     */
    public void onEndPage(PdfWriter writer, Document document) {
        PdfPTable tableCabecera = new PdfPTable(3);
       
        try {
            // Se determina el ancho y altura de la tabla
        	tableCabecera.setWidths(new int[]{2, 6, 3});
        	tableCabecera.setTotalWidth(457);
        	tableCabecera.setLockedWidth(true);
        	tableCabecera.getDefaultCell().setFixedHeight(20);
            // Borde de la celda
        	tableCabecera.getDefaultCell().setBorder(Rectangle.BOTTOM);
        	Font titleRemision = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
        	tableCabecera.addCell(new Phrase(encabezado,titleRemision));
        	tableCabecera.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        	//tableCabecera.addCell(String.format("Pagina " +writer.getPageNumber()+ " de ",total));
        	tableCabecera.addCell("");
            //PdfPCell cell = new PdfPCell(Image.getInstance(total));
            PdfPCell cell = new PdfPCell();
            cell.setBorder(Rectangle.BOTTOM);
            tableCabecera.addCell(cell);
            
            //Datos de empresa
            ////////
            //tipo de documento
              PdfPCell cellCabeceraRemision = new PdfPCell();
              PdfPTable  datosEmpresa= new PdfPTable(1); 
              Font titleFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 16, BaseColor.BLACK);
              datosEmpresa.addCell(UtilidadesPDF.createCell("SOLO ANDAMIOS SAS", 0, 1,1, Element.ALIGN_LEFT,0,titleFont));
              datosEmpresa.addCell(UtilidadesPDF.createCell("ALQUILER AL INSTANTE", 0, 1,1, Element.ALIGN_LEFT,0));
              datosEmpresa.addCell(UtilidadesPDF.createCell("Nit 900.545.142-1. IVA REGIMEN COMUN", 0, 1,1, Element.ALIGN_LEFT,0));
              datosEmpresa.addCell(UtilidadesPDF.createCell("ACTIVIDAD ECONOMICA 7730", 0, 1,1, Element.ALIGN_LEFT,0));
              datosEmpresa.addCell(UtilidadesPDF.createCell("ICA 77302-TARIFA ICA 7.0 X 1000", 0, 1,1, Element.ALIGN_LEFT,0));
              cellCabeceraRemision = new PdfPCell(datosEmpresa);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setRowspan(2);
              cellCabeceraRemision.setColspan(2);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setHorizontalAlignment(Element.ALIGN_LEFT);
              cellCabeceraRemision.setPaddingLeft(65);
              cellCabeceraRemision.setPaddingTop(3);
              tableCabecera.addCell(cellCabeceraRemision);
  //    
              try
              {
                    // asi sale ejecutandolo desde el main
              	//  Image foto = Image.getInstance("../JavaServerFaces/src/main/webapp/images/logo.png");
              	  //String rutaLogo= new File(".").getCanonicalPath()+"/src/main/webapp/images/logo.png";
              	  //String rutaLogo= "/Users/RONIE/git/factura/JavaServerFaces/src/main/webapp/images/logo.png";
              	  Image foto;
              	  try{
              		foto = Image.getInstance("http://"+SessionUtils.getRequest().getLocalName()+":"+SessionUtils.getRequest().getLocalPort()+SessionUtils.getRequest().getContextPath()+"/images/logo.png");
              	  }catch(Exception e){
              		foto = Image.getInstance("/Users/RONIE/git/factura/JavaServerFaces/src/main/webapp/images/logo.png");
              	  }	  
              	  foto.scaleToFit(100, 100);
                  foto.setAbsolutePosition(70, 690);
                  foto.setAlignment(Chunk.ALIGN_MIDDLE);
                  document.add(foto);
              }
              catch ( Exception e )
              {
                    e.printStackTrace();
              }
              
              //tipo de documento
              
              PdfPTable  tableDireccion= new PdfPTable(1);
              Font titleTableDireccion = FontFactory.getFont(FontFactory.COURIER_BOLD, 5, BaseColor.BLACK);
              tableDireccion.addCell(UtilidadesPDF.createCell("No somos grandes contribuyentes", 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              titleTableDireccion = FontFactory.getFont(FontFactory.HELVETICA, 5, BaseColor.BLACK);
              tableDireccion.addCell(UtilidadesPDF.createCell("Calle 70b No. 34 - 130 Te 3690411", 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              tableDireccion.addCell(UtilidadesPDF.createCell("Cel. 3013433318 - 304394090", 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              tableDireccion.addCell(UtilidadesPDF.createCell("email: soloandamiossas@hotmail.com", 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              tableDireccion.addCell(UtilidadesPDF.createCell("Barranquilla - Colombia", 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              titleTableDireccion = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
              tableDireccion.addCell(UtilidadesPDF.createCell(tipoFactura, 0, 1, 1,Element.ALIGN_RIGHT,1,titleTableDireccion));
              PdfPCell celdaTableDireccion= new PdfPCell(tableDireccion);
              celdaTableDireccion.setBorder(Rectangle.NO_BORDER);
              tableCabecera.addCell(celdaTableDireccion);
              
              // numero de venta
              PdfPTable  alquiler= new PdfPTable(2);  // creamos dos columnas en una meteremos un espacio y en la siguiente la caja de texto.
              alquiler.setWidths(new int[]{1,4});
              alquiler.addCell(UtilidadesPDF.createCell("", 0, 1,1, Element.ALIGN_RIGHT,4));
              //creamos el cuadro con el borde redondo
              PdfPTable  cuadro= new PdfPTable(1);
              cuadro.addCell(UtilidadesPDF.createCell("No. "+id, 0, 1,1, Element.ALIGN_RIGHT,4));
              cellCabeceraRemision = new PdfPCell(cuadro);
              cellCabeceraRemision.setCellEvent(new UtilidadesPDF.RoundRectangle());// creamos las esquinas ovaladas
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              alquiler.addCell(cellCabeceraRemision);//alquiler de equipo
              PdfPCell quitarborder= new PdfPCell(alquiler);
              quitarborder.setBorder(Rectangle.NO_BORDER);
              tableCabecera.addCell(quitarborder);
              
              //fecha
              PdfPTable  cellFecha= new PdfPTable(1); 
              cellFecha.addCell(UtilidadesPDF.createCellConBackgroung("FECHA", 0, 1, 1,Element.ALIGN_CENTER,1));
              cellFecha.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("dd/MM/yyyy").format(fecha), 0, 1, 1,Element.ALIGN_CENTER,4));
              cellCabeceraRemision = new PdfPCell(cellFecha);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setCellEvent(new UtilidadesPDF.RoundRectangle());
              //tableCabecera.addCell(cellCabeceraRemision);//fecha
              
              
            
            // Esta linea escribe la tabla como encabezado
            tableCabecera.writeSelectedRows(0, -1, 70, 803, writer.getDirectContent());
            
            /* CREACION DEL PIE DE PAGINA*/
            PdfPTable piedePagina = new PdfPTable(1);
            // Se determina el ancho y altura de la tabla
            piedePagina.setWidths(new int[]{1});
            piedePagina.setTotalWidth(500);
        
          //firmas y sellos    	       
	        PdfPTable tableFirmaSello = new PdfPTable(6);
	        tableFirmaSello.setWidths(new int[]{1,7,2,2,7,1});
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("EMISOR", 1, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("ACEPTADO", 1, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("FIRMA Y SELLO", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("FIRMA Y SELLO CLIENTE", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        
	        //eliminamos el borde de la tabla
	        PdfPCell celdaFirmaSello= new PdfPCell(tableFirmaSello);
	        celdaFirmaSello.setBorder(Rectangle.NO_BORDER);
	        celdaFirmaSello.setColspan(3); // combinamos 3 columnas que tiene definido el pie de pagina
	        celdaFirmaSello.setPaddingBottom(10);// dejamos margen inferior considerable para la firma y sello.
 	        piedePagina.addCell(celdaFirmaSello);
 	        
            
            // Borde de la celda
            piedePagina.getDefaultCell().setBorder(Rectangle.TOP);
            Font titleFontpePafina = FontFactory.getFont(FontFactory.HELVETICA, 6, BaseColor.BLACK);
	    	PdfPTable info = new PdfPTable(1);
	    	info.addCell(UtilidadesPDF.createCell("FECHA DE VENCIMIENTO DE ESTA FACTURA", 0, 1, 1,Element.ALIGN_LEFT,4,titleFontpePafina));
	    	info.addCell(UtilidadesPDF.createCell("--------------------------------------------------------------------------------------------------------", 0, 1, 1,Element.ALIGN_CENTER,0));
	    	titleFontpePafina = FontFactory.getFont(FontFactory.HELVETICA, 4, BaseColor.BLACK);
	    	//creamos la fila con margin top
	    	info.addCell(UtilidadesPDF.createCell("RESOLUCIÓN DIAN No 18762009336441 DE FECHA 2018/07/24 DEL 3666 al 10000 HABILITACIÓN", 0, 1, 1,Element.ALIGN_CENTER,3,0,0,0,titleFontpePafina));
            info.addCell(UtilidadesPDF.createCell("La presente factura de venta se asimila en todos sus efectos a una letra de cambio artículo 772,773,774 del código de comercio. El comprador declara haber recibido las mercancías y servicios antes descritas y enumeradas.", 0, 1, 1,Element.ALIGN_CENTER,0,titleFontpePafina));
            //creamos la final con margin botton
            info.addCell(UtilidadesPDF.createCell("Las mercancías antes descritas seguiran de propiedad de SOLO ANDAMIOS S.A.S., hasta la cancelación total de la presente factura quedando afectadas con prenda sin tenencia a la misma condición.", 0, 1, 1,Element.ALIGN_CENTER,0,0,0,7,titleFontpePafina));
            PdfPCell celdaPie = new PdfPCell(info);
            celdaPie.setCellEvent(new UtilidadesPDF.RoundRectangle());
            celdaPie.setBorder(Rectangle.NO_BORDER);
	       // a.setPadding(8);
            piedePagina.addCell(celdaPie); //columna 1 datos de la empresa
            piedePagina.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

            // Esta linea escribe la tabla como footer
            // se define la posicion del pie de pagina 90 margen izq 70 margen inferior
            piedePagina.writeSelectedRows(0, -1, 50, 90, writer.getDirectContent());
        }
        catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    
    /**
     * Realiza el conteo de paginas al momento de cerrar el documento
     */
    public void onCloseDocument(PdfWriter writer, Document document) {
    	Font titleFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber()),titleFont), 2, 2, 0);
    }    
    
    // Getter and Setters
    
    public String getEncabezado() {
        return encabezado;
    }
    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the tipoFactura
	 */
	public String getTipoFactura() {
		return tipoFactura;
	}

	/**
	 * @param tipoFactura the tipoFactura to set
	 */
	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}

}
