package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleDevolucionBean;
import com.ruribe.util.bean.DevolucionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class DevolucionPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Devolucion 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,IDevolucionBean iDevolucionBean) throws IOException, DocumentException{
//		 String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_DEVOLUCION")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 Cabecera encabezado = new Cabecera();
		 encabezado.setEncabezado("DEVOLUCION");// tipo de reporte
		 encabezado.setTipoGestion("ALQUILER DE EQUIPOS");
		 encabezado.setFecha(iDevolucionBean.getFecha_entrega());
		 encabezado.setId(iDevolucionBean.getIdDevolucion());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iDevolucionBean);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iDevolucionBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,IDevolucionBean iDevolucionBean) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
            
            //cuerpo factura datos del cliente
	        PdfPTable  datosCliente= new PdfPTable(4); 
            datosCliente.setWidths(new int[]{2,4,1,2});
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getRazon_social().toUpperCase(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nit.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getId_cliente(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dir. Ofic:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Direccion Obra:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIobraBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre contacto:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIobraBean().getContacto(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Hora de Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("hh:mm").format(iDevolucionBean.getFecha()), 0, 1, 1,Element.ALIGN_LEFT));
            
            UtilidadesPDF.crearCaja(outertable,datosCliente);
	        
	        //detalle de facura
	        String[] cabecera = {"Cod.","Descripcion de equipo.","Cant.","Peso"};     
	        PdfPTable detalleDevolucion = new PdfPTable(cabecera.length);
	        detalleDevolucion.setWidths(new int[]{2,4,1,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleDevolucion.getDefaultCell().setUseAscender(true);
	        detalleDevolucion.getDefaultCell().setUseDescender(true);
	        int cantidadAcumulada=0;
	        for (int i = 0; i < iDevolucionBean.getDetalleDevoluciones().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_LEFT,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.setHeaderRows(1);
            		//detalleDevolucion.setFooterRows(1);
            	}
            	
            	//agrupamos por codigo_producto, la ultima fila la agregamos siempre manual.
            	if (i<iDevolucionBean.getDetalleDevoluciones().size()-1)
            		//si el producto siguiente es igual sumamos la cantidad, cuando sea diferente lo pintamos con la cantidad acumulada.
	            	if (iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getCodigo_producto()!=iDevolucionBean.getDetalleDevoluciones().get(i+1).getIproductoBean().getCodigo_producto()){
	                  	//detalle de remision
	                  		cantidadAcumulada += iDevolucionBean.getDetalleDevoluciones().get(i).getCantidad();
		            	//detalle de remision
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell((iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Double.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
		        		cantidadAcumulada=0;
	            	}else{
		        		cantidadAcumulada +=iDevolucionBean.getDetalleDevoluciones().get(i).getCantidad();
		        	}
            	else{// ultima fila
            		//detalle de remision
              		cantidadAcumulada += iDevolucionBean.getDetalleDevoluciones().get(i).getCantidad();
	            	//detalle de remision
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell((iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Double.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
	        		cantidadAcumulada=0;
            	}
            }
	        
	        // detalleDevolucion.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleDevolucion);
	        
	        
	      //Observaciones
	        String[] titulo = {"Observaciones:"};     
	        PdfPTable observaciones = new PdfPTable(1);           
	        observaciones.addCell(UtilidadesPDF.createCell(titulo[0], 0, 1, 1,Element.ALIGN_TOP));
	        String obser=iDevolucionBean.getObservaciones();
	        observaciones.addCell(UtilidadesPDF.createCell(obser, 0, 1, 0,Element.ALIGN_LEFT));
        
	        UtilidadesPDF.crearCaja(outertable,observaciones);
	        
	      //tranportador    
	        PdfPTable tranportador = new PdfPTable(6); 
	        tranportador.setWidths(new int[]{4,4,4,3,2,2});
	        tranportador.addCell(UtilidadesPDF.createCell("Transportador:", 0, 1, 1,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell(iDevolucionBean.getItransportadorBean().getNombre(), 0, 1, 0,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell("Transporte", 0, 1, 1,Element.ALIGN_RIGHT));
	        tranportador.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iDevolucionBean.getTransporte()), 0, 1, 0,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell("PLaca:", 0, 1, 1,Element.ALIGN_RIGHT));
	        tranportador.addCell(UtilidadesPDF.createCell(iDevolucionBean.getItransportadorBean().getPlaca(), 0, 1, 1,Element.ALIGN_LEFT));
	        UtilidadesPDF.crearCaja(outertable,tranportador);
	        
	      //quien entrega    
	        PdfPTable entrega = new PdfPTable(4);    
	        entrega.setWidths(new int[]{2,7,2,7});
	        entrega.addCell(UtilidadesPDF.createCell("Quien Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("Quien Recibe:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        UtilidadesPDF.crearCaja(outertable,entrega);
  
	        // add the table	       
	        document.add(outertable);
	       
	        
//	        PdfPTable table = new PdfPTable(3);
//	        cell = new PdfPCell();
//	        cell = getCell("These cells have rounded borders at the top.");
//	        table.addCell(cell);
//	        cell = getCell("These cells aren't rounded at the bottom.");
//	        table.addCell(cell);
//	        cell = getCell("A custom cell event was used to achieve this.");
//	        table.addCell(cell);
//	        document.add(table);
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Devolucion.pdf"; 
    	IDevolucionBean iDevolucionBean = new DevolucionBean();
    	iDevolucionBean.setFecha(new Date());
    	iDevolucionBean.setIdDevolucion(120);
    	iDevolucionBean.setObservaciones("Observaciones");
    	iDevolucionBean.setTransporte(20000);
        
    	IProductoBean producto ;
        IDetalleDevolucionBean detalle;
        for (int i =0; i<100; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setDescripcion("descripcion" + i);
         producto.setCodigo_producto(7);
        // producto.setPeso(10);
         producto.setDescripcion("descripcion" + i);
         detalle = (IDetalleDevolucionBean) new DetalleDevolucionBean();
         detalle.setIproductoBean(producto);
         iDevolucionBean.setDetalleDevoluciones(detalle);	
        }
        
        new DevolucionPDF().crearPDF(DEST,iDevolucionBean);
       
        	
	}

}
