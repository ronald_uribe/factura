package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.bean.DetalleSalidaBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.SalidaBean;
import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.ISalidaBean;

public class SalidaPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Salida 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,ISalidaBean iSalidaBean) throws IOException, DocumentException{
		// String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_SALIDA")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 Cabecera encabezado = new Cabecera();
		 encabezado.setEncabezado("SALIDA");// tipo de reporte
		 encabezado.setTipoGestion("SALIDA DE EQUIPOS");
		 encabezado.setFecha(iSalidaBean.getFecha());
		 encabezado.setId(iSalidaBean.getIdSalida());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iSalidaBean);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iSalidaBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,ISalidaBean iSalidaBean) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
            
            //cuerpo factura datos del cliente
	        PdfPTable  datosCliente= new PdfPTable(4); 
            datosCliente.setWidths(new int[]{2,4,1,2});
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iSalidaBean.getIproveedorBean().getRazon_social(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nit.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iSalidaBean.getIproveedorBean().getNo_documento(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dir. Ofic:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iSalidaBean.getIproveedorBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dirección Obra:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("", 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre contacto:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Hora de Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("HH:mm").format(iSalidaBean.getFecha()), 0, 1, 1,Element.ALIGN_LEFT));
            
            UtilidadesPDF.crearCaja(outertable,datosCliente);
	        
	        //detalle de facura
	        String[] cabecera = {"Cód.","Descripción de equipo.","Cant.","Peso"};     
	        PdfPTable detalleSalida = new PdfPTable(cabecera.length);
	        detalleSalida.setWidths(new int[]{2,4,1,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleSalida.getDefaultCell().setUseAscender(true);
	        detalleSalida.getDefaultCell().setUseDescender(true);
	        for (int i = 0; i < iSalidaBean.getDetalleSalidas().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleSalida.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleSalida.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_LEFT,Rectangle.RIGHT));
            		detalleSalida.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleSalida.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleSalida.setHeaderRows(1);
            		//detalleSalida.setFooterRows(1);
            	}
            	//detalle de remision
        		detalleSalida.addCell(UtilidadesPDF.createCell(Integer.toString(iSalidaBean.getDetalleSalidas().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
        		detalleSalida.addCell(UtilidadesPDF.createCell((iSalidaBean.getDetalleSalidas().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
        		detalleSalida.addCell(UtilidadesPDF.createCell(Integer.toString(iSalidaBean.getDetalleSalidas().get(i).getCantidad()), 1, 1, 1,Element.ALIGN_CENTER));
        		detalleSalida.addCell(UtilidadesPDF.createCell(Double.toString(iSalidaBean.getDetalleSalidas().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
            	
            }
	        
	        // detalleSalida.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleSalida);
	        
	        
	      //Observaciones
	        String[] titulo = {"Observaciones:"};     
	        PdfPTable observaciones = new PdfPTable(1);           
	        observaciones.addCell(UtilidadesPDF.createCell(titulo[0], 0, 1, 1,Element.ALIGN_TOP));
	       // String obser=iSalidaBean.getObservaciones();
	        observaciones.addCell(UtilidadesPDF.createCell("", 0, 1, 0,Element.ALIGN_LEFT));
        
	        UtilidadesPDF.crearCaja(outertable,observaciones);
	        
	      
	        
	      //quien entrega    
	        PdfPTable entrega = new PdfPTable(4);    
	        entrega.setWidths(new int[]{2,7,2,7});
	        entrega.addCell(UtilidadesPDF.createCell("Quien Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("Quien Recibe:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        UtilidadesPDF.crearCaja(outertable,entrega);
  
	        // add the table	       
	        document.add(outertable);

	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Salida.pdf"; 
    	ISalidaBean iSalidaBean = new SalidaBean();
    	iSalidaBean.setFecha(new Date());
    	iSalidaBean.setIdSalida(120);
        
    	IProductoBean producto ;
        IDetalleSalidaBean detalle;
        for (int i =0; i<100; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setDescripcion("descripcion" + i);
         producto.setCodigo_producto(7);
       //  producto.setPeso(10);
         producto.setDescripcion("descripcion" + i);
         detalle = (IDetalleSalidaBean) new DetalleSalidaBean();
         detalle.setIproductoBean(producto);
         iSalidaBean.setDetalleSalidas(detalle);	
        }
        
        new SalidaPDF().crearPDF(DEST,iSalidaBean);
       
        	
	}

}
