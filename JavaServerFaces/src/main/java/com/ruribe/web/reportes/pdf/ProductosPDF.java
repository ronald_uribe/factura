package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;

public class ProductosPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Remision 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,List<IReferenciaBean> listaReferencia) throws IOException, DocumentException{
		// String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_CATALOGO")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 CabeceraListado encabezado = new CabeceraListado();
		 encabezado.setEncabezado("PRODUCTOS");// tipo de reporte
		 encabezado.setTipoGestion("LSTADO PRODUCTOS");
		 encabezado.setFecha(new Date());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,listaReferencia);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param listaReferencia
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,List<IReferenciaBean> listaReferencia) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
	        outertable.addCell(UtilidadesPDF.createCell("CATÁLOGO DE PRODUCTOS", 0, 1,1, Element.ALIGN_LEFT,4));
	        //detalle de facura
	        String[] cabecera = {"Cód.","Descripción de equipo.","peso","precio.","reposicion"};     
	        PdfPTable detalleRemision = new PdfPTable(cabecera.length);
	        detalleRemision.setWidths(new int[]{1,5,1,2,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleRemision.getDefaultCell().setUseAscender(true);
	        detalleRemision.getDefaultCell().setUseDescender(true);
	        for (int i = 0; i < listaReferencia.size(); i++)
            {
            	if (i==0){//cabecera
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_LEFT,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_RIGHT,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_RIGHT,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[4], 0, 1, Element.ALIGN_RIGHT,Rectangle.RIGHT));
            		detalleRemision.setHeaderRows(1);
            		//detalleRemision.setFooterRows(1);
            	}
            	//detalle de remision
        		detalleRemision.addCell(UtilidadesPDF.createCell(Integer.toString(listaReferencia.get(i).getCodigo_interno()), 1, 1, 1,Element.ALIGN_CENTER));
        		detalleRemision.addCell(UtilidadesPDF.createCell((listaReferencia.get(i).getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
        		detalleRemision.addCell(UtilidadesPDF.createCell(Double.toString(listaReferencia.get(i).getPeso()), 1, 1, 1,Element.ALIGN_RIGHT));
        		detalleRemision.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(listaReferencia.get(i).getPrecio_alquiler()), 1, 1, 1,Element.ALIGN_RIGHT));
        		detalleRemision.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(listaReferencia.get(i).getPrecio_reposicion()), 1, 1, 1,Element.ALIGN_RIGHT));
        		
            }
	        
	        // detalleRemision.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleRemision);
	          
	        // add the table	       
	        document.add(outertable);
	       
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "catalogo.pdf"; 
    	List<IReferenciaBean> listaReferencia = new ArrayList<IReferenciaBean>();
    	
        for (int i =0; i<100; i++){
        	IReferenciaBean ref = new ReferenciaBean();
        	ref.setCodigo_interno(i);
        	ref.setDescripcion("descripcion"+i);
        	ref.setPeso(10.00);
        	ref.setPrecio_alquiler(100000);
        	ref.setPrecio_reposicion(1000000);
        	listaReferencia.add(ref);
        }
        
       new ProductosPDF().crearPDF(DEST,listaReferencia);
       
        	
	}

}
