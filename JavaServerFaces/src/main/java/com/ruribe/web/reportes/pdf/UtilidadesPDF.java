package com.ruribe.web.reportes.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

public class UtilidadesPDF {
	/**
	 * crea una celda con fondo gris con los bordes 
	 * @author RONIE
	 *
	 */
	public static class  RoundRectangle implements PdfPCellEvent {
        public void cellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] canvas) {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth()-3,
                rect.getHeight()-3, 4);	          
            cb.stroke();
        }
    }
	/**
	 * 
	 * @author RONIE
	 *
	 */
	public static class CellBackground implements PdfPCellEvent {
	    public void cellLayout(PdfPCell cell, Rectangle rect,
	            PdfContentByte[] canvas) {
	        PdfContentByte cb = canvas[PdfPTable.BACKGROUNDCANVAS];
	        cb.roundRectangle(
	        		 rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth()-3,
		                rect.getHeight()-3, 4);
	        cb.setCMYKColorFill(0000, 0000, 0000, 2222);
	        cb.fill();
	    }
	}
	
	/**
     * crear una celda con tamano de celda variable.
     * @param content descripcion
     * @param borderWidth
     * @param colspan
     * @param rowspan
     * @param alignment
     * @param padding
     * @param size tamano de letra
     * @return
     */
    public static PdfPCell createCell(String content, int borderWidth, int colspan, int rowspan, int alignment, int padding, Font titleFont) {
   	 
        Paragraph docTitle = new Paragraph(content, titleFont);
    	PdfPCell cell = new PdfPCell(docTitle);
        cell.setBorder(borderWidth);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setHorizontalAlignment(alignment);
        cell.setPadding(padding);
        return cell;
    }
    
    /**
     * Crea una celda recibiendo como parametro texto, borde, combinacion de columnas y filas, margenes y tipo de letra.
     * @param content
     * @param borderWidth
     * @param colspan
     * @param rowspan
     * @param alignment
     * @param paddingT
     * @param paddingL
     * @param paddingR
     * @param paddingB
     * @param titleFont
     * @return PdfPCell
     */
    public static PdfPCell createCell(String content, int borderWidth, int colspan, int rowspan, int alignment, int paddingT,int paddingL,int paddingR,int paddingB, Font titleFont) {
   	 
        Paragraph docTitle = new Paragraph(content, titleFont);
    	PdfPCell cell = new PdfPCell(docTitle);
        cell.setBorder(borderWidth);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingBottom(paddingB);
        cell.setPaddingLeft(paddingL);
        cell.setPaddingRight(paddingR);
        cell.setPaddingTop(paddingT);
        return cell;
    }
    
    /**
     * crear una celda con tamano de letra fijo a 8 .
     * @param content descripcion
     * @param borderWidth
     * @param colspan
     * @param rowspan
     * @param alignment
     * @param padding
     * @return
     */
    public static PdfPCell createCell(String content, int borderWidth, int colspan, int rowspan, int alignment, int padding) {
   	 
    	Font titleFont = FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK);
        Paragraph docTitle = new Paragraph(content, titleFont);
    	PdfPCell cell = new PdfPCell(docTitle);
        cell.setBorder(borderWidth);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setHorizontalAlignment(alignment);
        cell.setPadding(padding);
        return cell;
    }

    /**
     * crear celda con color de fondo.
     * @param content descripcion del campo
     * @param borderWidth tamano del borde
     * @param colspan agrupamiendo de columnas
     * @param rowspan agrupamiento de filas
     * @param alignment alineacion
     * @param padding margen
     * @return
     */
    public static PdfPCell createCellConBackgroung(String content, int borderWidth, int colspan, int rowspan, int alignment, int padding) {
	   	 
    	Font titleFont = FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK);
        Paragraph docTitle = new Paragraph(content, titleFont);
    	PdfPCell cell = new PdfPCell(docTitle);
        cell.setBorder(borderWidth);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setFixedHeight(22);
        cell.setHorizontalAlignment(alignment);
        cell.setPadding(padding);
        cell.setCellEvent(new UtilidadesPDF.CellBackground());
        return cell;
    }
    
    public PdfPCell getCell(String content) {
        PdfPCell cell = new PdfPCell(new Phrase(content));
        cell.setCellEvent(new SpecialRoundedCell());
        cell.setPadding(5);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
    
    /**
     * 
     * @author RONIE
     *
     */
    class SpecialRoundedCell implements PdfPCellEvent {
        public void cellLayout(PdfPCell cell, Rectangle position,
            PdfContentByte[] canvases) {
            PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
            float llx = position.getLeft() + 2;
            float lly = position.getBottom() + 2;
            float urx = position.getRight() - 2;
            float ury = position.getTop() - 2;
            float r = 4;
            float b = 0.4477f;
            canvas.moveTo(llx, lly);
            canvas.lineTo(urx, lly);
            canvas.lineTo(urx, ury - r);
            canvas.curveTo(urx, ury - r * b, urx - r * b, ury, urx - r, ury);
            canvas.lineTo(llx + r, ury);
            canvas.curveTo(llx + r * b, ury, llx, ury - r * b, llx, ury - r);
            canvas.lineTo(llx, lly);
            canvas.stroke();
        }
    }
    
	 
		/**
		 * 
		 * @param content valor 
		 * @param borderWidth tamano del borde
		 * @param colspan filas a agrupar
		 * @param rowspan columnas a agrupar
		 * @param alignment alineacion del texto
		 * @return PdfPCell
		 */
	    public static PdfPCell createCell(String content, int borderWidth, int colspan, int rowspan, int alignment) {
	 
	    	Font titleFont = FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK);
           Paragraph docTitle = new Paragraph(content, titleFont);
	    	PdfPCell cell = new PdfPCell(docTitle);
	        cell.setBorder(borderWidth);
	        cell.setColspan(colspan);
	        cell.setRowspan(rowspan);
	        cell.setHorizontalAlignment(alignment);
	        return cell;
	    }
	    
	    
	    /**
		 * 
		 * @param pintar cabecera de un detalle 
		 * @param borderWidth tamano del borde
		 * @param colspan filas a agrupar
		 * @param alignment alineacion del texto
		 * @param noBorder 
		 * @return PdfPCell
		 */
	    public static PdfPCell pintarCabecera(String content, float borderWidth, int colspan, int alignment, int noBorder) {
	        PdfPCell cell = new PdfPCell(new Phrase(content));
	        cell.setBorder(0);
	       // cell.setColspan(colspan);
	        cell.setHorizontalAlignment(alignment);
	      //  cell.setBorder(noBorder);		
			cell.setFixedHeight(22);
			//cell.setCellEvent(new RoundRectangle());
	    //  cell.setCellEvent(new CellBackground());
	        //cell.setPadding(0);
	        cell.setBackgroundColor(BaseColor.GRAY);
	        
	        return cell;
	    }
	    
	    /**
		 * 
		 * @param pintar cabecera de un detalle 
		 * @param borderWidth tamano del borde
		 * @param colspan filas a agrupar
		 * @param alignment alineacion del texto
		 * @param noBorder 
		 * @return PdfPCell
		 */
	    public static PdfPCell pintarCabecera(String content, float borderWidth, int colspan, int alignment, int noBorder,Font fontTitleCabeceraTable) {
	    	PdfPCell cell = new PdfPCell(new Phrase(content,fontTitleCabeceraTable));
	        cell.setBorder(0);
			cell.setFixedHeight(22);
	        cell.setBackgroundColor(BaseColor.GRAY);
	        
	        return cell;
	    }
	    
	    /**
	     * crea una los bordes redondos de una tabla 
	     * @param outertable
	     * @param tableGenerica
	     */
	    public static void crearCaja(PdfPTable outertable,PdfPTable tableGenerica){
	    	PdfPCell cell;
	       // PdfPCellEvent roundRectangle = 
	        // outer table
	        
	    	cell = new PdfPCell(tableGenerica);
	        cell.setCellEvent(new UtilidadesPDF.RoundRectangle());
	       // cell.setCellEvent(new CellBackground());
	        
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setPadding(8);
	        outertable.addCell(cell);	        
	    }
	    
	    /**
	     * crea una tabla sin borde 
	     * @param outertable
	     * @param tableGenerica
	     */
	    public static void crearCajaSinBorde(PdfPTable outertable,PdfPTable tableGenerica){
	    	PdfPCell cell;
	    	cell = new PdfPCell(tableGenerica);
	        cell.setBorder(Rectangle.NO_BORDER);
	        //cell.setPadding(8);
	        outertable.addCell(cell);	        
	    }

}
