package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.DetalleFacturaBean;
import com.ruribe.util.bean.FacturaBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleFacturaBean;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class FacturaPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo factura 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,IFacturaBean iFacturaBean) throws IOException, DocumentException{
		 File path = new File(Propiedades.leerPropiedad("SHARED_FACTURA")).getAbsoluteFile();
		 DEST = path + "/"+nombre;   
	     File file = new File(DEST).getAbsoluteFile();
	     file.getParentFile().mkdirs();
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 130);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 CabeceraFacturaPDF encabezado = new CabeceraFacturaPDF();
		 encabezado.setEncabezado("FACTURA");// tipo de reporte
		 encabezado.setTipoFactura("FACTURA DE VENTA");
		 encabezado.setFecha(iFacturaBean.getFecha_factura());
		 encabezado.setId(iFacturaBean.getIdFactura());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iFacturaBean);
         document.close();
         //generar archivo del lado servidor
         //Desktop d = Desktop.getDesktop();  
         //d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iFacturaBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,IFacturaBean iFacturaBean) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
	        outertable.addCell(UtilidadesPDF.createCell("Fecha de Factura:"+ new SimpleDateFormat("dd/MM/yyyy").format(iFacturaBean.getFecha_factura()), 0, 1,1, Element.ALIGN_LEFT,4));
            //cuerpo factura datos del cliente
	        PdfPTable  datosCliente= new PdfPTable(2); 
            datosCliente.setWidths(new int[]{1,9});
            datosCliente.addCell(UtilidadesPDF.createCell(iFacturaBean.getIclienteBean().getRazon_social().toUpperCase(), 0, 2, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("NIT.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iFacturaBean.getIclienteBean().getId_cliente(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("OBRA:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iFacturaBean.getIobraBean().getNombre(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("OFIC.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iFacturaBean.getIobraBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("TEL:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iFacturaBean.getIobraBean().getTelefono(), 0, 3, 1,Element.ALIGN_LEFT));
            
            UtilidadesPDF.crearCaja(outertable,datosCliente);
            
            outertable.addCell(UtilidadesPDF.createCell("DEBE EL VALOR DEL SIGUIENTE EQUIPO EN ALQUILER", 0, 1,1, Element.ALIGN_LEFT,4));
	        
            //detalle de facura
	        String[] cabecera = {"Cant","Descripción de equipo","Inicio","corte","días","Vr./Unit","Vr./Total"};     
	        PdfPTable detalleFatura = new PdfPTable(cabecera.length);
	        detalleFatura.setWidths(new int[]{4,22,8,8,4,7,8});
	        
	        // define el numero de columnas cabeceras.
	        detalleFatura.getDefaultCell().setUseAscender(true);
	        detalleFatura.getDefaultCell().setUseDescender(true);
	        Font fontTitleCabeceraTable = FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK);
	        
	        int vrParcial=0;
	        for (int i = 0; i < iFacturaBean.getListadetalleFactura().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.LEFT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[4], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[5], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.addCell(UtilidadesPDF.pintarCabecera(cabecera[6], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT,fontTitleCabeceraTable));
            		detalleFatura.setHeaderRows(1);
            		//detalleDevolucion.setFooterRows(1);
            	}
            	
	            	//detalle de factura
            		detalleFatura.addCell(UtilidadesPDF.createCell(Integer.toString(iFacturaBean.getListadetalleFactura().get(i).getCantidad()), 1, 1, 1,Element.ALIGN_CENTER));
            		detalleFatura.addCell(UtilidadesPDF.createCell((iFacturaBean.getListadetalleFactura().get(i).getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
            		
            		if (iFacturaBean.getListadetalleFactura().get(i).getNo_dias()==0){
            			detalleFatura.addCell(UtilidadesPDF.createCell("PENDIENTE", 1, 2, 0,Element.ALIGN_CENTER));
            		}else{
            			detalleFatura.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("dd/MM/yyyy").format(iFacturaBean.getListadetalleFactura().get(i).getFecha_inicial()), 1, 1, 1,Element.ALIGN_CENTER));
            			detalleFatura.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("dd/MM/yyyy").format(iFacturaBean.getListadetalleFactura().get(i).getFecha_final()), 1, 1, 1,Element.ALIGN_CENTER));
            		}
            		detalleFatura.addCell(UtilidadesPDF.createCell(Integer.toString(iFacturaBean.getListadetalleFactura().get(i).getNo_dias()), 1, 1, 1,Element.ALIGN_CENTER));
            		detalleFatura.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getListadetalleFactura().get(i).getVr_unitario()), 1, 1, 1,Element.ALIGN_RIGHT));
            		detalleFatura.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getListadetalleFactura().get(i).getVr_total()), 1, 1, 1,Element.ALIGN_RIGHT));
            		vrParcial+=iFacturaBean.getListadetalleFactura().get(i).getVr_total();
            }	
	        iFacturaBean.setVr_parcial((double) vrParcial);
	        int subtotal=(int) (vrParcial+iFacturaBean.getTransporte()-iFacturaBean.getDescuento());
	        // detalleFatura.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleFatura);
	        
	        //eliminamos el borde de la tabla
	        PdfPCell cell= new PdfPCell(); // celda utilizada para observaciones y firma y sello
// 	        cell.setBorder(Rectangle.NO_BORDER);
//	        //Observaciones y valores
	        PdfPTable tableObservaciones = new PdfPTable(4); 
	        tableObservaciones.setWidths(new int[]{1,4,1,1});
	        //tableObservaciones.addCell(UtilidadesPDF.createCell("Observaciones", 0, 1, 1,Element.ALIGN_LEFT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell(iFacturaBean.getObservaciones(), 0, 2, 0,Element.ALIGN_CENTER));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("PARCIAL", 		0, 1, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getVr_parcial()), 	0, 1, 1,Element.ALIGN_RIGHT));
	      
	        //solo mostramos la linea cuando tiene transporte
	        if (iFacturaBean.getTransporte()!=0){
	        	tableObservaciones.addCell(UtilidadesPDF.createCell("TRANSPORTE", 	0, 3, 1,Element.ALIGN_RIGHT));
	        	tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getTransporte().intValue()), 	0, 1, 1,Element.ALIGN_RIGHT));
	        }
	        //solo mostramos la linea cuando tiene descuentos
	        if (iFacturaBean.getDescuento()!=0){
		        tableObservaciones.addCell(UtilidadesPDF.createCell("DESCUENTO", 	0, 3, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getDescuento()), 	0, 1, 1,Element.ALIGN_RIGHT));
	        }
	        tableObservaciones.addCell(UtilidadesPDF.createCell("SUB-TOTAL", 	0, 3, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(subtotal), 	0, 1, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("IVA 19%", 		0, 3, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getIva()), 		0, 1, 1,Element.ALIGN_RIGHT));
	      
	        //solo mostramos la linea cuando tiene reposicion
	        if (iFacturaBean.getReposicion()!=0){
	        	tableObservaciones.addCell(UtilidadesPDF.createCell("REPOSICIÓN", 	0, 3, 1,Element.ALIGN_RIGHT));
	        	tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getReposicion()), 	0, 1, 1,Element.ALIGN_RIGHT));
	        }
	        tableObservaciones.addCell(UtilidadesPDF.createCell("TOTAL", 		0, 3, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iFacturaBean.getTotal()), 	0, 1, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.setWidthPercentage(100);
	        // metemos la tabla en la celda para quitar los bordes
	        cell.addElement(tableObservaciones);
	        cell.setBorder(0);
	        cell.setPaddingRight(3);// 
	        outertable.addCell(cell);
	        
	     
	        //firmas y sellos    
	       /*
	        PdfPTable tableFirmaSello = new PdfPTable(4);
	        tableFirmaSello.setWidths(new int[]{7,2,2,7});
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("EMISOR", 1, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("CLIENTE", 1, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("FIRMA Y SELLO", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_CENTER));
	        tableFirmaSello.addCell(UtilidadesPDF.createCell("FIRMA Y SELLO", 0, 1, 1,Element.ALIGN_CENTER));
	        
	        cell= new PdfPCell();// reiniciamos la celda para meter los valores de la nueva tabla
	        cell.addElement(tableFirmaSello);
 	        cell.setBorder(Rectangle.NO_BORDER);
 	        cell.setPaddingTop(50);
	        outertable.addCell(cell);
	        */
	        // add the table	       
	        document.add(outertable);
	       
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Factura3.pdf"; 
    	IFacturaBean iFacturaBean = new FacturaBean();
    	iFacturaBean.setFecha_factura(new Date());
    	iFacturaBean.setIdFactura(2219370);
    	iFacturaBean.setObservaciones("Alquiler minimo 7 dias.");
    	iFacturaBean.setTransporte((long) 20000);
    	IClienteBean iclienteBean = new ClienteBean();
    	iclienteBean.setRazon_social("INGENIERIA Y SERVICIOS ABC EU");
    	iclienteBean.setId_cliente("900.875.145-y");
        iFacturaBean.setIclienteBean(iclienteBean);
    	IProductoBean producto ; 
    	IDetalleFacturaBean detalle;
        for (int i =0; i<15; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setCodigo_producto(7);
         detalle = (IDetalleFacturaBean) new DetalleFacturaBean();
         detalle.setDescripcion("descripcion" + i);
         detalle.setIdRemision(11070);
         detalle.setIproductoBean(producto);
         detalle.setCantidad(57);
         detalle.setVr_unitario(25000);
         //detalle.setVr_total(500000);
         detalle.setFecha_final(new Date());
         detalle.setFecha_inicial(new Date());
         detalle.setNo_dias(20);
         iFacturaBean.setListadetalleFactura(detalle);	
        }
        
        new FacturaPDF().crearPDF(DEST,(IFacturaBean) iFacturaBean);
       
        	
	}

}
