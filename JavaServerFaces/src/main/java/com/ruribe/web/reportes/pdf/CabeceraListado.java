package com.ruribe.web.reportes.pdf;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Phrase;
import com.ruribe.util.SessionUtils;

/**
 * Clase que maneja los eventos de pagina necesarios para agregar un encabezado y  conteo de paginas a un documento.
 * El encabezado, definido en onEndPage, consiste en una tabla con 3 celdas que contienen:
 * Frase del encabezado | pagina de | total de paginas, con una linea horizontal separando el
 * encabezado del texto
 *
 * Referencia: http://itextpdf.com/examples/iia.php?id=104
 * https://hashblogeando.wordpress.com/2013/08/09/creando-encabezados-y-conteo-de-paginas-en-archivos-pdf-con-java-e-itext/
 *
 * @author Ruribe
 */

public class CabeceraListado extends PdfPageEventHelper {
    private String encabezado;
    private String tipoGestion;
    private Date fecha;
    PdfTemplate total;
    
    /**
     * Crea el objecto PdfTemplate el cual contiene el numero total de
     * paginas en el documento
     */
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30, 16);
    }
    
    /**
     * Esta es el metodo a llamar cuando ocurra el evento onEndPage, es en este evento
     * donde crearemos el encabeazado de la pagina con los elementos indicados.
     */
    public void onEndPage(PdfWriter writer, Document document) {
        PdfPTable tableCabecera = new PdfPTable(3);
        PdfPTable piedePagina = new PdfPTable(3);
        try {
            // Se determina el ancho y altura de la tabla
        	tableCabecera.setWidths(new int[]{1, 3, 1});
        	tableCabecera.setTotalWidth(457);
        	tableCabecera.setLockedWidth(true);
        	tableCabecera.getDefaultCell().setFixedHeight(20);
            // Borde de la celda
        	tableCabecera.getDefaultCell().setBorder(Rectangle.BOTTOM);
        	Font titleRemision = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
        	tableCabecera.addCell(new Phrase(encabezado,titleRemision));
        	tableCabecera.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        	//tableCabecera.addCell(String.format("Pagina " +writer.getPageNumber()+ " de ",total));
        	tableCabecera.addCell("");
            //PdfPCell cell = new PdfPCell(Image.getInstance(total));
            PdfPCell cell = new PdfPCell();
            cell.setBorder(Rectangle.BOTTOM);
            tableCabecera.addCell(cell);
            
            //Datos de empresa
            ////////
            //tipo de documento
              PdfPCell cellCabeceraRemision = new PdfPCell();
              PdfPTable  datosEmpresa= new PdfPTable(1); 
              Font titleFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 16, BaseColor.BLACK);
              datosEmpresa.addCell(UtilidadesPDF.createCell("SOLO ANDAMIOS SAS", 0, 1,1, Element.ALIGN_LEFT,0,titleFont));
              datosEmpresa.addCell(UtilidadesPDF.createCell("Alquiler al instante", 0, 1,1, Element.ALIGN_LEFT,0));
              datosEmpresa.addCell(UtilidadesPDF.createCell("Nit 900.545.142-1.", 0, 1,1, Element.ALIGN_LEFT,0));
              cellCabeceraRemision = new PdfPCell(datosEmpresa);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setRowspan(2);
              cellCabeceraRemision.setColspan(2);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setHorizontalAlignment(Element.ALIGN_LEFT);
              cellCabeceraRemision.setPaddingLeft(60);
              tableCabecera.addCell(cellCabeceraRemision);
  //    
              try
              {
                    // asi sale ejecutandolo desde el main
              	//  Image foto = Image.getInstance("../JavaServerFaces/src/main/webapp/images/logo.png");
              	  //String rutaLogo= new File(".").getCanonicalPath()+"/src/main/webapp/images/logo.png";
              	  //String rutaLogo= "/Users/RONIE/git/factura/JavaServerFaces/src/main/webapp/images/logo.png";
              	  Image foto = Image.getInstance("http://"+SessionUtils.getRequest().getLocalName()+":"+SessionUtils.getRequest().getLocalPort()+SessionUtils.getRequest().getContextPath()+"/images/logo.png");
                    foto.scaleToFit(100, 100);
                    foto.setAbsolutePosition(70, 690);
                    foto.setAlignment(Chunk.ALIGN_MIDDLE);
                    document.add(foto);
              }
              catch ( Exception e )
              {
                    e.printStackTrace();
              }
              
              //tipo de documento
              
              PdfPTable  alquiler= new PdfPTable(1); 
  	          alquiler.addCell(UtilidadesPDF.createCellConBackgroung(tipoGestion, 0, 1,1, Element.ALIGN_CENTER,1));
              alquiler.addCell(UtilidadesPDF.createCell("", 0, 1,1, Element.ALIGN_RIGHT,4));
              cellCabeceraRemision = new PdfPCell(alquiler);
              cellCabeceraRemision.setCellEvent(new UtilidadesPDF.RoundRectangle());
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              tableCabecera.addCell(cellCabeceraRemision);//alquiler de equipo
             
              //fecha
              PdfPTable  cellFecha= new PdfPTable(1); 
              cellFecha.addCell(UtilidadesPDF.createCellConBackgroung("FECHA", 0, 1, 1,Element.ALIGN_CENTER,1));
              cellFecha.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("dd/MM/yyyy").format(fecha), 0, 1, 1,Element.ALIGN_RIGHT,4));
              cellCabeceraRemision = new PdfPCell(cellFecha);
              cellCabeceraRemision.setBorder(Rectangle.NO_BORDER);
              cellCabeceraRemision.setCellEvent(new UtilidadesPDF.RoundRectangle());
              tableCabecera.addCell(cellCabeceraRemision);//fecha
              cellCabeceraRemision = new PdfPCell();
              tableCabecera.addCell(cellCabeceraRemision);
              
            
            // Esta linea escribe la tabla como encabezado
            tableCabecera.writeSelectedRows(0, -1, 70, 803, writer.getDirectContent());
            
            
            
         // Se determina el ancho y altura de la tabla
            piedePagina.setWidths(new int[]{25, 6, 1});
            piedePagina.setTotalWidth(415);
            
            // Borde de la celda
            piedePagina.getDefaultCell().setBorder(Rectangle.TOP);
            Font titleFontpePafina = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
            Paragraph docTitle = new Paragraph("Calle 70b No 34-130 Tel. 369 0411 - (301)343 3318-(304)394 6090 - soloandamios@hotmail.com Bararanquilla - Colombia", titleFontpePafina);
	    	piedePagina.setHorizontalAlignment(Element.ALIGN_LEFT);
            piedePagina.addCell(docTitle); //columna 1 datos de la empresa
           
            piedePagina.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
          //columna 2 pagina x de
            piedePagina.addCell(new Paragraph(String.format("Página " +writer.getPageNumber()+ " de ",total), titleFontpePafina));
            
            //pagina total
            PdfPCell cell2 = new PdfPCell(Image.getInstance(total));
            cell2.setBorder(Rectangle.TOP);
            piedePagina.setHorizontalAlignment(Element.ALIGN_RIGHT);
            piedePagina.addCell(cell2); //columna 3 total de paginas
            // Esta linea escribe la tabla como footer
            
            // se define la posicion del pie de pagina 90 margen izq 70 margen inferior
            piedePagina.writeSelectedRows(0, -1, 70, 70, writer.getDirectContent());
        }
        catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    
    /**
     * Realiza el conteo de paginas al momento de cerrar el documento
     */
    public void onCloseDocument(PdfWriter writer, Document document) {
    	Font titleFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 8, BaseColor.BLACK);
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber()),titleFont), 2, 2, 0);
    }    
    
    // Getter and Setters
    
    public String getEncabezado() {
        return encabezado;
    }
    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the tipoGestion
	 */
	public String getTipoGestion() {
		return tipoGestion;
	}

	/**
	 * @param tipoGestion the tipoGestion to set
	 */
	public void setTipoGestion(String tipoGestion) {
		this.tipoGestion = tipoGestion;
	}
}
