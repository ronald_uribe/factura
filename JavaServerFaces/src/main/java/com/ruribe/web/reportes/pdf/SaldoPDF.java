package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.bean.DetalleDevolucionBean;
import com.ruribe.util.bean.DevolucionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;

public class SaldoPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Devolucion 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,IDevolucionBean iDevolucionBean) throws IOException, DocumentException{
//		 String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_SALDO")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 CabeceraListado encabezado = new CabeceraListado();
		 encabezado.setEncabezado("SALDO");// tipo de reporte
		 encabezado.setTipoGestion("EQUIPOS PENDIENTES");
		 encabezado.setFecha(new Date());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iDevolucionBean);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iDevolucionBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,IDevolucionBean iDevolucionBean) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
            
            //cuerpo factura datos del cliente
	        PdfPTable  datosCliente= new PdfPTable(4); 
            datosCliente.setWidths(new int[]{2,4,1,2});
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getRazon_social().toUpperCase(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nit.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getId_cliente(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dir. Ofic:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIclienteBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Direccion Obra:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIobraBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre contacto:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iDevolucionBean.getIobraBean().getContacto(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
            
            UtilidadesPDF.crearCaja(outertable,datosCliente);
	        
	        //detalle de facura
	        String[] cabecera = {"Cod.","Descripcion de equipo.","Cant.","Peso"};     
	        PdfPTable detalleDevolucion = new PdfPTable(cabecera.length);
	        detalleDevolucion.setWidths(new int[]{2,4,1,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleDevolucion.getDefaultCell().setUseAscender(true);
	        detalleDevolucion.getDefaultCell().setUseDescender(true);
	        int cantidadAcumulada=0;
	        for (int i = 0; i < iDevolucionBean.getDetalleDevoluciones().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_LEFT,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleDevolucion.setHeaderRows(1);
            		//detalleDevolucion.setFooterRows(1);
            	}
            	
            	//agrupamos por codigo_producto, la ultima fila la agregamos siempre manual.
            	if (i<iDevolucionBean.getDetalleDevoluciones().size()-1)
            		//si el producto siguiente es igual sumamos la cantidad, cuando sea diferente lo pintamos con la cantidad acumulada.
	            	if (iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getCodigo_interno()!=iDevolucionBean.getDetalleDevoluciones().get(i+1).getIproductoBean().getReferenciaBean().getCodigo_interno()){
	                  	//detalle de remision
	                  		cantidadAcumulada += iDevolucionBean.getDetalleDevoluciones().get(i).getPendiente();
		            	//detalle de remision
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getCodigo_interno()), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell((iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Double.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
		        		cantidadAcumulada=0;
	            	}else{
		        		cantidadAcumulada +=iDevolucionBean.getDetalleDevoluciones().get(i).getPendiente();
		        	}
            	else{// ultima fila
            		//detalle de remision
              		cantidadAcumulada += iDevolucionBean.getDetalleDevoluciones().get(i).getPendiente();
	            	//detalle de remision
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getCodigo_interno()), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell((iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleDevolucion.addCell(UtilidadesPDF.createCell(Double.toString(iDevolucionBean.getDetalleDevoluciones().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
	        		cantidadAcumulada=0;
            	}
            }
	        
	        UtilidadesPDF.crearCaja(outertable,detalleDevolucion);
	        	        
         // add the table	       
	        document.add(outertable);
	       
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Devolucion.pdf"; 
    	IDevolucionBean iDevolucionBean = new DevolucionBean();
    	iDevolucionBean.setFecha(new Date());
    	iDevolucionBean.setIdDevolucion(120);
    	iDevolucionBean.setObservaciones("Observaciones");
    	iDevolucionBean.setTransporte(20000);
        
    	IProductoBean producto ;
        IDetalleDevolucionBean detalle;
        for (int i =0; i<100; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setDescripcion("descripcion" + i);
         producto.setCodigo_producto(7);
        // producto.setPeso(10);
         producto.setDescripcion("descripcion" + i);
         detalle = (IDetalleDevolucionBean) new DetalleDevolucionBean();
         detalle.setIproductoBean(producto);
         iDevolucionBean.setDetalleDevoluciones(detalle);	
        }
        
        new SaldoPDF().crearPDF(DEST,iDevolucionBean);
       
        	
	}

}
