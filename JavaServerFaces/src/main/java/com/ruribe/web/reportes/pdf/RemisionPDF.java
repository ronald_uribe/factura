package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleRemisionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.RemisionBean;
import com.ruribe.util.bean.interfaces.IDetalleRemisionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IRemisionBean;

public class RemisionPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Remision 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,IRemisionBean iRemisionBean) throws IOException, DocumentException{
		// String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_REMISION")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 Cabecera encabezado = new Cabecera();
		 encabezado.setEncabezado("REMISIÓN");// tipo de reporte
		 encabezado.setTipoGestion("ALQUILER DE EQUIPOS");
		 encabezado.setFecha(iRemisionBean.getFecha_entrega());
		 encabezado.setId(iRemisionBean.getIdRemision());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iRemisionBean);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iRemisionBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,IRemisionBean iRemisionBean) throws IOException, DocumentException {
	      
	       
	        // outer table
	        PdfPTable outertable = new PdfPTable(1);
            
            //cuerpo factura datos del cliente
	        PdfPTable  datosCliente= new PdfPTable(4); 
            datosCliente.setWidths(new int[]{2,4,1,2});
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iRemisionBean.getIclienteBean().getRazon_social(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nit.:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iRemisionBean.getIclienteBean().getId_cliente(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dir. Ofic:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iRemisionBean.getIclienteBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Dirección Obra:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iRemisionBean.getIobraBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Nombre contacto:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(iRemisionBean.getIobraBean().getContacto(), 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell("Hora de Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
            datosCliente.addCell(UtilidadesPDF.createCell(new SimpleDateFormat("HH:mm").format(iRemisionBean.getFecha()), 0, 1, 1,Element.ALIGN_LEFT));
            
            UtilidadesPDF.crearCaja(outertable,datosCliente);
	        
	        //detalle de facura
	        String[] cabecera = {"Cód.","Descripción de equipo.","Cant.","Peso"};     
	        PdfPTable detalleRemision = new PdfPTable(cabecera.length);
	        detalleRemision.setWidths(new int[]{2,4,1,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleRemision.getDefaultCell().setUseAscender(true);
	        detalleRemision.getDefaultCell().setUseDescender(true);
	        for (int i = 0; i < iRemisionBean.getDetalleRemisions().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_LEFT,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleRemision.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleRemision.setHeaderRows(1);
            		//detalleRemision.setFooterRows(1);
            	}
            	//detalle de remision
        		detalleRemision.addCell(UtilidadesPDF.createCell(Integer.toString(iRemisionBean.getDetalleRemisions().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
        		detalleRemision.addCell(UtilidadesPDF.createCell((iRemisionBean.getDetalleRemisions().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
        		detalleRemision.addCell(UtilidadesPDF.createCell(Integer.toString(iRemisionBean.getDetalleRemisions().get(i).getCantidad()), 1, 1, 1,Element.ALIGN_CENTER));
        		detalleRemision.addCell(UtilidadesPDF.createCell(Double.toString(iRemisionBean.getDetalleRemisions().get(i).getIproductoBean().getReferenciaBean().getPeso()), 1, 1, 1,Element.ALIGN_CENTER));
            	
            }
	        
	        // detalleRemision.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleRemision);
	        
	        
	      //Observaciones
	        String[] titulo = {"Observaciones:"};     
	        PdfPTable observaciones = new PdfPTable(1);           
	        observaciones.addCell(UtilidadesPDF.createCell(titulo[0], 0, 1, 1,Element.ALIGN_TOP));
	        String obser=iRemisionBean.getObservaciones();
	        observaciones.addCell(UtilidadesPDF.createCell(obser, 0, 1, 0,Element.ALIGN_LEFT));
        
	        UtilidadesPDF.crearCaja(outertable,observaciones);
	        
	      //tranportador    
	        PdfPTable tranportador = new PdfPTable(6); 
	        tranportador.setWidths(new int[]{4,4,4,3,2,2});
	        tranportador.addCell(UtilidadesPDF.createCell("Transportador:", 0, 1, 1,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell(iRemisionBean.getItransportadorBean().getNombre(), 0, 1, 0,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell("Transporte:", 0, 1, 1,Element.ALIGN_RIGHT));
	        tranportador.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(iRemisionBean.getTransporte()), 0, 1, 0,Element.ALIGN_LEFT));
	        tranportador.addCell(UtilidadesPDF.createCell("Placa:", 0, 1, 1,Element.ALIGN_RIGHT));
	        tranportador.addCell(UtilidadesPDF.createCell(iRemisionBean.getItransportadorBean().getPlaca(), 0, 1, 1,Element.ALIGN_LEFT));
	        UtilidadesPDF.crearCaja(outertable,tranportador);
	        
	      //quien entrega    
	        PdfPTable entrega = new PdfPTable(4);    
	        entrega.setWidths(new int[]{2,7,2,7});
	        entrega.addCell(UtilidadesPDF.createCell("Quien Entrega:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("Quien Recibe:", 0, 1, 1,Element.ALIGN_LEFT));
	        entrega.addCell(UtilidadesPDF.createCell("", 0, 1, 1,Element.ALIGN_LEFT));
	        UtilidadesPDF.crearCaja(outertable,entrega);
  
	        // add the table	       
	        document.add(outertable);
	       
	        
//	        PdfPTable table = new PdfPTable(3);
//	        cell = new PdfPCell();
//	        cell = getCell("These cells have rounded borders at the top.");
//	        table.addCell(cell);
//	        cell = getCell("These cells aren't rounded at the bottom.");
//	        table.addCell(cell);
//	        cell = getCell("A custom cell event was used to achieve this.");
//	        table.addCell(cell);
//	        document.add(table);
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Remision.pdf"; 
    	IRemisionBean iRemisionBean = new RemisionBean();
    	iRemisionBean.setFecha(new Date());
    	iRemisionBean.setIdRemision(120);
    	iRemisionBean.setObservaciones("Observaciones");
    	iRemisionBean.setTransporte(1137000);
        
    	IProductoBean producto ;
        IDetalleRemisionBean detalle;
        for (int i =0; i<100; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setDescripcion("descripcion" + i);
         producto.setCodigo_producto(7);
       //  producto.setPeso(10);
         producto.setDescripcion("descripcion" + i);
         detalle = (IDetalleRemisionBean) new DetalleRemisionBean();
         detalle.setIproductoBean(producto);
         iRemisionBean.setDetalleRemisions(detalle);	
        }
        
        new RemisionPDF().crearPDF(DEST,iRemisionBean);
       
        	
	}

}
