package com.ruribe.web.reportes.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ruribe.util.Propiedades;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleReposicionBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ReposicionBean;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IReposicionBean;

public class ReposicionPDF {
	static String DEST;
	
	 /**
	  * Crea un documento de tipo Reposicion 
	  * @param Nombre de documento
	  * @throws IOException
	  * @throws DocumentException
	  */
	 public void crearPDF(String nombre,IReposicionBean iReposicionBean) throws IOException, DocumentException{
//		 String path = new File(".").getCanonicalPath();
		 File path = new File(Propiedades.leerPropiedad("SHARED_REPOSICION")).getAbsoluteFile();
	     DEST = path + "/"+nombre;   
	     File file = new File(DEST);
	     file.getParentFile().mkdirs();
		
	     // Margenes del documento
	     Document document = new Document(PageSize.A4, 10, 10, 160, 80);
		 //Clase que maneja los eventos de pagina necesarios para agregar un encabezado
		 CabeceraFacturaPDF encabezado = new CabeceraFacturaPDF();
		 encabezado.setEncabezado("REPOSICION");// tipo de reporte
		 encabezado.setTipoFactura("FACTURA DE REPOSICIÓN");
		 encabezado.setFecha(iReposicionBean.getFecha());
		 encabezado.setId(iReposicionBean.getIdReposicion());
		 // con el setPageEvent(encabezado) se crea el header y footer del PDF
		 PdfWriter.getInstance(document, new FileOutputStream(DEST)).setPageEvent(encabezado);
	     
	     document.open();// nuevo documento
	     generarCuerpoPdf(document,iReposicionBean);
         document.close();
//         Desktop d = Desktop.getDesktop();  
//         d.open(new File(DEST));
	 }
	 
	 
	/**
	 * Generar PDF
	 * @param document
	 * @param iReposicionBean
	 * @throws IOException
	 * @throws DocumentException
	 */
	 public void generarCuerpoPdf(Document document,IReposicionBean iReposicionBean) throws IOException, DocumentException {
	      
	       
		// outer table
	     PdfPTable outertable = new PdfPTable(1);
	     outertable.addCell(UtilidadesPDF.createCell("Fecha de Reposicion:"+ new SimpleDateFormat("dd/MM/yyyy").format(iReposicionBean.getFecha()), 0, 1,1, Element.ALIGN_LEFT,4));

         //cuerpo factura datos del cliente
	     PdfPTable  datosCliente= new PdfPTable(2); 
         datosCliente.setWidths(new int[]{1,9});
         datosCliente.addCell(UtilidadesPDF.createCell(iReposicionBean.getIclienteBean().getRazon_social(), 0, 2, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell("NIT.:", 0, 1, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell(iReposicionBean.getIclienteBean().getId_cliente(), 0, 1, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell("OBRA:", 0, 1, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell(iReposicionBean.getIobraBean().getNombre(), 0, 3, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell("OFIC.:", 0, 1, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell(iReposicionBean.getIobraBean().getDireccion(), 0, 3, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell("TEL:", 0, 1, 1,Element.ALIGN_LEFT));
         datosCliente.addCell(UtilidadesPDF.createCell(iReposicionBean.getIobraBean().getTelefono(), 0, 3, 1,Element.ALIGN_LEFT));
         
         UtilidadesPDF.crearCaja(outertable,datosCliente);
         
         outertable.addCell(UtilidadesPDF.createCell("DEBE EL VALOR DEL SIGUIENTE EQUIPO EN REPOSICION", 0, 1,1, Element.ALIGN_LEFT,4));
      
	        //detalle de facura
	        String[] cabecera = {"Cod.","Descripcion de equipo.","Cant.","Vr. Unitario","Vr. total"};     
	        PdfPTable detalleReposicion = new PdfPTable(cabecera.length);
	        detalleReposicion.setWidths(new int[]{2,4,1,2,2});
	        
	        // define el numero de columnas cabeceras.
	        detalleReposicion.getDefaultCell().setUseAscender(true);
	        detalleReposicion.getDefaultCell().setUseDescender(true);
	        int cantidadAcumulada=0;
	        int valorTotal=0; // se encarga de obtener la cantidad x el vr unitario de cada fila.
	        for (int i = 0; i < iReposicionBean.getDetalleReposiciones().size(); i++)
            {
            	if (i==0){//cabecera
            		detalleReposicion.addCell(UtilidadesPDF.pintarCabecera(cabecera[0], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleReposicion.addCell(UtilidadesPDF.pintarCabecera(cabecera[1], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleReposicion.addCell(UtilidadesPDF.pintarCabecera(cabecera[2], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleReposicion.addCell(UtilidadesPDF.pintarCabecera(cabecera[3], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleReposicion.addCell(UtilidadesPDF.pintarCabecera(cabecera[4], 0, 1, Element.ALIGN_CENTER,Rectangle.RIGHT));
            		detalleReposicion.setHeaderRows(1);
            		//detalleReposicion.setFooterRows(1);
            	}
            	
            	//agrupamos por codigo_producto, la ultima fila la agregamos siempre manual.
            	if (i<iReposicionBean.getDetalleReposiciones().size()-1)
            		//si el producto siguiente es igual sumamos la cantidad, cuando sea diferente lo pintamos con la cantidad acumulada.
	            	if (iReposicionBean.getDetalleReposiciones().get(i).getIproductoBean().getCodigo_producto()!=iReposicionBean.getDetalleReposiciones().get(i+1).getIproductoBean().getCodigo_producto()){
	                  	//detalle de remision
	                  		cantidadAcumulada += iReposicionBean.getDetalleReposiciones().get(i).getCantidad();
		            	//detalle de remision
		        		detalleReposicion.addCell(UtilidadesPDF.createCell(Integer.toString(iReposicionBean.getDetalleReposiciones().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleReposicion.addCell(UtilidadesPDF.createCell((iReposicionBean.getDetalleReposiciones().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
		        		detalleReposicion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
		        		detalleReposicion.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles((int) iReposicionBean.getDetalleReposiciones().get(i).getPrecio_reposicion()), 1, 1, 1,Element.ALIGN_CENTER));
		        		valorTotal=iReposicionBean.getDetalleReposiciones().get(i).getCantidad()*iReposicionBean.getDetalleReposiciones().get(i).getPrecio_reposicion();
		        		detalleReposicion.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles((int)valorTotal ), 1, 1, 1,Element.ALIGN_CENTER));
		        		cantidadAcumulada=0;
	            	}else{
		        		cantidadAcumulada +=iReposicionBean.getDetalleReposiciones().get(i).getCantidad();
		        	}
            	else{// ultima fila
            		//detalle de remision
              		cantidadAcumulada += iReposicionBean.getDetalleReposiciones().get(i).getCantidad();
	            	//detalle de remision
	        		detalleReposicion.addCell(UtilidadesPDF.createCell(Integer.toString(iReposicionBean.getDetalleReposiciones().get(i).getIproductoBean().getCodigo_producto()), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleReposicion.addCell(UtilidadesPDF.createCell((iReposicionBean.getDetalleReposiciones().get(i).getIproductoBean().getDescripcion()), 1, 1, 1,Element.ALIGN_LEFT));
	        		detalleReposicion.addCell(UtilidadesPDF.createCell(Integer.toString(cantidadAcumulada), 1, 1, 1,Element.ALIGN_CENTER));
	        		detalleReposicion.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles((int) iReposicionBean.getDetalleReposiciones().get(i).getPrecio_reposicion()), 1, 1, 1,Element.ALIGN_CENTER));
	        		valorTotal=iReposicionBean.getDetalleReposiciones().get(i).getCantidad()*iReposicionBean.getDetalleReposiciones().get(i).getPrecio_reposicion();
	        		detalleReposicion.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles((int)valorTotal ), 1, 1, 1,Element.ALIGN_CENTER));
	        		cantidadAcumulada=0;
            	}
            }
	         
	        // detalleReposicion.setHeadersInEvent(false);
	        UtilidadesPDF.crearCaja(outertable,detalleReposicion);
	        
	      //eliminamos el borde de la tabla
	        PdfPCell cell= new PdfPCell(); // celda utilizada para observaciones y firma y sello
// 	        cell.setBorder(Rectangle.NO_BORDER);
//	        //Observaciones y valores
	        PdfPTable tableObservaciones = new PdfPTable(4); 
	        tableObservaciones.setWidths(new int[]{1,4,1,1});
	        //tableObservaciones.addCell(UtilidadesPDF.createCell("Observaciones", 0, 1, 1,Element.ALIGN_LEFT));
	        
	        
	      
	        //solo mostramos la linea cuando tiene descuentos
	        if (iReposicionBean.getDescuento()!=0){
	        	tableObservaciones.addCell(UtilidadesPDF.createCell(iReposicionBean.getObservaciones(), 0, 2, 0,Element.ALIGN_CENTER));
	        	tableObservaciones.addCell(UtilidadesPDF.createCell("PARCIAL", 		0, 1, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(58000), 	0, 1, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("DESCUENTO", 	0, 3, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(77000), 	0, 1, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("SUB-TOTAL", 	0, 3, 1,Element.ALIGN_RIGHT));
		        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles(25000), 	0, 1, 1,Element.ALIGN_RIGHT));
	        }else{
	        	tableObservaciones.addCell(UtilidadesPDF.createCell(iReposicionBean.getObservaciones(), 0, 4, 0,Element.ALIGN_CENTER));
	        }
	         
	        tableObservaciones.addCell(UtilidadesPDF.createCell("TOTAL", 		0, 3, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.addCell(UtilidadesPDF.createCell("$"+Utilidades.separadorDeMiles((int) iReposicionBean.getTotal()), 	0, 1, 1,Element.ALIGN_RIGHT));
	        tableObservaciones.setWidthPercentage(100);
	        // metemos la tabla en la celda para quitar los bordes
	        cell.addElement(tableObservaciones);
	        cell.setBorder(0);
	        cell.setPaddingRight(3);// 
	        outertable.addCell(cell);
	       
	        //ANADIMOS LAS CELDAS AL DOCUMENTO
	        document.add(outertable);
	    }
	 
	
	public static void main(String[] args) throws IOException, DocumentException {
		
    	DEST = "Reposicion.pdf"; 
    	IReposicionBean iReposicionBean = new ReposicionBean();
    	iReposicionBean.setFecha(new Date());
    	iReposicionBean.setIdReposicion(120);
    	iReposicionBean.setObservaciones("Observaciones");
    	iReposicionBean.setTransporte(20000);
        
    	IProductoBean producto ;
        IDetalleReposicionBean detalle;
        for (int i =0; i<100; i++){
         producto = (IProductoBean) new ProductoBean();
         producto.setDescripcion("descripcion" + i);
         producto.setCodigo_producto(7);
       //  producto.setPeso(10);
         producto.setDescripcion("descripcion" + i);
         detalle = (IDetalleReposicionBean) new DetalleReposicionBean();
         detalle.setIproductoBean(producto);
         iReposicionBean.setDetalleReposiciones(detalle);	
        }
        
        new ReposicionPDF().crearPDF(DEST,iReposicionBean);
       
        	
	}

}
