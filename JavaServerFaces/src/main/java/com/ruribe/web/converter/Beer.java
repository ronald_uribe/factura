package com.ruribe.web.converter;

import java.io.Serializable;

public class Beer implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
    private String brand;

    public Beer(Integer id, String brand) {
        this.id = id;
        this.brand = brand;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}