package com.ruribe.web.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@RequestScoped
public class BeersBean implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Beer selectedBeer;
    private List<Beer> beers;

    public BeersBean(){
        beers = new ArrayList<Beer>();
        beers.add(new Beer(10, "La Chouffe"));
        beers.add(new Beer(20, "Stella Artois"));
        beers.add(new Beer(30, "Westmalle Trippel"));
    }

    public Beer getSelectedBeer() {
        return selectedBeer;
    }

    public void setSelectedBeer(Beer selectedBeer) {
        this.selectedBeer = selectedBeer;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }

    public Beer getBeer(Integer id) {
        if (id == null){
            throw new IllegalArgumentException("no id provided");
        }
        for (Beer beer : beers){
            if (id.equals(beer.getId())){
                return beer;
            }
        }
        return null;
    }
}