package com.ruribe.web.converter;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.web.controlador.ControladorProducto;

@FacesConverter(value = "productoBeanConverter")
public class ProductoBeanConverter implements Converter {

	 // returns a IProductoBean object
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String codigoID) {
    	ValueExpression vex =
    			facesContext.getApplication().getExpressionFactory()
                        .createValueExpression(facesContext.getELContext(),
                                "#{mbeanProducto}", ControladorProducto.class);
    	
    	ControladorProducto beers = (ControladorProducto)vex.getValue(facesContext.getELContext());
          return beers.getProducto(Integer.valueOf(codigoID));
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o instanceof IProductoBean) {
        	ProductoBean c = (ProductoBean) o;
        	
            return String.valueOf(c.getCodigo_producto()) ;
        }
        return "";
    }

}