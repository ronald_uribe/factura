package com.ruribe.web.controlador;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class App 
{
 
	public static void main(String[] args)
    {
        writePDF();
    }
    
    private static void writePDF() {
 
        Document document = new Document();
 
        try {
        	System.out.println("prueba 2222222222");
        	String path = new File(".").getCanonicalPath();
        	String FILE_NAME = path + "/itext-test-file.pdf";
        	
            PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));
 
            document.open();
 
            Paragraph paragraphHello = new Paragraph();
            paragraphHello.add("Hello iText paragraph!");
            paragraphHello.setAlignment(Element.ALIGN_JUSTIFIED);
 
            document.add(paragraphHello);
 
            Paragraph paragraphLorem = new Paragraph();
            paragraphLorem.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            		+ "Maecenas finibus fringilla turpis, vitae fringilla justo."
            		+ "Sed imperdiet purus quis tellus molestie, et finibus risus placerat."
            		+ "Donec convallis eget felis vitae interdum. Praesent varius risus et dictum hendrerit."
            		+ "Aenean eu semper nunc. Aenean posuere viverra orci in hendrerit. Aenean dui purus, eleifend nec tellus vitae,"
            		+ " pretium dignissim ex. Aliquam erat volutpat. ");
            
            java.util.List<Element> paragraphList = new ArrayList<Element>();
            
            paragraphList = paragraphLorem.breakUp();
            System.out.println("prueba 2222222222222222");
            Font f = new Font();
            f.setFamily(FontFamily.COURIER.name());
            f.setStyle(Font.BOLDITALIC);
            f.setSize(8);
            System.out.println("prueba 3333333333333333333");
            Paragraph p3 = new Paragraph();
            p3.setFont(f);
            p3.addAll(paragraphList);
            p3.add("TEST LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISCING ELIT!");
 
            document.add(paragraphLorem);
            document.add(p3);
            document.close();
            System.out.println("prueba 44444444444444444");
        } catch (FileNotFoundException  e) {
            e.printStackTrace();
            System.out.println("prueba 11111111111" + e.getMessage());
        }catch (DocumentException  e) {
            e.printStackTrace();
            System.out.println("prueba 11111111111" + e.getMessage());
        } 
        catch (IOException e) {
			e.printStackTrace();
			System.out.println("prueba 11111111111" + e.getMessage());
		}
 
    }
}
