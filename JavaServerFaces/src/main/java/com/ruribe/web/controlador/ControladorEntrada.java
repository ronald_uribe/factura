package com.ruribe.web.controlador;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.itextpdf.text.DocumentException;
import com.ruribe.servicio.interfaces.EntradaService;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.DetalleEntradaBean;
import com.ruribe.util.bean.EntradaBean;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ProveedorBean;
import com.ruribe.util.bean.interfaces.IDetalleEntradaBean;
import com.ruribe.util.bean.interfaces.IEntradaBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.web.reportes.pdf.EntradaPDF;

@Named(value = "mbeanEntrada")
@Scope("request")
public class ControladorEntrada extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Inject
	ProductoService productoService;
	@Inject
	EntradaService entradaService;
	@Inject
	ProveedorService proveedorService;
	private IEntradaBean iEntradaBean;
	private List<IEntradaBean> listaEntradas;
	private List<IProveedorBean> listaProveedor;
	/** setea el nuevo valor cuando se ejecuta el evento ValueChangeEvent */
	private HtmlSelectOneMenu tipoProductoSeleccionado;
	private HtmlSelectOneMenu productoSeleccionado;
	private boolean mostrarDetalle;
	private boolean onlyread = false;// comprueba si la entrada esta creada.

	public ControladorEntrada() {
		iEntradaBean = new EntradaBean();
	}

	/**
	 * Inicializa el formulario de entrada.
	 * 
	 * @return redirige a la pagina de entrada.
	 */
	public String iniciarEntrada() {
		try {
			iEntradaBean = new EntradaBean();
			setProveedorSelect("Seleccionar");
			iEntradaBean.setCodTipoProductoSelect("Seleccionar");
			// cargar proveedors
			consultarProveedores();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ENTRADA";
	}

	/**
	 * Consulta usuario todos los usuarios
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarProveedores() throws SAExcepcion {
		listaProveedor = proveedorService.obtenerProveedores();
	}

	/**
	 * Selecciona un proveedor.
	 * 
	 * @param evento
	 */
//	private HtmlSelectOneMenu combotipoProducto;
	public void seleccionarProveedor(ValueChangeEvent evento) {
		try {
			FacesContext cx = FacesContext.getCurrentInstance();
			iEntradaBean.setIproveedorBean(new ProveedorBean());
			iEntradaBean.setProducto(new ProductoBean());
			if (iEntradaBean.getListaProducto() != null && !iEntradaBean.getListaProducto().isEmpty()) {
				iEntradaBean.getListaProducto().clear();
			}
			if (listaTipoProducto != null && !listaTipoProducto.isEmpty()) {
				listaTipoProducto.clear();
			}
			iEntradaBean.getDetalleEntradas().clear();
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idproveedorSelect = evento.getNewValue().toString();
				// se selecciona el proveedor de la lista de proveedors.
				iEntradaBean.setIproveedorBean(getProveedor(idproveedorSelect));
				listaTipoProducto = productoService.obtenerTipoProducto();
				iEntradaBean.setCodTipoProductoSelect("Seleccionar");
				iEntradaBean.setSelectProducto("Seleccionar");
			}
			tipoProductoSeleccionado.setSubmittedValue("Seleccionar");
			productoSeleccionado.setSubmittedValue("Seleccionar");
			cx.renderResponse();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Selecciona un proveedor.
	 * 
	 * @param evento
	 */
	public void seleccionarTipoProducto(ValueChangeEvent evento) {
		try {
			if (iEntradaBean.getListaProducto() != null && !iEntradaBean.getListaProducto().isEmpty()) {
				iEntradaBean.getListaProducto().clear();
			}
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				int idTipoProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
				iEntradaBean.setListaProducto(productoService.obtenerProductosPorTipoProveedor(idTipoProductoSelect,
						iEntradaBean.getIproveedorBean().getCodigo_proveedor()));
			}
			FacesContext.getCurrentInstance().renderResponse();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Selecciona un proveedor.
	 * 
	 * @param evento
	 */
	public void seleccionarProducto(ValueChangeEvent evento) {
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			int idProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
			// obtenemos el objeto producto seleccionado
			iEntradaBean.setProducto(iEntradaBean.getProducto(idProductoSelect));
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Agrega un producto a la remison en curso
	 */
	public void agregarAcarrito() {
		if (validarCarrito()) {
			// Sirve para saber si tenemos agregado el producto al carrito de compras
			boolean existe = false;
			// si el producto ya esta en lista se le suma la cantidad.
			for (IDetalleEntradaBean entrada : iEntradaBean.getDetalleEntradas()) {
				if (entrada.getIproductoBean()
						.getCodigo_producto() == (iEntradaBean.getProducto().getCodigo_producto())) {
					int nuevaCantidad = entrada.getCantidad() + iEntradaBean.getCantidad();
					entrada.setCantidad(nuevaCantidad);
					existe = true;
					break;
				}
			}
			// si el producto no esta en lista y la cantidad > 0 lo agregamos.
			if (!existe) {
				if (iEntradaBean.getCantidad() > 0) {
					IDetalleEntradaBean detalle = new DetalleEntradaBean();
					detalle.setCantidad(iEntradaBean.getCantidad());
					detalle.setIproductoBean(iEntradaBean.getProducto());
					iEntradaBean.setDetalleEntradas(detalle);
				}
			}

			// reiniciamos los valores del formulario
			iEntradaBean.setCantidad(0);
			FacesContext.getCurrentInstance().renderResponse();
		}
	}

	/**
	 * Validar entrada de carrito.
	 * 
	 * @return
	 */
	private boolean validarCarrito() {
		System.out.println("TIPO:" + iEntradaBean.getCodTipoProductoSelect());
		System.out.println("PROD:" + iEntradaBean.getSelectProducto());
		if (iEntradaBean.getCodProveedorSelect() == null
				|| iEntradaBean.getCodProveedorSelect().equals("Seleccionar")) {
			SessionUtils.addError("seleccionar proveedor");
			return false;
		}
		if (iEntradaBean.getCodTipoProductoSelect() == null
				|| iEntradaBean.getCodTipoProductoSelect().equals("Seleccionar")) {
			SessionUtils.addError("seleccionar Tipo Producto");
			return false;
		}
		if (iEntradaBean.getSelectProducto() == null || iEntradaBean.getSelectProducto().equals("Seleccionar")) {
			SessionUtils.addError("seleccionar Producto");
			return false;
		}
		if (iEntradaBean.getProducto() == null) {
			SessionUtils.addError("seleccionar un Producto");
			return false;
		}
		if (iEntradaBean.getCantidad() == 0) {
			SessionUtils.addError("La cantidad debe ser mayor a 0");
			return false;
		}
		return true;
	}

	/**
	 * Validar entrada de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		if (iEntradaBean.getFecha() == null) {
			SessionUtils.addError("Seleccionar una fecha valida");
			return false;
		}
		if (iEntradaBean.getIproveedorBean().getCodigo_proveedor() == null) {
			SessionUtils.addError("Seleccionar un proveedor");
			return false;
		}
		if (iEntradaBean.getDetalleEntradas() == null || iEntradaBean.getDetalleEntradas().isEmpty()) {
			SessionUtils.addError("La entrada no contiene items.");
			return false;
		}
		return true;
	}

	/**
	 * Agrega un producto a la remison en curso
	 */
	public void guardarEntrada() {
		try {
			if (this.validar()) {
				// la la factura es 0 indica que es nueva, una vez guardada se aginga un nuevo
				// valor.
				if (iEntradaBean.getIdEntrada() == 0) {
					iEntradaBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
					iEntradaBean.setEstado(true);
					iEntradaBean = entradaService.crearEntrada(iEntradaBean);
					SessionUtils.addInfo("Entrada guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva entrada presione nueva entrada.");
				}
			}
		} catch (Exception e) {
			SessionUtils.addError("Entrada no guardada correctamente" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * reinicia el formulario
	 */
	public void nuevaEntrada() {
		iEntradaBean = new EntradaBean();
		setProveedorSelect("Seleccionar");
		setSelectTransportador("Seleccionar");
		setCodTipoProductoSelect("Seleccionar");
	}

	/**
	 * @return the iEntradaBean
	 */
	public IEntradaBean getiEntradaBean() {
		return iEntradaBean;
	}

	/**
	 * @param iEntradaBean the iEntradaBean to set
	 */
	public void setiEntradaBean(IEntradaBean iEntradaBean) {
		this.iEntradaBean = iEntradaBean;
	}

	/**
	 * habilita el detalle de carrito de entrada y bloquea los campos
	 * 
	 * @return
	 */
	public final boolean isMostrarDetalle() {
		if (iEntradaBean.getDetalleEntradas() != null && iEntradaBean.getDetalleEntradas().size() > 0) {
			mostrarDetalle = true;
		} else {
			mostrarDetalle = false;
		}

		return mostrarDetalle;
	}

	public String verEntrada(IEntradaBean iEntradaBean) {
		try {
			this.iEntradaBean = iEntradaBean;
			// cargar proveedors
			consultarProveedores();
			this.iEntradaBean.setCodProveedorSelect(iEntradaBean.getIproveedorBean().getCodigo_proveedor());
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "MODIFICAR_ENTRADA";
	}

	/**
	 * Modificar entrada
	 */
	public void modificarEntrada() {
		try {
			if (this.validar()) {
//					iEntradaBean.getItransportadorBean().setNo_documento(getSelectTransportador());
				iEntradaBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
				entradaService.editarEntrada(iEntradaBean);
				SessionUtils.addError(FacesMessage.SEVERITY_INFO, "Entrada guardada correctamente");
			}
		} catch (Exception e) {
			SessionUtils.addError("Entrada no guardada correctamente" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Imprimir Entrada
	 */
	public void imprimir(EntradaBean iEntradaBean) {

		try {
			if (iEntradaBean.getIdEntrada() != 0) {
				String ARCHIVO = "Entrada" + iEntradaBean.getIdEntrada() + ".pdf";
				if (!new File(Propiedades.leerPropiedad("SHARED_ENTRADA") + ARCHIVO).exists()) {
						new EntradaPDF().crearPDF(ARCHIVO, entradaService.consultarIDEntrada(iEntradaBean));
				}
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_ENTRADA"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la Entrada.");
			}
		} catch (IOException e) {
			SessionUtils.addError("Error al leer la Entrada.");
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * comprueba si la factura nueva o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (iEntradaBean.getIdEntrada() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}

	/**
	 * @param mostrarDetalle the mostrarDetalle to set
	 */
	public final void setMostrarDetalle(boolean mostrarDetalle) {
		this.mostrarDetalle = mostrarDetalle;
	}

	/**
	 * @return the listaEntradas
	 */
	public List<IEntradaBean> getListaEntradas() {
		return listaEntradas;
	}

	/**
	 * @param listaEntradas the listaEntradas to set
	 */
	public void setListaEntradas(List<IEntradaBean> listaEntradas) {
		this.listaEntradas = listaEntradas;
	}

	/**
	 * @return the listaProveedor
	 */
	public List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	/**
	 * retorna un objeto de una lista de tipo IProveedorBean
	 * 
	 * @param String codigo_proveedor
	 * @return IProveedorBean
	 */
	public IProveedorBean getProveedor(String id) {

		for (IProveedorBean proveedor : listaProveedor) {
			if (id.equals(proveedor.getCodigo_proveedor())) {
				return proveedor;
			}
		}
		return null;
	}

	/**
	 * @return the tipoProductoSeleccionado
	 */
	public HtmlSelectOneMenu getTipoProductoSeleccionado() {
		return tipoProductoSeleccionado;
	}

	/**
	 * @param tipoProductoSeleccionado the tipoProductoSeleccionado to set
	 */
	public void setTipoProductoSeleccionado(HtmlSelectOneMenu tipoProductoSeleccionado) {
		this.tipoProductoSeleccionado = tipoProductoSeleccionado;
	}

	/**
	 * @return the productoSeleccionado
	 */
	public HtmlSelectOneMenu getProductoSeleccionado() {
		return productoSeleccionado;
	}

	/**
	 * @param productoSeleccionado the productoSeleccionado to set
	 */
	public void setProductoSeleccionado(HtmlSelectOneMenu productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

}