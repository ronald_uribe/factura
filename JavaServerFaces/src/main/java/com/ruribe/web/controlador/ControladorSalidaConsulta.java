package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.servicio.interfaces.SalidaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.SalidaConsultaBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.bean.interfaces.ISalidaConsultaBean;

/**
 * @author ruribe
 */

@Named(value = "mbeanSalidaConsulta")
@Scope("request")
public class ControladorSalidaConsulta extends ControladorBase implements Serializable {

	@Inject
	ProveedorService proveedorService;
	@Inject
	SalidaService salidaService;

	private List<IProveedorBean> listaProveedor;
	private ISalidaConsultaBean consultaBean;

	/**
	 * constructor.
	 */
	public ControladorSalidaConsulta() {
		consultaBean = new SalidaConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Inicializa el formulario de reposicion con las 10 ultimas remisiones.
	 * 
	 * @return redirige a la pagina de remision.
	 */
	public String iniciarConsultarSalida() {
		try {
			// cargar clientes
			consultarProveedores();
			// cargamos las 10 ultimas remisiones
			consultaBean.setListaSalidas(salidaService.consultarSalidas(consultaBean));
			if (consultaBean.getListaSalidas().isEmpty()) {
				SessionUtils.addError("No hay nignuna Salida creada");
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "CONSULTAR_SALIDA";
	}

	/**
	 * obtiene las remisiones segun filtros de busqueda
	 */
	public String buscarSalidas() {
		try {
			// cargar clientes
			consultarProveedores();
			consultaBean.setIdProveedor(proveedorSelect);
			consultaBean.setListaSalidas(salidaService.consultarSalidas(consultaBean));
			if (consultaBean.getListaSalidas().isEmpty()) {
				SessionUtils.addError(FacesMessage.SEVERITY_INFO, "No hay datos para esta consulta");
			}
		} catch (Exception e) {
			SessionUtils.addError("No hay datos para esta consulta");
		}
		return "CONSULTAR_SALIDA";
	}

	/**
	 * Consulta usuario todos los usuarios
	 * @throws SAExcepcion 
	 */
	public final void consultarProveedores() throws SAExcepcion {
		listaProveedor = proveedorService.obtenerProveedores();
	}

	/**
	 * @return the consultaBean
	 */
	public ISalidaConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(ISalidaConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

	/**
	 * @return the listaProveedor
	 */
	public List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

}