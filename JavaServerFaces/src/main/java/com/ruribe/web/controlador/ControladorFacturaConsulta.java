package com.ruribe.web.controlador;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.FacturaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.FacturaConsultaBean;
import com.ruribe.util.bean.interfaces.IFacturaConsultaBean;
 
/**
 * @author ruribe
 */
@Named(value = "mbeanFacturaConsulta")
@Scope("request")
public class ControladorFacturaConsulta extends ControladorBase implements Serializable {
 
	private IFacturaConsultaBean consultaBean;
	
	/**
	 * constructor.
	 */
	public ControladorFacturaConsulta() {
		consultaBean= new FacturaConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	FacturaService facturaService;
	
	/**
	 * obtiene las facturaes segun filtros de busqueda
	 */
	
	/**
	 * Inicializa el formulario de factura con las 10 ultimas.
	 * @return redirige a la pagina de factura.
	 */
	public String iniciarConsultarFactura(){
		//cargar clientes
		try {
			consultarClientes();
			//cargamos las 10 ultimas facturas
			consultaBean.setListaFactura(facturaService.consultarFacturas(consultaBean));
			if (consultaBean.getListaFactura().isEmpty()){
				SessionUtils.addInfo("No hay nignuna factura creada");	
			}
		}catch(Exception e)	{
			SessionUtils.addError("No hay datos para esta consulta");	
		}	
		return "CONSULTAR_FACTURA";
	}
	
	/**
	 * 
	 * @return
	 */
	public String buscarFacturas(){
		try {
		//cargar clientes
		consultarClientes();
		consultaBean.setIdCliente(clienteSelect);
		consultaBean.setListaFactura(facturaService.consultarFacturas(consultaBean));
		if (consultaBean.getListaFactura().isEmpty()){
			SessionUtils.addError(FacesMessage.SEVERITY_INFO,"No hay datos para esta consulta");	
		}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "CONSULTAR_FACTURA";
	}

	/**
	 * @return the consultaBean
	 */
	public IFacturaConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(IFacturaConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

}