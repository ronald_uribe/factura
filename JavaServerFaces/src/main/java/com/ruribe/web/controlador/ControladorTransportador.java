package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.servicio.interfaces.TransportadorService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.TransportadorBean;
import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;
import com.ruribe.util.bean.interfaces.ITransportadorBean;

@Named(value = "mbeanTransportador")
@Scope("request")
public class ControladorTransportador extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;
	@Inject
	TransportadorService transportadorService;
	@Inject
	TipoDocumentoService tipoDocumentoService;
	@Inject
	CiudadService ciudadService;
	private ITransportadorBean iTransportador;
	private List<ITransportadorBean> listaTransportador;
	private List<ITipoDocumentoBean> listaTipoDocumento;
	private final Logger log = Logger.getLogger(ControladorTransportador.class.getName());

	public ControladorTransportador() {
		iTransportador = new TransportadorBean();
	}

	/**
	 * Inicializa el formulario de transportadores.
	 * 
	 * @return redirige a la pagina de transportadores.
	 */
	public String iniciarTransportador() {
		try {
			listaTransportador = transportadorService.obtenerTransportadores();
			listaTipoDocumento = tipoDocumentoService.obtenerTipoDocumentos();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "TRANSPORTADORES";
	}

	/**
	 * guardar usuario
	 */
	public final void guardarTransportador() {
		try {
			// tomamos el valor del combo y lo convertimos a entero
			if (this.validarTransportador(iTransportador)) {
				iTransportador.setCod_tipo_documento(Integer.parseInt(iTransportador.getTipoDocumentoSelect()) * -1);
				transportadorService.crearTransportador(iTransportador);
				iTransportador = new TransportadorBean();
				SessionUtils.addInfo("Transportador guardado correctamente");
				this.consultarTransportadores();
			}
		} catch (Exception e) {
			SessionUtils.addFatalError("Error al crear el Transportador");
			log.severe(e.getMessage());
		}
	}

	/**
	 * validar proveedor
	 * 
	 * @param iTransportador
	 * @return
	 */
	public boolean validarTransportador(ITransportadorBean iTransportador) {
		if (iTransportador.getNo_documento().length() < 5) {
			SessionUtils.addError("Documento es un campo obligatorio minimo 5 caracteres.");
			return false;
		}
		if (iTransportador.getNo_documento() == null || iTransportador.getNo_documento().isEmpty()) {
			SessionUtils.addError("Documento es un campo obligatorio.");
			return false;
		}
		if (iTransportador.getNombre() == null || iTransportador.getNombre().isEmpty()) {
			SessionUtils.addError("Nombre es un campo obligatorio.");
			return false;
		}
		if (iTransportador.getPlaca() == null || iTransportador.getPlaca().isEmpty()) {
			SessionUtils.addError("Placa es un campo obligatorio.");
			return false;
		}
		return true;
	}

	/**
	 * Consulta usuario todos los usuarios
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarTransportadores() throws SAExcepcion {
		listaTransportador = transportadorService.obtenerTransportadores();
	}

	/**
	 * Consulta un usuario por su identificador
	 * 
	 * @throws SAExcepcion
	 */
	public final void buscarTransportador() throws SAExcepcion {
		listaTransportador = transportadorService.obtenerTransportador(iTransportador);
	}

	/**
	 * guardar usuario
	 */
	public final void editarTransportador() {
		try {
			if (this.validarTransportador(iTransportador)) {
				iTransportador.setCod_tipo_documento(Integer.parseInt(iTransportador.getTipoDocumentoSelect()) * -1);
				transportadorService.editarTransportador(iTransportador);
				SessionUtils.addInfo("Transportador Actualizado correctamente");
				iTransportador = new TransportadorBean();
				this.consultarTransportadores();
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public String saveAction() {
		if (listaTransportador != null && !listaTransportador.isEmpty()) {
			for (ITransportadorBean iTransportador : listaTransportador) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iTransportador.isEditable()) {
					transportadorService.editarTransportador(iTransportador);
					iTransportador.setEditable(false);
				}
			}
		}
		return null;
	}

	/**
	 * guardar usuario
	 */
	public final void limpiarFormulario() {
		try {
			iTransportador = new TransportadorBean();
			this.consultarTransportadores();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iTransportador
	 */
	public void editarAction(ITransportadorBean transportador) {
		transportador.setEditable(true);
		transportador.setTipoDocumentoSelect("-" + transportador.getCod_tipo_documento());
		this.setiTransportador(transportador);
	}

	/**
	 * desactiva el modo de edicion de una fila
	 */
	public void cancelarEditarAction(ITransportadorBean transportador) {
		transportador.setEditable(false);
		this.iTransportador = new TransportadorBean();
		;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param ITransportadorBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(ITransportadorBean transportador) {

		try {
			transportadorService.borrarTransportador(transportador);
			this.consultarTransportadores();
			SessionUtils.addInfo("Transportador borrado correctamente:" + transportador.getNombre());
		} catch (Exception e) {
			SessionUtils.addFatalError("Error al Borrar el Transportador:" + transportador.getNombre());
			log.severe(e.getMessage());
		}
	}

	/**
	 * @return the listaTransportador
	 */
	public List<ITransportadorBean> getListaTransportador() {
		return listaTransportador;
	}

	/**
	 * @param listaTransportador the listaTransportador to set
	 */
	public void setListaTransportador(List<ITransportadorBean> listaTransportador) {
		this.listaTransportador = listaTransportador;

	}

	/**
	 * @return the listaTipoDocumento
	 */
	public final List<ITipoDocumentoBean> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	/**
	 * @param listaTipoDocumento the listaTipoDocumento to set
	 */
	public final void setListaTipoDocumento(List<ITipoDocumentoBean> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	/**
	 * @return the iTransportador
	 */
	public ITransportadorBean getiTransportador() {
		return iTransportador;
	}

	/**
	 * @param iTransportador the iTransportador to set
	 */
	public void setiTransportador(ITransportadorBean iTransportador) {
		this.iTransportador = iTransportador;
	}

}