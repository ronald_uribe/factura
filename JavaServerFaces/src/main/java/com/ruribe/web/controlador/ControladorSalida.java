package com.ruribe.web.controlador;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.itextpdf.text.DocumentException;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.servicio.interfaces.SalidaService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.ProveedorBean;
import com.ruribe.util.bean.SalidaBean;
import com.ruribe.util.bean.interfaces.IDetalleSalidaBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.bean.interfaces.ISalidaBean;
import com.ruribe.web.reportes.pdf.SalidaPDF;

@Named(value = "mbeanSalida")
@Scope("request")
public class ControladorSalida extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Inject
	ProductoService productoService;
	@Inject
	SalidaService salidaService;
	@Inject
	ProveedorService proveedorService;
	private ISalidaBean iSalidaBean;
	private List<ISalidaBean> listaSalidas;
	private List<IProveedorBean> listaProveedor;
	private boolean mostrarDetalle;
	private boolean onlyread = false;// comprueba si la salida esta creada.

	public ControladorSalida() {
		iSalidaBean = new SalidaBean();
	}

	/**
	 * Inicializa el formulario de salida.
	 * 
	 * @return redirige a la pagina de salida.
	 */
	public String iniciarSalida() {
		try {
			iSalidaBean = new SalidaBean();
			setProveedorSelect("Seleccionar");
			// cargar proveedors
			consultarProveedores();
			this.listaProveedor = proveedorService.obtenerProveedores();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "SALIDA";
	}

	/**
	 * Consulta usuario todos los usuarios
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarProveedores() throws SAExcepcion {
		this.listaProveedor = proveedorService.obtenerProveedores();
	}

	/**
	 * Selecciona un proveedor.
	 * 
	 * @param evento
	 */
	public void seleccionarProveedor(ValueChangeEvent evento) {
		try {
			FacesContext cx = FacesContext.getCurrentInstance();
			iSalidaBean.setIproveedorBean(new ProveedorBean());
			iSalidaBean.setProducto(new ProductoBean());
			iSalidaBean.getDetalleSalidas().clear();
			System.out.println("CAMBIANDO VALOR DE TIPO A SELECCIONAR");
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idproveedorSelect = evento.getNewValue().toString();
				// se selecciona el proveedor de la lista de proveedors.
				iSalidaBean.setIproveedorBean(getProveedor(idproveedorSelect));
				iSalidaBean.setDetalleSalidas(salidaService.obtenerProductosPendientesPorProveedor(idproveedorSelect));
			}
			cx.renderResponse();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Validar salida de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		if (iSalidaBean.getFecha() == null) {
			SessionUtils.addError("Seleccione una fecha valida");
			return false;
		}
		boolean devuelve = false; // comprueba si al menos devuelve un producto
		for (IDetalleSalidaBean iDetalleSalidaBean : iSalidaBean.getDetalleSalidas()) {
			if (iDetalleSalidaBean.getCantidad() > iDetalleSalidaBean.getPendiente()) {
				SessionUtils.addError("La cantidad a devolver no puede ser mayor que la pendiente.");
				return false;
			} else if (iDetalleSalidaBean.getCantidad() > 0) {
				devuelve = true;
			}
		}
		if (!devuelve) {
			SessionUtils.addError("No ha digitado ninguna cantidad para devolver");
			return false;
		}
		return true;
	}

	/**
	 * Agrega un producto a la remison en curso
	 */
	public void guardarSalida() {
		try {
			if (this.validar()) {
				// la la factura es 0 indica que es nueva, una vez guardada se aginga un nuevo
				// valor.
				if (iSalidaBean.getIdSalida() == 0) {
					iSalidaBean.setIproveedorBean(getProveedor(iSalidaBean.getCodProveedorSelect()));
					iSalidaBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
					iSalidaBean.setEstado(true);
					iSalidaBean = salidaService.crearSalida(iSalidaBean);
					SessionUtils.addInfo("Salida guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva salida presione nueva salida.");
				}
			}
		} catch (Exception e) {
			SessionUtils.addError("ERROR al guardar la Salida, consulte con el administrador del sistema");
			e.printStackTrace();
		}
	}

	/**
	 * reinicia el formulario
	 */
	public void nuevaSalida() {
		iSalidaBean = new SalidaBean();
		setProveedorSelect("Seleccionar");
	}

	/**
	 * @return the iSalidaBean
	 */
	public ISalidaBean getiSalidaBean() {
		return iSalidaBean;
	}

	/**
	 * @param iSalidaBean the iSalidaBean to set
	 */
	public void setiSalidaBean(ISalidaBean iSalidaBean) {
		this.iSalidaBean = iSalidaBean;
	}

	/**
	 * habilita el detalle de carrito de salida y bloquea los campos
	 * 
	 * @return
	 */
	public final boolean isMostrarDetalle() {
		if (iSalidaBean.getDetalleSalidas() != null && iSalidaBean.getDetalleSalidas().size() > 0) {
			mostrarDetalle = true;
		} else {
			mostrarDetalle = false;
		}

		return mostrarDetalle;
	}

	/**
	 * Ver salida.
	 * 
	 * @param iSalidaBean
	 * @return pagina de modificar Salida
	 */
	public String verSalida(ISalidaBean iSalidaBean) {
		try {
			this.iSalidaBean = iSalidaBean;
			consultarProveedores();
			this.iSalidaBean.setCodProveedorSelect(iSalidaBean.getIproveedorBean().getCodigo_proveedor());
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "MODIFICAR_SALIDA";
	}

	/**
	 * Modificar salida
	 */
	public void modificarSalida() {
		try {
			if (this.validar()) {
				iSalidaBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
				salidaService.editarSalida(iSalidaBean);
				SessionUtils.addInfo("Salida guardada correctamente");
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al guardar la salida." + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Imprimir Salida
	 */
	public void imprimir(SalidaBean iSalidaBean) {

		try {
			if (iSalidaBean.getIdSalida() != 0) {
				String ARCHIVO = "Entrada" + iSalidaBean.getIdSalida() + ".pdf";
				if (!new File(Propiedades.leerPropiedad("SHARED_SALIDA") + ARCHIVO).exists()) {
					salidaService.consultarIDSalida(iSalidaBean);
					new SalidaPDF().crearPDF(ARCHIVO, salidaService.consultarIDSalida(iSalidaBean));
				}
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_SALIDA"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la Salida.");
			}
		} catch (IOException e) {
			SessionUtils.addError("Error al leer la Salida.");
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * comprueba si la salida es nueva o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (iSalidaBean.getIdSalida() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}

	/**
	 * @param mostrarDetalle the mostrarDetalle to set
	 */
	public final void setMostrarDetalle(boolean mostrarDetalle) {
		this.mostrarDetalle = mostrarDetalle;
	}

	/**
	 * @return the listaSalidas
	 */
	public List<ISalidaBean> getListaSalidas() {
		return listaSalidas;
	}

	/**
	 * @param listaSalidas the listaSalidas to set
	 */
	public void setListaSalidas(List<ISalidaBean> listaSalidas) {
		this.listaSalidas = listaSalidas;
	}

	/**
	 * @return the listaProveedor
	 */
	public List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	/**
	 * retorna un objeto de una lista de tipo IProveedorBean
	 * 
	 * @param String codigo_proveedor
	 * @return IProveedorBean
	 */
	public IProveedorBean getProveedor(String id) {

		for (IProveedorBean proveedor : listaProveedor) {
			if (id.equals(proveedor.getCodigo_proveedor())) {
				return proveedor;
			}
		}
		return null;
	}

}