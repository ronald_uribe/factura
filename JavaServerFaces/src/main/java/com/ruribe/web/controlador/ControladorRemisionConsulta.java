package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.RemisionService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.RemisionConsultaBean;
import com.ruribe.util.bean.interfaces.IRemisionConsultaBean;

/**
 * @author ruribe
 */

@Named(value = "mbeanRemisionConsulta")
@Scope("request")
public class ControladorRemisionConsulta extends ControladorBase implements Serializable {

	private IRemisionConsultaBean consultaBean;
	private final Logger log = Logger.getLogger(ControladorRemisionConsulta.class.getName());

	/**
	 * constructor.
	 */
	public ControladorRemisionConsulta() {
		consultaBean = new RemisionConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	RemisionService remisionService;

	/**
	 * Inicializa el formulario de reposicion con las 10 ultimas remisiones.
	 * 
	 * @return redirige a la pagina de remision.
	 */
	public String iniciarConsultarRemision() {
		try {
			// cargar clientes
			consultarClientes();
			// cargamos las 10 ultimas remisiones
			consultaBean.setListaRemisiones(remisionService.consultarRemisiones(consultaBean));
			if (consultaBean.getListaRemisiones().isEmpty()) {
				SessionUtils.addError("No hay datos para esta consulta");
			}
		} catch (SAExcepcion e) {
			//log.severe(e.getMessage());
			SessionUtils.addFatalError("Error controlado en el Sistema:" + e.getClave());
		}catch (Exception e) {
			log.severe(e.getMessage());
			SessionUtils.addFatalError("Error en el Sistema:");
		}
		return "CONSULTAR_REMISION";
	}

	/**
	 * obtiene las remisiones segun filtros de busqueda
	 */
	public String buscarRemisiones() {
		// cargar clientes
		try {
			consultarClientes();
			consultaBean.setIdCliente(clienteSelect);
			consultaBean.setListaRemisiones(remisionService.consultarRemisiones(consultaBean));
			if (consultaBean.getListaRemisiones().isEmpty()) {
				SessionUtils.addError("No hay datos para esta consulta");
			}

		} catch (Exception e) {
			log.severe("Error al consultar la remision:" + e.getMessage());
			SessionUtils.addError("No hay datos para esta consulta");
		}
		return "CONSULTAR_REMISION";
	}

	/**
	 * @return the consultaBean
	 */
	public IRemisionConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(IRemisionConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

}