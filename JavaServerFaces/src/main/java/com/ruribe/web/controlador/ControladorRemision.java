package com.ruribe.web.controlador;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.RemisionService;
import com.ruribe.servicio.interfaces.TransportadorService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.DetalleRemisionBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.RemisionBean;
import com.ruribe.util.bean.interfaces.IDetalleRemisionBean;
import com.ruribe.util.bean.interfaces.IRemisionBean;
import com.ruribe.web.converter.Beer;
import com.ruribe.web.reportes.pdf.RemisionPDF;

@Named(value = "mbeanRemision")
@Scope("request")
public class ControladorRemision extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	@Inject
	ProductoService productoService;
	@Inject
	RemisionService remisionService;
	@Inject
	TransportadorService transportadorService;
	private IRemisionBean iRemisionBean;
	private List<IRemisionBean> listaRemisiones;
	private boolean mostrarDetalle;
	private boolean onlyread = false;// comprueba si la remision esta creada.
	Logger log = Logger.getLogger(ControladorUsuario.class.getName());

	public ControladorRemision() {
		iRemisionBean = new RemisionBean();
	}

	/**
	 * Inicializa el formulario de remision.
	 * 
	 * @return redirige a la pagina de remision.
	 */
	public String iniciarRemision() {
		try {
			iRemisionBean = new RemisionBean();
			setClienteSelect("Seleccionar");
			setSelectTransportador("Seleccionar");
			setCodTipoProductoSelect("Seleccionar");
			// cargar clientes
			consultarClientes();
			listaTransportador = transportadorService.obtenerTransportadores();
			listaTipoProducto = productoService.obtenerTipoProducto();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			SessionUtils.addError("Error al cargar la informacion");
		}
		
		
		return "REMISION";
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarCliente(ValueChangeEvent evento) {
		setObraSelect("Seleccionar");
		iRemisionBean.setIclienteBean(new ClienteBean());
		iRemisionBean.setIobraBean(new ObraBean());
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			String idclienteSelect = evento.getNewValue().toString();
			// se selecciona el cliente de la lista de clientes.
			iRemisionBean.setIclienteBean(getCliente(idclienteSelect));
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarObra(ValueChangeEvent evento) {
		try {
			setObraSelect("Seleccionar");
			iRemisionBean.setIobraBean(new ObraBean());
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idobraSelect = evento.getNewValue().toString();
				// hay que crear el buscar cliente por llave primaria.
				iRemisionBean.setIobraBean(clienteService.obtenerObraPK(Integer.parseInt(idobraSelect)*-1));
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarTipoProducto(ValueChangeEvent evento) {
		try {
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				int idTipoProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
				iRemisionBean
						.setListaProducto(productoService.buscarProductoPorTipoProductoConStock(idTipoProductoSelect));
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarProducto(ValueChangeEvent evento) {
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			int idProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
			// obtenemos el objeto producto seleccionado
			iRemisionBean.setProducto(iRemisionBean.getProducto(idProductoSelect));
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Agrega un producto a la remison en curso
	 */
	public void agregarAcarrito() {
		if (validarCarrito()) {
			// Sirve para saber si tenemos agregado el producto al carrito de compras
			boolean existe = false;
			// si el producto ya esta en lista se le suma la cantidad.
			for (IDetalleRemisionBean remision : iRemisionBean.getDetalleRemisions()) {
				if (remision.getIproductoBean()
						.getCodigo_producto() == (iRemisionBean.getProducto().getCodigo_producto())) {
					int nuevaCantidad = remision.getCantidad() + iRemisionBean.getCantidad();
					remision.setCantidad(nuevaCantidad);
					existe = true;
					break;
				}
			}
			// si el producto no esta en lista y la cantidad > 0 lo agregamos.
			if (!existe) {
				if (iRemisionBean.getCantidad() > 0) {
					IDetalleRemisionBean detalle = new DetalleRemisionBean();
					detalle.setCantidad(iRemisionBean.getCantidad());
					detalle.setIproductoBean(iRemisionBean.getProducto());
					iRemisionBean.setDetalleRemisions(detalle);
				}
			}

			// reiniciamos los valores del formulario
			iRemisionBean.setCantidad(0);
			FacesContext.getCurrentInstance().renderResponse();
		}
	}

	/**
	 * Validar entrada de carrito.
	 * 
	 * @return
	 */
	private boolean validarCarrito() {
		if (iRemisionBean.getFecha() == null) {
			SessionUtils.addError("Seleccionar una fecha valida");
			return false;
		}
		if (iRemisionBean.getFecha_entrega()!= null && Utilidades.fechaesAMayorOIgualfechaB(iRemisionBean.getFecha(), iRemisionBean.getFecha_entrega())) {
			SessionUtils.addError("La fecha de remision no puede ser mayor a la fecha de entrega.");
			return false;
		}
		if (iRemisionBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("Seleccionar un cliente");
			return false;
		}
		if (iRemisionBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("Seleccionar una obra");
			return false;
		}
		if (getSelectTransportador() == null || getSelectTransportador().equals("Seleccionar")) {
			SessionUtils.addError("Seleccionar el tranportador");
			return false;
		}
		if (iRemisionBean.getProducto() == null) {
			SessionUtils.addError("Seleccionar un Producto");
			return false;
		}
		if (iRemisionBean.getCantidad() == 0) {
			SessionUtils.addError("La cantidad debe ser mayor a 0");
			return false;
		}
		if (iRemisionBean.getCantidad() > iRemisionBean.getProducto().getStock()) {
			SessionUtils.addError("La cantidad no debe superar el stock");
			return false;
		}
		return true;
	}

	/**
	 * Validar entrada de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		
		if (iRemisionBean.getIdRemision() != 0 && remisionService.remisionIsPrinted(iRemisionBean.getIdRemision())) {
			SessionUtils.addError("La remisión ya ha sido impresa y no se puede modificar");
			return false;
		}
		if (iRemisionBean.getFecha() == null) {
			SessionUtils.addError("Seleccionar una fecha valida");
			return false;
		}
		if (iRemisionBean.getFecha_entrega()!= null && Utilidades.fechaesAMayorOIgualfechaB(iRemisionBean.getFecha(), iRemisionBean.getFecha_entrega())) {
			SessionUtils.addError("La fecha de remision no puede ser mayor a la fecha de entrega.");
			return false;
		}
		if (iRemisionBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("Seleccionar un cliente");
			return false;
		}
		if (iRemisionBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("Seleccionar una obra");
			return false;
		}
		if (getSelectTransportador() == null || getSelectTransportador().equals("Seleccionar")) {
			SessionUtils.addError("Seleccionar el tranportador");
			return false;
		}
		if (iRemisionBean.getDetalleRemisions() == null || iRemisionBean.getDetalleRemisions().isEmpty()) {
			SessionUtils.addError("La remisión no contiene items.");
			return false;
		}
		return true;
	}

	/**
	 * Agrega un producto a la remison en curso
	 */
	public void guardarRemision() {

		try {
			if (this.validar()) {
				// la la factura es 0 indica que es nueva, una vez guardada se aginga un nuevo
				// valor.
				if (iRemisionBean.getIdRemision() == 0) {
					log.info("Guardando remision.");
					iRemisionBean.getItransportadorBean().setNo_documento(getSelectTransportador());
					iRemisionBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
					iRemisionBean.setEstado(true);
					iRemisionBean.setPrinted(false);
					iRemisionBean = remisionService.crearRemision(iRemisionBean);
					SessionUtils.addInfo("Remision guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva remision presione nueva remision.");
				}
			}
		} catch (Exception e) {
			SessionUtils.addFatalError("Remision no guardada correctamente");
			log.log(Level.SEVERE, "Error al crear el usuario", e);
			e.printStackTrace();
		}
	}

	/**
	 * reinicia el formulario
	 */
	public void nuevaRemision() {
		iRemisionBean = new RemisionBean();
		setClienteSelect("Seleccionar");
		setSelectTransportador("Seleccionar");
		setCodTipoProductoSelect("Seleccionar");
	}

	/**
	 * imprimir remision de empresa.
	 */
	public void imprimir(RemisionBean iRemisionBean) {

		try {
			iRemisionBean = (RemisionBean) remisionService.consultarIDRemision(iRemisionBean);
			if (iRemisionBean.getIdRemision() != 0) {
				remisionService.marcarRemisionImpresa(iRemisionBean.getIdRemision());
				String ARCHIVO = "Remision" + iRemisionBean.getIdRemision() + ".pdf";
				if (!new File(Propiedades.leerPropiedad("SHARED_REMISION") + ARCHIVO).exists()) {
					new RemisionPDF().crearPDF(ARCHIVO, iRemisionBean);
				}
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_REMISION"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la remision.");
			}
		} catch (Exception e) {
			SessionUtils.addFatalError("Error al leer la Remision.");
			e.printStackTrace();
		}
	}

	/**
	 * imprimir remision de cliente.
	 */
	public void imprimirPDFCliente(RemisionBean remisionBean) {

		try {
			iRemisionBean = (RemisionBean) remisionService.consultarRemisionPDFCliente(remisionBean);
			if (iRemisionBean.getIdRemision() != 0) {
				remisionService.marcarRemisionImpresa(iRemisionBean.getIdRemision());
				String ARCHIVO = "Remision-" + iRemisionBean.getIdRemision() + ".pdf";
				if (!new File(Propiedades.leerPropiedad("SHARED_REMISION") + ARCHIVO).exists()) {
					new RemisionPDF().crearPDF(ARCHIVO, iRemisionBean);
				}
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_REMISION"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la remision.");
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al Imprimir la Remision.");
			e.printStackTrace();
		}
	}

	public void seleccionarBeer(ValueChangeEvent evento) {
		if (evento.getNewValue() != null) {
			Beer beers = (Beer) evento.getNewValue();
			// hay que crear el buscar cliente por llave primaria.
		}
	}

	/**
	 * @return the iRemisionBean
	 */
	public IRemisionBean getiRemisionBean() {
		return iRemisionBean;
	}

	/**
	 * @param iRemisionBean the iRemisionBean to set
	 */
	public void setiRemisionBean(IRemisionBean iRemisionBean) {
		this.iRemisionBean = iRemisionBean;
	}

	/**
	 * habilita el detalle de carrito de remision y bloquea los campos
	 * 
	 * @return
	 */
	public final boolean isMostrarDetalle() {
		if (iRemisionBean.getDetalleRemisions() != null && iRemisionBean.getDetalleRemisions().size() > 0) {
			mostrarDetalle = true;
		} else {
			mostrarDetalle = false;
		}

		return mostrarDetalle;
	}

	public String verRemision(IRemisionBean iRemisionBean) {
		try {
			this.iRemisionBean = iRemisionBean;
			// cargar clientes
			consultarClientes();
			listaTransportador = transportadorService.obtenerTransportadores();
			setClienteSelect(iRemisionBean.getIclienteBean().getId_cliente());
			setObraSelect(String.valueOf(iRemisionBean.getIobraBean().getIdObra()*-1));
			setSelectTransportador(iRemisionBean.getItransportadorBean().getNo_documento());
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "MODIFICAR_REMISION";
	}

	/**
	 * Modificar remision
	 */
	public void modificarRemision() {
		try {
			if (this.validar()) {
				iRemisionBean.getItransportadorBean().setNo_documento(getSelectTransportador());
				iRemisionBean.setUsuario_creacion(SessionUtils.getUserName());// Usuario de creacion
				remisionService.editarRemision(iRemisionBean);
				SessionUtils.addError(FacesMessage.SEVERITY_INFO, "Remision modificada correctamente");
			}
		} catch (Exception e) {
			SessionUtils.addFatalError("Error al modificar la Remision" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * comprueba si la factura nueva o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (iRemisionBean.getIdRemision() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}

	/**
	 * @param mostrarDetalle the mostrarDetalle to set
	 */
	public final void setMostrarDetalle(boolean mostrarDetalle) {
		this.mostrarDetalle = mostrarDetalle;
	}

	/**
	 * @return the listaRemisiones
	 */
	public List<IRemisionBean> getListaRemisiones() {
		return listaRemisiones;
	}

	/**
	 * @param listaRemisiones the listaRemisiones to set
	 */
	public void setListaRemisiones(List<IRemisionBean> listaRemisiones) {
		this.listaRemisiones = listaRemisiones;
	}

}