/**
 * 
 */
package com.ruribe.web.controlador;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import com.ruribe.servicio.interfaces.ClienteService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;
import com.ruribe.util.bean.interfaces.ITransportadorBean;

/**
 * @author RONIE
 *
 */
public abstract class ControladorBase {

	private boolean editable;
	protected boolean verListado=false;
	@Inject
	ClienteService clienteService;
	protected List<IClienteBean> listaCliente;
	protected List<ITransportadorBean> listaTransportador;
	protected List<IProductoBean> listaProducto;
	protected List<IObraBean> listaObra;
	protected String clienteSelect;
	protected String obraSelect;
	protected String codTipoProductoSelect;
	protected String proveedorSelect;
	protected List<ITipoProductoBean> listaTipoProducto;
	protected String selectProducto;
	protected String selectTransportador;
	protected String selectDescuento;
	private String idError;
	 
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
	         throws ValidatorException {
	      if (((String)arg2).length()<5) {
	         throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"El Campo no puede ser vacio.",""));
	      }
	   }

	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	
	
	/**
	 * Consulta usuario todos los usuarios
	 * @throws SAExcepcion 
	 */
	public final void consultarClientes() throws SAExcepcion{
			listaCliente=clienteService.obtenerClientes();
	}
	
	/**
	 * @return the listaCliente
	 */
	public List<IClienteBean> getListaCliente() {
		return listaCliente;
	}

	/**
	 * @param listaCliente the listaCliente to set
	 */
	public void setListaCliente(List<IClienteBean> listaCliente) {
		this.listaCliente =listaCliente;
		
	}

	/**
	 * @return the listaObra
	 */
	public List<IObraBean> getListaObra() {
		return listaObra;
	}

	/**
	 * @param listaObra the listaObra to set
	 */
	public void setListaObra(List<IObraBean> listaObra) {
		this.listaObra = listaObra;
	}

	/**
	 * @return the verListado
	 */
	public boolean isVerListado() {
		return verListado;
	}

	/**
	 * @param verListado the verListado to set
	 */
	public void setVerListado(boolean verListado) {
		this.verListado = verListado;
	}
	/**
	 * @return the clienteSelect
	 */
	public final String getClienteSelect() {
		return clienteSelect;
	}
	/**
	 * @param clienteSelect the clienteSelect to set
	 */
	public final void setClienteSelect(String clienteSelect) {
		this.clienteSelect = clienteSelect;
	}
	/**
	 * @return the obraSelect
	 */
	public final String getObraSelect() {
		return obraSelect;
	}
	/**
	 * @param obraSelect the obraSelect to set
	 */
	public void setObraSelect(String obraSelect) {
		this.obraSelect = obraSelect;
	}
	/**
	 * @return the codTipoProductoSelect
	 */
	public String getCodTipoProductoSelect() {
		return codTipoProductoSelect;
	}
	/**
	 * @param codTipoProductoSelect the codTipoProductoSelect to set
	 */
	public void setCodTipoProductoSelect(String codTipoProductoSelect) {
		this.codTipoProductoSelect = codTipoProductoSelect;
	}
	/**
	 * @return the selectProducto
	 */
	public String getSelectProducto() { 
		return selectProducto;
	}
	/**
	 * @param selectProducto the selectProducto to set
	 */
	public final void setSelectProducto(String selectProducto) {
		this.selectProducto = selectProducto;
	}
	/**
	 * @return the selectTransportador
	 */
	public final String getSelectTransportador() {
		return selectTransportador;
	}
	/**
	 * @param selectTransportador the selectTransportador to set
	 */
	public final void setSelectTransportador(String selectTransportador) {
		this.selectTransportador = selectTransportador;
	}
	
	/**
	 * @return the proveedorSelect
	 */
	public String getProveedorSelect() {
		return proveedorSelect;
	}
	/**
	 * @param proveedorSelect the proveedorSelect to set
	 */
	public void setProveedorSelect(String proveedorSelect) {
		this.proveedorSelect = proveedorSelect;
	}
	/**
	 * @return the listaTransportador
	 */
	public List<ITransportadorBean> getListaTransportador() {
		return listaTransportador;
	}

	/**
	 * @param listaTransportador the listaTransportador to set
	 */
	public void setListaTransportador(List<ITransportadorBean> listaTransportador) {
		this.listaTransportador =listaTransportador;
		
	}
	
	/**
	 * @return the listaProducto
	 */
	public final List<IProductoBean> getListaProducto() {
		return listaProducto;
	}

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public void setListaProducto(List<IProductoBean> listaProducto) {
		this.listaProducto =listaProducto;
		
	}
	
	/**
	 * @return the listaTipoProducto
	 */
	public final List<ITipoProductoBean> getListaTipoProducto() {
		return listaTipoProducto;
				
	}

	/**
	 * @param listaTipoProducto the listaTipoProducto to set
	 */
	public final void setListaTipoProducto(List<ITipoProductoBean> listaTipoProducto) {
		this.listaTipoProducto = listaTipoProducto;
	}
	
	/**
	 * retorna un objeto de una lista de tipo IClienteBean
	 * @param String codigo_cliente
	 * @return IClienteBean
	 */
	public IClienteBean getCliente(String id) {
        
        for (IClienteBean cliente : listaCliente){
            if (id.equals(cliente.getId_cliente())){
            	return cliente;
            }
        }
        return null;
    }
	/**
	 * @return the selectDescuento
	 */
	public String getSelectDescuento() {
		return selectDescuento;
	}
	/**
	 * @param selectDescuento the selectDescuento to set
	 */
	public void setSelectDescuento(String selectDescuento) {
		this.selectDescuento = selectDescuento;
	}
	
	public String getIdError() {
		return idError;
	}

	public void setIdError(String idError) {
		this.idError = idError;
	}
	

}
