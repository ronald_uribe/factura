package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.EntradaService;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.EntradaConsultaBean;
import com.ruribe.util.bean.interfaces.IEntradaConsultaBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;

/**
 * @author ruribe
 */

@Named(value = "mbeanEntradaConsulta")
@Scope("request")
public class ControladorEntradaConsulta extends ControladorBase implements Serializable {

	@Inject
	ProveedorService proveedorService;

	@Inject
	EntradaService salidaService;
	private IEntradaConsultaBean consultaBean;
	private List<IProveedorBean> listaProveedor;

	/**
	 * constructor.
	 */
	public ControladorEntradaConsulta() {
		consultaBean = new EntradaConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Inicializa el formulario de reposicion con las 10 ultimas salidaes.
	 * 
	 * @return redirige a la pagina de salida.
	 */
	public String iniciarConsultarEntrada() {
		try {
			// cargar clientes
			consultarProveedores();
			// cargamos las 10 ultimas salidaes
			consultaBean.setListaEntradas(salidaService.consultarEntradas(consultaBean));
			if (consultaBean.getListaEntradas().isEmpty()) {
				SessionUtils.addError("No hay nignuna entrada registrada");
			}
		} catch (Exception e) {
			SessionUtils.addError("No hay datos para esta consulta");
		}
		return "CONSULTAR_ENTRADA";
	}

	/**
	 * obtiene las salidaes segun filtros de busqueda
	 */
	public String buscarEntradas() {
		try {
			// cargar clientes
			consultarProveedores();
			consultaBean.setIdProveedor(proveedorSelect);
			consultaBean.setListaEntradas(salidaService.consultarEntradas(consultaBean));
			if (consultaBean.getListaEntradas().isEmpty()) {
				SessionUtils.addError("No hay datos para esta consulta");
			}
		} catch (Exception e) {
			SessionUtils.addError("No hay datos para esta consulta");
		}
		return "CONSULTAR_ENTRADA";
	}

	/**
	 * Consulta usuario todos los usuarios
	 * @throws SAExcepcion 
	 */
	public final void consultarProveedores() throws SAExcepcion {
		listaProveedor = proveedorService.obtenerProveedores();
	}

	/**
	 * @return the consultaBean
	 */
	public IEntradaConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(IEntradaConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

	/**
	 * @return the listaProveedor
	 */
	public List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

}