package com.ruribe.web.controlador;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.FacturaService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.FacturaBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.interfaces.IFacturaBean;
import com.ruribe.web.reportes.pdf.FacturaPDF;

@Named(value="mbeanFactura")
@Scope("request")
public class ControladorFactura extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	private IFacturaBean ifacturaBean;
	private IFacturaBean lastFactura;
	@Inject
	FacturaService facturaService;
	private boolean onlyread = false;// comprueba si la factura esta creada.

	public ControladorFactura() {
		ifacturaBean = new FacturaBean();
	}

	/**
	 * Inicializa el formulario de factura.
	 * 
	 * @return redirige a la pagina de factura.
	 */
	public String iniciarFactura() {
		try {
			ifacturaBean = new FacturaBean();
			setClienteSelect("Seleccionar");
			lastFactura = null;
			// cargar clientes
			consultarClientes();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "FACTURA";
	}

	/**
	 * calcula un periodo de factura para un determinado cliente.
	 */
	public void procesarFactura() {
		try {
			ifacturaBean.getListadetalleFactura().clear();
			if (validarProcesarFactura()) {
				ifacturaBean.setVr_parcial((double) 0);
				ifacturaBean = facturaService.procesarFactura(ifacturaBean);
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Guardar factura
	 */
	public void guardarFactura() {
		try {
			if (this.validar()) {
				// la factura es 0 indica que es nueva, una vez guardada se asigna un nuevo valor
				if (ifacturaBean.getIdFactura() == 0) {
					ifacturaBean.setUsuario(SessionUtils.getUserName());
					ifacturaBean.setEstado(true);
					ifacturaBean = facturaService.crearFactura(ifacturaBean);
					SessionUtils.addInfo("Factura guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva remision presione nueva remision.");
				}
			}

		} catch (Exception e) {
			SessionUtils.addError("Error al crear la Factura: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * ver factura
	 * 
	 * @param IFacturaBean ifacturaBean
	 */
	public String verFactura(IFacturaBean ifacturaBean) {
		try {
			this.ifacturaBean = ifacturaBean;
			// cargar clientes
			consultarClientes();
			setClienteSelect(ifacturaBean.getIclienteBean().getId_cliente());
			setObraSelect(String.valueOf(ifacturaBean.getIobraBean().getIdObra()*-1));
		} catch (SAExcepcion e) {
			e.printStackTrace();
		}
		return "MODIFICAR_FACTURA";
	}

	/**
	 * Nueva factura
	 */
	public String nuevaFactura() {

		ifacturaBean = new FacturaBean();
		lastFactura = null;
		setClienteSelect("Seleccionar");
		return "FACTURA";
	}

	/**
	 * Seleccionar cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarCliente(ValueChangeEvent evento) {
		setObraSelect("Seleccionar");
		ifacturaBean.setIclienteBean(new ClienteBean());
		ifacturaBean.setIobraBean(new ObraBean());
		ifacturaBean.getListadetalleFactura().clear();
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			String idclienteSelect = evento.getNewValue().toString();
			// se selecciona el cliente de la lista de clientes.
			ifacturaBean.setIclienteBean(getCliente(idclienteSelect));
			lastFactura = null;
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Seleccionar obra.
	 * 
	 * @param evento
	 */
	public void seleccionarObra(ValueChangeEvent evento) {
		try {
			setObraSelect("Seleccionar");
			ifacturaBean.setIobraBean(new ObraBean());
			ifacturaBean.getListadetalleFactura().clear();
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idobraSelect = evento.getNewValue().toString();
				// hay que crear el buscar cliente por llave primaria.
				ifacturaBean.setIobraBean(clienteService.obtenerObraPK(Integer.parseInt(idobraSelect)*-1));
				lastFactura = facturaService.obtenerUltimaFactura(ifacturaBean);
			}
		} catch (SAExcepcion e) {
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	public void aplicarDescuento(ValueChangeEvent evento) throws AbortProcessingException {
		if (null != evento.getNewValue() && ifacturaBean.getVr_parcial()!=null) {
			Integer porcentajeDescuento = Integer.parseInt((String) evento.getNewValue());
			ifacturaBean.setDescuento((porcentajeDescuento * ifacturaBean.getVr_parcial()) / 100);
			ifacturaBean.setIva(
					((ifacturaBean.getVr_parcial() - ifacturaBean.getDescuento() + ifacturaBean.getTransporte()) * 0.19));
			ifacturaBean.setTotal((ifacturaBean.getVr_parcial() - ifacturaBean.getDescuento() + ifacturaBean.getIva()
					+ ifacturaBean.getTransporte() + ifacturaBean.getReposicion()));
			FacesContext.getCurrentInstance().renderResponse();
		}
	}

	public void aplicarReposicion(ValueChangeEvent evento) throws AbortProcessingException {
		if (null != evento.getNewValue() && (Long) evento.getNewValue() != 0) {
			Long reposicion = (Long) evento.getNewValue() == null ? 0 : (Long) evento.getNewValue();
			ifacturaBean.setReposicion(reposicion.intValue());
			ifacturaBean.setTotal((ifacturaBean.getVr_parcial() - ifacturaBean.getDescuento() + ifacturaBean.getIva()
					+ ifacturaBean.getTransporte() + reposicion));
			FacesContext.getCurrentInstance().renderResponse();
		}
	}

	/**
	 * calcular descuento y reposicion
	 */
	public void calcular() {
		try {
			ifacturaBean.setIva(
					((ifacturaBean.getVr_parcial() - (ifacturaBean.getDescuento()==null?0:ifacturaBean.getDescuento()) + ifacturaBean.getTransporte()) * 0.19));
			ifacturaBean.setTotal((ifacturaBean.getVr_parcial() - (ifacturaBean.getDescuento()==null?0:ifacturaBean.getDescuento()) + ifacturaBean.getIva()
					+ ifacturaBean.getTransporte() + ifacturaBean.getReposicion()));
			
		}catch(Exception e) {
			SessionUtils.addError("Error al calcular el descuento: ");
			e.printStackTrace();
		}
	}

	/**
	 * imprimir factura, descarga el fichero pdf y lo saca por pantalla.
	 */
	public void imprimir(IFacturaBean iFacturaBean) {

		try {
			if (iFacturaBean.getIdFactura() != 0) {
				iFacturaBean = (FacturaBean) facturaService.consultarFactura(iFacturaBean.getIdFactura());
				String ARCHIVO = "Factura" + iFacturaBean.getIdFactura() + ".pdf";
				// comprobamos si el fichero existe.
				// if(!new File(Constantes.SHARED_FACTURA+ARCHIVO).exists()){
				new FacturaPDF().crearPDF(ARCHIVO, iFacturaBean);
				// }
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_FACTURA"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la Factura.");
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al leer la Factura.");
			e.printStackTrace();
		}
	}

	/**
	 * imprimir factura, descarga el fichero pdf y lo saca por pantalla.
	 */
	public void imprimirPDFCliente(IFacturaBean ifacturapdf) {

		try {
			if (ifacturapdf.getIdFactura() != 0) {
				ifacturapdf = (FacturaBean) facturaService.consultarFacturaPDFCliente(ifacturapdf);
				String ARCHIVO = "Factura-" + ifacturapdf.getIdFactura() + ".pdf";
				new FacturaPDF().crearPDF(ARCHIVO, ifacturapdf);
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_FACTURA"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la Factura.");
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al leer la Factura.");
			e.printStackTrace();
		}
	}

	/**
	 * Validar entrada de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		if (ifacturaBean.getFecha_factura() == null) {
			SessionUtils.addError("Seleccionar una fecha valida");
			return false;
		} else if (Utilidades.fechaesAMayorOIgualfechaB(ifacturaBean.getFecha_factura(),ifacturaBean.getFecha_corte())) {
			SessionUtils.addError("La fecha de factura no puede ser menor que la fecha de corte");
			return false;
		} else if (Utilidades.fechaesAMayorOIgualfechaB(ifacturaBean.getFecha_corte(),ifacturaBean.getFecha_inicio())) {
			SessionUtils.addError("La fecha de corte no puede ser menor que la fecha de inicio");
			return false;
		} else if (ifacturaBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("Seleccionar un cliente");
			return false;
		} else if (ifacturaBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("Seleccionar una obra");
			return false;
		} else if (ifacturaBean.getListadetalleFactura() == null || ifacturaBean.getListadetalleFactura().isEmpty()) {
			SessionUtils.addError("La Factura no contiene items.");
			return false;
		}
		return true;
	}

	/**
	 * Validar procesar factura o generar corte.
	 * 
	 * @return
	 */
	private boolean validarProcesarFactura() {
		if (ifacturaBean.getFecha_factura() == null) {
			SessionUtils.addError("Digite la fecha de Factura");
			setIdError("fecha");
			return false;
		}  else if (ifacturaBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("Seleccionar un cliente");
			setIdError("comboTipoCliente");
			return false;
		} else  if (ifacturaBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("Seleccionar una obra");
			setIdError("comboObra");
			return false;
		} else	if (ifacturaBean.getFecha_inicio() == null) {
			SessionUtils.addError("Digite la fecha de inicio");
			return false;
		}else if (ifacturaBean.getFecha_corte() == null) {
			SessionUtils.addError("Digite la fecha de corte");
			return false;
		} else if (Utilidades.fechaesAMayorOIgualfechaB(ifacturaBean.getFecha_corte(), ifacturaBean.getFecha_inicio())) {
			SessionUtils.addError("La fecha de corte no puede ser menor que la fecha de inicio");
			return false;
		}
		return true;
	}

	/**
	 * @return the ifacturaBean
	 */
	public IFacturaBean getIfacturaBean() {
		return ifacturaBean;
	}

	/**
	 * @param ifacturaBean the ifacturaBean to set
	 */
	public void setIfacturaBean(IFacturaBean ifacturaBean) {
		this.ifacturaBean = ifacturaBean;
	}

	/**
	 * comprueba si la factura esta creada o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (ifacturaBean.getIdFactura() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	/**
	 * @return the lastFactura
	 */
	public IFacturaBean getLastFactura() {
		return lastFactura;
	}

	/**
	 * @param lastFactura the lastFactura to set
	 */
	public void setLastFactura(IFacturaBean lastFactura) {
		this.lastFactura = lastFactura;
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}
}