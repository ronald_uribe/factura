package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.servicio.interfaces.ClienteService;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IObraBean;

@Named(value = "mbeanObra")
@Scope("request")
public class ControladorObra extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	@Inject
	TipoDocumentoService tipoDocumentoService;
	@Inject
	CiudadService ciudadService;
	private IClienteBean iCliente;
	private IObraBean iObra;

	public void setUserBo(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	public ControladorObra() {
		iCliente = new ClienteBean();
		iObra = new ObraBean();
	}

	/**
	 * Inicializa el formulario de vincular obras.
	 * 
	 * @return redirige a la pagina de vincular obras.
	 */
	public String iniciarVincularObra() {
		try {
			consultarClientes();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "VINCULAROBRA";
	}

	/**
	 * guardar obra
	 */
	public final void vincularObraCliente() {
		try {
			if (validar()) {
				iObra.setIclientebean(iCliente);
				clienteService.vincularObraCliente(iObra);
				SessionUtils.addInfo("La obra " + iObra.getNombre().toUpperCase() + " se ha vinculado correctamente");
				iObra = new ObraBean();
				setListaObra(clienteService.obtenerObrasCliente(iCliente.getId_cliente()));
				this.verListado = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SessionUtils.addError("Error al vincular la obra:" + iObra.getNombre());
		}
	}

	/** validar proveedor */
	public boolean validar() {
		if (iCliente.getId_cliente() == null || iCliente.getId_cliente().isEmpty()) {
			SessionUtils.addError("Campo Cliente obligatorio");
			return false;
		}
		if (iObra.getNombre() == null || iObra.getNombre().isEmpty()) {
			SessionUtils.addError("Campo Nombre de obra obligatorio");
			return false;
		}
		if (iObra.getDireccion() == null || iObra.getDireccion().isEmpty()) {
			SessionUtils.addError("Campo Direccion de obra obligatorio");
			return false;
		}
		return true;
	}

	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		if (((String) arg2).length() < 5) {
			throw new ValidatorException(new FacesMessage("Al menos 5 caracteres "));
		}
	}

	/**
	 * Consulta un usuario por su identificador
	 */
	public final void buscarCliente() {
		setListaCliente(clienteService.obtenerCliente(iCliente));
	}

	/**
	 * Editar obra vinculada
	 */
	public final void editarObra() {
		try {
			iObra.setIclientebean(iCliente);
			clienteService.modificaObraCliente(iObra);
			setListaObra(clienteService.obtenerObrasCliente(iCliente.getId_cliente()));
			SessionUtils.addInfo("Obra Actualizado correctamente" + iObra.getNombre());
			iObra = new ObraBean();
			setEditable(false);
		} catch (Exception e) {
			System.out.println("Error al editar " + e.getMessage());
			SessionUtils.addError("Se ha producido un error intentelo mas tarde.");
			e.printStackTrace();
		}
	}

	/**
	 * limpiar formulario
	 */
	public final void limpiarFormulario() {
		iCliente = null;
		iObra = null;
		iCliente = new ClienteBean();
		iObra = new ObraBean();
		iCliente.setTipoDocumentoSelect("Seleccionar");
		iCliente.setCiudadSelect("Seleccionar");
		iCliente.setClienteSelect("Seleccionar");
		setListaObra(null);
		setEditable(false);
		setVerListado(false);

	}

	/**
	 * evento onchange que carga las obras de un cliente seleccionado
	 * 
	 * @param evento
	 */
	public void seleccionarCliente(ValueChangeEvent evento) {
		try {
			setListaObra(null);
			setVerListado(false);
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idclienteSelect = evento.getNewValue().toString();
				List<IClienteBean> clientes = getListaCliente();
				Iterator<IClienteBean> iterador = clientes.iterator();
				boolean encontrado = false;
				while (iterador.hasNext() && !encontrado) {
					IClienteBean cliente = iterador.next();
					// Comparamos el código y la sección del cliente
					if ((cliente.getId_cliente()).equals(idclienteSelect)) {
						setListaObra(clienteService.obtenerObrasCliente(idclienteSelect));
						this.setiCliente(cliente);
						encontrado = true;// break.
						// solo se mostrara el pandel de listado si hay contenidos.
						if (!this.getListaObra().isEmpty()) {
							setVerListado(true);
						}
					}
				}
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iCliente
	 * @return
	 */
	public String editarAction(IObraBean iobra) {
		setEditable(true);
		this.setiObra(iobra);
		return null;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IClienteBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IObraBean iObra) {
		try {
			iObra.setIclientebean(iCliente);
			clienteService.desvincularObra(iObra);
			setListaObra(clienteService.obtenerObrasCliente(iCliente.getId_cliente()));
			if (this.getListaObra().isEmpty()) {
				this.verListado = false;
			}
			SessionUtils.addInfo("Obra de " + iObra.getNombre() + " borrada correctamente");
		} catch (Exception e) {
			SessionUtils.addError("Error al borrar la obra " + iObra.getNombre());
		}
	}

	/**
	 * @return the iCliente
	 */
	public IClienteBean getiCliente() {
		return iCliente;
	}

	/**
	 * @param iCliente the iCliente to set
	 */
	public void setiCliente(IClienteBean iCliente) {
		this.iCliente = iCliente;
	}

	/**
	 * @return the iObra
	 */
	public IObraBean getiObra() {
		return iObra;
	}

	/**
	 * @param iObra the iObra to set
	 */
	public void setiObra(IObraBean iObra) {
		this.iObra = iObra;
	}
}