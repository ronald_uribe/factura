package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.context.annotation.Scope;
import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.ProveedorBean;
import com.ruribe.util.bean.interfaces.ICiudadBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;

@Named(value = "mbeanProveedor")
@Scope("request")
public class ControladorProveedor extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;
	@Inject
	ProveedorService proveedorService;
	@Inject
	TipoDocumentoService tipoDocumentoService;
	@Inject
	CiudadService ciudadService;
	private IProveedorBean iproveedor;
	private List<IProveedorBean> listaProveedor;
	private List<ITipoDocumentoBean> listaTipoDocumento;
	private List<ICiudadBean> listaCiudad;
	

//	public void setUserBo(ProveedorService proveedorService) {
//		this.proveedorService = proveedorService;
//	}

	public ControladorProveedor() {
		iproveedor = new ProveedorBean();
	}

	/**
	 * Inicializa el formulario de Producto.
	 * 
	 * @return redirige a la pagina de producto.
	 */
	public String iniciarProveedor() {
		try {
			listaTipoDocumento = tipoDocumentoService.obtenerTipoDocumentos();
			listaProveedor = proveedorService.obtenerProveedores();
			listaCiudad = ciudadService.obtenerCiudad();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "PROVEEDORES";
	}

	/**
	 * guardar usuario
	 */
	public final void guardarProveedor() {
		try {
			// tomamos el valor del combo y lo convertimos a entero
			if (this.validarProveedor(iproveedor)) {
				iproveedor.setCod_tipo_documento(Integer.parseInt(iproveedor.getTipoDocumentoSelect()) * -1);
				iproveedor.setCod_ciudad(Integer.parseInt(iproveedor.getCiudadSelect()) * -1);
				proveedorService.crearProveedor(iproveedor);
				SessionUtils.addInfo("Proveedor creado correctamente" + iproveedor.getRazon_social());
				iproveedor = new ProveedorBean();
				iproveedor.setTipoDocumentoSelect("Seleccionar");
				iproveedor.setCiudadSelect("Seleccionar");
				this.consultarProveedores();
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al crear el proveedor: " + iproveedor.getRazon_social());
			e.printStackTrace();
		}
	}

	/** validar proveedor */
	public boolean validarProveedor(IProveedorBean iproveedor) {
		if (iproveedor.getNo_documento() == null || iproveedor.getNo_documento().isEmpty()) {
			SessionUtils.addError("Documento incorrecto");
			this.setIdError("no_documento");
			return false;
		}
		if (iproveedor.getRazon_social() == null || iproveedor.getRazon_social().isEmpty()) {
			SessionUtils.addError("Razon social incorrecto");
			this.setIdError("razonSocial");
			return false;
		}
		return true;
	}

	/**
	 * obtiene todos los proveedores.
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarProveedores() throws SAExcepcion {
		listaProveedor = proveedorService.obtenerProveedores();
	}

	/**
	 * Consulta un usuario por su identificador
	 * 
	 * @throws SAExcepcion
	 */
	public final void buscarProveedor() throws SAExcepcion {
		setListaProveedor(proveedorService.obtenerProveedor(iproveedor));
	}

	/**
	 * guardar usuario
	 */
	public final void editarProveedor() {
		try {
			if (this.validarProveedor(iproveedor)) {
				iproveedor.setCod_tipo_documento(Integer.parseInt(iproveedor.getTipoDocumentoSelect()) * -1);
				iproveedor.setCod_ciudad(Integer.parseInt(iproveedor.getCiudadSelect()) * -1);
				proveedorService.editarProveedor(iproveedor);
				SessionUtils.addInfo("Proveedor " + iproveedor.getRazon_social() + " Actualizado correctamente");
				iproveedor = new ProveedorBean();
				this.consultarProveedores();
			}
		} catch (SAExcepcion e) {
			SessionUtils.addError("Error al editar el proveedor: "+ iproveedor.getRazon_social());
			e.printStackTrace();
		}
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public void saveAction() {
		if (listaProveedor != null && !listaProveedor.isEmpty()) {
			for (IProveedorBean iproveedor : listaProveedor) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iproveedor.isEditable()) {
					proveedorService.editarProveedor(iproveedor);
					iproveedor.setEditable(false);
				}
			}
		}
	}

	/**
	 * guardar usuario
	 */
	public final void limpiarFormulario() {
		try {
			iproveedor = new ProveedorBean();
			this.consultarProveedores();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iproveedor
	 * @return
	 */
	public void editarAction(IProveedorBean proveedor) {
		proveedor.setEditable(true);
		proveedor.setTipoDocumentoSelect("-" + proveedor.getCod_tipo_documento());
		proveedor.setCiudadSelect("-" + proveedor.getCod_ciudad());
		this.setIproveedor(proveedor);
	}

	/**
	 * desactiva el modo de edicion de una fila
	 */
	public void cancelarEditarAction(IProveedorBean proveedor) {
		proveedor.setEditable(false);
		this.iproveedor = new ProveedorBean();
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IProveedorBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IProveedorBean iproveedor) {
		try {
			proveedorService.borrarProveedor(iproveedor);
			this.consultarProveedores();
			SessionUtils.addInfo("Proveedor borrado correctamente:" + iproveedor.getRazon_social());
		} catch (Exception e) {
			SessionUtils.addError("Error al borrar el proveedor:" + iproveedor.getRazon_social());
		}
	}

	/**
	 * @return the listaProveedor
	 */
	public final List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;

	}

	/**
	 * @return the listaTipoDocumento
	 */
	public final List<ITipoDocumentoBean> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	/**
	 * @param listaTipoDocumento the listaTipoDocumento to set
	 */
	public final void setListaTipoDocumento(List<ITipoDocumentoBean> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	/**
	 * @return the listaCiudad
	 */
	public final List<ICiudadBean> getListaCiudad() {
		return listaCiudad;
	}

	/**
	 * @param listaCiudad the listaCiudad to set
	 */
	public final void setListaCiudad(List<ICiudadBean> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	/**
	 * @return the iproveedor
	 */
	public final IProveedorBean getIproveedor() {
		return iproveedor;
	}

	/**
	 * @param iproveedor the iproveedor to set
	 */
	public final void setIproveedor(IProveedorBean iproveedor) {
		this.iproveedor = iproveedor;
	}

	
	

}