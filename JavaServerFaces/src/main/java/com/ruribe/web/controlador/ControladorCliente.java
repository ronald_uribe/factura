package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.interfaces.ICiudadBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.ITipoDocumentoBean;

@Named(value = "mbeanCliente")
@Scope("request")
public class ControladorCliente extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	@Inject
	TipoDocumentoService tipoDocumentoService;
	@Inject
	CiudadService ciudadService;
	private IClienteBean iCliente;
	private List<ITipoDocumentoBean> listaTipoDocumento;
	private List<ICiudadBean> listaCiudad;
	private String findcliente;

	public ControladorCliente() {
		iCliente = new ClienteBean();
	}

	/**
	 * Inicializa el formulario de clientes.
	 * 
	 * @return redirige a la pagina de clientes.
	 */
	public String iniciarCliente() {
		// cargar clientes
		try {
			consultarClientes();
			listaCiudad = ciudadService.obtenerCiudad();

			listaTipoDocumento = tipoDocumentoService.obtenerTipoDocumentos();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "CLIENTE";
	}
	
	/**
	 * Obtiene una lista de clientes por razon social
	 */
	public void obtenerClientesByRazonSocial(){
		try {
			listaCliente = clienteService.obtenerClientesByRazonSocial(findcliente);
		}  catch (Exception e) {
			SessionUtils.addError("Error en la busqueda de cliente:" + findcliente);
			e.printStackTrace();
		}
    }

	/**
	 * guardar usuario
	 */
	public final void guardarCliente() {
		try {
			// tomamos el valor del combo y lo convertimos a entero
			if (this.validarCliente(iCliente)) {
				iCliente.setCod_tipo_documento(Integer.parseInt(iCliente.getTipoDocumentoSelect()) * -1);
				iCliente.setCod_ciudad(Integer.parseInt(iCliente.getCiudadSelect()) * -1);
				clienteService.crearCliente(iCliente);
				SessionUtils.addInfo("Cliente " + iCliente.getRazon_social() + " guardado correctamente");
				iCliente = new ClienteBean();
				iCliente.setTipoDocumentoSelect("Seleccionar");
				iCliente.setCiudadSelect("Seleccionar");
				this.buscarCliente();
				this.consultarClientes();
			}
		} catch (Exception e) {
			e.printStackTrace();
			SessionUtils.addError("Error al crear el cliente:" + iCliente.getRazon_social());
		}
	}

	/** validar proveedor */
	public boolean validarCliente(IClienteBean iCliente) {
		if (iCliente.getId_cliente() == null || iCliente.getId_cliente().isEmpty()) {
			SessionUtils.addError("Documento incorrecto");
			return false;
		}
		if (iCliente.getRazon_social() == null || iCliente.getRazon_social().isEmpty()) {
			SessionUtils.addError("Razon social incorrecto");
			return false;
		}
		if (iCliente.getTipoDocumentoSelect() == null || iCliente.getTipoDocumentoSelect().equals("Seleccionar")) {
			SessionUtils.addError("Tipo de documento es obligatorio");
			return false;
		}
		if (iCliente.getCiudadSelect() == null || iCliente.getCiudadSelect().equals("Seleccionar")) {
			SessionUtils.addError("Ciudad es un campo obligatorio");
			return false;
		}

		return true;
	}

	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		if (((String) arg2).length() < 5) {
			throw new ValidatorException(new FacesMessage("Al menos 5 caracteres "));
		}
	}

	/**
	 * Consulta un usuario por su identificador
	 */
	public final void buscarCliente() {
		setListaCliente(clienteService.obtenerCliente(iCliente));
	}

	/**
	 * guardar cliente
	 */
	public final void editarCliente() {
		try {
			if (this.validarCliente(iCliente)) {
				iCliente.setCod_tipo_documento(Integer.parseInt(iCliente.getTipoDocumentoSelect()) * -1);
				iCliente.setCod_ciudad(Integer.parseInt(iCliente.getCiudadSelect()) * -1);
				clienteService.editarCliente(iCliente);
				SessionUtils.addInfo("Se ha modificado correctamente el cliente:" + iCliente.getRazon_social());
				iCliente = new ClienteBean();
				iCliente.setTipoDocumentoSelect("Seleccionar");
				iCliente.setCiudadSelect("Seleccionar");
				this.consultarClientes();
				setEditable(false);
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al modificar el cliente:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public String saveAction() {
		if (listaCliente != null && !listaCliente.isEmpty()) {
			for (IClienteBean iCliente : listaCliente) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iCliente.isEditable()) {
					clienteService.editarCliente(iCliente);
					setEditable(false);
				}
			}
		}
		return null;
	}

	/**
	 * limpiar formulario
	 */
	public final void limpiarFormulario() {
		try {
			iCliente = null;
			iCliente = new ClienteBean();
			iCliente.setTipoDocumentoSelect("Seleccionar");
			iCliente.setCiudadSelect("Seleccionar");
			iCliente.setClienteSelect("Seleccionar");
			setEditable(false);
			setVerListado(false);
			this.consultarClientes();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iCliente
	 * @return
	 */
	public String editarAction(IClienteBean iCliente) {
		setEditable(true);
		iCliente.setTipoDocumentoSelect("-" + iCliente.getCod_tipo_documento());
		iCliente.setCiudadSelect("-" + iCliente.getCod_ciudad());
		this.setiCliente(iCliente);
		return null;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IClienteBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IClienteBean iCliente) {
		try {
			clienteService.borrarCliente(iCliente);
			SessionUtils.addInfo("Cliente borrado:" + iCliente.getRazon_social());
			this.consultarClientes();
		} catch (Exception e) {
			SessionUtils.addError("Error al borrar el Cliente " + iCliente.getRazon_social());
		}
	}

	/**
	 * @return the listaTipoDocumento
	 */
	public final List<ITipoDocumentoBean> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	/**
	 * @param listaTipoDocumento the listaTipoDocumento to set
	 */
	public final void setListaTipoDocumento(List<ITipoDocumentoBean> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	/**
	 * @return the iCliente
	 */
	public IClienteBean getiCliente() {
		return iCliente;
	}

	/**
	 * @param iCliente the iCliente to set
	 */
	public void setiCliente(IClienteBean iCliente) {
		this.iCliente = iCliente;
	}

	/**
	 * @return the listaCiudad
	 */
	public final List<ICiudadBean> getListaCiudad() {
		return listaCiudad;
	}

	/**
	 * @param listaCiudad the listaCiudad to set
	 */
	public final void setListaCiudad(List<ICiudadBean> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public String getFindcliente() {
		return findcliente;
	}

	public void setFindcliente(String findcliente) {
		this.findcliente = findcliente;
	}
	
	

}