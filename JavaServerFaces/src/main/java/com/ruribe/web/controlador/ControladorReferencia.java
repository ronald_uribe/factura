package com.ruribe.web.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.itextpdf.text.DocumentException;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.ReferenciaService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ReferenciaBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;
import com.ruribe.util.bean.interfaces.ITipoProductoBean;
import com.ruribe.web.reportes.pdf.ProductosPDF;

@Named(value = "mbeanReferencia")
@Scope("request")
public class ControladorReferencia implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;
	@Inject
	ProductoService productoService;
	@Inject
	ReferenciaService referenciaService;
	private IReferenciaBean ireferencia;
	private List<IReferenciaBean> listaReferencia;
	private List<ITipoProductoBean> listaTipoProducto;
	private String filtro;

	public ControladorReferencia() {
		ireferencia = new ReferenciaBean();
	}

	/**
	 * Inicializa el formulario de Referencia.
	 * 
	 * @return redirige a la pagina de Referencia.
	 */
	public String iniciarReferencia() {
		try {
			consultarReferencias();
			listaTipoProducto = productoService.obtenerTipoProducto();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "REFERENCIA";
	}

	/**
	 * guardar Referencia
	 */
	public final void guardarReferencia() {
		// tomamos el valor del combo y lo convertimos a entero
		try {
			if (validar()) {
				ireferencia.setTipo_producto(
						getTipoProducto(Integer.parseInt(ireferencia.getCodTipoProductoSelect()) * -1));
				referenciaService.crearReferencia(ireferencia);
				SessionUtils
						.addInfo("La referencia: " + ireferencia.getDescripcion() + " se ha guardado correctamente");
				ireferencia = new ReferenciaBean();
				ireferencia.setCodTipoProductoSelect("Seleccionar");
				consultarReferencias();
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e.getCause());
			System.out.println("ERROR:" + e.getMessage());
			SessionUtils.addError("Error al crear la Referencia " + ireferencia.getDescripcion());
			e.printStackTrace();
		}
	}

	/**
	 * Consultar Referencias
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarReferencias() throws SAExcepcion {
		listaReferencia = referenciaService.obtenerReferencias();
	}
	
	/**
	 * Obtiene una lista de clientes por razon social
	 */
	public void obtenerReferenciasByDescripcion(){
		
		try {
			listaReferencia = referenciaService.obtenerReferenciasByDescripcion(filtro);
		}  catch (Exception e) {
			SessionUtils.addError("Error en la busqueda de Referencias:" + filtro);
			e.printStackTrace();
		}
    }
	
	

	/**
	 * Consulta una Referencia por su identificador
	 */
	public final void buscarReferencia() {
		setListaReferencia(referenciaService.obtenerReferencia(ireferencia));
	}

	/**
	 * guardar producto
	 */
	public final void editarReferencia() {
		try {
			if (validar()) {
				referenciaService.editarReferencia(ireferencia);
				SessionUtils.addInfo("Producto " + ireferencia.getDescripcion() + " Actualizado correctamente");
				ireferencia = new ReferenciaBean();
				this.consultarReferencias();
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e.getCause());
			System.out.println("ERROR:" + e.getMessage());
			SessionUtils.addError("Error al editar el producto " + ireferencia.getDescripcion());
			e.printStackTrace();
		}
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos la Referencia y reiniciamos
	 * el set "editable" a false
	 */
	public void saveAction() {
		if (listaReferencia != null && !listaReferencia.isEmpty()) {
			for (IReferenciaBean ireferencia : listaReferencia) {
				// condicion que permite solo guardar aquellos que se han editado
				if (ireferencia.isEditable()) {
					referenciaService.editarReferencia(ireferencia);
					ireferencia.setEditable(false);
				}
			}
		}
	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param ireferencia
	 */
	public void editarAction(IReferenciaBean referencia) {
		referencia.setEditable(true);
		referencia.setCodTipoProductoSelect("-" + referencia.getTipo_producto().getCod_tipo_producto());
		this.setIreferencia(referencia);
	}

	/**
	 * desactiva el modo de edicion de una fila
	 */
	public void cancelarEditarAction(IReferenciaBean ireferencia) {
		ireferencia.setEditable(false);
	}

	/**
	 * limpiar formulario
	 */
	public final void limpiarFormulario() {
		try {
			ireferencia = new ReferenciaBean();
			this.consultarReferencias();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IProductoBean El objeto a eliminar del medio persistente.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IReferenciaBean ireferencia) {
		try {
			referenciaService.borrarReferencia(ireferencia);
			SessionUtils.addInfo("Referencia: " + ireferencia.getDescripcion() + " Borrada");
			this.consultarReferencias();
		} catch (Exception e) {
			SessionUtils.addError("Error al borrar la Referencia " + ireferencia.getDescripcion());
			e.printStackTrace();
		}
	}

	/**
	 * imprimir productos
	 */
	public void imprimirReferencias() {

		try {

			String ARCHIVO = "catalogoProductos.pdf";
			new ProductosPDF().crearPDF(ARCHIVO, listaReferencia);
			Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_CATALOGO"), ARCHIVO);
		} catch (IOException e) {
			SessionUtils.addError("Error al general la lista de documentos.");
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return the listaReferencia
	 */
	public List<IReferenciaBean> getListaReferencia() {
		return listaReferencia;
	}

	/**
	 * @param listaReferencia the listaReferencia to set
	 */
	public void setListaReferencia(List<IReferenciaBean> listaReferencia) {
		this.listaReferencia = listaReferencia;
	}

	/**
	 * @return the listaTipoProducto
	 */
	public List<ITipoProductoBean> getListaTipoProducto() {
		return listaTipoProducto;
	}

	/**
	 * @param listaTipoProducto the listaTipoProducto to set
	 */
	public void setListaTipoProducto(List<ITipoProductoBean> listaTipoProducto) {
		this.listaTipoProducto = listaTipoProducto;
	}

	/**
	 * retorna un objeto de una lista de tipo IReferenciaBean
	 * 
	 * @param String codigo
	 * @return IReferenciaBean
	 */
	public ITipoProductoBean getTipoProducto(int codigo) {

		for (ITipoProductoBean tipo : listaTipoProducto) {
			if (codigo == tipo.getCod_tipo_producto()) {
				return tipo;
			}
		}
		return null;
	}

	/**
	 * Validar entrada de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		if (ireferencia.getDescripcion() == null || ireferencia.getDescripcion().trim().equals("")) {
			SessionUtils.addError("Nombre de producto es obligatorio ");
			return false;
		}
		if (ireferencia.getPrecio_alquiler() == 0) {
			SessionUtils.addError("Digite un valor valido en el precio de alquiler");
			return false;
		}
		if (ireferencia.getPrecio_reposicion() == 0) {
			SessionUtils.addError("Digite un valor valido en el precio de reposicion");
			return false;
		}
		if (ireferencia.getCodTipoProductoSelect() == null
				|| ireferencia.getCodTipoProductoSelect().equals("Seleccionar")) {
			SessionUtils.addError("Tipo Producto es obligatorio");
			return false;
		}
		return true;
	}

	/**
	 * @return the ireferencia
	 */
	public IReferenciaBean getIreferencia() {
		return ireferencia;
	}

	/**
	 * @param ireferencia the ireferencia to set
	 */
	public void setIreferencia(IReferenciaBean ireferencia) {
		this.ireferencia = ireferencia;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}
	
	

}