package com.ruribe.web.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.itextpdf.text.DocumentException;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.ReposicionService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.ReposicionBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleReposicionBean;
import com.ruribe.util.bean.interfaces.IReposicionBean;
import com.ruribe.web.reportes.pdf.ReposicionPDF;

@Named(value = "mbeanReposicion")
@Scope("request")
public class ControladorReposicion extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	@Inject
	ProductoService productoService;
	@Inject
	ReposicionService reposicionService;
	private IReposicionBean iReposicionBean;
	private List<IReposicionBean> listaReposiciones;
	private boolean mostrarDetalle;
	private boolean onlyread = false;

	public ControladorReposicion() {
		iReposicionBean = new ReposicionBean();
	}

	/**
	 * Inicializa el formulario de reposicion.
	 * 
	 * @return redirige a la pagina de reposicion.
	 */
	public String iniciarReposicion() {
		try {
			iReposicionBean = new ReposicionBean();
			// cargar clientes
			consultarClientes();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "REPOSICION";
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarCliente(ValueChangeEvent evento) {
		setObraSelect("Seleccionar");
		iReposicionBean.setIclienteBean(new ClienteBean());
		iReposicionBean.setIobraBean(new ObraBean());
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			String idclienteSelect = evento.getNewValue().toString();
			// se selecciona el cliente de la lista de clientes.
			iReposicionBean.setIclienteBean(getCliente(idclienteSelect));
			iReposicionBean.getDetalleReposiciones().clear();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarObra(ValueChangeEvent evento) {
		try {
			setObraSelect("Seleccionar");
			iReposicionBean.setIobraBean(new ObraBean());
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idobraSelect = evento.getNewValue().toString();
				// hay que crear el buscar cliente por llave primaria.
				iReposicionBean.setIobraBean(clienteService.obtenerObraPK(Integer.parseInt(idobraSelect)*-1));
				iReposicionBean.setDetalleReposiciones(reposicionService.obtenerProductosPendientesPorCliente(
						iReposicionBean.getIclienteBean().getId_cliente(), idobraSelect));
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarTipoProducto(ValueChangeEvent evento) {
		try {
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				int idTipoProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
				iReposicionBean.setListaProducto(productoService.obtenerProductosPorTipo(idTipoProductoSelect));
			}
			FacesContext.getCurrentInstance().renderResponse();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarProducto(ValueChangeEvent evento) {
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			int idProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
			// obtenemos el objeto producto seleccionado
			iReposicionBean.setProducto(iReposicionBean.getProducto(idProductoSelect));
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	private boolean validar() {
		if (iReposicionBean.getFecha() == null) {
			SessionUtils.addError("seleccione una fecha valida");
			return false;
		}
		if (iReposicionBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("seleccione un cliente");
			return false;
		}
		if (iReposicionBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("seleccione una obra");
			return false;
		}
		boolean devuelve = false; // comprueba si al menos devuelve un producto
		for (IDetalleReposicionBean iDtetalleReposicionBean : iReposicionBean.getDetalleReposiciones()) {
			if (iDtetalleReposicionBean.getCantidad() > iDtetalleReposicionBean.getPendiente()) {
				SessionUtils.addError("La cantidad a reponer no puede ser mayor que la pendiente.");
				return false;
			} else if (iDtetalleReposicionBean.getCantidad() > 0) {
				devuelve = true;
			}
		}
		if (!devuelve) {
			SessionUtils.addError("No ha digitado ninguna cantidad para reponer");
			return false;
		}
		return true;
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public String saveAction() {
		if (listaCliente != null && !listaCliente.isEmpty()) {
			for (IClienteBean iCliente : listaCliente) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iCliente.isEditable()) {
					clienteService.editarCliente(iCliente);
					setEditable(false);
				}
			}
		}
		return null;
	}

	/**
	 * crear remision
	 */
	public void guardarReposicion() {

		try {
			if (this.validar()) {
				if (iReposicionBean.getIdReposicion() == 0) {
					iReposicionBean.setUsuario_creacion(SessionUtils.getUserName());
					iReposicionBean = reposicionService.crearReposicion(iReposicionBean);
					SessionUtils.addInfo("Reposicion guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva Reposicion presione nueva reposicion.");
				}
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al guardar la Reposicion" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Editar remision
	 */
	public void modificarReposicion() {

		try {
			iReposicionBean.setUsuario_creacion(SessionUtils.getUserName());
			reposicionService.editarReposicion(iReposicionBean);
			SessionUtils.addInfo("Reposicion modificada correctamente");
		} catch (Exception e) {
			SessionUtils.addError("Reposicion no modificada:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * reinicia el formulario
	 */
	public void nuevaReposicion() {
		iReposicionBean = new ReposicionBean();
		setClienteSelect("Seleccionar");
		setSelectTransportador("Seleccionar");
		setCodTipoProductoSelect("Seleccionar");
	}

	/**
	 * Imprimir Reposicion
	 * 
	 * @param ReposicionBean
	 */
	public void imprimir(ReposicionBean iReposicionBean) {

		try {
			// se realiza la agrupacion por java al momento de imprimir.
			iReposicionBean = (ReposicionBean) reposicionService
					.consultarReposicionesPDF(iReposicionBean.getIdReposicion());
			if (iReposicionBean.getIdReposicion() != 0) {
				String ARCHIVO = "Reposicion" + iReposicionBean.getIdReposicion() + ".pdf";
				new ReposicionPDF().crearPDF(ARCHIVO, iReposicionBean);
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_REPOSICION"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la Reposicion.");
			}
		} catch (IOException e) {
			SessionUtils.addError("Error al leer la Reposicion.");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * ver reposicion
	 * 
	 * @param iReposicionBean
	 * @return
	 */
	public String verReposicion(IReposicionBean iReposicionBean) {
		try {
			this.iReposicionBean = iReposicionBean;
			// cargar clientes
			consultarClientes();
			setClienteSelect(iReposicionBean.getIclienteBean().getId_cliente());
			setObraSelect(String.valueOf(iReposicionBean.getIobraBean().getIdObra()));
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "MODIFICAR_REPOSICION";
	}

	public void buscarReposiciones() throws SAExcepcion {
		listaReposiciones = reposicionService.consultarReposiciones();
	}

	/**
	 * @return the iReposicionBean
	 */
	public IReposicionBean getiReposicionBean() {
		return iReposicionBean;
	}

	/**
	 * @param iReposicionBean the iReposicionBean to set
	 */
	public void setiReposicionBean(IReposicionBean iReposicionBean) {
		this.iReposicionBean = iReposicionBean;
	}

	/**
	 * habilita el detalle de carrito de remision y bloquea los campos
	 * 
	 * @return
	 */
	public final boolean isMostrarDetalle() {
		if (iReposicionBean.getDetalleReposiciones() != null && iReposicionBean.getDetalleReposiciones().size() > 0) {
			mostrarDetalle = true;
		} else {
			mostrarDetalle = false;
		}
		return mostrarDetalle;
	}

	/**
	 * @param mostrarDetalle the mostrarDetalle to set
	 */
	public final void setMostrarDetalle(boolean mostrarDetalle) {
		this.mostrarDetalle = mostrarDetalle;
	}

	/**
	 * comprueba si la factura esta creada o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (iReposicionBean.getIdReposicion() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}

	/**
	 * @return the listaReposiciones
	 */
	public List<IReposicionBean> getListaReposiciones() {
		return listaReposiciones;
	}

	/**
	 * @param listaReposiciones the listaReposiciones to set
	 */
	public void setListaReposiciones(List<IReposicionBean> listaReposiciones) {
		this.listaReposiciones = listaReposiciones;
	}

}