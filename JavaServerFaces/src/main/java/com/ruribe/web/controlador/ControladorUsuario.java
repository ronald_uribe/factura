package com.ruribe.web.controlador;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.servicio.interfaces.TipoDocumentoService;
import com.ruribe.servicio.interfaces.UsuarioService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.LoginBean;
import com.ruribe.util.bean.User;
import com.ruribe.util.bean.UsuarioBean;
import com.ruribe.util.bean.interfaces.IRoleBean;
import com.ruribe.util.bean.interfaces.IUser;
import com.ruribe.util.bean.interfaces.IUsuarioBean;

@Named(value = "mbeanUsuario")
@Scope("request")
public class ControladorUsuario extends ControladorBase  {

	@Inject
	UsuarioService usuarioService;
	@Inject
	ProveedorService proveedorService;
	@Inject
	TipoDocumentoService tipoDocumentoService;

	private IUser usuario;
	private IUsuarioBean iusuario;
	private List<IUsuarioBean> listaUsuario;
	private List<IRoleBean> roles;
	

	public ControladorUsuario() {
		usuario = new User();
		iusuario = new UsuarioBean();
	}

	/**
	 * Inicializa el formulario de listado de usuarios.
	 * 
	 * @return redirige a la pagina de usuarios.
	 */
	public String iniciarConsultaUsuarios() {
		try {
			this.consultarUsuarios();
		}catch (Exception e) {
			Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, "Error al crear el usuario", e);
			SessionUtils.addError("[Error] al iniciar la carga de usuario");
		}
		return "CONSULTAUSUARIO";
	}

	// logout event, invalidate session
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		SessionUtils.addInfo("Sesion cerrada correctamente");
		return "LOGIN";
	}

	/**
	 * @return the usuario
	 */
	public IUser getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(IUser usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the iusuario
	 */
	public IUsuarioBean getIusuario() {
		return iusuario;
	}

	/**
	 * @param iusuario the iusuario to set
	 */
	public void setIusuario(IUsuarioBean iusuario) {
		this.iusuario = iusuario;
	}

	/**
	 * guardar usuario
	 */
	public final void guardarUsuario() {
		try {
			if (validar()) {
				iusuario.setClave(LoginBean.generateMD5Signature(iusuario.getClave()));
				//usuarioService.borrarUsuario(iusuario);
				usuarioService.crearUsuario(iusuario);
				SessionUtils.addInfo("Usuario creado correctamente: " + iusuario.getUsername());
				iusuario = new UsuarioBean();
			}

		} catch (Exception e) {
			Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, "Error al crear el usuario", e);
			SessionUtils.addError("[Error] al crear el usuario");
		}
	}

	/**
	 * validar proveedor
	 * 
	 * @return boolean
	 */
	private boolean validar() {

		if (iusuario.getNombre() == null || iusuario.getNombre().isEmpty()) {
			SessionUtils.addError("Nombre es un campo obligatorio");
			return false;
		}
		if (iusuario.getApellido() == null || iusuario.getApellido().isEmpty()) {
			SessionUtils.addError("Apellido es un campo obligatorio.");
			return false;
		}
		if (iusuario.getUsername() == null || iusuario.getUsername().isEmpty()) {
			SessionUtils.addError("Usuario es un campo obligatorio.");
			return false;
		}
		try {
			if (usuarioService.validarUsername(iusuario.getUsername())) {
				SessionUtils.addError("Usuario ya registrado, Intente con otro.");
				return false;
			}
		} catch (SAExcepcion e) {
			e.printStackTrace();
		}
		if (iusuario.getClave() == null || iusuario.getClave().isEmpty()) {
			SessionUtils.addError("Clave es un campo obligatorio.");
			return false;
		}

		return true;
	}

	/**
	 * valida que el usuario existe.
	 * 
	 * @param arg0
	 * @param arg1
	 * @param valor
	 * @throws ValidatorException
	 * @throws SAExcepcion
	 */
	public void validarUsername(FacesContext arg0, UIComponent arg1, Object valor)
			throws ValidatorException, SAExcepcion {
		if (usuarioService.validarUsername((String) valor)) {
			throw new ValidatorException(
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username ya registrado, intente con otro.", ""));
		}
	}

	/**
	 * Consulta usuario todos los usuarios
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarUsuarios() throws SAExcepcion {
		listaUsuario = usuarioService.obtenerUsuarios();
	}
	
	
	public final void obtenerRoles() throws SAExcepcion {
		roles = usuarioService.obtenerRoles();
	}

	/**
	 * Consulta un usuario por su identificador
	 * 
	 * @throws SAExcepcion
	 */
	public final void buscarUsuario() throws SAExcepcion {
		setListaUsuario(usuarioService.obtenerUsuario(iusuario));
	}

	/**
	 * guardar usuario
	 */
	public final void editarUsuario() {
		usuarioService.editarUsuario(iusuario);
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public void saveAction() {
		try {
			if (listaUsuario != null && !listaUsuario.isEmpty()) {
				for (IUsuarioBean iusuario : listaUsuario) {
					// condicion que permite solo guardar aquellos que se han editado
					if (iusuario.isEditable()) {
						usuarioService.editarUsuario(iusuario);
						iusuario.setEditable(false);
					}
				}
			}
		} catch (Exception e) {
			Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, "Error al editar el usuario",
					e.getMessage());
			SessionUtils.addError("Error al editar el usuario");
		}
	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iusuario
	 * @return
	 */
	public String editarAction(IUsuarioBean iusuario) {
		iusuario.setEditable(true);
		return null;
	}

	/**
	 * desactiva el modo de edicion de una fila
	 */
	public String cancelarEditarAction(IUsuarioBean iusuario) {
		iusuario.setEditable(false);
		return null;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IUsuarioBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IUsuarioBean iusuario) {
		try {
			usuarioService.borrarUsuario(iusuario);
			SessionUtils.addInfo("Usuario borrado correctamente");
			this.consultarUsuarios();
		} catch (Exception e) {
			Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, "Error al Borrar el usuario" 
					+iusuario.getUsername(),
					e.getMessage());
			SessionUtils.addError("Error al borrar el usuario:" +iusuario.getUsername());
		}
	}

	/**
	 * consulta un usuario pasado por parametro
	 * 
	 * @param IUsuarioBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void verDetalleUsuario(IUsuarioBean iusuario) {
		// Pendiente de implementar
	}

	public final void odenarTabla(IUsuarioBean iusuario) {
		// Pendiente de implementar
		// https://www.mkyong.com/jsf2/jsf-2-datatable-sorting-example/
		// http://juan-andres-programas.blogspot.com.es/2013/01/sistema-de-facturacion-e-inventario-en.html
	}

	/**
	 * @return the listaUsuario
	 */
	public final List<IUsuarioBean> getListaUsuario() {
		return listaUsuario;
	}

	/**
	 * @param listaUsuario the listaUsuario to set
	 */
	public void setListaUsuario(List<IUsuarioBean> listaUsuario) {
		this.listaUsuario = listaUsuario;

	}
	
	public List<IRoleBean> getRoles() {
		try {
			roles= usuarioService.obtenerRoles();
		} catch (SAExcepcion e) {
			SessionUtils.addError("Error al borrar roles");
			e.printStackTrace();
		}
		return roles;
	}

	public static String writePDF() {
		return null;
	}

	public static void main(String arg[]) {

		ControladorUsuario.writePDF();
	}


	
	

}