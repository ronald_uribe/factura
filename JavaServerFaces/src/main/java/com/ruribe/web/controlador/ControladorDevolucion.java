package com.ruribe.web.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.itextpdf.text.DocumentException;
import com.ruribe.servicio.interfaces.DevolucionService;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.TransportadorService;
import com.ruribe.util.Propiedades;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.Utilidades;
import com.ruribe.util.bean.ClienteBean;
import com.ruribe.util.bean.DevolucionBean;
import com.ruribe.util.bean.ObraBean;
import com.ruribe.util.bean.interfaces.IClienteBean;
import com.ruribe.util.bean.interfaces.IDetalleDevolucionBean;
import com.ruribe.util.bean.interfaces.IDevolucionBean;
import com.ruribe.web.reportes.pdf.DevolucionAgrupadoPDF;
import com.ruribe.web.reportes.pdf.DevolucionPDF;
import com.ruribe.web.reportes.pdf.SaldoPDF;

@Named(value = "mbeanDevolucion")
@Scope("request")
public class ControladorDevolucion extends ControladorBase implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;

	@Inject
	ProductoService productoService;
	@Inject
	DevolucionService devolucionService;
	@Inject
	TransportadorService transportadorService;
	private IDevolucionBean iDevolucionBean;
	private List<IDevolucionBean> listaDevoluciones;
	private boolean mostrarDetalle;
	private boolean onlyread = false;
	private final Logger log = Logger.getLogger(ControladorDevolucion.class.getName());

	public ControladorDevolucion() {
		iDevolucionBean = new DevolucionBean();
	}

	/**
	 * Inicializa el formulario de devolucion.
	 * 
	 * @return redirige a la pagina de devolucion.
	 */
	public String iniciarDevolucion() {
		try {
			iDevolucionBean = new DevolucionBean();
			setClienteSelect("Seleccionar");
			setSelectTransportador("Seleccionar");
			setCodTipoProductoSelect("Seleccionar");
			// cargar clientes
			consultarClientes();
			listaTransportador = transportadorService.obtenerTransportadores();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "DEVOLUCION";
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarCliente(ValueChangeEvent evento) {
		setObraSelect("Seleccionar");
		this.iDevolucionBean.getDetalleDevoluciones().clear();
		iDevolucionBean.setIclienteBean(new ClienteBean());
		iDevolucionBean.setIobraBean(new ObraBean());
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			String idclienteSelect = evento.getNewValue().toString();
			// se selecciona el cliente de la lista de clientes.
			iDevolucionBean.setIclienteBean(getCliente(idclienteSelect));
			// iDevolucionBean.setDetalleDevoluciones(devolucionService.obtenerProductosPendientesPorCliente(idclienteSelect));

		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarObra(ValueChangeEvent evento) {
		try {
			setObraSelect("Seleccionar");
			iDevolucionBean.setIobraBean(new ObraBean());
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				String idobraSelect = evento.getNewValue().toString();
				// hay que crear el buscar cliente por llave primaria.

				iDevolucionBean.setIobraBean(clienteService.obtenerObraPK(Integer.parseInt(idobraSelect)*-1));
				iDevolucionBean.setDetalleDevoluciones(devolucionService.obtenerProductosPendientesPorCliente(
						iDevolucionBean.getIclienteBean().getId_cliente(),  
						Integer.parseInt(idobraSelect)*-1));
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarTipoProducto(ValueChangeEvent evento) {
		try {
			if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
				int idTipoProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
				iDevolucionBean.setListaProducto(productoService.obtenerProductosPorTipo(idTipoProductoSelect));
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Selecciona un cliente.
	 * 
	 * @param evento
	 */
	public void seleccionarProducto(ValueChangeEvent evento) {
		if (evento.getNewValue() != null && !"Seleccionar".equals(evento.getNewValue())) {
			int idProductoSelect = Integer.parseInt((String) evento.getNewValue()) * -1;
			// obtenemos el objeto producto seleccionado
			iDevolucionBean.setProducto(iDevolucionBean.getProducto(idProductoSelect));
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	private boolean validar() {
		if (iDevolucionBean.getIdDevolucion() != 0 && devolucionService.devolucionIsPrinted(iDevolucionBean.getIdDevolucion())) {
			SessionUtils.addError("La devolución ya ha sido impresa y no se puede modificar");
			return false;
		}else if (iDevolucionBean.getFecha() == null) {
			SessionUtils.addError("seleccione una fecha valida");
			return false;
		}else if (iDevolucionBean.getIclienteBean().getId_cliente() == null) {
			SessionUtils.addError("seleccione un cliente");
			return false;
		}else if (iDevolucionBean.getIobraBean().getIdObra() == 0) {
			SessionUtils.addError("seleccione una obra");
			return false;
		}else if (getSelectTransportador() == null || getSelectTransportador().equals("Seleccionar")) {
			SessionUtils.addError("seleccione el tranportador");
			return false;
		}
		if (iDevolucionBean.getIdDevolucion() == 0) {
			boolean devuelve = false; // comprueba si al menos devuelve un producto
			for (IDetalleDevolucionBean iDtetalleDevolucionBean : iDevolucionBean.getDetalleDevoluciones()) {
				if (iDtetalleDevolucionBean.getCantidad() > iDtetalleDevolucionBean.getPendiente()) {
					SessionUtils.addError("La cantidad a devolver no puede ser mayor que la pendiente.");
					return false;
				} else if (iDtetalleDevolucionBean.getCantidad() > 0) {
					devuelve = true;
				}
			}
			if (!devuelve) {
				SessionUtils.addError("No ha digitado ninguna cantidad para devolver");
				return false;
			}
		}	
		return true;
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el usuario y reiniciamos el
	 * set "editable" a false
	 * 
	 * @return
	 */
	public String saveAction() {
		if (listaCliente != null && !listaCliente.isEmpty()) {
			for (IClienteBean iCliente : listaCliente) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iCliente.isEditable()) {
					clienteService.editarCliente(iCliente);
					setEditable(false);
				}
			}
		}
		return null;
	}

	/**
	 * crear devolucion
	 */
	public void guardarDevolucion() {

		try {
			if (this.validar()) {
				if (iDevolucionBean.getIdDevolucion() == 0) {
					iDevolucionBean.getItransportadorBean().setNo_documento(getSelectTransportador());
					iDevolucionBean.setUsuario_creacion(SessionUtils.getUserName());
					iDevolucionBean.setEstado(true);
					iDevolucionBean.setPrinted(false);
					iDevolucionBean = devolucionService.crearDevolucion(iDevolucionBean);
					SessionUtils.addInfo("Devolucion guardada correctamente");
				} else {
					SessionUtils.addError("Para crear una nueva devolucion presione nueva devolucion.");
				}
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al crear la Devolucion");
			log.log(Level.SEVERE, e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Editar devolucion
	 */
	public void modificarDevolucion() {

		try {
			if (validar()) {
				iDevolucionBean.getItransportadorBean().setNo_documento(getSelectTransportador());
				iDevolucionBean.setUsuario_creacion(SessionUtils.getUserName());
				devolucionService.editarDevolucion(iDevolucionBean);
				SessionUtils.addInfo("Devolución modificada correctamente");
			}
		} catch (Exception e) {
			SessionUtils.addError("Error al modificar la devolución:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * reinicia el formulario
	 */
	public void nuevaDevolucion() {
		iDevolucionBean = new DevolucionBean();
		setClienteSelect("Seleccionar");
		setSelectTransportador("Seleccionar");
		setCodTipoProductoSelect("Seleccionar");
	}

	/**
	 * Imprimir Devolucion
	 * 
	 * @param DevolucionBean
	 */
	public void imprimir(DevolucionBean iDevolucionBean) {

		try {
			// se realiza la agrupacion por java al momento de imprimir.
			iDevolucionBean = (DevolucionBean) devolucionService
					.consultarDevolucionesPDF(iDevolucionBean.getIdDevolucion());
			if (iDevolucionBean.getIdDevolucion() != 0) {
				devolucionService.marcarDevolucionImpresa(iDevolucionBean.getIdDevolucion());
				String ARCHIVO = "Devolucion" + iDevolucionBean.getIdDevolucion() + ".pdf";
				new DevolucionPDF().crearPDF(ARCHIVO, iDevolucionBean);
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_DEVOLUCION"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la devolucion.");
			}
		} catch (IOException e) {
			SessionUtils.addError("Error al leer la Devolucion.");
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Imprimir Devolucion agrupado por codigo interno
	 * 
	 * @param DevolucionBean
	 */
	public void imprimirAgrupado(DevolucionBean iDevolucionBean) {

		try {
			// se realiza la agrupacion por java al momento de imprimir.
			iDevolucionBean = (DevolucionBean) devolucionService
					.consultarDevolucionesPDF(iDevolucionBean.getIdDevolucion());
			if (iDevolucionBean.getIdDevolucion() != 0) {
				devolucionService.marcarDevolucionImpresa(iDevolucionBean.getIdDevolucion());
				String ARCHIVO = "Devolucion_" + iDevolucionBean.getIdDevolucion() + ".pdf";
				new DevolucionAgrupadoPDF().crearPDF(ARCHIVO, iDevolucionBean);
				Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_DEVOLUCION"), ARCHIVO);
			} else {
				SessionUtils.addError("Primero debe guardar la devolucion.");
			}
		} catch (IOException e) {
			SessionUtils.addError("Error al leer la Devolucion.");
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Imprimir Devolucion
	 * 
	 * @param DevolucionBean
	 */
	public void imprimirSaldo(DevolucionBean iDevolucionBean) {

		try {
			// se realiza la agrupacion por java al momento de imprimir.
			// iDevolucionBean=(DevolucionBean)
			// devolucionService.consultarDevolucionesPDF(iDevolucionBean.getIdDevolucion());
			String ARCHIVO = "Saldo" + iDevolucionBean.getIobraBean().getIclientebean().getRazon_social() + ".pdf";
			new SaldoPDF().crearPDF(ARCHIVO, iDevolucionBean);
			Utilidades.descargarPDF(Propiedades.leerPropiedad("SHARED_SALDO"), ARCHIVO);
		} catch (IOException e) {
			SessionUtils.addError("Error al leer saldo de equipo.");
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * buscar devoluciones
	 * 
	 * @throws SAExcepcion
	 */
	public void buscarDevoluciones() throws SAExcepcion {
		listaDevoluciones = devolucionService.consultarDevoluciones();

	}

	/**
	 * @return the iDevolucionBean
	 */
	public IDevolucionBean getiDevolucionBean() {
		return iDevolucionBean;
	}

	/**
	 * @param iDevolucionBean the iDevolucionBean to set
	 */
	public void setiDevolucionBean(IDevolucionBean iDevolucionBean) {
		this.iDevolucionBean = iDevolucionBean;
	}

	/**
	 * habilita el detalle de carrito de devolucion y bloquea los campos
	 * 
	 * @return
	 */
	public final boolean isMostrarDetalle() {
		if (iDevolucionBean.getDetalleDevoluciones() != null && iDevolucionBean.getDetalleDevoluciones().size() > 0) {
			mostrarDetalle = true;
		} else {
			mostrarDetalle = false;
		}

		return mostrarDetalle;
	}

	/**
	 * @param mostrarDetalle the mostrarDetalle to set
	 */
	public final void setMostrarDetalle(boolean mostrarDetalle) {
		this.mostrarDetalle = mostrarDetalle;
	}

	/**
	 * comprueba si la factura esta creada o no.
	 * 
	 * @return the onlyread
	 */
	public boolean isOnlyread() {
		if (iDevolucionBean.getIdDevolucion() == 0) {
			onlyread = false;
		} else {
			onlyread = true;
		}
		return onlyread;
	}

	public String verDevolucion(IDevolucionBean iDevolucionBean) {
		try {
			this.iDevolucionBean = iDevolucionBean;
			// cargar clientes
			consultarClientes();
			listaTransportador = transportadorService.obtenerTransportadores();
			setClienteSelect(iDevolucionBean.getIclienteBean().getId_cliente());
			setObraSelect(String.valueOf(iDevolucionBean.getIobraBean().getIdObra()*-1));
			setSelectTransportador(iDevolucionBean.getItransportadorBean().getNo_documento());
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "MODIFICAR_DEVOLUCION";
	}

	public String verDetalleSaldos(IDevolucionBean devolucion) {
		try {
			this.iDevolucionBean = devolucion;
			iDevolucionBean.setDetalleDevoluciones(devolucionService.obtenerProductosPendientesPorCliente(
					devolucion.getIobraBean().getIclientebean().getId_cliente(),
					devolucion.getIobraBean().getIdObra()));
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "DETALLE_SALDOS";
	}

	/**
	 * @param onlyread the onlyread to set
	 */
	public void setOnlyread(boolean onlyread) {
		this.onlyread = onlyread;
	}

	/**
	 * @return the listaDevoluciones
	 */
	public List<IDevolucionBean> getListaDevoluciones() {
		return listaDevoluciones;
	}

	/**
	 * @param listaDevoluciones the listaDevoluciones to set
	 */
	public void setListaDevoluciones(List<IDevolucionBean> listaDevoluciones) {
		this.listaDevoluciones = listaDevoluciones;
	}

}