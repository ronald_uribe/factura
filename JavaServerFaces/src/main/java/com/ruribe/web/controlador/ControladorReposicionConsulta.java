package com.ruribe.web.controlador;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.ReposicionService;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.ReposicionConsultaBean;
import com.ruribe.util.bean.interfaces.IReposicionConsultaBean;
 
/**
 * @author ruribe
 */

@Named(value = "mbeanReposicionConsulta")
@Scope("request")
public class ControladorReposicionConsulta extends ControladorBase implements Serializable {
 
	private IReposicionConsultaBean consultaBean;
	
	/**
	 * constructor.
	 */
	public ControladorReposicionConsulta() {
		consultaBean= new ReposicionConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	ReposicionService reposicionService;
	
	
	/**
	 * Inicializa el formulario de reposicion.
	 * @return redirige a la pagina de reposicion.
	 */
	public String iniciarConsultarReposicion(){
		try {
		//cargar clientes
			consultarClientes();
			//cargamos las 10 ultimas reposiciones
			consultaBean.setListaReposiciones(reposicionService.consultarReposiciones(consultaBean));
			if (consultaBean.getListaReposiciones().isEmpty()){
				SessionUtils.addError(FacesMessage.SEVERITY_INFO,"No hay nignuna Reposicion creada");	
			}
		}catch(Exception e)	{
			SessionUtils.addError("No hay datos para esta consulta");	
		}
		return "CONSULTAR_REPOSICION";
	}
	
	/**
	 * obtiene las reposiciones segun filtros de busqueda
	 */
	public String buscarReposiciones(){
		try {
		//cargar clientes
			consultarClientes();
			consultaBean.setIdCliente(clienteSelect);
			consultaBean.setListaReposiciones(reposicionService.consultarReposiciones(consultaBean));
			if (consultaBean.getListaReposiciones().isEmpty()){
				SessionUtils.addError(FacesMessage.SEVERITY_INFO,"No hay datos para esta consulta");	
			}
		}catch(Exception e)	{
			SessionUtils.addError("No hay datos para esta consulta");	
		}
		return "CONSULTAR_REPOSICION";
	}

	/**
	 * @return the consultaBean
	 */
	public IReposicionConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(IReposicionConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

}