/**
 * Example written by Bruno Lowagie in answer to the following question:
 * http://stackoverflow.com/questions/31146249/how-to-set-up-a-table-display-in-itextpdf
 */
package com.ruribe.web.controlador;
/*
 * Example written in answer to the SO question:
 * http://stackoverflow.com/questions/39154089
 */
 
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RowBackground implements PdfPCellEvent {
	static String DEST;
	
	
        public void createPdf(String dest) throws IOException, DocumentException {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(dest));
            document.open();
            Font titleFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 11, BaseColor.BLACK);
            Paragraph docTitle = new Paragraph("UCSC Direct - Direct Payment Form", titleFont);
            document.add(docTitle);
            Font subtitleFont = FontFactory.getFont("Times Roman", 9, BaseColor.BLACK);
            Paragraph subTitle = new Paragraph("(not to be used for reimbursement of services)", subtitleFont);
            document.add(subTitle);
            Font importantNoticeFont = FontFactory.getFont("Courier", 9, BaseColor.RED);
            Paragraph importantNotice = new Paragraph("Important: Form must be filled out in Adobe Reader or Acrobat Professional 8.1 or above. To save completed forms, Acrobat Professional is required. For technical and accessibility assistance, contact the Campus Controller's Office.", importantNoticeFont);
            document.add(importantNotice);
     
            PdfPTable table = new PdfPTable(10); // the arg is the number of columns
            PdfPCell cell = new PdfPCell(docTitle);
            cell.setColspan(3);
            cell.setBorder(PdfPCell.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            PdfPCell cellCaveat = new PdfPCell(subTitle);
            cellCaveat.setColspan(2);
            cellCaveat.setBorder(PdfPCell.NO_BORDER);
            table.addCell(cellCaveat);
            PdfPCell cellImportantNote = new PdfPCell(importantNotice);
            cellImportantNote.setColspan(5);
            cellImportantNote.setBorder(PdfPCell.NO_BORDER);
            table.addCell(cellImportantNote);
            document.add(table);
     
            document.add(new Paragraph(20, "This is the same table, created differently", subtitleFont));
            table = new PdfPTable(3);
            table.setWidths(new int[]{3, 2, 5});
            cell.setColspan(1);
            table.addCell(cell);
            cellCaveat.setColspan(1);
            table.addCell(cellCaveat);
            cellImportantNote.setColspan(1);
            table.addCell(cellImportantNote);
            document.add(table);
     
            
            
            
            
         // inner table 1
            PdfPTable innertable = new PdfPTable(5);
            innertable.setWidths(new int[]{8, 12, 1, 4, 12});
            // first row
            // column 1
            cell = new PdfPCell(new Phrase("Record Ref:"));
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
            // column 2
            cell = new PdfPCell(new Phrase("GN Staff"));
            cell.setPaddingLeft(2);
            innertable.addCell(cell);
            // column 3
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
            // column 4
            cell = new PdfPCell(new Phrase("Date: "));
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
            // column 5
            cell = new PdfPCell(new Phrase("30/4/2015"));
            cell.setPaddingLeft(2);
            innertable.addCell(cell);
            // spacing
            cell = new PdfPCell();
            cell.setColspan(5);
            cell.setFixedHeight(3);
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
              
            
            document.close();
            Desktop d = Desktop.getDesktop();  
            d.open(new File(DEST));
        }
        
        public void cellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] canvas) {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3,
                rect.getHeight() - 3, 4);
            cb.stroke();
        }
     


    /**
     * Main method.
     * @param    args    no arguments needed
     * @throws DocumentException 
     * @throws IOException 
     * @throws SQLException
     */
    public static void main(String[] args) throws DocumentException, IOException {
    	String path = new File(".").getCanonicalPath();
    	DEST = path + "/itext-test-file333.pdf";       	
        
    	File file = new File(DEST);
        file.getParentFile().mkdirs();
        new RowBackground().createPdf(DEST);
    }
}