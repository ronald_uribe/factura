package com.ruribe.web.controlador;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.CiudadService;
import com.ruribe.servicio.interfaces.ProductoService;
import com.ruribe.servicio.interfaces.ProveedorService;
import com.ruribe.servicio.interfaces.ReferenciaService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.ProductoBean;
import com.ruribe.util.bean.interfaces.IProductoBean;
import com.ruribe.util.bean.interfaces.IProveedorBean;
import com.ruribe.util.bean.interfaces.IReferenciaBean;

@Named(value = "mbeanProducto")
@Scope("request")
public class ControladorProducto implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5260116730324981516L;
	@Inject
	ProductoService productoService;
	@Inject
	ReferenciaService referenciaService;
	@Inject
	ProveedorService tipoDocumentoService;
	@Inject
	CiudadService ciudadService;
	@Inject
	ProveedorService proveedorService;
	private IProductoBean iproducto;
	private List<IProductoBean> listaProducto;
	private List<IReferenciaBean> listaReferencia;
	private List<IProveedorBean> listaProveedor;
	private String filtro;

	private IProductoBean selectedPro;

	public IProductoBean getSelectedPro() {
		try {
			consultarProductos();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return selectedPro;
	}

	public void setSelectedPro(IProductoBean selectedPro) {
		this.selectedPro = selectedPro;
	}

	public ControladorProducto() {
		iproducto = new ProductoBean();
	}

	/**
	 * @return the iproducto
	 */
	public final IProductoBean getIproducto() {
		return iproducto;
	}

	/**
	 * @param iproducto the iproducto to set
	 */
	public final void setIproducto(IProductoBean iproducto) {
		this.iproducto = iproducto;
	}

	/**
	 * Inicializa el formulario de Producto.
	 * 
	 * @return redirige a la pagina de producto.
	 */
	public String iniciarProducto() {
		try {
			consultarProductos();
			listaProveedor = proveedorService.obtenerProveedores();
			listaReferencia = referenciaService.obtenerReferencias();
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "PRODUCTO";
	}
	
	/**
	 * Obtiene una lista de clientes por razon social
	 */
	public void obtenerProductosByDescripcion(){
		
		try {
			listaProducto = productoService.obtenerProductosByDescripcion(filtro);
		}  catch (Exception e) {
			SessionUtils.addError("Error en la busqueda de Referencias:" + filtro);
			e.printStackTrace();
		}
    }


	/**
	 * guardar producto
	 */
	public final void guardarProducto() {
		// tomamos el valor del combo y lo convertimos a entero
		try {
			if (validar()) {
				iproducto.setProveedor(getProveedor(iproducto.getCodProveedorSelect()));
				iproducto.setReferenciaBean(getReferencia(Integer.parseInt(iproducto.getCodReferenciaSelect()) * -1));
				// asignamos la descripcion de manera automatica
				iproducto.setDescripcion(iproducto.getReferenciaBean().getDescripcion() + " "
						+ iproducto.getProveedor().getCodigo_proveedor());
				iproducto.setTipo_producto(iproducto.getReferenciaBean().getTipo_producto());
				productoService.crearProducto(iproducto);
				SessionUtils.addInfo("El producto: " + iproducto.getDescripcion() + " se ha guardado correctamente");
				iproducto = new ProductoBean();
				iproducto.setCodProveedorSelect("Seleccionar");
				iproducto.setCodReferenciaSelect("Seleccionar");
				consultarProductos();
			}

		} catch (Exception e) {
			System.out.println("ERROR:" + e.getCause());
			System.out.println("ERROR:" + e.getMessage());
			SessionUtils.addError("Error al crear el Producto " + iproducto.getDescripcion());
			e.printStackTrace();
		}
	}

	/**
	 * Consulta producto todos los productos
	 * 
	 * @throws SAExcepcion
	 */
	public final void consultarProductos() throws SAExcepcion {
		listaProducto = productoService.obtenerProductos();
	}

	/**
	 * Consulta un producto por su identificador
	 * 
	 * @throws SAExcepcion
	 */
	public final void buscarProducto() throws SAExcepcion {
		setListaProducto(productoService.obtenerProducto(iproducto));
	}

	/**
	 * guardar producto
	 */
	public final void editarProducto() {
		productoService.editarProducto(iproducto);
	}

	/**
	 * Obtenemos todos los valores de la tabla, editamos el producto y reiniciamos
	 * el set "editable" a false
	 * 
	 * @return
	 */
	public String saveAction() {
		if (listaProducto != null && !listaProducto.isEmpty()) {
			for (IProductoBean iproducto : listaProducto) {
				// condicion que permite solo guardar aquellos que se han editado
				if (iproducto.isEditable()) {
					productoService.editarProducto(iproducto);
					iproducto.setEditable(false);
				}
			}
		}
		return null;
	}

	/**
	 * Activa el modo de edicion de una fila
	 * 
	 * @param iproducto
	 * @return
	 */
	public String editarAction(IProductoBean iproducto) {
		iproducto.setEditable(true);
		return null;
	}

	/**
	 * desactiva el modo de edicion de una fila
	 */
	public String cancelarEditarAction(IProductoBean iproducto) {
		iproducto.setEditable(false);
		return null;
	}

	/**
	 * Elimina del almacenamiento persistente los datos del objeto pasado por
	 * parámetro.
	 * 
	 * @param IProductoBean El objeto a eliminar del medio persistente.
	 * @return el objeto eliminado.
	 * @see com.ruribe.web.controlador#deleteAction(java.lang.Object)
	 */
	public final void deleteAction(IProductoBean iproducto) {
		try {
			productoService.borrarProducto(iproducto);
			SessionUtils.addInfo("Producto: " + iproducto.getDescripcion() + " desvinculado correctamente");
			this.consultarProductos();
		} catch (Exception e) {
			SessionUtils.addError("Error al desvincular el producto: " + iproducto.getDescripcion());
			e.printStackTrace();
		}
	}

	/**
	 * retorna un objeto de una lista de tipo IReferenciaBean
	 * 
	 * @param String codigo
	 * @return IReferenciaBean
	 */
	public IReferenciaBean getReferencia(int codigo) {

		for (IReferenciaBean ref : listaReferencia) {
			if (codigo == ref.getCodigo_interno()) {
				return ref;
			}
		}
		return null;
	}

	/**
	 * retorna un objeto de una lista de tipo IProveedorBean
	 * 
	 * @param String codigo
	 * @return IProveedorBean
	 */
	public IProveedorBean getProveedor(String codigo) {

		for (IProveedorBean ref : listaProveedor) {
			if (codigo.equals(ref.getNo_documento())) {
				return ref;
			}
		}
		return null;
	}

	/**
	 * Validar entrada de formulario.
	 * 
	 * @return
	 */
	private boolean validar() {
		if (iproducto.getCodReferenciaSelect() == null || iproducto.getCodReferenciaSelect().equals("Seleccionar")) {
			SessionUtils.addError("Poducto es un campo obligatorio");
			return false;
		}
		if (iproducto.getCodProveedorSelect() == null || iproducto.getCodProveedorSelect().equals("Seleccionar")) {
			SessionUtils.addError("Proveedor es campo obligatorio");
			return false;
		}
		return true;
	}

	/**
	 * @param listaProveedor the listaProveedor to set
	 */
	public final void setListaProveedor(List<IProveedorBean> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	/**
	 * @return the listaTipoProducto
	 */
	public final List<IProveedorBean> getListaProveedor() {
		return listaProveedor;
	}

	/**
	 * @return the listaProducto
	 */
	public final List<IProductoBean> getListaProducto() {
		return listaProducto;

	}

	/**
	 * @param listaProducto the listaProducto to set
	 */
	public final void setListaProducto(List<IProductoBean> listaProducto) {
		this.listaProducto = listaProducto;
	}

	/**
	 * @return the listaReferencia
	 */
	public List<IReferenciaBean> getListaReferencia() {
		return listaReferencia;
	}

	/**
	 * @param listaReferencia the listaReferencia to set
	 */
	public void setListaReferencia(List<IReferenciaBean> listaReferencia) {
		this.listaReferencia = listaReferencia;
	}

	public IProductoBean getProducto(Integer id) {
		if (id == null) {
			throw new IllegalArgumentException("no id provided");
		}
		for (IProductoBean p : this.listaProducto) {
			if (id.equals(p.getCodigo_producto())) {
				return p;
			}
		}
		return null;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}
	
	

}