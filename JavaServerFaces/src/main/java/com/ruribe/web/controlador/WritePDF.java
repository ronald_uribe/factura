package com.ruribe.web.controlador;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
 
public class WritePDF{
 
public static String imprimirPDF() {
           
        Document document = new Document();
        try {
            System.out.println("prueba 2222222222");
 
         // Se crea el documento
            String path = new File(".").getCanonicalPath();
   	     String DEST = path + "/"+"test";       	
   	     File file = new File(DEST);
   	     file.getParentFile().mkdirs();
   	        
   	     	PdfWriter.getInstance(document, new FileOutputStream(DEST));
   	        document.open();
   	        document.close();
   	        Desktop d = Desktop.getDesktop();  
   	        d.open(new File(DEST));
   	        
            // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
         //   FileOutputStream ficheroPdf = new FileOutputStream("C://Users/99GU6544/Pictures/fichero3.pdf");
 
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(document,new FileOutputStream(DEST)).setInitialLeading(20);
 
            // Se abre el documento.
            document.open();
           
            
            Paragraph paragraphHello = new Paragraph();
            paragraphHello.add("hola iText paragraph!");
            paragraphHello.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(paragraphHello);
            document.add(new Paragraph("Esto es el primer párrafo, normalito"));
 
            document.add(new Paragraph("Este es el segundo y tiene una fuente rara",
                        FontFactory.getFont("arial",   // fuente
                        22,                            // tamaño
                        Font.ITALIC,                   // estilo
                        BaseColor.CYAN)));             // color
          
            try
            {
                  Image foto = Image.getInstance("../logo.png");
                  foto.scaleToFit(100, 100);
                  foto.setAlignment(Chunk.ALIGN_MIDDLE);
                  document.add(foto);
            }
            catch ( Exception e )
            {
                  e.printStackTrace();
            }
           
            PdfPTable tabla = new PdfPTable(3);
            for (int i = 0; i < 15; i++)
            {
                  tabla.addCell("celda " + i);
            }
            document.add(tabla);
            document.close();
           
            System.out.println("prueba 44444444444444444");
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+"C://Users/99GU6544/Pictures/fichero3.pdf");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("prueba 11111111111" + e.getMessage());
        }catch (DocumentException e) {
            e.printStackTrace();
            System.out.println("prueba 11111111111" + e.getMessage());
        } catch (IOException e) {
                  e.printStackTrace();
                  System.out.println("prueba 11111111111" + e.getMessage());
            }
            return null;
    }
 
      public static void main (String arg[]){
            WritePDF.imprimirPDF();
           
      }
}