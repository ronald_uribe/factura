package com.ruribe.web.controlador;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.ruribe.servicio.interfaces.DevolucionService;
import com.ruribe.util.SAExcepcion;
import com.ruribe.util.SessionUtils;
import com.ruribe.util.bean.DevolucionConsultaBean;
import com.ruribe.util.bean.interfaces.IDevolucionConsultaBean;

/**
 * @author ruribe
 */

@Named(value = "mbeanDevolucionConsulta")
@Scope("request")
public class ControladorDevolucionConsulta extends ControladorBase implements Serializable {

	private IDevolucionConsultaBean consultaBean;

	/**
	 * constructor.
	 */
	public ControladorDevolucionConsulta() {
		consultaBean = new DevolucionConsultaBean();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	DevolucionService devolucionService;

	/**
	 * Inicializa el formulario de reposicion con las 10 ultimas devoluciones.
	 * 
	 * @return redirige a la pagina de reposicion.
	 */
	public String iniciarConsultarDevolucion() {
		try {
			// cargar clientes
			consultarClientes();
			// cargamos las 10 ultimas devoluciones
			consultaBean.setListaDevoluciones(devolucionService.consultarDevoluciones(consultaBean));
			if (consultaBean.getListaDevoluciones().isEmpty()) {
				SessionUtils.addError("No hay nignuna Devolucion creada");
			}
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "CONSULTAR_DEVOLUCION";
	}

	public String iniciarConsultaSaldosPendientes() {
		try {
			consultaBean.setListaDevoluciones(devolucionService.consultarAllSaldosPendientes());
		} catch (SAExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "SALDOS";
	}

	/**
	 * obtiene las devoluciones segun filtros de busqueda
	 */
	public String buscarDevoluciones() {
		// cargar clientes
		try {
			consultarClientes();
			consultaBean.setIdCliente(clienteSelect);
			consultaBean.setListaDevoluciones(devolucionService.consultarDevoluciones(consultaBean));
			if (consultaBean.getListaDevoluciones().isEmpty()) {
				SessionUtils.addError("No hay datos para esta consulta");
			}
		} catch (Exception e) {
			SessionUtils.addError("No hay datos para esta consulta");
		}
		return "CONSULTAR_DEVOLUCION";
	}

	/**
	 * @return the consultaBean
	 */
	public IDevolucionConsultaBean getConsultaBean() {
		return consultaBean;
	}

	/**
	 * @param consultaBean the consultaBean to set
	 */
	public void setConsultaBean(IDevolucionConsultaBean consultaBean) {
		this.consultaBean = consultaBean;
	}

}