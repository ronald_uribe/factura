package com.ruribe.web.validador;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

	public class MiValidador implements Validator {

	    @Override
	    public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
	            throws ValidatorException {
	        if (!(arg2 instanceof Long)) {
	            throw new ValidatorException(new FacesMessage("Debe ser un entero"));
	        }

	        int valor = ((Long)arg2).intValue();;

	        if ((valor == 0)) {
	            throw new ValidatorException(new FacesMessage(
	                    "Por favor Registre la cantidad"));
	        }
	    }
	}