
/**
 * Método que comprueba que la fecha cumple el formato dd/mm/aa, si no mostrará un popup 
 * @param campoFecha
 */function compruebaFecha(campoFecha) {
	
	if (campoFecha.value == "dd/mm/aaaa" || campoFecha.value == "") {
		return;
	}
	
	/* la forma de verificar el formato es la que ya comentamos */
	re=/^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$/;
	if(campoFecha.value.length==0 || !re.exec(campoFecha.value)) {
		muestraPopup();
		campoFecha.value = "dd/mm/aaaa";
		return;
	}

	/* comprobamos que la fecha es válida */
	var d = new Date()
	/* la función tiene como entrada: año, mes, día */
	d.setFullYear(campoFecha.value.substring(6,10), 
			campoFecha.value.substring(3,5)-1,
			campoFecha.value.substring(0,2))

	/* ¿el mes del objeto Date es el mes introducido por el usuario?
	   OJO: getMonth() devuelve el número de mes del 0 al 11

	   ¿el día del objeto Date es el día introducido por el usuario?
	   OJO: getDate() devuelve el día del mes */
	if(d.getMonth() != campoFecha.value.substring(3,5)-1 
		|| d.getDate() != campoFecha.value.substring(0,2))
	{
		muestraPopup();
		campoFecha.value = "dd/mm/aaaa";
		return;
	}
}

/**
 * Metodo que comprueba si value del elemento esta vacio y se introduce su help text
 * @param element Elemento html
 * @return
 */
function resetHelpText(element) {	
	 
	 if(jQuery(element).attr("value") == null || jQuery(element).attr("value") == "") {
		 jQuery(element).attr("value", getHelpText(element));
	 }
	 
}

/**
 * Método que cierra el popUp
 */
function closePopup() {
	document.getElementById('popupScreenFecha').style.display = 'none';
	document.getElementById('coverFecha').style.display = 'none';
}

/**
 * Método que abre el popUp
 */
function muestraPopup(){
	document.getElementById('popupScreenFecha').style.display = 'block';
	document.getElementById('coverFecha').style.display = 'block';
}