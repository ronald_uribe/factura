function isMicrosoftExplorer(){
	var isExplorer = false;
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
		var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number

		 if (ieversion>=8)
		 	isExplorer = true;
		 else if (ieversion>=7)
		 	isExplorer = true;
		 else if (ieversion>=6)
		 	isExplorer = true;
		 else if (ieversion>=5)
		 	isExplorer = true;
	} 
	return isExplorer;
}

function displayMenu(elemento) {

}

function iniciarMenu() {
	document.getElementById('menu').style.display ='';
}

function lanzar(idFormulario) {
	
	var fireOnThis = document.getElementById('formulario:lanzador');
	
	if (document.createEvent) {
	  var evObj = document.createEvent('MouseEvents');
	  evObj.initEvent( 'click', true, false );
	  fireOnThis.dispatchEvent(evObj);
	} else if (document.createEventObject) {
	   fireOnThis.fireEvent('onclick');
	}	
	displayMenu(parent.document);
}
function lanzarSinCerrar(idFormulario) {
	
	var fireOnThis = document.getElementById('lanzador');
	
	if (document.createEvent) {
	  var evObj = document.createEvent('MouseEvents');
	  evObj.initEvent( 'click', true, false );
	  fireOnThis.dispatchEvent(evObj);
	} else if (document.createEventObject) {
	   fireOnThis.fireEvent('onclick');
	}	

}
