/**
 * FUNCIONES PARA RECUPERAR LA FECHA DEL SISTEMA
 * usada en la cabecera.
 * Ruribe.
 * */
function dameFecha(){
var Fecha, dia , mes ,ano,ds;
Fecha = new Date();
ds = diaSem(Fecha.getDay());
dia = Fecha.getDate();
mes = LitMes(Fecha.getMonth());
ano = Fecha.getFullYear();
return ('<B>' + ds + '</B>, ' + dia + ' de ' + mes + ' de ' + ano);}

function diaSem(d){
	if (d == 0) return "domingo";if (d == 1) return "lunes";
	if (d == 2) return "martes";if (d == 3) return "mi&eacute;rcoles";
	if (d == 4) return "jueves";if (d == 5) return "viernes";
	if (d == 6) return "s&aacute;bado";
}

function LitMes(m){
	if (m == 0) return "enero";if (m == 1) return "febrero"; if (m == 2) return "marzo";
	if (m == 3) return "abril";if (m == 4) return "mayo";if (m == 5) return "junio";
	if (m == 6) return "julio";if (m == 7) return "agosto";if (m == 8) return "septiembre";
if (m == 9) return "octubre";if (m == 10) return "noviembre";if (m == 11) return "diciembre";
}

function mostrarMenuEnError() {
	if (parent.document.getElementById("cabecera")==null) {
		document.getElementById("cabeceraError").style.display='';
		document.getElementById("separacion5emError").style.display='';
		document.getElementById("herramientasError").style.display='';
	}
}

