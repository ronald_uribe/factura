function dameFechaActual()
{
var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var f=new Date();
document.write(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
}

function filtrarTable() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("formId:myInput").value;
  filter = input.value;
  table = document.getElementById("formId:tableid:tbody_element");
  tr = table.getElementsByTagName("tr");
  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
    	txtValue= td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(input) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

function mostrar() {
    document.getElementById("sidebar").style.width = "300px";
    document.getElementById("labelMenu").style.marginLeft = "300px";
    document.getElementById("contenido").style.marginLeft = "300px";
    document.getElementById("abrir").style.display = "none";
    document.getElementById("cerrar").style.display = "inline";
}

function ocultar() {
    document.getElementById("sidebar").style.width = "0";
    document.getElementById("labelMenu").style.marginLeft = "0";
    document.getElementById("contenido").style.marginLeft = "0";
    document.getElementById("abrir").style.display = "inline";
    document.getElementById("cerrar").style.display = "none";
    
}


$(document).ready(function(){
	$(':checkbox[readonly=readonly]').click(function(){
	return false;        
	});
})
