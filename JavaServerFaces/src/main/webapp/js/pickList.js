/**
 * Mueve varios elementos de una tabla origen a una destino.
 * Deben identificarse tanto los identificadores de las tablas como los estilos afectados.
 * 
 * @param idTablaOrigen Identificador de la tabla de origen de los datos.
 * @param idTablaDestino Identificador de la tabla de destino de los datos.
 * @param estiloSeleccionado Estilo que indica que un elemento ha sido seleccionado.
 * @param estiloNuevaFila Estilo de la nueva fila en la tabla destino.
 */
function mueveVarios(idTablaOrigen, idTablaDestino, estiloSeleccionado, estiloNuevaFila) {
	
	var tablaOrigen = document.getElementById(idTablaOrigen);
	var filasOrigen = document.getElementById(idTablaOrigen).rows;
	
	var tablaDestino = document.getElementById(idTablaDestino);
	
	recuento = true
	
	while (recuento) {
		// RECORRER TODAS LAS FILAS DE LA TABLA DE ORIGEN
		for (count = 1; count < filasOrigen.length; count++) {
			// PARA EL ESTILO CSS DE LA FILA QUE INDICA QUE SE HA SELECCIONADO SE REALIZA LA COPIA
			if (filasOrigen[count].className == estiloSeleccionado) {
				numeroFilasDestino=document.getElementById(idTablaDestino).rows.length;
	
				// CREA LA NUEVA FILA
				nuevaFila = tablaDestino.insertRow(numeroFilasDestino);
				nuevaColumna1 = nuevaFila.insertCell(0);
				nuevaColumna2 = nuevaFila.insertCell(1);
				nuevaColumna3 = nuevaFila.insertCell(2);
				nuevaColumna4 = nuevaFila.insertCell(3);
				
				// COPIA LOS VALORES DE LA ANTIGUA FILA
				nuevaFila.className = estiloNuevaFila
				nuevaColumna1.innerHTML = filasOrigen[count].cells[0].innerHTML;
				nuevaColumna2.innerHTML = filasOrigen[count].cells[1].innerHTML;
				nuevaColumna3.innerHTML = filasOrigen[count].cells[2].innerHTML;
				nuevaColumna4.innerHTML = filasOrigen[count].cells[3].innerHTML;
				
				// A�ADE EVENTO ONCLICK
				if (nuevaFila.addEventListener) {  // all browsers except IE before version 9
					nuevaFila.addEventListener("click", function() { selectRowMultiple(idTablaDestino, this, event); }, false);
				} else {
					if (nuevaFila.attachEvent) {   // IE before version 9
						nuevaFila.attachEvent("click", function() { selectRowMultiple(idTablaDestino, this, event); });
					}
				}		
				
				// BORRA EL ELEMENTO DE LA FILA
				tablaOrigen.deleteRow(count);
				
				// AL BORRAR LA FILA, PARA NO PERDER EL �NDICE, HAY QUE COMENZAR DE NUEVO EL RECUENTO
				break;
			}
		}
		// EN CASO DE QUE SE HAYA TERMINADO DE RECORRER LA TABLA, SE SALE DEL BUCLE
		if (count == filasOrigen.length) {
			recuento = false;
		}
	}
	recalculaContadores(idTablaOrigen, idTablaDestino);
	return;
	
}

/**
 * Recalcula los contadores de las tablasPickList
 * 
 * @param idTablaUno identificador de la primera tabla.
 * @param idTablaDos identificador de la segunda tabla.
 * 
 */
function recalculaContadores(idTablaUno, idTablaDos) {
	var numFilasUno = document.getElementById(idTablaUno).rows.length-1;
	var numFilasDos = document.getElementById(idTablaDos).rows.length-1;
	
	document.getElementById('label_'+idTablaUno).innerHTML="("+numFilasUno+")"
	document.getElementById('label_'+idTablaDos).innerHTML="("+numFilasDos+")"
}
/**
 * Mueve todos los elementos de una tabla origen a una destino.
 * Deben identificarse tanto los identificadores de las tablas como el estilo de la nueva tabla.
 * 
 * @param idTablaOrigen Identificador de la tabla de origen de los datos.
 * @param idTablaDestino Identificador de la tabla de destino de los datos.
 * @param estiloNuevaFila Estilo de la nueva fila en la tabla destino.
 */
function mueveTodos(idTablaOrigen, idTablaDestino, estiloNuevaFila) {

	var tablaOrigen = document.getElementById(idTablaOrigen);
	var filasOrigen = document.getElementById(idTablaOrigen).rows;
	
	var tablaDestino = document.getElementById(idTablaDestino);
	
	// RECORRE LA TABLA ORIGEN REALIZANDO COPIAS ID�NTICAS EN LA TABLA DESTINO DE CADA UNA DE LAS FILAS
	for (count = 1; count < filasOrigen.length; count++) {
		numeroFilasDestino=document.getElementById(idTablaDestino).rows.length;
		nuevaFila = tablaDestino.insertRow(numeroFilasDestino);
		
		for (countColumns = 0; countColumns < filasOrigen[count].cells.length; countColumns++) {
			nuevaColumna = nuevaFila.insertCell(countColumns);
			nuevaColumna.innerHTML = filasOrigen[count].cells[countColumns].innerHTML;
		}
		
		nuevaFila.className = estiloNuevaFila
		
		if (nuevaFila.addEventListener) {  // all browsers except IE before version 9
			nuevaFila.addEventListener("click", function() { selectRowMultiple(idTablaDestino, this, event); }, false);
		} else {
			if (nuevaFila.attachEvent) {   // IE before version 9
				nuevaFila.attachEvent("click", function() { selectRowMultiple(idTablaDestino, this, event); });
			}
		}		
			
	}
	
	// FIJA EL VALOR DEL TAMA�O DE LA TABLA PARA QUE NO SE VEA AFECTADO AL MODIFICARLO DINAMICAMENTE
	numeroFilasOrigen = filasOrigen.length
	for (count = 1; count < numeroFilasOrigen; count++) {
		
		tablaOrigen.deleteRow(1);
	}
	recalculaContadores(idTablaOrigen, idTablaDestino);
	return;
}

/**
 * A�ade la primera columna de cada fila de la tabla al campo del formulario.
 * 
 *  @param idTablaEnvio identificador de la tabla donde se encuentran los datos a enviar
 *  @param idCampoFormulario identificador del formulario que se enviar�
 */
function enviaLista(idTablaEnvio, idCampoFormulario) {
	
	var filas = document.getElementById(idTablaEnvio).rows;
	var campoFormulario = document.getElementById(idCampoFormulario);
	
	campoFormulario.value="";
	for (count = 1; count < filas.length; count++) {
		campoFormulario.value = campoFormulario.value + filas[count].cells[0].innerHTML  + ";";
	}
	
	return;
}


//**********************************************************************
//  Codigo JavaScript de manejo de tablas


//referencia a la fila de la tabla seleccionada

var selectedRow ;

//datos de la tabla
var dataTable; 
var rowCount;

var opt_selected = new Array();
var index = 0;
var shifttecleado1 =0;
var shifttecleado2 =0;

function buscarElemento(idTable,row) {
/*
Busca row en idTable (�ngel) 
*/ 	
var indice=0;

for (count2 = 1; count2 < idTable.length; count2++) {
               if (idTable[count2] == row)
               {
               	indice = count2;
               	break;
               }
  }
 
return indice;
}

//funcion invocada al seleccionar una fila de la tabla
//recibe como argumentos el identificador de la tabla y la referencia a la
//fila seleccionada
//
function selectRow(idTable,row) {
/*
Marca como seleccionada la fila donde se ha pulsado (�ngel) 
*/ 	
var rows=document.getElementById(idTable).rows ;
         // eliminamos selecciones de fila anteriores
       for (count = 0; count < rows.length; count++) {
									// Volvemos a poner el estilo inicial
             rows[count].className = 'filaBlanca' ;
  }
         // cambiamos el estilo de la fila seleccionada
         row.className = 'filaBlancaClick'; 
         //almacenamos la referencia a la fila seleccionada
         selectedRow = row ;
}
//-->


function selectRowMultiple(idTable,row,event) {
/*
 Nueva versi�n para poder realizar selecci�n m�ltiple de filas (�ngel) 
*/ 	
 var diferencia1=0;
 var diferencia2=0;
 var shifttecleado1Posicion=0;
 var shifttecleado2Posicion=0;
 var filaSelecc=0;
 var indiceFila=0;

 var rows=document.getElementById(idTable).rows ;
 selectedRow = row ;
/*
Buscamos en que posici�n esta pulsando, y en que posiciones est�n el primero y �ltimo de los elementos seleccionado hasta el momento
*/
for (count = 1; count < rows.length; count++) 
  {
    if (rows[count] == selectedRow)
    {
    	filaSelecc = count;
    	indiceFila= rows[count].id;
  	} 
    if ((shifttecleado1Posicion == 0) && (buscarElemento(opt_selected, rows[count].id)!= 0))
    {
    	shifttecleado1Posicion = count;
  	} 
    if (buscarElemento(opt_selected, rows[count].id)!= 0)
    {
    	shifttecleado2Posicion = count;
  	} 
	}
	

if ((event.shiftKey==1) || (event.ctrlKey==1))
	{
		if (event.shiftKey==1)
		{
		    if (shifttecleado1 != 0)
		    {
	
			    if (shifttecleado2 != 0)
			    {
			    	diferencia1= Math.abs(shifttecleado1Posicion - filaSelecc);
			    	diferencia2= Math.abs(shifttecleado2Posicion - filaSelecc);
		        if (diferencia1 < diferencia2)
		        {
		        	if (shifttecleado1Posicion < filaSelecc)
		        	{
		        		shifttecleado2 = selectedRow;
	 		      		shifttecleado2Posicion = filaSelecc;
		        	}
		        	else
		        	{
			      		shifttecleado2 = shifttecleado1;
			      		shifttecleado2Posicion = shifttecleado1Posicion;
			      		shifttecleado1 = selectedRow;
			      		shifttecleado1Posicion = filaSelecc;
		        	}
		        }
		      	else
		      		{
			        	if (shifttecleado2Posicion < filaSelecc)
			        	{
			        		shifttecleado1 = shifttecleado2;
			        		shifttecleado2 = selectedRow;
				      		shifttecleado1Posicion = shifttecleado2Posicion;
				      		shifttecleado2Posicion = filaSelecc;
			        	}
			        	else
			        	{
			        		shifttecleado1 = selectedRow;
		 		      		shifttecleado1Posicion = filaSelecc;
			        	}
							}        		
			    }
			    else
			    {
			    	diferencia1= Math.abs(shifttecleado1Posicion - filaSelecc);
	
		      	if (shifttecleado1Posicion < filaSelecc)
		      	{
		      		shifttecleado2 = selectedRow;
		      		shifttecleado2Posicion = filaSelecc;
		      	}
		      	else
		      	{
		      		shifttecleado2 = shifttecleado1;
		      		shifttecleado2Posicion = shifttecleado1Posicion;
		      		shifttecleado1 = selectedRow;
		      		shifttecleado1Posicion = filaSelecc;
		      	}
			    }
		      
					// Borramos lo hasta ahora seleccionado
		      index = 0;
		      for (count =index; count < rows.length; count++)
		         {
	              delete opt_selected[count];
		            index++;
		         }
		      index = 0;
		        // Marcamos lo nuevo seleccionado
		      for (count = 1; count < rows.length; count++) 
		         {
		          if ((count <= shifttecleado2Posicion) && (count >= shifttecleado1Posicion)) 
		          {
		            // Marcamos la l�nea
		            rows[count].className = 'filaBlancaClick'; 
		            index++;
		            opt_selected[index] = rows[count].id;
		          }
		          else
		          	{
									// Volvemos a poner el estilo inicial
						       rows[count].className = 'filaBlanca';
								}
		         }
		    }
		    else
		    {
		    	shifttecleado1 = selectedRow;
	         // eliminamos selecciones de fila anteriores
		      index = 0;
	        for (count = 0; count < rows.length; count++)
	        {
					// Volvemos a poner el estilo inicial
	                rows[count].className = 'filaBlanca' ;
					        delete opt_selected[count];
	   			}
	        // cambiamos el estilo de la fila seleccionada
	        row.className = 'filaBlancaClick'; 
	        index++;
	        opt_selected[index] = indiceFila;
		    }
		}    
		else
		{
			if (row.className == 'filaBlancaClick')
			{
				// Volvemos a poner el estilo inicial
				  row.className = 'filaBlanca';
				  var posicion= buscarElemento(opt_selected, rows[filaSelecc].id);
					if (posicion!= 0)
					{
		           delete opt_selected[posicion];
					 }
					}
			else
				{  
		    // cambiamos el estilo de la fila seleccionada
		    row.className = 'filaBlancaClick'; 
	      index++;
		    opt_selected[index] = indiceFila;
				}
		     //almacenamos la referencia a la fila seleccionada
		}	
	}
else
	{
		if (event.keyCode == 38 || event.keyCode ==40)
		{
			if (row.className == 'filaBlancaClick')
			{
				// Volvemos a poner el estilo inicial
				  row.className = 'filaBlanca';
				  var posicion= buscarElemento(opt_selected, rows[filaSelecc].id);
					if (posicion!= 0)
					{
		           delete opt_selected[posicion];
					 }
			}
			else
				{  
		    // cambiamos el estilo de la fila seleccionada
		    row.className = 'filaBlancaClick'; 
	      index++;
		    opt_selected[index] = indiceFila;
				}
		     //almacenamos la referencia a la fila seleccionada
	
		}
	else
		{
		// Limpiar todo
	  var rows=document.getElementById(idTable).rows ;
	    // eliminamos selecciones de fila anteriores
	  index = 0;
	  for (count = 0; count < rows.length; count++) 
	     {
				// Volvemos a poner el estilo inicial
	      rows[count].className = 'filaBlanca';
	      delete opt_selected[count];
	      }
	  // cambiamos el estilo de la fila seleccionada
	  row.className = 'filaBlancaClick'; 
	  index++;
	  opt_selected[index] = indiceFila;
		shifttecleado1 =selectedRow;
		shifttecleado2 =0;
		diferencia1=0;
		diferencia2=0;				
		}
	}
}