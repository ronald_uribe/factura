var jQuery_1_8_0 = jQuery.noConflict(true);
var	claseIcoAbierta;
var claseIcoCerrada;
var claseMenuCierre;
var claseMenuLinea;
var	claseMenuSelec;
var claseNodoSeleccionado;
var claseNodoN3Seleccionado;
var claseNodoLinea;
var claseNodoN3Linea;
var contexto = "";
var idMenuAbierto = 0;
var idSubMenuAbierto = "";
var alturasSubmenus;
var alturasSubmenus_N3 = new Object();
var nodoSeleccionado = "";
var nodos;
var nodosTotal;
var navegador = navigator.userAgent.toLowerCase();
var SUBMENU = "submenu";
var SUBMENU_N3 = "submenu_N3";
var NODO = "nodo";

Array.prototype.contains = function ( contenido ) {
	for (i in this) {
		if (this[i] == contenido) {
			return true;
		}
	}

	return false;
}

function guardarClasesNodo(_claseNodoSeleccionado, _claseNodoN3Seleccionado, _claseNodoLinea, _claseNodoN3Linea)
{
	claseNodoSeleccionado = _claseNodoSeleccionado;
	claseNodoN3Seleccionado = _claseNodoN3Seleccionado;
	claseNodoLinea = _claseNodoLinea;
	claseNodoN3Linea = _claseNodoN3Linea;
}

function guardarClasesMenu(_claseIcoAbierta, _claseIcoCerrada, _claseMenuSelec, _claseMenuLinea, _claseMenuCierre)
{
	claseIcoAbierta = _claseIcoAbierta;
	claseIcoCerrada = _claseIcoCerrada;
	claseMenuSelec = _claseMenuSelec;
	claseMenuLinea = _claseMenuLinea;
	claseMenuCierre = _claseMenuCierre;
}

function ocultarSubmenuN3(idSubmenu, idSubmenu_N3)
{
	alturasSubmenus[idSubmenu - 1] = alturasSubmenus[idSubmenu - 1] - alturasSubmenus_N3[idSubmenu_N3];

	if (navegador.indexOf("msie") != -1)
	{
		document.getElementById(idSubmenu_N3).style.height = "1";
		document.getElementById(idSubmenu_N3).style.display = 'none';
	}
	else
	{
		document.getElementById(idSubmenu_N3).style.height = 0;
	}
	document.getElementById(idSubmenu_N3).style.visibility = "hidden";
}

function esconderMenusNivel3(i, submenusDesplegados, expandido)
{
	//Con esto se esconden los submenús de nivel 3 si los hubiese.
	for (var j = 1; j <= nodosTotal[i - 1]; j++)
	{
		var idSubmenu_N3 = "submenu3_" + i + "_" + j;

		if (document.getElementById(idSubmenu_N3) != null) 
		{
			var idSubmenu = idSubmenu_N3.substr(idSubmenu_N3.indexOf("_") + 1, 1);

			alturasSubmenus_N3[idSubmenu_N3] = document.getElementById(idSubmenu_N3).scrollHeight;

			if (!expandido)
			{
				if (!submenusDesplegados.contains(idSubmenu_N3))
				{
					ocultarSubmenuN3(idSubmenu, idSubmenu_N3);
				}
				else
				{
					idSubMenuAbierto = idSubmenu_N3;
				}				
			}
			else
			{
				var submenusReplegados = submenusDesplegados;
				if (submenusReplegados.contains(idSubmenu_N3))
				{
					ocultarSubmenuN3(idSubmenu, idSubmenu_N3);
				}
			}
		}
	}
}

function iniciarMenuAcordeon(contextoAPP, submenusDesplegadosSesion, idNodoSeleccionadoSesion, numSubmenus, nodosSubmenus, nodosSubmenusTotal, expandido)
{
	contexto = contextoAPP;
	alturasSubmenus = new Array(numSubmenus);
	nodos = nodosSubmenus.split('|');
	nodosTotal = nodosSubmenusTotal.split('|');
	submenusDesplegados = submenusDesplegadosSesion.split('|');

	for (var i = 1; i <= numSubmenus; i++)
	{
		if (document.getElementById("submenu_" + i) != null)
		{
			alturasSubmenus[i - 1] = document.getElementById("submenu_" + i).scrollHeight;
			if (!expandido) 
			{
				if (!submenusDesplegados.contains("submenu_" + i)) {
					document.getElementById("submenu_" + i).style.height = "1";
					document.getElementById("submenu_" + i).style.visibility = "hidden";
					if (document.getElementById("cierre_" + i) != null)
					{
						document.getElementById("cierre_" + i).className = "";
					}

					esconderMenusNivel3(i, submenusDesplegados, expandido);
				} else {
					idMenuAbierto = i;
					document.getElementById("ico_" + i).className = claseIcoAbierta;
					document.getElementById("sm_" + i).className = claseMenuSelec;

					esconderMenusNivel3(i, submenusDesplegados, expandido);
				}
			}
			else
			{
				//En este caso el registro pasa a ser de submenusContraidos, ya que al inicio todos están desplegados, por lo que se comprueba a la inversa y se quita el NOT (!) del IF.
				var submenusReplegados = submenusDesplegados;

				esconderMenusNivel3(i, submenusReplegados, expandido);

				if (submenusReplegados.contains("submenu_" + i)) {
					document.getElementById("submenu_" + i).style.height = "1";
					document.getElementById("submenu_" + i).style.visibility = "hidden";
					if (document.getElementById("cierre_" + i) != null)
					{
						document.getElementById("cierre_" + i).className = "";
					}
				} else {
					document.getElementById("ico_" + i).className = claseIcoAbierta;
					document.getElementById("sm_" + i).className = claseMenuSelec;
				}

				//Con esto se guardan las alturas de los submenús de nivel 3 si los hubiese.
				for (var j = 1; j <= nodosTotal[i - 1]; j++)
				{
					var idSubmenu_N3 = "submenu3_" + i + "_" + j;

					if (document.getElementById(idSubmenu_N3) != null)
					{
						alturasSubmenus_N3[idSubmenu_N3] = document.getElementById(idSubmenu_N3).scrollHeight;
					}
				}
			}
		}
		else
		{
			if ((expandido) && (document.getElementById("sm_" + i)))
			{
				document.getElementById("sm_" + i).className = claseMenuSelec;
			}
			alturasSubmenus[i - 1] = 0;
		}
	}

	if (!expandido)
	{
		for (var i = 1; i <= numSubmenus; i++)
		{
			if (document.getElementById("submenu_" + i) != null)
			{
				alturasSubmenus[i - 1] = document.getElementById("submenu_" + i).scrollHeight;
			}
		}
	}

	//Se selecciona el nodo anteriormente seleccionado.
	if (document.getElementById(idNodoSeleccionadoSesion))
	{
		nodoSeleccionado = idNodoSeleccionadoSesion;	

		var tipoNodo = nodoSeleccionado.split("_");
		if (tipoNodo.length == 3)
		{
			//Si idNodo tiene 2 ocurrencias de "_" es nodo nivel 2.
			document.getElementById(idNodoSeleccionadoSesion).className = claseNodoSeleccionado;
		}
		else
		{
			if (tipoNodo.length == 4)
			{
				//Si idNodo tiene 3 ocurrencias de "_" es de nodo nivel 3.
				document.getElementById(idNodoSeleccionadoSesion).className = claseNodoN3Seleccionado;
			}
		}
	}
}

function accionarUnSebmenu(idSubmenu, enlaceSubmenu)
{
	var velocidad = 15;
	ocultar = function()
	{
		var altura = 0;
		if (document.getElementById("submenu_" + idMenuAbierto) != null)
		{
			altura = document.getElementById("submenu_" + idMenuAbierto).offsetHeight;
		}
		if (altura <= velocidad)
		{
			document.getElementById("ico_" + idMenuAbierto).className = claseIcoCerrada;
			if (idSubmenu == idMenuAbierto)
			{
				if (document.getElementById("submenu_" + idSubmenu) != null)
				{
					document.getElementById("submenu_" + idSubmenu).style.height = 1;
					document.getElementById("submenu_" + idSubmenu).style.visibility = "hidden";
					document.getElementById("sm_" + idSubmenu).className = claseMenuLinea;

					idMenuAbierto = 0;
				}
				if (document.getElementById("cierre_" + idSubmenu) != null)
				{
					document.getElementById("cierre_" + idSubmenu).className = "";
				}
			}
			else
			{
				if (document.getElementById("submenu_" + idMenuAbierto) != null)
				{
					document.getElementById("submenu_" + idMenuAbierto).style.visibility = "hidden";
				}
				if (document.getElementById("submenu_" + idSubmenu) != null)
				{
					document.getElementById("ico_" + idSubmenu).className = claseIcoAbierta;
					document.getElementById("submenu_" + idSubmenu).style.visibility = "visible";
				}
				document.getElementById("sm_" + idMenuAbierto).className = claseMenuLinea;
				document.getElementById("sm_" + idSubmenu).className = claseMenuSelec;
				if (document.getElementById("cierre_" + idMenuAbierto) != null)
				{
					document.getElementById("cierre_" + idMenuAbierto).className = "";
				}
				if (document.getElementById("cierre_" + idSubmenu) != null)
				{
					document.getElementById("cierre_" + idSubmenu).className = claseMenuCierre;
				}
				mostrar();
			}
		}
		else
		{
			var alturaTemporal = altura - velocidad;
			if (alturaTemporal <= velocidad)
			{
				alturaTemporal = 1;
			}
			document.getElementById("submenu_" + idMenuAbierto).style.height = alturaTemporal;
			setTimeout(ocultar, 10);
		}
	}
	mostrar = function()
	{
		var altura = alturasSubmenus[idSubmenu - 1];
		if (document.getElementById("submenu_" + idSubmenu) != null)
		{
			altura = document.getElementById("submenu_" + idSubmenu).offsetHeight;
		}
		if (altura >= alturasSubmenus[idSubmenu - 1])
		{
			clearTimeout(inter);
			idMenuAbierto = idSubmenu;

			arreglarAltura(idSubmenu);
		}
		else
		{
			var alturaTemporal = altura + velocidad;
			if (alturaTemporal > alturasSubmenus[idSubmenu - 1])
			{
				alturaTemporal = alturasSubmenus[idSubmenu - 1];
			}
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporal;
			var inter = setTimeout(mostrar, 10);
		}
	}

	if ((nodoSeleccionado != '') && (enlaceSubmenu != ''))
	{
		deseleccionarNodo(nodoSeleccionado);
	}
	if (idMenuAbierto != 0)
	{
		ocultar();
	}
	else
	{
		if (document.getElementById("submenu_" + idSubmenu) != null)
		{
			document.getElementById("ico_" + idSubmenu).className = claseIcoAbierta;
			document.getElementById("submenu_" + idSubmenu).style.visibility = "visible";
		}
		document.getElementById("sm_" + idSubmenu).className = claseMenuSelec;
		if (document.getElementById("cierre_" + idSubmenu) != null)
		{
			document.getElementById("cierre_" + idSubmenu).className = claseMenuCierre;
		}

		mostrar();
	}

	actualizarEstadoMenu("submenu_" + idSubmenu, SUBMENU);
}

function accionarVariosSebmenus(idSubmenu, enlaceSubmenu)
{
	var velocidad = 15;
	ocultar = function()
	{
		var altura = 0;
		if (document.getElementById("submenu_" + idSubmenu) != null)
		{
			altura = document.getElementById("submenu_" + idSubmenu).offsetHeight;
		}
		if (altura <= velocidad)
		{
			if (document.getElementById("submenu_" + idSubmenu) != null)
			{
				document.getElementById("ico_" + idSubmenu).className = claseIcoCerrada;
				document.getElementById("submenu_" + idSubmenu).style.height = 1;
				document.getElementById("submenu_" + idSubmenu).style.visibility = "hidden";
			}
			document.getElementById("sm_" + idSubmenu).className = claseMenuLinea;
			if (document.getElementById("cierre_" + idSubmenu) != null)
			{
				document.getElementById("cierre_" + idSubmenu).className = "";
			}

			for (var i = 1; i <= alturasSubmenus.length; i++)
			{
				arreglarAltura(i);
			}
		}
		else
		{
			var alturaTemporal = altura - velocidad;
			if (alturaTemporal <= velocidad)
			{
				alturaTemporal = 1;
			}
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporal;
			setTimeout(ocultar, 10);
		}
	}
	mostrar = function()
	{
		var altura = alturasSubmenus[idSubmenu - 1];
		if (document.getElementById("submenu_" + idSubmenu))
		{
			altura = document.getElementById("submenu_" + idSubmenu).offsetHeight;
		}
		if (altura >= alturasSubmenus[idSubmenu - 1])
		{	
			clearTimeout(inter);

			for (var i = 1; i <= alturasSubmenus.length; i++)
			{
				arreglarAltura(i);
			}
		}
		else
		{
			var alturaTemporal = altura + velocidad;
			if (alturaTemporal > alturasSubmenus[idSubmenu - 1])
			{
				alturaTemporal = alturasSubmenus[idSubmenu - 1];
			}
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporal;
			var inter = setTimeout(mostrar, 10);
		}
	}
	if ((nodoSeleccionado != '') && (enlaceSubmenu != ''))
	{
		deseleccionarNodo(nodoSeleccionado);
	}
	if (document.getElementById("ico_" + idSubmenu).className == claseIcoCerrada)
	{
		if (document.getElementById("submenu_" + idSubmenu) != null)
		{
			document.getElementById("ico_" + idSubmenu).className = claseIcoAbierta;
			document.getElementById("submenu_" + idSubmenu).style.visibility = "visible";
		}
		document.getElementById("sm_" + idSubmenu).className = claseMenuSelec;
		if (document.getElementById("cierre_" + idSubmenu) != null)
		{
			document.getElementById("cierre_" + idSubmenu).className = claseMenuCierre;
		}

		mostrar();		
	}
	else
	{
		ocultar();
	}

	actualizarEstadoMenu("submenu_" + idSubmenu, SUBMENU);
}

function accionarUnSebmenu_Nivel3(idSubmenu_N3, enlaceSubmenu3)
{
	var velocidad = 15;
	var idSubmenu = idSubmenu_N3.substr(idSubmenu_N3.indexOf("_") + 1, 1);
	var idFilaSubmenu3 = "filasubmenu3_" + idSubmenu_N3.substr(idSubmenu_N3.indexOf("_") + 1, 3);

	ocultar = function()
	{
		var altura = document.getElementById(idSubMenuAbierto).offsetHeight;
		if (altura <= velocidad)
		{
			alturasSubmenus[idSubmenu - 1] = alturasSubmenus[idSubmenu - 1] - alturasSubmenus_N3[idSubMenuAbierto];

			if (idSubmenu_N3 == idSubMenuAbierto)
			{
				if (navegador.indexOf("msie") != -1)
				{
					document.getElementById(idSubMenuAbierto).style.height = 1;
					document.getElementById(idSubMenuAbierto).style.display = 'none';
				}
				else
				{
					document.getElementById(idSubMenuAbierto).style.height = 0;
				}
				document.getElementById("submenu_" + idSubmenu).style.height = alturasSubmenus[idSubmenu - 1];
	
				idSubMenuAbierto = "";
			}
			else
			{
				document.getElementById(idSubMenuAbierto).style.visibility = "hidden";
				document.getElementById(idSubmenu_N3).style.visibility = "visible";

				if (navegador.indexOf("msie") != -1)
				{
					document.getElementById(idSubMenuAbierto).style.display = 'none';
					document.getElementById(idSubmenu_N3).style.display = 'block';
				}

				mostrar();
			}
		}
		else
		{
			var alturaTemporal = altura - velocidad;
			var alturaTemporalSubmenu = document.getElementById("submenu_" + idSubmenu).offsetHeight - velocidad;
			if (alturaTemporal <= velocidad)
			{
				if (navegador.indexOf("msie") != -1)
				{
					alturaTemporal = 1;
					document.getElementById(idSubMenuAbierto).style.display = 'none';
				}
				else
				{
					alturaTemporal = 0;
				}

			}

			document.getElementById(idSubMenuAbierto).style.height = alturaTemporal;
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporalSubmenu;

			setTimeout(ocultar, 10);
		}
	}
	mostrar = function()
	{
		var altura = document.getElementById(idSubmenu_N3).offsetHeight;
		if (altura >= alturasSubmenus_N3[idSubmenu_N3])
		{
			clearTimeout(inter);
			idSubMenuAbierto = idSubmenu_N3;

			//arreglarAltura(idSubmenu_N3);
		}
		else
		{
			var alturaTemporal = altura + velocidad;
			var alturaTemporalSubmenu = document.getElementById("submenu_" + idSubmenu).offsetHeight + velocidad;

			if (alturaTemporal > alturasSubmenus_N3[idSubmenu_N3])
			{
				alturaTemporal = alturasSubmenus_N3[idSubmenu_N3];
				alturaTemporalSubmenu = alturasSubmenus[idSubmenu - 1] + alturasSubmenus_N3[idSubmenu_N3];
				alturasSubmenus[idSubmenu - 1] = alturaTemporalSubmenu;
			}

			document.getElementById(idSubmenu_N3).style.height = alturaTemporal;
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporalSubmenu;

			var inter = setTimeout(mostrar, 10);
		}
	}
	if (enlaceSubmenu3 != "")
	{
		deseleccionarNodo(nodoSeleccionado);
		seleccionarNodo(idFilaSubmenu3, false);
		document.getElementById(nodoSeleccionado).className = claseNodoLinea;
		document.getElementById(idFilaSubmenu3).className = claseNodoSeleccionado;
	}
	if (idSubMenuAbierto != "")
	{
		ocultar();
	}
	else
	{
		document.getElementById(idSubmenu_N3).style.visibility = "visible";

		if (navegador.indexOf("msie") != -1)
		{
			document.getElementById(idSubmenu_N3).style.display = 'block';
		}

		mostrar();
	}

	actualizarEstadoMenu("submenu_" + idSubmenu, SUBMENU, idSubmenu_N3, SUBMENU_N3);
}

function accionarVariosSebmenus_Nivel3(idSubmenu_N3, enlaceSubmenu3)
{
	var velocidad = 15;
	var idSubmenu = idSubmenu_N3.substr(idSubmenu_N3.indexOf("_") + 1, 1);
	var idFilaSubmenu3 = "filasubmenu3_" + idSubmenu_N3.substr(idSubmenu_N3.indexOf("_") + 1, 3);

	ocultar = function()
	{
		var altura = document.getElementById(idSubmenu_N3).offsetHeight;
		if (altura <= velocidad)
		{
			alturasSubmenus[idSubmenu - 1] = alturasSubmenus[idSubmenu - 1] - alturasSubmenus_N3[idSubmenu_N3];

			if (navegador.indexOf("msie") != -1)
			{
				document.getElementById(idSubmenu_N3).style.height = 1;
				document.getElementById(idSubmenu_N3).style.display = 'none';
			}
			else
			{
				document.getElementById(idSubmenu_N3).style.height = 0;
			}
			document.getElementById("submenu_" + idSubmenu).style.height = alturasSubmenus[idSubmenu - 1];
			document.getElementById(idSubmenu_N3).style.visibility = "hidden";
		}
		else
		{
			var alturaTemporal = altura - velocidad;
			var alturaTemporalSubmenu = document.getElementById("submenu_" + idSubmenu).offsetHeight - velocidad;
			if (alturaTemporal <= velocidad)
			{
				if (navegador.indexOf("msie") != -1)
				{
					alturaTemporal = 1;
					document.getElementById(idSubmenu_N3).style.display = 'none';
				}
				else
				{
					alturaTemporal = 0;
				}

			}

			document.getElementById(idSubmenu_N3).style.height = alturaTemporal;
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporalSubmenu;

			setTimeout(ocultar, 10);
		}
	}
	mostrar = function()
	{
		var altura = document.getElementById(idSubmenu_N3).offsetHeight;
		if (altura >= alturasSubmenus_N3[idSubmenu_N3])
		{
			clearTimeout(inter);
		}
		else
		{
			var alturaTemporal = altura + velocidad;
			var alturaTemporalSubmenu = document.getElementById("submenu_" + idSubmenu).offsetHeight + velocidad;

			if (alturaTemporal > alturasSubmenus_N3[idSubmenu_N3])
			{
				alturaTemporal = alturasSubmenus_N3[idSubmenu_N3];
				alturaTemporalSubmenu = alturasSubmenus[idSubmenu - 1] + alturasSubmenus_N3[idSubmenu_N3];
				alturasSubmenus[idSubmenu - 1] = alturaTemporalSubmenu;
			}

			document.getElementById(idSubmenu_N3).style.height = alturaTemporal;
			document.getElementById("submenu_" + idSubmenu).style.height = alturaTemporalSubmenu;

			var inter = setTimeout(mostrar, 10);
		}
	}
	if (enlaceSubmenu3 != "")
	{
		deseleccionarNodo(nodoSeleccionado);
		seleccionarNodo(idFilaSubmenu3, false);
		document.getElementById(nodoSeleccionado).className = claseNodoLinea;
		document.getElementById(idFilaSubmenu3).className = claseNodoSeleccionado;
	}
	if ((document.getElementById(idSubmenu_N3).offsetHeight == 0) || (document.getElementById(idSubmenu_N3).offsetHeight == 1))
	{
		document.getElementById(idSubmenu_N3).style.visibility = "visible";

		if (navegador.indexOf("msie") != -1)
		{
			document.getElementById(idSubmenu_N3).style.display = 'block';
		}

		mostrar();		
	}
	else
	{
		ocultar();
	}

	actualizarEstadoMenu("submenu_" + idSubmenu, SUBMENU, idSubmenu_N3, SUBMENU_N3);
}

function cargarEnlaceDiv(enlace, idDiv, idNodoEnlace, nivel3)
{
	if (document.getElementById(idDiv) != null)
	{
		cargarPagina(enlace, idDiv);
		seleccionarNodo(idNodoEnlace, nivel3);
	}

	return false;
}

function cargarEnlaceIframe(enlace, idIframe, idNodoEnlace, nivel3)
{
	if ((document.getElementById(idIframe) != null) || (idIframe == "_self"))
	{
		seleccionarNodo(idNodoEnlace, nivel3);		
	}
	
	return false;
}

function cargarEnlaceTarget(enlace, target, idNodoEnlace, nivel3)
{
	if (target != "_blank")
	{
		seleccionarNodo(idNodoEnlace, nivel3);
	}
	
	return false;
}

function deseleccionarNodo(nodoSeleccionado)
{
	if (nodoSeleccionado != "")
	{
		var tipoNodo = nodoSeleccionado.split("_");
		if (tipoNodo.length == 3)
		{
			//Si idNodo tiene 2 ocurrencias de "_" es nodo nivel 2.
			document.getElementById(nodoSeleccionado).className = claseNodoLinea;
		}
		else
		{
			if (tipoNodo.length == 4)
			{
				//Si idNodo tiene 3 ocurrencias de "_" es de nodo nivel 3.
				document.getElementById(nodoSeleccionado).className = claseNodoN3Linea;
			}
		}
	}
}

function seleccionarNodo(idNodo, nivel3)
{
	actualizarEstadoMenu(idNodo, NODO);

	var claseSeleccionado = nivel3 ? claseNodoN3Seleccionado : claseNodoSeleccionado;

	deseleccionarNodo(nodoSeleccionado);
	document.getElementById(idNodo).className = claseSeleccionado;
	nodoSeleccionado = idNodo;
}

//Arregla el desfase de alturas en los submenús provocado por la barra vertical de scroll. Funciona bien en IE pero no en Firefox.
function arreglarAltura(submenu)
{
	if (document.getElementById("submenu_" + submenu) != null)
	{
		if ((document.getElementById("submenu_" + submenu).clientHeight > 1)
		&& (document.getElementById("submenu_" + submenu).clientHeight != document.getElementById("submenu_" + submenu).scrollHeight))
		{
			document.getElementById("submenu_" + submenu).style.height = document.getElementById("submenu_" + submenu).scrollHeight;
		}
	}
}

function cargarPagina(enlace, idDiv)
{
	//jQuery.noConflict();
	jQuery_1_8_0(document).ready(function()
	{
		var id = '#' + idDiv;
		jQuery_1_8_0(id).load(enlace);
	});    

}

function actualizarEstadoMenu(idElemento, tipoElemento, idElementoN3, tipoElementoN3)
{
	var url = contexto + "/faces/menu/actualizarMenu/?horaPeticion=" + new Date().getTime() + "&" + tipoElemento + "=" + idElemento;
	if (idElementoN3 && tipoElementoN3)
	{
		url = url + "&" + tipoElementoN3 + "=" + idElementoN3;
	}

	var xmlHttp = crearXMLHttpRequest();

	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function crearXMLHttpRequest()
{
	var xmlHttp = null;

	if (window.XMLHttpRequest)
	{
		xmlHttp = new XMLHttpRequest();
	}
	else
	{
		if (window.ActiveXObject)
		{
			try
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
	}

	return xmlHttp;
}



