<?xml version='1.0' encoding='UTF-8' ?>
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:t="http://myfaces.apache.org/tomahawk">

	
	
	<div id="popupScreenFecha" > 
		<div>
			<t:outputText  id="tituloPopupScreenFecha" forceId="true" styleClass="tituloPopup"  value="Formato de fecha incorrecto">
			</t:outputText>
		</div>
		<input type="button" class="cancel" onclick="closePopup();"/>				
		
		<div>
			<t:outputText value="#{trazas.ERR_VALID_FECHA}" styleClass="textoPopup"/>
		</div>				
	</div> 
	<div id="coverFecha"> 
	</div> 

</ui:composition>
