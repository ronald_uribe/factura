<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE html>
<ui:composition  xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
<h:head>
	<script type="text/javascript" src="/soloandamios/js/main.js"></script>
	<link rel="stylesheet" type="text/css" href="/soloandamios/css/estilos.css" />
</h:head>
<div id="labelMenu">
  <a id="abrir" class="abrir-cerrar" href="javascript:void(0)" onclick="mostrar()">
   <label for="abrir-cerrar">&#9776; <span class="abrir">#{msg.abrir}</span></label>
  </a>
  <a id="cerrar" class="abrir-cerrar" href="#" onclick="ocultar()">
  	<label for="abrir-cerrar">&#9776; <span class="cerrar">#{msg.cerrar}</span></label>
  </a>
   <label for="hora"><span class="hora"><script type="text/javascript">dameFechaActual();</script></span></label>
  <span class="flagCabecera">
  	<h:commandLink styleClass="flagCabecera us" action="#{language.changeLanguage('us')}" value="#{msg.idioma_ingles}" rendered="#{language.locale != 'us'}"/>
	<h:commandLink styleClass="flagCabecera spain" action="#{language.changeLanguage('es')}" value="#{msg.idioma_espanol}" rendered="#{language.locale != 'es'}"/>
  </span>
 
</div> 

<div id="sidebar" class="sidebar">
    <a href="#" class="boton-cerrar" onclick="ocultar()"> x</a>
    <div class="alinear">
	<ul class="menu">
	    <li><h:outputLink value="/soloandamios/jsp/aplicacion/inicio.xhtml">#{msg.inicio}</h:outputLink> </li>
        <li class="submenu">
			<a href="#">#{msg.remision}</a>
			<ul class="children">
				 <li><h:commandLink action="#{mbeanRemision.iniciarRemision}">#{msg.crear_remision}</h:commandLink>  </li>
      		     <li><h:commandLink action="#{mbeanRemisionConsulta.iniciarConsultarRemision}">#{msg.consultar_remision}</h:commandLink> </li>
			</ul>
		</li>
		<li class="submenu">
			<a href="#">#{msg.devolucion}</a>
			<ul class="children">
				 <li><h:commandLink action="#{mbeanDevolucion.iniciarDevolucion}">#{msg.crear_devolucion}</h:commandLink> </li>
       			 <li><h:commandLink action="#{mbeanDevolucionConsulta.iniciarConsultarDevolucion}">#{msg.consultar_devolucion}</h:commandLink> </li> 
			</ul>
		</li>
      	<li class="submenu">
			<a href="#">#{msg.factura}</a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanFactura.iniciarFactura}">#{msg.crear_factura}</h:commandLink> </li> 
      			<li><h:commandLink action="#{mbeanFacturaConsulta.iniciarConsultarFactura}">#{msg.consultar_factura}</h:commandLink></li>
			</ul>
		</li>
		<li class="submenu">
			<a href="#">#{msg.reposicion}</a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanReposicion.iniciarReposicion}">#{msg.reposicion}</h:commandLink>  </li>
        		<li><h:commandLink action="#{mbeanReposicionConsulta.iniciarConsultarReposicion}">#{msg.consultar_reposicion} </h:commandLink></li>
			</ul>
		</li>
      	
      	<li class="submenu">
			<a href="#">#{msg.entrada}</a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanEntrada.iniciarEntrada}">#{msg.crear_entrada}</h:commandLink>  </li>
				<li><h:commandLink action="#{mbeanEntradaConsulta.iniciarConsultarEntrada}">#{msg.consultar_entradas}</h:commandLink>  </li>
			</ul>
		</li>
		<li class="submenu">
			<a href="#">#{msg.salida}</a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanSalida.iniciarSalida}">#{msg.crear_salida}</h:commandLink>  </li>
				<li><h:commandLink action="#{mbeanSalidaConsulta.iniciarConsultarSalida}">#{msg.consultar_salidas}</h:commandLink>  </li>
			</ul>
		</li>
		<li class="submenu">
			<a href="#">#{msg.cliente}</a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanCliente.iniciarCliente}">#{msg.crear_cliente}</h:commandLink> </li> 
				<li><h:commandLink action="#{mbeanObra.iniciarVincularObra}">#{msg.vincular_obra}</h:commandLink> </li> 
				<li><h:commandLink action="#{mbeanDevolucionConsulta.iniciarConsultaSaldosPendientes}">#{msg.consultar_saldos}</h:commandLink> </li> 
			</ul>
		</li>
		
		
		<li><h:commandLink action="#{mbeanReferencia.iniciarReferencia}">#{msg.producto}</h:commandLink> </li>
		<li><h:commandLink action="#{mbeanProducto.iniciarProducto}">#{msg.vincular_producto}</h:commandLink></li> 
		<li><h:commandLink action="#{mbeanProveedor.iniciarProveedor}">#{msg.proveedores}</h:commandLink> </li>
		<li><h:commandLink action="#{mbeanTransportador.iniciarTransportador}">#{msg.transportadores}</h:commandLink></li>
		<li class="submenu">
			<a href="#">#{msg.configuracion}  </a>
			<ul class="children">
				<li><h:commandLink action="#{mbeanUsuario.iniciarConsultaUsuarios}" rendered="#{loginBean.tienePermiso('super Administrador')}">#{msg.usuario}</h:commandLink></li>
				<li><h:outputLink value="/soloandamios/jsp/aplicacion/cambiarPassword.xhtml"> #{msg.cambiar_contrasena} </h:outputLink> </li>
				<li><h:outputLink value="/soloandamios/jsp/aplicacion/cambiarIdioma.xhtml"> #{msg.cambiar_idioma} </h:outputLink> </li>
			</ul>
		</li>
		<li> <h:commandLink action="#{mbeanUsuario.logout}" value="#{msg.cerrar_sesion}"/> 
		 </li>
	</ul>
	</div>
</div>
</ui:composition>
