<?xml version='1.0' encoding='UTF-8' ?>
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:t="http://myfaces.apache.org/tomahawk">

	
	<t:div id="popupScreenAceptacion" forceId="true" rendered="#{mBean.consentimientoInformativo}"> 
		<div>
			<h:outputText value="#{titulo}" styleClass="tituloPopup">
			</h:outputText>
		</div>
		<div style="margin: 20px">
			<h:outputText value="#{msg.label_confirmacion_solicitud}" styleClass="textoPopupAceptacion"/>
		</div>
		<div>
			<table class="ancho100">
				<tr>
					<td align="center">
						<t:commandButton  styleClass = "botonSINimgen margen5 vAligC" value="#{msg.btn_cancelar}" action="#{mBean.rechazarAviso}" />
						<t:commandButton styleClass = "botonSINimgen margen5 vAligC" value="#{msg.btn_aceptar}" type="submit" action="#{mBean.aceptarAviso}"/>				
					</td>	
				</tr>	
			</table>	
		</div>		
		
	</t:div> 
	<t:div id="coverAceptacion" forceId="true" rendered="#{mBean.consentimientoInformativo}"> 
	</t:div> 

</ui:composition>