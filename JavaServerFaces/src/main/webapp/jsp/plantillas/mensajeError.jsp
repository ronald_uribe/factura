<?xml version='1.0' encoding='UTF-8' ?>
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:t="http://myfaces.apache.org/tomahawk">

	<t:saveState value="#{mBean.mensajeError}"	id="idMensajeError" />
	<t:saveState value="#{mBean.muestraAviso}"	id="idMuestraAviso" />
	
	<t:div id="popupScreen" forceId="true" rendered="#{mBean.muestraAviso}"> 
		<div>
			<h:outputText value="#{titulo}" styleClass="tituloPopup">
			</h:outputText>
		</div>
		<h:commandButton styleClass="cancel" action="#{mBean.cerrarAviso}"/>				
		
		<div>
			<h:outputText value="#{mBean.mensajeError}" styleClass="textoPopup"/>
		</div>				
	</t:div> 
	<t:div id="cover" forceId="true" rendered="#{mBean.muestraAviso}"> 
	</t:div> 

</ui:composition>