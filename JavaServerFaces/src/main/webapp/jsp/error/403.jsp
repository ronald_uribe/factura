<?xml version='1.0' encoding='UTF-8' ?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
				xmlns:ui="http://java.sun.com/jsf/facelets"
				xmlns:f="http://java.sun.com/jsf/core"
				xmlns:h="http://java.sun.com/jsf/html"
				xmlns:t="http://myfaces.apache.org/tomahawk">

	<h:head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="/JavaServerFaces/css/estilosComObl.css" />
		<link rel="stylesheet" type="text/css" href="/JavaServerFaces/css/estilosComOpc.css" />
		<link rel="stylesheet" type="text/css" href="/JavaServerFaces/css/estiloPropio.css" />
		<script type="text/javascript" src="/JavaServerFaces/js/comunes.js"></script>
        
    </h:head>
	
	<body class="noScroll" onload="javascript:mostrarMenuEnError();">
		<f:loadBundle basename="ficherosConfiguracion.mensajes" var="msg"/>
		<div id="cabeceraError" style="display:none">
			<div id="cabLogoMod">
				<img src="/JavaServerFaces/images/comun/cab1.jpg" style="height:60px;float:left;repeat" /> 
			</div>
		</div>
		
		<div id="separacion5emError" class="separacion5emError" style="display:none"></div>
		
		<div id="herramientasError" style="display:none">
		</div>
		
		<div style="height:8em"></div>
		<div class="rastroMigas">
			<div class="letraRastro">
				<b>#{msg.error_403} </b>  #{requestScope['javax.servlet.error.message']}
			</div>
		</div>
	</body>	
	
</html>